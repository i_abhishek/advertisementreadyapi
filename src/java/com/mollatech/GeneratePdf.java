package com.mollatech;

import java.io.FileOutputStream;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class GeneratePdf {

    private static String FILE = null;

    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);

    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);

    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

    public static void createPdf(String status, String txid, String invoiceid, String amount, String desc, String opt) {
        try {
            FILE = "web/pdf/" + invoiceid + ".pdf";
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            document.add(addTitle("Invoice"));
            document.add(addTitle(status));
            document.add(createTable(txid, invoiceid, amount, desc, opt));
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Paragraph addTitle(String title) {
        Font fontbold = FontFactory.getFont("Times-Roman", 26, Font.BOLD);
        Paragraph p = new Paragraph(title, fontbold);
        p.setSpacingAfter(20);
        return p;
    }

    public static PdfPTable createTable(String txid, String invoiceid, String amount, String desc, String opt) {
        PdfPTable table = new PdfPTable(2);
        table.getDefaultCell().setBorder(0);
        table.addCell("Transaction Id");
        table.addCell(txid);
        table.addCell("Invoice Id");
        table.addCell(invoiceid);
        table.addCell("Pay Amount");
        table.addCell(amount);
        table.addCell("Desription");
        table.addCell(desc);
        table.addCell("Payment Option");
        table.addCell(opt);
        return table;
    }
}
