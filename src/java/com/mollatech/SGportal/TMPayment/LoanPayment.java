/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.TMPayment;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgLoanDetails;
import com.mollatech.serviceguard.nucleus.db.SgPaymentdetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.LoanManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "LoanPayment", urlPatterns = {"/LoanPayment"})
public class LoanPayment extends HttpServlet {

    static final Logger logger = Logger.getLogger(LoanPayment.class);
    
    public static final int PENDING = 2;
    public static final int SEND = 0;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #LoanPayment from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String channelName = "ServiceGuard Portal";
        String result = "success";
        String message = "Loan has been paid successfully";
        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
                        
        String packageName = (String)request.getSession().getAttribute("_originalPackageName");
        String invoiceId = (String)request.getSession().getAttribute("_invoiceId");
        String paidAmount = (String)request.getSession().getAttribute("_grossAmount");
        String loanId   = (String)request.getSession().getAttribute("_loanId");
        
        float pAmount   = 0;
        Integer iLoanId = 0;
        if(loanId != null){
            iLoanId = Integer.parseInt(loanId);
        }
        if (paidAmount != null) {
            pAmount = Float.parseFloat(paidAmount);
        }
        SgLoanDetails loanDetails = new LoanManagement().getLoanDetailsByLoanId(channelId, iLoanId);
        try {
            if(loanDetails != null){                
                float loan = loanDetails.getLoanAmount();
                float interestRate = loanDetails.getInterestRate();
                String description     = loan + ":" + interestRate; 
                loanDetails.setStatus(GlobalStatus.PAID);
                loanDetails.setUpdationDate(new Date());
                new LoanManagement().editLoanDetails(SessionId, loanDetails);
                
                SgPaymentdetails paymentObj = new SgPaymentdetails();
                paymentObj.setInvoiceNo(invoiceId);
                paymentObj.setPaidOn(new Date());
                paymentObj.setPaidamount(pAmount);
                paymentObj.setSubscriptionId(loanDetails.getSubscriptionId());
                paymentObj.setPartnerId(partnerObj.getPartnerId());
                paymentObj.setDescription(description);
                new PaymentManagement().createPaymentDetails(SessionId, channelId, paymentObj);
                    
                int productType = 3;
                Operators[] operatorObj = new OperatorsManagement().getAllOperators(channelId);
                String[] operatorEmail = null;
                if (operatorObj != null) {
                    operatorEmail = new String[operatorObj.length];
                    for (int i = 0; i < operatorObj.length; i++) {
                        operatorEmail[i] = (String) operatorObj[i].getEmailid();
                    }
                }                    
                String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.loan.payment");                    
                if (tmessage != null) {
                    tmessage = tmessage.replaceAll("#partnerName#", partnerObj.getPartnerName());
                    tmessage = tmessage.replaceAll("#channel#", channelName);
                    tmessage = tmessage.replaceAll("#datetime#", UtilityFunctions.getTMReqDate(new Date()));
                    tmessage = tmessage.replaceAll("#package#", packageName);
                    tmessage = tmessage.replaceAll("#invoiceid#", invoiceId);
                    tmessage = tmessage.replaceAll("#paidAmount#", String.valueOf(pAmount));
                    tmessage = tmessage.replaceAll("#paymentDate#", UtilityFunctions.getTMReqDate(new Date()));
                    tmessage = tmessage.replaceAll("#email#", partnerObj.getPartnerEmailid());
                }
                SGStatus status = new SendNotification().SendEmail(channelId, partnerObj.getPartnerEmailid(), "Loan Payment Successfully Done", tmessage, operatorEmail, null, null, null, productType);
                if (status.iStatus != PENDING && status.iStatus != SEND) {
                        result = "error";
                        message = "Did the loan payment successfully but failed to sent email notification.";
                        try {
                            json.put("_result", result);
                            logger.debug("Response of LoanPayment Servlet's Parameter  result is " + result);
                            json.put("_message", message);
                            logger.debug("Response of LoanPayment Servlet's Parameter  message is " + message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        out.print(json);
                        out.flush();
                        return;
                    }                
                json.put("result", result);
                json.put("message", message);
            }else{
                result = "error";
                message = "loan details did not find.";
                try {
                        json.put("_result", result);
                        logger.debug("Response of LoanPayment Servlet's Parameter  result is " + result);
                        json.put("_message", message);
                        logger.debug("Response of LoanPayment Servlet's Parameter  message is " + message);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    out.print(json);
                    out.flush();
                    return;
            }
            out.print(json);
            out.flush();
            out.close();
            return;            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #LoanPayment from #PPortal " + json.toString());
            logger.info("Response of #LoanPayment from #PPortal Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
