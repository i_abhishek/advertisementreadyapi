package com.mollatech.SGportal.billing;

import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgPaymentdetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.SgTaxDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.TaxDetailsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author urmila
 */
@WebServlet(name = "AddTaxDetails", urlPatterns = {"/AddTaxDetails"})
public class AddTaxDetails extends HttpServlet {

    static final Logger logger = Logger.getLogger(AddTaxDetails.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #AddTaxDetails from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String channelName = "ServiceGuard Portal";
        String result = "success";
        String message = "Tax details are added successfully";

        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        
        String gstTax = request.getParameter("gstTax");
        String vatTax = request.getParameter("vatTax");
        String stTax = request.getParameter("stTax");
        String amountRate = request.getParameter("_Amount");
        
        try {

            SgSubscriptionDetails packageSubscribed = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(partnerObj.getPartnerId());
            if(packageSubscribed != null){
                SgPaymentdetails paymentObj = new PaymentManagement().getPaymentDetailsbyPartnerAndSubscriptionID(packageSubscribed.getBucketId(), partnerObj.getPartnerId());
                if(paymentObj != null){
                    SgTaxDetails details = new SgTaxDetails();

                    float gst = Float.parseFloat(gstTax);
                    float vat = Float.parseFloat(vatTax);
                    float st = Float.parseFloat(stTax);
                    float amount = Float.parseFloat(amountRate);

                    details.setPartnerid(paymentObj.getPartnerId());
                    details.setPaymentDate(paymentObj.getPaidOn());
                    details.setPaymentid(paymentObj.getPaymentId());
                    details.setSubscriptionid(packageSubscribed.getBucketId());
                    details.setGstTax(gst);
                    details.setVatTax(vat);
                    details.setStTax(st);
                    details.setTotalAmount(amount);
                    int retValue = new TaxDetailsManagement().addTax(details);
                    if(retValue != 1){
                        result = "error";
                        message = "Tax details are not saved";
                    }
                    json.put("result", result);
                    json.put("message", message);
                    
                    json.put("result", result);
                    logger.debug("Response of #AddTaxDetails from #PPortal Servlet's Parameter  result is " + result);
                    json.put("message", message);
                    logger.debug("Response of #AddTaxDetails from #PPortal Servlet's Parameter  message is " + message);
                    out.print(json);
                    out.flush();
                    out.close();
                    return;
                }else{
                    result  = "error";
                    message = "Payment details are not found";
                    json.put("result", result);
                    json.put("message", message);
                    out.print(json);
                    out.flush();
                    out.close();
                    return;
                }
            }else{
                result  = "error";
                message = "Subscription details are not found";
                json.put("result", result);
                json.put("message", message);
                out.print(json);
                out.flush();
                out.close();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #AddTaxDetails from #PPortal " + json.toString());
            logger.info("Response of #AddTaxDetails from #PPortal Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
