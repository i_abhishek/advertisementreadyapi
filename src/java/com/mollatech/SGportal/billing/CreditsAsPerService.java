/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.SGportal.reports.ApiDashboard;
import com.mollatech.serviceguard.nucleus.db.ResourceDetails;
import com.mollatech.serviceguard.nucleus.db.SgHourlytransactiondetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.HourlyTxManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
@WebServlet(name = "CreditsAsPerService", urlPatterns = {"/CreditsAsPerService"})
public class CreditsAsPerService extends HttpServlet {

    int ActiveStatus = 0;
    ArrayList<ApiDashboard> sample = new ArrayList<ApiDashboard>();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Map<String, Integer> map = new HashMap<String, Integer>();
        ArrayList<ApiDashboard> sample = new ArrayList<ApiDashboard>();
        try {
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
            Integer partnerId = (Integer) request.getSession().getAttribute("_partnerID");
            ResourceManagement resourseObj = new ResourceManagement();
            ResourceDetails[] resoursedetails = resourseObj.getAllResources();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date stratDate = new Date();
            stratDate.setHours(00);
            stratDate.setMinutes(00);
            stratDate.setSeconds(00);
            Date endDate = new Date();
            endDate.setHours(23);
            endDate.setMinutes(59);
            endDate.setSeconds(59);
            HourlyTxManagement hrTxObj = new HourlyTxManagement();
            SgHourlytransactiondetails[] sgHrTxDetails = hrTxObj.getTxDetailsBypartner(partnerId, stratDate, endDate);
            JSONObject data = null;
            String[] commaSepData = null;
            String[] colonSep = null;
            List<String> list = new ArrayList<String>();
            if (sgHrTxDetails != null) {
                for (int i = 0; i < sgHrTxDetails.length; i++) {
                    data = new JSONObject(sgHrTxDetails[i].getRawData());
                    list.add(data.toString());

                }
                Map<String, Double> finalData = new HashMap<>();
                if (!list.isEmpty() && list != null) {
                    for (int j = 0; j < list.size(); j++) {
                        commaSepData = list.get(j).split(",");
                        for (String d : commaSepData) {
                            colonSep = d.split(":");
                            String resourceName;
                            int resourceId = Integer.parseInt(colonSep[1]);
                            for (int l = 0; l < resoursedetails.length; l++) {
                                if (resourceId == resoursedetails[l].getResourceId()) {
                                    resourceName = resoursedetails[l].getName();
                                    colonSep[6] = colonSep[6].replaceAll("}", "");
                                    double credits = Double.valueOf(colonSep[5].substring(1)) * Double.valueOf(colonSep[6].substring(0, colonSep[6].length() - 1));
                                    String temp = resourceName + "," + credits;
                                    boolean check = false;
                                    if (finalData.size() > 0) {
                                        for (Map.Entry<String, Double> entrySet : finalData.entrySet()) {
                                            String key = entrySet.getKey();
                                            Double value = entrySet.getValue();
                                            if (key == resourceName) {
                                                credits = value + credits;
                                                check = true;
                                            }

                                        }
                                        finalData.put(resourceName, credits);
                                    } else {
                                        finalData.put(resourceName, credits);
                                    }

                                }
                            }
                        }
                    }
                }
                list.clear();
                for (int k = 0; k < resoursedetails.length; k++) {
                    list.add(resoursedetails[k].getName());
                }
                if (finalData != null) {
                    for (Map.Entry<String, Double> entrySet : finalData.entrySet()) {
                        String key = entrySet.getKey();
                        Double value = entrySet.getValue();
                        sample.add(new ApiDashboard(key, value));
                        for (int k = 0; k < list.size(); k++) {
                            if (list.get(k) == key) {
                                list.remove(k);
                            }
                        }
                        System.out.println("key :: " + key + "  value :: " + value);
                    }
                    for (String li : list) {
                        sample.add(new ApiDashboard(li, 0.0));
                    }
                } else {
                    for (ResourceDetails res : resoursedetails) {
                        sample.add(new ApiDashboard(res.getName(), 0.0));
                    }
                }
            } else {
                for (ResourceDetails res : resoursedetails) {
                    sample.add(new ApiDashboard(res.getName(), 0.0));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(sample, new TypeToken<List<ApiDashboard>>() {
        }.getType());
        JsonArray jsonArray = element.getAsJsonArray();
        out.print(jsonArray);
        out.flush();
        out.close();
        return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
