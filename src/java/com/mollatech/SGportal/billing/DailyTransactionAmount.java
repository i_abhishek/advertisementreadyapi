/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.SGportal.reports.ApiDashboard;
import com.mollatech.serviceguard.nucleus.db.SgDailytransactiondetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.DailyTxManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
//@WebServlet(name = "DailyTransactionAmount", urlPatterns = {"/DailyTransactionAmount"})
public class DailyTransactionAmount extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        try{
        JSONObject json = new JSONObject();
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        Integer partnerId = (Integer) request.getSession().getAttribute("_partnerID");
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");

        JSONObject data = null;

//        String[] count = null;
        List<String> list = new ArrayList<String>();
        DailyTxManagement daily = new DailyTxManagement();
        double totalAmount = 0.0;
        DecimalFormat fractionFormat = new DecimalFormat("0.00");
        ArrayList<ApiDashboard> sample = new ArrayList<ApiDashboard>();
        for (int j = 14; j >= 0; j--) {
            totalAmount = 0.0;
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -j);
            Date _startDate = cal.getTime();
            _startDate.setHours(00);
            _startDate.setMinutes(00);
            _startDate.setSeconds(00);
            Date _endDate = cal.getTime();
            _endDate.setHours(23);
            _endDate.setMinutes(59);
            _endDate.setSeconds(59);
            String[] count = null;
            SgDailytransactiondetails dailytx = daily.getTxByDate(partnerId, _startDate, _endDate);
            if (dailytx != null) {
                data = new JSONObject(dailytx.getRawData());
                Iterator itr = data.keys();
                while (itr.hasNext()) {
                    String key = (String) itr.next();
                    String value = data.getString(key);
                    list.add(value);
                }
                for (int i = 0; i < list.size(); i++) {
                    count = list.get(i).split(":");
                    totalAmount = totalAmount + Double.valueOf(count[0]) * Double.valueOf(count[1]);
                }
                list.clear();
                String strTotalAmt = fractionFormat.format(totalAmount);
                totalAmount = Double.parseDouble(strTotalAmt);
                sample.add(new ApiDashboard(dailytx.getExecuutionDate(), totalAmount));
            }else{
                String sd = format.format(_startDate);
                sample.add(new ApiDashboard(_startDate, totalAmount));
            }
        }
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(sample, new TypeToken<List<ApiDashboard>>() {
        }.getType());
        JsonArray jsonArray = element.getAsJsonArray();
        out.print(jsonArray);
        out.flush();
        out.close();
        return;
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(DailyTransactionAmount.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(DailyTransactionAmount.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
