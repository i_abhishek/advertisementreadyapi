/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.mollatech.serviceguard.nucleus.db.SgCreditInfo;
import com.mollatech.serviceguard.nucleus.db.SgHourlytransactiondetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.HourlyTxManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

@WebServlet(name = "DailyUsedCredits", urlPatterns = {"/DailyUsedCredits"})
public class DailyUsedCredits extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        double mainCredit = 0;
        double percentage = 0;
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String result = "success";
        double totalAmount = 0.0;
        BigDecimal bigDPercentage = null;
        BigDecimal bigDTotalAmount = null;
        try {
            String operation = request.getParameter("operation");
            Integer partnerId = (Integer) request.getSession().getAttribute("_partnerID");
            DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            Date _startDate = null;
            Date _endDate = new Date();
            if (operation == "today" || operation.equals("today")) {
                _startDate = new Date();
                _startDate.setHours(00);
                _startDate.setMinutes(00);
                _startDate.setSeconds(00);
                _endDate.setHours(23);
                _endDate.setMinutes(59);
                _endDate.setSeconds(59);
            } else if (operation == "lastweek" || operation.equals("lastweek")) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -7);
                _startDate = cal.getTime();
                _startDate.setHours(00);
                _startDate.setMinutes(00);
                _startDate.setSeconds(00);
                _endDate.setHours(23);
                _endDate.setMinutes(59);
                _endDate.setSeconds(59);
            } else if (operation == "month" || operation.equals("month")) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -30);
                _startDate = cal.getTime();
                _startDate.setHours(00);
                _startDate.setMinutes(00);
                _startDate.setSeconds(00);
                _endDate.setHours(23);
                _endDate.setMinutes(59);
                _endDate.setSeconds(59);
            }
            SgCreditInfo creditObj1 = null;
            creditObj1 = new CreditManagement().getDetails(partnerId);

            mainCredit = creditObj1.getMainCredit();
            new BigDecimal(mainCredit).doubleValue();
            JSONObject data = null;
            List<String> map = new ArrayList<String>();
            HourlyTxManagement hTmanagement = new HourlyTxManagement();
            SgHourlytransactiondetails[] sghrTransaction = hTmanagement.getTxDetailsBypartner(partnerId, _startDate, _endDate);
            String[] count = null;
            if (sghrTransaction != null) {
                for (int i = 0; i < sghrTransaction.length; i++) {
                    if (sghrTransaction[i].getRawData() != null) {
                        data = new JSONObject(sghrTransaction[i].getRawData());
                        Iterator iter = data.keys();
                        while (iter.hasNext()) {
                            String key = (String) iter.next();
                            String value = data.getString(key);
                            map.add(value);
                        }
                    }
                }

                for (int i = 0; i < map.size(); i++) {
                    count = map.get(i).split(":");
                    totalAmount = totalAmount + Double.valueOf(count[0]) * Double.valueOf(count[1]);
                }
            }
            if (new BigDecimal(mainCredit).doubleValue() != 0.0) {
                percentage = (totalAmount / new BigDecimal(mainCredit + totalAmount).doubleValue()) * 100;
            }
            bigDPercentage = new BigDecimal(percentage);
            bigDPercentage = bigDPercentage.setScale(2, BigDecimal.ROUND_HALF_EVEN);

            bigDTotalAmount = new BigDecimal(totalAmount);
            bigDTotalAmount = bigDTotalAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {

            json.put("result", result);
            json.put("amount", bigDTotalAmount);
            json.put("percentage", String.format("%.2f", bigDPercentage));
            json.put("remainingCredit", mainCredit);
        } catch (Exception e) {
            e.printStackTrace();
        }
        out.print(json.toString());
        out.flush();
        return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
