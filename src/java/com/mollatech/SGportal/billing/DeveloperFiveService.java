/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.SGportal.reports.ApiDashboard;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.ResourceDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestTrackingManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "DeveloperFiveService", urlPatterns = {"/DeveloperFiveService"})
public class DeveloperFiveService extends HttpServlet {

    int ActiveStatus = 0;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Map<String, Long> map = new HashMap<String, Long>();
        ArrayList<ApiDashboard> sample = new ArrayList<ApiDashboard>();
        try {
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
            Integer partnerId = (Integer) request.getSession().getAttribute("_partnerID");
            ResourceManagement resourseObj = new ResourceManagement();
            ResourceDetails[] resoursedetails = resourseObj.getAllResources();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date endDate = new Date();
            endDate.setHours(23);
            endDate.setMinutes(59);
            endDate.setSeconds(59);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, (-30));
            Date froDate1 = cal.getTime();
            froDate1.setHours(00);
            froDate1.setMinutes(00);
            froDate1.setSeconds(00);
            Map<Integer, String> resourceMap = new HashMap();
            if (resoursedetails != null) {
                for (ResourceDetails resData : resoursedetails) {
                    resourceMap.put(resData.getResourceId(), resData.getName());
                }
            }
            RequestTrackingManagement requesTr = new RequestTrackingManagement();
            List<Object[]> objects = requesTr.getCountUsingHQL(partnerId, froDate1, endDate, true);
            if (objects != null) {
                int count = 0;
                for (Object[] os : objects) {
                    if (count < 5) {
                        String resName = resourceMap.get(os[1]);
                        if (!map.containsKey(resName)) {
                            map.put(resName, (Long) os[0]);
                        }
                    } else {
                        break;
                    }
                }
            }
            if (map.size() < 5) {
                for (int i = map.size(); i < 6; i++) {
                    for (int key : resourceMap.keySet()) {
                        String name = resourceMap.get(key);
                        if (!map.containsKey(name)) {
                            map.put(name, 0l);
                            break;
                        }
                    }
                }
            }
            Object[] a = map.entrySet().toArray();
            Arrays.sort(a, new Comparator() {
                public int compare(Object o1, Object o2) {
                    return ((Map.Entry<String, Long>) o2).getValue()
                            .compareTo(((Map.Entry<String, Long>) o1).getValue());
                }
            });
            for (int i = 0; i < 5; i++) {
                String apiname = ((Map.Entry<String, Long>) a[i]).getKey();
                Long apicount = ((Map.Entry<String, Long>) a[i]).getValue();
                sample.add(new ApiDashboard(apiname, apicount));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(sample, new TypeToken<List<ApiDashboard>>() {
        }.getType());
        JsonArray jsonArray = element.getAsJsonArray();
        out.print(jsonArray);
        out.flush();
        out.close();
        return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
