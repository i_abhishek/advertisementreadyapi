/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.SGportal.reports.ApiDashboard;
import com.mollatech.SortMapByValue;
import com.mollatech.serviceguard.nucleus.db.ResourceDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestTrackingManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author bluebricks
 */
@WebServlet(name = "DeveloperLeastService", urlPatterns = {"/DeveloperLeastService"})
public class DeveloperLeastService extends HttpServlet {

    int ActiveStatus = 0;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Map<String, Long> map = new HashMap<String, Long>();
        ArrayList<ApiDashboard> sample = new ArrayList<ApiDashboard>();
        try {
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
            Integer partnerId = (Integer) request.getSession().getAttribute("_partnerID");
            ResourceManagement resourseObj = new ResourceManagement();
            ResourceDetails[] resoursedetails = resourseObj.getAllResources();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date endDate = new Date();
            endDate.setHours(23);
            endDate.setMinutes(59);
            endDate.setSeconds(59);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, (-30));
            Date froDate1 = cal.getTime();
            froDate1.setHours(00);
            froDate1.setMinutes(00);
            froDate1.setSeconds(00);

            RequestTrackingManagement requesTr = new RequestTrackingManagement();
            if (resoursedetails != null) {

                for (ResourceDetails resData : resoursedetails) {
                    List<Integer> partList=new ArrayList();
                    partList.add(partnerId);
//                    Long count = requesTr.getRequestCountByDate(resData.getResourceId(), partList, froDate1, endDate, ActiveStatus);
//                    map.put(resData.getName(), count);
                }
            }
            int i = 0;
            map = SortMapByValue.sortByComparator(map, SortMapByValue.DESC);
            Map<String, Long> tMap = new HashMap<String, Long>();
            for (String key : map.keySet()) {
                if (i < 6) {
                    tMap.put(key, map.get(key));
                }
                i++;
            }
            i = 0;
            tMap = SortMapByValue.sortByComparatorLong(tMap, SortMapByValue.ASC);
            for (String key : tMap.keySet()) {
                if (i < 5) {
                    sample.add(new ApiDashboard(key, tMap.get(key)));
                }
                i++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(sample, new TypeToken<List<ApiDashboard>>() {
        }.getType());
        JsonArray jsonArray = element.getAsJsonArray();
        System.out.println("JSON Array :: " + jsonArray);
        out.print(jsonArray);
        out.flush();
        out.close();
        return;

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
