/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import blue.bricks.documentsigner.AxiomPdfResponse;
import blue.bricks.documentsigner.AxiomSignerResponse;
import com.mollatech.pdfsigning.AxiomPDFSignConnector;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgPaymentdetails;
import com.mollatech.serviceguard.nucleus.db.SgSapReceiptFile;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PDFInvoiceManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SAPBillManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "GetOnlineBillReceipt", urlPatterns = {"/GetOnlineBillReceipt"})
public class GetOnlineBillReceipt extends HttpServlet {

    static final Logger logger = Logger.getLogger(GetOnlineBillReceipt.class);
    private static final int BUFSIZE = 4096;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #GetInvoiceDownload from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());

        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String invoiceId = request.getParameter("invoiceId");
        String paymentId = request.getParameter("paymentId");
        String subscriptionId = request.getParameter("subscriptionId");
        JSONObject tierOrSlabUsage = null;
//        JSONObject CaasServiceUsage = null;
//        JSONObject pgTransactionUsage = null;
        int ipayId = 0;
        int isubscribeId = 0;
        try {
            if (paymentId != null) {
                ipayId = Integer.parseInt(paymentId);
            }
            if (subscriptionId != null) {
                isubscribeId = Integer.parseInt(subscriptionId);
            }
            Double packageAmount = null;
            Double changePackageCharge = null;
            Double subscribedserviceCharge = null;
            Double subscribedreactivationCharge = null;
            Double subscribedcancellationCharge = null;
            Double subscribedlatePenaltyCharge = null;
            String invoiceFilePath = null;
            SgSubscriptionDetails packageSubscribed = new PackageSubscriptionManagement().getSubscriptionbyId(SessionId, channelId, isubscribeId);
            SgSapReceiptFile receiptFile = new SAPBillManagement().getSapReceiptByPartnerIdSubscribeId(partnerObj.getPartnerId(), packageSubscribed.getBucketId());
            String appurl = (request.getRequestURL().toString());
            URL myAppUrl = new URL(appurl);
            int port = myAppUrl.getPort();
            if (myAppUrl.getProtocol().equals("https") && port == -1) {
                port = 443;
            } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
                port = 80;
            }
            String contextPath = request.getContextPath();
            String pdfSaveURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + contextPath + "/";
            String signImageURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + contextPath + "/";
            String pdfSigImg = LoadSettings.pdfSavePath;
            if (receiptFile != null && receiptFile.getDocuments() != null) {
                invoiceFilePath = pdfSigImg + packageSubscribed.getBucketName() + invoiceId + ".pdf";
                File file = new File(invoiceFilePath);
                FileOutputStream fop = new FileOutputStream(file);
                fop.write(receiptFile.getDocuments());
                fop.flush();
                fop.close();
            } else {
                SgPaymentdetails paymentObj = new PaymentManagement().getPaymentDetailsbyPaymentID(ipayId);

                if (packageSubscribed.getServiceCharge() != null) {
                    subscribedserviceCharge = Double.parseDouble(packageSubscribed.getServiceCharge());
                }
                if (paymentObj != null && paymentObj.getTierOrSlabUsage() != null) {
                    tierOrSlabUsage = new JSONObject(paymentObj.getTierOrSlabUsage());
                }
                packageAmount = new Float(packageSubscribed.getPlanAmount()).doubleValue();

                invoiceFilePath = new PDFInvoiceManagement().createReadyAPIInvoice(tierOrSlabUsage, changePackageCharge, subscribedserviceCharge, subscribedreactivationCharge, subscribedcancellationCharge, subscribedlatePenaltyCharge, packageAmount, partnerObj, null, invoiceId, packageSubscribed, null, null, null);

                String pdfSignchannelId = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.channelId");
                String pdfSignloginId = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.loginId");
                String pdfSignloginpassword = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.loginPassword");
                String signInfoUser = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoUser");
                String signInfoEmail = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoEmail");
                String pdfSignloginaddress = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoaddress");
                String signInfoDesignation = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.designation");
                String signInfoReason = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoReason");
                JSONObject reqJSONObj = null;
                String securityAlert = packageSubscribed.getSecurityAndAlertDetails();
                if (securityAlert != null) {
                    JSONArray alertJson = new JSONArray(securityAlert);
                    for (int i = 0; i < alertJson.length(); i++) {
                        JSONObject jsonexists1 = alertJson.getJSONObject(i);
                        if (jsonexists1.has(packageSubscribed.getBucketName())) {
                            reqJSONObj = jsonexists1.getJSONObject(packageSubscribed.getBucketName());
                            if (reqJSONObj != null) {
                                break;
                            }
                        }
                    }
                }
                if (reqJSONObj != null && reqJSONObj.getString("pdfSigning").equals("enable")) {
                    logger.info("#GetOnlineBillReceipt from #PPortal PDF Siging started ");
                    String sessionID = AxiomPDFSignConnector.openSession(pdfSignchannelId, pdfSignloginId, pdfSignloginpassword);
                    String signImagePath = signImageURL + "Signiture.png";
                    logger.info("#GetOnlineBillReceipt from #PPortal PDF Siging sessionID " + sessionID);
                    logger.info("#GetOnlineBillReceipt from #PPortal PDF Siging pdfSaveURL " + pdfSaveURL);
                    System.out.println("signImagePath >> " + signImagePath);
                    logger.info("#GetOnlineBillReceipt from #PPortal PDF Siging signImagePath " + signImagePath);
                    File f = new File(invoiceFilePath);
                    String crtfileName = f.getName();
                    String pdfURLPath = pdfSaveURL + crtfileName;
                    AxiomSignerResponse serviceResponse = AxiomPDFSignConnector.sendSignatureRequest(sessionID, pdfURLPath, signInfoUser, signInfoEmail, pdfSignloginaddress, signInfoDesignation, signImagePath, null, null, signInfoReason);
                    //AxiomSignerResponse serviceResponse = AxiomPDFSignConnector.sendSignatureRequest(sessionID, "https://www.readyapis.com/invoice1.pdf",signInfoUser,signInfoEmail,pdfSignloginaddress,signInfoDesignation,"https://www.readyapis.com/Signiture.png",null,null,signInfoReason);
                    //String signedPDF = serviceResponse.getBase64SignedPdf();
                    String signedPDFPath = serviceResponse.getSingedUrl();
                    logger.info("#GetOnlineBillReceipt from #PPortal PDF Siging getSingedUrl() " + signedPDFPath);
                    logger.info("#GetOnlineBillReceipt from #PPortal PDF Siging server response " + serviceResponse.getErrormsg() + " with error code " + serviceResponse.getErrorcode());
//                if (signedPDF != null) {
//                    byte[] decodePDF = Base64.decodeBase64(signedPDF);
//                    File file = new File(invoiceFilePath);
//                    FileOutputStream fop = new FileOutputStream(file);
//                    fop.write(decodePDF);
//                    fop.flush();
//                    fop.close();
//                }
                    if (signedPDFPath != null && !signedPDFPath.isEmpty()) {
                        File signFile = new File(invoiceFilePath);
                        signFile.delete();
                        if (reqJSONObj != null && reqJSONObj.getString("encryptedPDF").equals("enable")) {
                            invoiceFilePath = downloadFile(signedPDFPath, pdfSigImg, packageSubscribed.getBucketName() + invoiceId);
                        } else {
                            invoiceFilePath = downloadFile(signedPDFPath, pdfSigImg, packageSubscribed.getBucketName() + invoiceId + ".pdf");
                        }
                        //System.out.println("encryptionPDF path >> " + invoiceFilePath);
                        logger.info("#GetOnlineBillReceipt encryptionPDF physical path >> " + invoiceFilePath);
                    }
                }
                if (reqJSONObj != null && reqJSONObj.getString("encryptedPDF").equals("enable")) {
                    String pdfPassword = "";
                    if (partnerObj.getPartnerEmailid().length() >= 6) {
                        pdfPassword = partnerObj.getPartnerEmailid().substring(0, 5);
                    } else {
                        pdfPassword = partnerObj.getPartnerEmailid();
                    }
                    String sessionID = AxiomPDFSignConnector.openSession(pdfSignchannelId, pdfSignloginId, pdfSignloginpassword);
                    logger.info("#GetOnlineBillReceipt from #PPortal PDF Encryption started ");
                    logger.info("#GetOnlineBillReceipt from #PPortal PDF encryptionPDF pdfSaveURL " + pdfSaveURL);
                    logger.info("#GetOnlineBillReceipt from #PPortal PDF encryptionPDF path sessionID pdfEncryption" + sessionID);
                    File f = new File(invoiceFilePath);
                    String fileName = f.getName();
                    String pdfURLPath = pdfSaveURL + fileName;
                    AxiomPdfResponse servicePDFResponse = AxiomPDFSignConnector.encryptOrDecryptPdf(sessionID, 1, null, pdfURLPath, pdfPassword, "12345", true, false, true, true, true, true, true, true, true, true, true);
                    //AxiomPdfResponse servicePDFResponse = AxiomPDFSignConnector.encryptOrDecryptPdf(sessionID, 1, null, "https://www.readyapis.com/invoice1.pdf", pdfPassword, "12345", true, false, true, true, true, true, true, true, true, true, true);
                    logger.info("#GetOnlineBillReceipt from #PPortal PDF encryption server response " + servicePDFResponse.getErrormsg() + " with error code " + servicePDFResponse.getErrorcode());
                    String pdfResponseURL = servicePDFResponse.getPDFDownloadUrl();
                    if (pdfResponseURL != null && !pdfResponseURL.isEmpty()) {
                        logger.info("#GetOnlineBillReceipt from #PPortal PDF encryptionPDF pdfResponseURL " + pdfResponseURL);
                        File encryFile = new File(invoiceFilePath);
                        encryFile.delete();
                        invoiceFilePath = downloadFile(pdfResponseURL, pdfSigImg, fileName + ".pdf");
                        System.out.println("encryptionPDF path >> " + invoiceFilePath);
                        logger.info("#GetOnlineBillReceipt from #PPortal PDF encryptionPDF path " + invoiceFilePath);
                    }
                }
                Path saveInvoicepath = Paths.get(invoiceFilePath, new String[0]);
                byte[] invoiceByteArray = Files.readAllBytes(saveInvoicepath);
                receiptFile = new SgSapReceiptFile();
                receiptFile.setDocuments(invoiceByteArray);
                receiptFile.setPartnerId(partnerObj.getPartnerId());
                receiptFile.setSubscriptionId(packageSubscribed.getBucketId());
                receiptFile.setCreatedOn(new Date());
                new SAPBillManagement().addSAPBillDetails(receiptFile);
            }
            File file = new File(invoiceFilePath);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(invoiceFilePath);
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) file.length());
            String fileName = (new File(invoiceFilePath)).getName();
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(file));
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }
            in.close();
            outStream.close();
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String downloadFile(String fileURL, String saveDir, String fileName) throws IOException {
        String saveFilePath = null;
        try {
            URL url = new URL(fileURL);
            int BUFFER_SIZE = 4096;
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            int responseCode = httpConn.getResponseCode();
            // always check HTTP response code first
            if (responseCode == HttpURLConnection.HTTP_OK) {
                //String fileName = "";
                String disposition = httpConn.getHeaderField("Content-Disposition");
                String contentType = httpConn.getContentType();
                int contentLength = httpConn.getContentLength();

//            if (disposition != null) {
//                // extracts file name from header field
//                int index = disposition.indexOf("filename=");
//                if (index > 0) {
//                    fileName = disposition.substring(index + 10,
//                            disposition.length() - 1);
//                }
//            } else {
//                // extracts file name from URL
//                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
//                        fileURL.length());
//            }
                System.out.println("Content-Type = " + contentType);
                System.out.println("Content-Disposition = " + disposition);
                System.out.println("Content-Length = " + contentLength);
                System.out.println("fileName = " + fileName);

                // opens input stream from the HTTP connection
                InputStream inputStream = httpConn.getInputStream();
                saveFilePath = saveDir + fileName;

                // opens an output stream to save into file
                FileOutputStream outputStream = new FileOutputStream(saveFilePath);

                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
                outputStream.close();
                inputStream.close();
                System.out.println("File downloaded");

            } else {
                System.out.println("No file to download. Server replied HTTP code: " + responseCode);
                return null;
            }
            httpConn.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return saveFilePath;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
