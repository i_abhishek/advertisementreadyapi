package com.mollatech.SGportal.billing;

import static com.mollatech.SGportal.billing.SubscribePostpaid.PENDING;
import static com.mollatech.SGportal.billing.SubscribePostpaid.SENT;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgCreditInfo;
import com.mollatech.serviceguard.nucleus.db.SgLoanDetails;
import com.mollatech.serviceguard.nucleus.db.SgPaymentdetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.LoanManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "PayLoan", urlPatterns = { "/PayLoan" })
public class PayLoan extends HttpServlet {

    static final Logger logger = Logger.getLogger(PayLoan.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #PayLoan from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Loan paid successfully";
        String channelName = "ServiceGuard Portal";
        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String packageName = request.getParameter("_packageName");
        String invoiceId = request.getParameter("_invoiceId");
        String paidAmount = request.getParameter("_amount");
        String subPac = request.getParameter("_subForPac");
        float pAmount = 0;
        if (paidAmount != null) {
            pAmount = Float.parseFloat(paidAmount);
        }
        try {
            SgSubscriptionDetails packageSubscribed = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(partnerObj.getPartnerId());
            int retValue = -1;
            Date d = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
            SgPaymentdetails paymentObj = new SgPaymentdetails();
            SgCreditInfo info = null;
            SgLoanDetails loanDetails = new LoanManagement().getLoanDetails(SessionId, channelId, packageSubscribed.getPartnerId(), null);
            if (loanDetails != null) {
                loanDetails.setStatus(GlobalStatus.PAID);
                loanDetails.setUpdationDate(new Date());
                retValue = new LoanManagement().editLoanDetails(SessionId, loanDetails);
            }
            if (retValue == 0) {
                paymentObj.setInvoiceNo(invoiceId);
                paymentObj.setPaidOn(new Date());
                paymentObj.setPaidamount(pAmount);
                paymentObj.setInvoiceData("Loan");
                paymentObj.setSubscriptionId(packageSubscribed.getBucketId());
                paymentObj.setPartnerId(partnerObj.getPartnerId());
                retValue = new PaymentManagement().createPaymentDetails(SessionId, channelId, paymentObj);
                SgReqbucketdetails packageObject = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, subPac);
                SgSubscriptionDetails subscriptionObject = new SgSubscriptionDetails();
                if (packageObject != null) {
                    subscriptionObject.setApRateDetails(packageObject.getApRateDetails());
                    subscriptionObject.setBucketDuration(packageObject.getBucketDuration());
                    subscriptionObject.setBucketName(packageObject.getBucketName());
                    subscriptionObject.setCancellationRate(packageObject.getCancellationRate());
                    subscriptionObject.setChangePackageRate(packageObject.getChangePackageRate());
                    subscriptionObject.setChannelId(packageObject.getChannelId());
                    subscriptionObject.setCreationDate(new Date());
                    subscriptionObject.setCreatedOn(new Date());
                    subscriptionObject.setBucketId(packageObject.getBucketId());
                    subscriptionObject.setDaysForFreeTrial(packageObject.getDaysForFreeTrial());
                    subscriptionObject.setFeatureList(packageObject.getFeatureList());
                    subscriptionObject.setFreeCredits(packageObject.getFreeCredits());
                    subscriptionObject.setLatePenaltyRate(packageObject.getLatePenalitesRate());
                    subscriptionObject.setMinimumBalance(packageObject.getMinimumBalance());
                    subscriptionObject.setPartnerId(partnerObj.getPartnerId());
                    subscriptionObject.setPaymentMode(packageObject.getPaymentMode());
                    subscriptionObject.setPlanAmount(packageObject.getPlanAmount());
                    subscriptionObject.setReActivationCharge(packageObject.getReActivationCharge());
                    subscriptionObject.setSecurityAndAlertDetails(packageObject.getSecurityAndAlertDetails());
                    subscriptionObject.setServiceCharge(packageObject.getServiceCharge());
                    subscriptionObject.setSlabApRateDetails(packageObject.getSlabApRateDetails());
                    subscriptionObject.setStatus(GlobalStatus.UNPAID);
                    if (packageObject.getPaymentMode().equalsIgnoreCase("prepaid")) {
                        subscriptionObject.setStatus(GlobalStatus.PAID);
                    }
                    subscriptionObject.setTax(packageObject.getTax());
                    subscriptionObject.setMainCredits(packageObject.getMainCredits());
                    int days = 0;
                    if (packageObject.getBucketDuration().equalsIgnoreCase("daily")) {
                        days = 1;
                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("weekly")) {
                        days = 7;
                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("biMonthly")) {
                        days = 15;
                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("monthly")) {
                        days = 30;
                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("quarterly")) {
                        days = 90;
                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("halfYearly")) {
                        days = 180;
                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("yearly")) {
                        days = 365;
                    }
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date());
                    calendar.add(Calendar.DATE, days);
                    subscriptionObject.setExpiryDateNTime(calendar.getTime());
                    retValue = new PackageSubscriptionManagement().CreateSubscriptionDetails(SessionId, channelId, subscriptionObject);
                }
                if (retValue > 0) {
                    info = new CreditManagement().getDetails(partnerObj.getPartnerId());
                    if (info != null) {
                        info.setStatus(1);
                        info.setUpdatedFreeCredits(subscriptionObject.getFreeCredits());
                        info.setUpdatedMainCredits(subscriptionObject.getMainCredits());
                        info.setMainCredit(0);
                        info.setApiRates(subscriptionObject.getApRateDetails());
                        retValue = new CreditManagement().updateDetails(info);
                    }
                    String alertandsecurity = subscriptionObject.getSecurityAndAlertDetails();
                    String alertChangeOnService = null;
                    int productType = 3;
                    Operators[] operatorObj = new OperatorsManagement().getAllOperators(channelId);
                    String[] operatorEmail = null;
                    if (operatorObj != null) {
                        operatorEmail = new String[operatorObj.length];
                        for (int i = 0; i < operatorObj.length; i++) {
                            operatorEmail[i] = (String) operatorObj[i].getEmailid();
                        }
                    }
                    boolean flag = true;
                    if (packageSubscribed != null && !packageSubscribed.getBucketName().contains(subscriptionObject.getBucketName())) {
                        alertChangeOnService = packageSubscribed.getSecurityAndAlertDetails();
                        if (alertChangeOnService != null) {
                            JSONObject reqJSONObj1 = null;
                            String changeOnPackageMessage = null;
                            JSONArray alertJson1 = new JSONArray(alertChangeOnService);
                            for (int i = 0; i < alertJson1.length(); i++) {
                                JSONObject jsonexists1 = alertJson1.getJSONObject(i);
                                if (jsonexists1.has(packageSubscribed.getBucketName())) {
                                    reqJSONObj1 = jsonexists1.getJSONObject(packageSubscribed.getBucketName());
                                    if (reqJSONObj1 != null) {
                                        changeOnPackageMessage = reqJSONObj1.getString("changeOnPackageAlertMesssage");
                                        break;
                                    }
                                }
                            }
                            if (changeOnPackageMessage != null && !changeOnPackageMessage.equals("")) {
                                String cmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.changeOnPackage");
                                if (cmessage != null) {
                                    cmessage = cmessage.replaceAll("#name#", partnerObj.getPartnerName());
                                    cmessage = cmessage.replaceAll("#channel#", channelName);
                                    cmessage = cmessage.replaceAll("#datetime#", sdf.format(d));
                                    cmessage = cmessage.replaceAll("#oldPackage#", packageSubscribed.getBucketName());
                                    cmessage = cmessage.replaceAll("#newPackage#", subscriptionObject.getBucketName());
                                    cmessage = cmessage.replaceAll("#messageBody#", changeOnPackageMessage);
                                    cmessage = cmessage.replaceAll("#email#", partnerObj.getPartnerEmailid());
                                }
                                SGStatus status = new SendNotification().SendEmail(channelId, partnerObj.getPartnerEmailid(), "Package Change  Successfully", cmessage, operatorEmail, null, null, null, productType);
                                if (status.iStatus == PENDING || status.iStatus == SENT) {
                                    flag = false;
                                }
                            }
                        }
                    } else {
                        JSONObject reqJSONObj3 = null;
                        String reactivationMessage = null;
                        if (alertandsecurity != null) {
                            JSONArray alertJson = new JSONArray(alertandsecurity);
                            for (int i = 0; i < alertJson.length(); i++) {
                                JSONObject jsonexists1 = alertJson.getJSONObject(i);
                                if (jsonexists1.has(packageName)) {
                                    reqJSONObj3 = jsonexists1.getJSONObject(packageName);
                                    if (reqJSONObj3 != null) {
                                        reactivationMessage = reqJSONObj3.getString("reActivationAlertMesssage");
                                        break;
                                    }
                                }
                            }
                            if (reactivationMessage != null && !reactivationMessage.equals("")) {
                                String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.reactivate");
                                reactivationMessage = reactivationMessage.replaceAll("#package#", packageName);
                                if (tmessage != null) {
                                    tmessage = tmessage.replaceAll("#name#", partnerObj.getPartnerName());
                                    tmessage = tmessage.replaceAll("#channel#", channelName);
                                    tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                                    tmessage = tmessage.replaceAll("#messageBody#", reactivationMessage);
                                    tmessage = tmessage.replaceAll("#email#", partnerObj.getPartnerEmailid());
                                }
                                SGStatus status = new SendNotification().SendEmail(channelId, partnerObj.getPartnerEmailid(), "Reactivate package successfully", tmessage, operatorEmail, null, null, null, productType);
                                if (status.iStatus == PENDING || status.iStatus == SENT) {
                                    flag = false;
                                }
                            }
                        }
                    }
                    if (flag) {
                        JSONObject reqJSONObj = null;
                        String subscribeMessage = null;
                        if (alertandsecurity != null) {
                            JSONArray alertJson = new JSONArray(alertandsecurity);
                            for (int i = 0; i < alertJson.length(); i++) {
                                JSONObject jsonexists1 = alertJson.getJSONObject(i);
                                if (jsonexists1.has(packageName)) {
                                    reqJSONObj = jsonexists1.getJSONObject(packageName);
                                    if (reqJSONObj != null) {
                                        subscribeMessage = reqJSONObj.getString("signUpAlertMesssage");
                                        break;
                                    }
                                }
                            }
                            if (subscribeMessage != null && !subscribeMessage.equals("")) {
                                String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.subscribe");
                                subscribeMessage = subscribeMessage.replaceAll("#package#", packageName);
                                if (tmessage != null) {
                                    tmessage = tmessage.replaceAll("#name#", partnerObj.getPartnerName());
                                    tmessage = tmessage.replaceAll("#channel#", channelName);
                                    tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                                    tmessage = tmessage.replaceAll("#messageBody#", subscribeMessage);
                                    tmessage = tmessage.replaceAll("#email#", partnerObj.getPartnerEmailid());
                                }
                                SGStatus status = new SendNotification().SendEmail(channelId, partnerObj.getPartnerEmailid(), "Package Subscribe Successfully", tmessage, operatorEmail, null, null, null, productType);
                            }
                        }
                    }
                }
                json.put("result", result);
                json.put("message", message);
            } else {
                result = "error";
                message = "Loan did not paid.";
                json.put("result", result);
                logger.debug("Response of #PayLoan from #PPortal Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #PayLoan from #PPortal Servlet's Parameter  message is " + message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #PayLoan from #PPortal " + json.toString());
            logger.info("Response of #PayLoan from #PPortal Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
