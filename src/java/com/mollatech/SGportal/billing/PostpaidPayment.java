/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import blue.bricks.documentsigner.AxiomSignerResponse;
import blue.bricks.documentsigner.AxiomSigningRequest;
import blue.bricks.documentsigner.SignersInfo;
import com.mollatech.pdfsigning.AxiomPDFSignConnector;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgCreditInfo;
import com.mollatech.serviceguard.nucleus.db.SgPartnerrequest;
import com.mollatech.serviceguard.nucleus.db.SgPaymentdetails;
import com.mollatech.serviceguard.nucleus.db.SgPromocode;
import com.mollatech.serviceguard.nucleus.db.SgPromocodeUsage;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PDFInvoiceManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeUsageManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

@WebServlet(name = "PostpaidPayment", urlPatterns = {"/PostpaidPayment"})
public class PostpaidPayment extends HttpServlet {
    
    static final Logger logger = Logger.getLogger(PostpaidPayment.class);
    
    public static final int PENDING = 2;
    public static final int SEND = 0;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        logger.info("Request servlet is #PostpaidPayment from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String channelName = "ServiceGuard Portal";
        String result = "success";
        String message = "Package have been subscriped successfully";
        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        SgPartnerrequest reqPar = new PartnerRequestManagement().getPartnerRequestsPartnerbyId(SessionId, partnerObj.getPartnerId());
        //String _promocodeId = request.getParameter("promoCodeId");
        String _promocodeId = (String) request.getSession().getAttribute("_promocodeId");
        //String changeOnPackageCharge = request.getParameter("changePackageCharge");
        String changeOnPackageCharge = (String) request.getSession().getAttribute("_changeOnPackageCharge");
        //String _reactivationCharge = request.getParameter("reactivationCharge");
        String _reactivationCharge = (String) request.getSession().getAttribute("_reactivationCharge");
        //String _cancellationCharge = request.getParameter("cancellationCharge");
        String _cancellationCharge = (String) request.getSession().getAttribute("_cancellationCharge");
        //String _latePenaltyCharge = request.getParameter("latePenaltyCharge");
        String _latePenaltyCharge = (String) request.getSession().getAttribute("_latePenaltyCharge");
        //String packageName = request.getParameter("packageName");
        String packageName = (String) request.getSession().getAttribute("_originalPackageName");
        //String invoiceId = request.getParameter("invoiceId");
        String invoiceId = (String) request.getSession().getAttribute("_invoiceId");
        //String paidAmount = request.getParameter("amount");
        String paidAmount = (String) request.getSession().getAttribute("_grossAmount");
        
        JSONObject tierOrSlabUsage = (JSONObject) request.getSession().getAttribute("tierOrSlabUsageDetails");
        
        JSONObject caasServiceUsage = (JSONObject) request.getSession().getAttribute("caasServiceUsageDetails");
        
        JSONObject pgTransactionUsage = (JSONObject) request.getSession().getAttribute("pgTransactionDetails");
        long invoiceNo = 0;
        String pAmount = "0.00";
        NumberFormat form = new DecimalFormat("#0.00");
        if (invoiceId != null) {
            invoiceNo = Long.parseLong(invoiceId);
        }
        if (paidAmount != null) {
            pAmount = form.format(Double.parseDouble(paidAmount));
        }
        Double packageAmount = null;
        Double changePackageCharge = null;
        Double subscribedserviceCharge = null;
        Double subscribedreactivationCharge = null;
        Double subscribedcancellationCharge = null;
        Double subscribedlatePenaltyCharge = null;
        
        SgSubscriptionDetails packageSubscribed = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(partnerObj.getPartnerId());
        int retValue = -1;
        Date d = new Date();
        SgCreditInfo info = null;
        SgPromocode promoObj = null;        
        try {
            if (packageSubscribed != null) {
                SgPaymentdetails paymentObj = new PaymentManagement().getPaymentDetailsbyPartnerAndSubscriptionID(packageSubscribed.getBucketId(), partnerObj.getPartnerId());
                if (paymentObj == null) {
                    paymentObj = new SgPaymentdetails();
                    if (_promocodeId != null && !_promocodeId.isEmpty() && !_promocodeId.equalsIgnoreCase("null")) {
                        String promocodeDetails = null;
                        int promoId = Integer.parseInt(_promocodeId);
                        promoObj = new PromocodeManagement().getPromocodeById(SessionId, channelId, promoId);
                        SgPromocodeUsage promoUsageObj = new SgPromocodeUsage();
                        promoUsageObj.setChannelId(channelId);
                        promoUsageObj.setPartnerId(partnerObj.getPartnerId());
                        promoUsageObj.setPromoCodeId(promoId);
                        promoUsageObj.setPromocodeUsedOn(new Date());
                        promoUsageObj.setSubscriptionId(packageSubscribed.getBucketId());
                        retValue = new PromocodeUsageManagement().addPromoUsageDetails(promoUsageObj);
                        if (promoObj.getDiscountType().equalsIgnoreCase("flat")) {
                            promocodeDetails = promoObj.getDiscount() + " ,In price";
                        } else {
                            promocodeDetails = promoObj.getDiscount() + " ,In percentage";
                        }
                        paymentObj.setPromocodeId(promoId);
                        paymentObj.setPromoCodeDetails(promocodeDetails);
                    }
                    if (packageSubscribed.getServiceCharge() != null) {
                        subscribedserviceCharge = Double.parseDouble(packageSubscribed.getServiceCharge());
                    }
                    if (changeOnPackageCharge != null && !changeOnPackageCharge.isEmpty()) {
                        paymentObj.setChangeOnPackageCharge(Float.parseFloat(changeOnPackageCharge));
                        changePackageCharge = Double.parseDouble(changeOnPackageCharge);
                    }
                    if (_reactivationCharge != null && !_reactivationCharge.isEmpty()) {
                        paymentObj.setReactivationPackageCharge(Float.parseFloat(_reactivationCharge));
                        subscribedreactivationCharge = Double.parseDouble(_reactivationCharge);
                    }
                    if (_cancellationCharge != null && !_cancellationCharge.isEmpty()) {
                        paymentObj.setCancellationCharge(Float.parseFloat(_cancellationCharge));
                        subscribedcancellationCharge = Double.parseDouble(_cancellationCharge);
                    }
                    if (_latePenaltyCharge != null && !_latePenaltyCharge.isEmpty()) {
                        paymentObj.setLatePenaltyCharge(Float.parseFloat(_latePenaltyCharge));
                        subscribedlatePenaltyCharge = Double.parseDouble(_latePenaltyCharge);
                    }
                    if (tierOrSlabUsage != null) {
                        paymentObj.setTierOrSlabUsage(tierOrSlabUsage.toString());
                    }
                    if(caasServiceUsage != null && caasServiceUsage.length()!=0){
                        paymentObj.setCaasServiceUsage(caasServiceUsage.toString());
                    }
                    if(pgTransactionUsage != null && pgTransactionUsage.length()!=0){
                        paymentObj.setPgtransactionUsage(pgTransactionUsage.toString());
                    }
                    packageAmount = Float.valueOf(packageSubscribed.getPlanAmount()).doubleValue();
                    paymentObj.setInvoiceNo(invoiceId);
                    paymentObj.setPaidOn(new Date());
                    paymentObj.setPaidamount(Float.parseFloat(pAmount));
                    paymentObj.setSubscriptionId(packageSubscribed.getBucketId());
                    paymentObj.setPartnerId(partnerObj.getPartnerId());
                    paymentObj.setPaymentStatus(GlobalStatus.PROCESSED);
                    retValue = new PaymentManagement().createPaymentDetails(SessionId, channelId, paymentObj);
                } else {
                    if (_promocodeId != null && !_promocodeId.isEmpty() && !_promocodeId.equalsIgnoreCase("null")) {
                        String promocodeDetails = null;
                        int promoId = Integer.parseInt(_promocodeId);
                        promoObj = new PromocodeManagement().getPromocodeById(SessionId, channelId, promoId);
                        SgPromocodeUsage promoUsageObj = new SgPromocodeUsage();
                        promoUsageObj.setChannelId(channelId);
                        promoUsageObj.setPartnerId(partnerObj.getPartnerId());
                        promoUsageObj.setPromoCodeId(promoId);
                        promoUsageObj.setPromocodeUsedOn(new Date());
                        promoUsageObj.setSubscriptionId(packageSubscribed.getBucketId());
                        retValue = new PromocodeUsageManagement().addPromoUsageDetails(promoUsageObj);
                        if (promoObj.getDiscountType().equalsIgnoreCase("flat")) {
                            promocodeDetails = promoObj.getDiscount() + " ,In price";
                        } else {
                            promocodeDetails = promoObj.getDiscount() + " ,In percentage";
                        }
                        paymentObj.setPromocodeId(promoId);
                        paymentObj.setPromoCodeDetails(promocodeDetails);
                    }
                    if (packageSubscribed.getServiceCharge() != null) {
                        subscribedserviceCharge = Double.parseDouble(packageSubscribed.getServiceCharge());
                    }
                    if (changeOnPackageCharge != null && !changeOnPackageCharge.isEmpty()) {
                        paymentObj.setChangeOnPackageCharge(Float.parseFloat(changeOnPackageCharge));
                        changePackageCharge = Double.parseDouble(changeOnPackageCharge);
                    }
                    if (_reactivationCharge != null && !_reactivationCharge.isEmpty()) {
                        paymentObj.setReactivationPackageCharge(Float.parseFloat(_reactivationCharge));
                        subscribedreactivationCharge = Double.parseDouble(_reactivationCharge);
                    }
                    if (_cancellationCharge != null && !_cancellationCharge.isEmpty()) {
                        paymentObj.setCancellationCharge(Float.parseFloat(_cancellationCharge));
                        subscribedcancellationCharge = Double.parseDouble(_cancellationCharge);
                    }
                    if (_latePenaltyCharge != null && !_latePenaltyCharge.isEmpty()) {
                        paymentObj.setLatePenaltyCharge(Float.parseFloat(_latePenaltyCharge));
                        subscribedlatePenaltyCharge = Double.parseDouble(_latePenaltyCharge);
                    }
                    if (tierOrSlabUsage != null) {
                        paymentObj.setTierOrSlabUsage(tierOrSlabUsage.toString());
                    }
                    if(caasServiceUsage != null && caasServiceUsage.length()!=0){
                        paymentObj.setCaasServiceUsage(caasServiceUsage.toString());
                    }
                    if(pgTransactionUsage != null && pgTransactionUsage.length()!=0){
                        paymentObj.setPgtransactionUsage(pgTransactionUsage.toString());
                    }
                    packageAmount = Float.valueOf(packageSubscribed.getPlanAmount()).doubleValue();
                    paymentObj.setInvoiceNo(invoiceId);
                    paymentObj.setPaidOn(new Date());
                    paymentObj.setPaidamount(Float.parseFloat(pAmount));
                    paymentObj.setSubscriptionId(packageSubscribed.getBucketId());
                    paymentObj.setPartnerId(partnerObj.getPartnerId());
                    paymentObj.setPaymentStatus(GlobalStatus.PROCESSED);
                    retValue = new PaymentManagement().updatePaymentDetails(paymentObj);
                }
                packageSubscribed.setStatus(GlobalStatus.PAID);
                retValue = new PackageSubscriptionManagement().updateDetails(packageSubscribed);
                if (retValue == 0) {
                    info = new CreditManagement().getDetails(partnerObj.getPartnerId());
                    if (info != null) {
                        info.setUpdatedFreeCredits(0);
                        info.setUpdatedMainCredits(0);
                        info.setApiRates("{}");
                        info.setFreeCredit(0);
                        info.setMainCredit(0);
                        info.setStatus(GlobalStatus.UPDATEASITIS);
                        new CreditManagement().updateDetails(info);
                    }
                }
                if (retValue == 0) {
                    int productType = 3;
                    Operators[] operatorObj = new OperatorsManagement().getAllOperators(channelId);
                    String[] operatorEmail = null;
                    if (operatorObj != null) {
                        operatorEmail = new String[operatorObj.length];
                        for (int i = 0; i < operatorObj.length; i++) {
                            operatorEmail[i] = (String) operatorObj[i].getEmailid();
                        }
                    }
                    JSONObject reqJSONObj = null;
                    String securityAlert = packageSubscribed.getSecurityAndAlertDetails();
                    if (securityAlert != null) {
                        JSONArray alertJson = new JSONArray(securityAlert);
                        for (int i = 0; i < alertJson.length(); i++) {
                            JSONObject jsonexists1 = alertJson.getJSONObject(i);
                            if (jsonexists1.has(packageSubscribed.getBucketName())) {
                                reqJSONObj = jsonexists1.getJSONObject(packageSubscribed.getBucketName());
                                if (reqJSONObj != null) {
                                    break;
                                }
                            }
                        }
                    }
                    String tmessage = "";
                    if (!reqJSONObj.getString("encryptedPDF").equals("enable")) {
                        tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.payment");
                    } else {
                        tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.payment.pdfPasswordProtect");
                    }
                    if (tmessage != null) {
                        tmessage = tmessage.replaceAll("#partnerName#", partnerObj.getPartnerName());
                        tmessage = tmessage.replaceAll("#channel#", channelName);
                        tmessage = tmessage.replaceAll("#datetime#", UtilityFunctions.getTMReqDate(d));
                        tmessage = tmessage.replaceAll("#package#", packageName);
                        tmessage = tmessage.replaceAll("#invoiceid#", invoiceId);
                        tmessage = tmessage.replaceAll("#paidAmount#", String.valueOf(pAmount));
                        tmessage = tmessage.replaceAll("#paymentDate#", UtilityFunctions.getTMReqDate(new Date()));
                        tmessage = tmessage.replaceAll("#email#", partnerObj.getPartnerEmailid());
                    }
                    String mimeType[] = {"application/octet-stream"};
                    String invoiceFilePath = new PDFInvoiceManagement().createInvoice(tierOrSlabUsage, changePackageCharge, subscribedserviceCharge, subscribedreactivationCharge, subscribedcancellationCharge, subscribedlatePenaltyCharge, packageAmount, partnerObj, reqPar.getAddress(), invoiceId, packageSubscribed, promoObj,caasServiceUsage,pgTransactionUsage);

                    // PDF Signing                    
                    if (reqJSONObj.getString("pdfSigning").equals("enable")) {
                        byte[] pdfBytes = Files.readAllBytes(Paths.get(invoiceFilePath));
                        String pdfBase64 = Base64.encodeBase64String(pdfBytes);
                        String pdfSignchannelId = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.channelId");
                        String pdfSignloginId = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.loginId");
                        String pdfSignloginpassword = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.loginPassword");
                        
                        String signInfoUser = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoUser");
                        String signInfoEmail = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoEmail");
                        String pdfSignloginaddress = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoaddress");
                        
                        String signInfoDesignation = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.designation");
                        String signInfoPageNoForSign = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfopageNoForSign");
                        String signInfoSignerSeqNo = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfosignerSeqNo");
                        String signInfoAlignLeftOrRight = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoalignLeftOrRight");
                        String signInfotopOrbottom = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoaligntopOrbottom");
                        String signInfoReason = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoReason");
                        String sessionID = AxiomPDFSignConnector.openSession(pdfSignchannelId, pdfSignloginId, pdfSignloginpassword);
                        
                        String signImagePath = LoadSettings.g_strPath + "Signiture.png";
                        BufferedImage img = ImageIO.read(new File(signImagePath));
                        String signInfobase64SignImage = encodeToString(img, "png");
                        
//                        SignersInfo signInfo = new SignersInfo();
//                        signInfo.setName(signInfoUser);
//                        signInfo.setEmailAddress(signInfoEmail);
//                        signInfo.setAddress(pdfSignloginaddress);
//                        signInfo.setBase64SignImage(signInfobase64SignImage);
//                        signInfo.setPageNoForSign(Integer.parseInt(signInfoPageNoForSign));
//                        signInfo.setSignerSeqNo(Integer.parseInt(signInfoSignerSeqNo));
//                        signInfo.setTopOrbottom(Integer.parseInt(signInfotopOrbottom));
//                        signInfo.setAlignLeftOrRight(Integer.parseInt(signInfoAlignLeftOrRight));
//                        signInfo.setDesignation(signInfoDesignation);
//                        signInfo.setReason(signInfoReason);
//                        AxiomSigningRequest serviceRequest = new AxiomSigningRequest();
//                        serviceRequest.setBase64PDFfile(pdfBase64);
//                        serviceRequest.getSignersInfo().add(signInfo);
                        AxiomSignerResponse serviceResponse = AxiomPDFSignConnector.sendSignatureRequest(sessionID, invoiceFilePath,signInfoUser,signInfoEmail,pdfSignloginaddress,signInfoDesignation,signImagePath,null,null,signInfoReason);
                        
                        String signedPDF = serviceResponse.getBase64SignedPdf();
                        if (signedPDF != null) {
                            byte[] decodePDF = Base64.decodeBase64(signedPDF);
                            File file = new File(invoiceFilePath);
                            FileOutputStream fop = new FileOutputStream(file);
                            fop.write(decodePDF);
                            fop.flush();
                            fop.close();
                        }
                    }
                    String[] arrInvoicePath = {invoiceFilePath};
                   SGStatus status = new SendNotification().SendEmailWithAttachment(channelId, partnerObj.getPartnerEmailid(), "Package Payment Successfully Done", tmessage, operatorEmail, null, arrInvoicePath, mimeType, productType);
                     //SGStatus status = new SendNotification().SendEmail(channelId, partnerObj.getPartnerEmailid(), "Package Payment Successfully Done", tmessage, operatorEmail, null, arrInvoicePath, mimeType, productType);
                    if (status.iStatus != PENDING && status.iStatus != SEND) {
                        result = "error";
                        message = "Package payment had been done successfully but failed to sent email notification.";
                        try {
                            json.put("_result", result);
                            logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                            json.put("_message", message);
                            logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        out.print(json);
                        out.flush();
                        return;
                    }
                    File deleteFile = new File(invoiceFilePath);
                    deleteFile.delete();
                    json.put("result", result);
                    json.put("message", message);
                }
                
            } else {
                result = "error";
                message = "Package payment failed.";
                json.put("result", result);
                logger.debug("Response of #PostpaidPayment from #PPortal Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #PostpaidPayment from #PPortal Servlet's Parameter  message is " + message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #PostpaidPayment from #PPortal " + json.toString());
            logger.info("Response of #PostpaidPayment from #PPortal Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
        
    }
    
    public static String encodeToString(BufferedImage image, String type) {
        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        try {
            ImageIO.write(image, type, bos);
            byte[] imageBytes = bos.toByteArray();
            imageString = Base64.encodeBase64String(imageBytes);
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
