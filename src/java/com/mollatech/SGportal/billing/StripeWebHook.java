/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import blue.bricks.documentsigner.AxiomPdfResponse;
import blue.bricks.documentsigner.AxiomSignerResponse;
import com.mollatech.GenerateInvoiceId;
import static com.mollatech.SGportal.billing.SubscribePackage.PENDING;
import static com.mollatech.SGportal.billing.SubscribePackage.SEND;
import com.mollatech.pdfsigning.AxiomPDFSignConnector;
import com.mollatech.service.nucleus.crypto.AES;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MSConfig;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgCreditInfo;
import com.mollatech.serviceguard.nucleus.db.SgPaymentdetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgSapReceiptFile;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.ChannelsUtils;
import com.mollatech.serviceguard.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.serviceguard.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PDFInvoiceManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SAPBillManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import com.stripe.Stripe;
import com.stripe.model.Event;
import com.stripe.model.EventData;
import com.stripe.model.Invoice;
import com.stripe.model.InvoiceLineItem;
import com.stripe.model.InvoiceLineItemCollection;
import com.stripe.model.Plan;
import com.stripe.model.StripeObject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "StripeWebHook", urlPatterns = {"/StripeWebHook"})
public class StripeWebHook extends HttpServlet {

    static final Logger logger = Logger.getLogger(StripeWebHook.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AES aesObj = new AES();
        response.addHeader("Access-Control-Allow-Origin", "*");
        int retValue = -1;
        SessionManagement sManagement = new SessionManagement();
        String _channelName = "ServiceGuard";
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        Channels channel = cUtil.getChannel(_channelName);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        String SessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
        String stripeapikey = LoadSettings.g_sSettings.getProperty("stripe.apiKey");
        String billingPeriodMonthly = LoadSettings.g_sSettings.getProperty("billing.period.month");
        String billingPeriodQuaterly = LoadSettings.g_sSettings.getProperty("billing.period.quarterly");
        String billingPeriodHalfYearly = LoadSettings.g_sSettings.getProperty("billing.period.halfYearly");
        String billingPeriodYearly = LoadSettings.g_sSettings.getProperty("billing.period.yearly");
        stripeapikey = aesObj.PINDecrypt(stripeapikey, AES.getSignature());
        Stripe.apiKey = stripeapikey;
        SgCreditInfo info = null;
        logger.info("Web Hook StripeAPIKey " + stripeapikey);
        System.out.println("Webhook called >>> " + stripeapikey);
        String sigHeader = request.getHeader("Stripe-Signature");
        DecimalFormat df = new DecimalFormat("#0.00");
        //Event event = null;
        //public Object handle(Request request, Response response) {
        // Retrieve the request's body and parse it as JSON
        try {
            String rawJson = IOUtils.toString(request.getInputStream());
            logger.info("rawJson " + rawJson);
            Event event = Event.GSON.fromJson(rawJson, Event.class);
            // Do something with event
            if (event != null) {
                String eventType = event.getType();
                logger.info("eventType " + eventType);
                if (eventType != null && eventType.equalsIgnoreCase("invoice.payment_succeeded")) {
                    EventData eventData = event.getData();
                    StripeObject stripeObj = eventData.getObject();
                    Invoice invoiceevent = Invoice.GSON.fromJson(stripeObj.toJson(), Invoice.class);
                    logger.info("invoiceevent " + invoiceevent.toString());
                    if (invoiceevent != null) {
                        String custId = invoiceevent.getCustomer();
                        logger.info("stripeCustId " + custId);
                        String subscriptionDetails = invoiceevent.getSubscription();
                        logger.info("stripeSubscriptionDetails " + subscriptionDetails);
                        Long grossTotalAmt = invoiceevent.getAmountDue();
                        Date currentDate = new Date();
                        SgSubscriptionDetails subscriObject1 = null;
                        PartnerDetails partnerObj = new PartnerManagement().getPartnerDetailsByStripeCustomerId(custId);
                        if (partnerObj != null) {
                            subscriObject1 = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(partnerObj.getPartnerId());
                        }
                        if (subscriObject1 != null) {
                            Date expDate = subscriObject1.getExpiryDateNTime();

                            if (currentDate.after(expDate)) {
                                InvoiceLineItemCollection invoiceLineItemColl = invoiceevent.getLines();
                                String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.payment");
                                List invoiceList = invoiceLineItemColl.getData();
                                InvoiceLineItem invoiceLineItemObj = (InvoiceLineItem) invoiceList.get(0);
                                Plan planObject = invoiceLineItemObj.getPlan();
                                Long planAmount = planObject.getAmount();
                                String strPlanAmount = String.valueOf(grossTotalAmt);
                                String newAmount = strPlanAmount.substring(0, strPlanAmount.length() - 2);
                                String lastTwo = strPlanAmount.substring(Math.max(strPlanAmount.length() - 2, 0));
                                newAmount += "." + lastTwo;
                                logger.info("planId " + planObject.getId());
                                SgReqbucketdetails packageObject = new RequestPackageManagement().getBucketRequestsbyStripePlanId(planObject.getId());
                                if (packageObject != null) {
                                    SgSubscriptionDetails subscriptionObject = new SgSubscriptionDetails();
                                    //SgSubscriptionDetails packageSubscribed = new PackageSubscriptionManagement().getSubscriptionbyPartnerId(SessionId, channelId, partnerObj.getPartnerId());
                                    SgUsers userObj = new UsersManagement().getSgUsersByPartnerId(partnerObj.getPartnerId());
                                    subscriptionObject.setTierRateDetails(packageObject.getTierRateDetails());
                                    subscriptionObject.setApRateDetails(packageObject.getApRateDetails());
                                    subscriptionObject.setBucketDuration(packageObject.getBucketDuration());
                                    subscriptionObject.setBucketName(packageObject.getBucketName());
                                    subscriptionObject.setCancellationRate(packageObject.getCancellationRate());
                                    subscriptionObject.setChangePackageRate(packageObject.getChangePackageRate());
                                    subscriptionObject.setChannelId(packageObject.getChannelId());
                                    subscriptionObject.setCreationDate(new Date());
                                    subscriptionObject.setCreatedOn(new Date());
                                    subscriptionObject.setBucketId(packageObject.getBucketId());
                                    subscriptionObject.setDaysForFreeTrial(packageObject.getDaysForFreeTrial());
                                    subscriptionObject.setFeatureList(packageObject.getFeatureList());
                                    subscriptionObject.setFreeCredits(packageObject.getFreeCredits());
                                    subscriptionObject.setLatePenaltyRate(packageObject.getLatePenalitesRate());
                                    subscriptionObject.setMinimumBalance(packageObject.getMinimumBalance());
                                    subscriptionObject.setPartnerId(partnerObj.getPartnerId());
                                    subscriptionObject.setPaymentMode(packageObject.getPaymentMode());
                                    subscriptionObject.setPlanAmount(packageObject.getPlanAmount());
                                    subscriptionObject.setReActivationCharge(packageObject.getReActivationCharge());
                                    subscriptionObject.setSecurityAndAlertDetails(packageObject.getSecurityAndAlertDetails());
                                    subscriptionObject.setServiceCharge(packageObject.getServiceCharge());
                                    subscriptionObject.setSlabApRateDetails(packageObject.getSlabApRateDetails());
                                    subscriptionObject.setApiThrottling(packageObject.getApiThrottling());
                                    subscriptionObject.setStatus(GlobalStatus.UNPAID);
                                    subscriptionObject.setTax(packageObject.getTax());
                                    subscriptionObject.setMainCredits(packageObject.getMainCredits());
                                    subscriptionObject.setFlatPrice(packageObject.getFlatPrice());
                                    subscriptionObject.setPackageDescription(packageObject.getPackageDescription());
                                    int days = 0;
                                    int billingDays = 0;
                                    if (packageObject.getBucketDuration().equalsIgnoreCase("daily")) {
                                        days = 1;
                                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("weekly")) {
                                        days = 7;
                                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("biMonthly")) {
                                        days = 15;
                                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("monthly")) {
                                        if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                                            billingDays = Integer.parseInt(billingPeriodMonthly);
                                        }
                                        days = billingDays;
                                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("quarterly")) {
                                        if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                                            billingDays = Integer.parseInt(billingPeriodQuaterly);
                                        }
                                        days = billingDays;
                                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("halfYearly")) {
                                        if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                                            billingDays = Integer.parseInt(billingPeriodHalfYearly);
                                        }
                                        days = billingDays;
                                    } else if (packageObject.getBucketDuration().equalsIgnoreCase("yearly")) {
                                        if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                                            billingDays = Integer.parseInt(billingPeriodYearly);
                                        }
                                        days = billingDays;
                                    }
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(new Date());
                                    calendar.add(Calendar.DATE, days);
                                    subscriptionObject.setExpiryDateNTime(calendar.getTime());
                                    retValue = new PackageSubscriptionManagement().CreateSubscriptionDetails(SessionId, channel.getChannelid(), subscriptionObject);

                                    // addtion of subscription package details end
                                    // assign a credit to developer
                                    info = new CreditManagement().getDetails(subscriptionObject.getPartnerId());
                                    if (info != null) {
                                        info.setStatus(GlobalStatus.UPDATED);
                                        info.setUpdatedFreeCredits(subscriptionObject.getFreeCredits());
                                        info.setUpdatedMainCredits(subscriptionObject.getMainCredits() + info.getUpdatedMainCredits());
                                        info.setApiRates(subscriptionObject.getApRateDetails());
                                        new CreditManagement().updateDetails(info);
                                    } else {
                                        info = new SgCreditInfo();
                                        info.setStatus(GlobalStatus.SUCCESS);
                                        info.setApiRates(subscriptionObject.getApRateDetails());
                                        info.setFreeCredit(subscriptionObject.getFreeCredits());
                                        info.setMainCredit(subscriptionObject.getMainCredits());
                                        info.setPartnerId(subscriptionObject.getPartnerId());
                                        new CreditManagement().addDetails(info);
                                    }
                                    // credit assignment end

                                    // add payment details
                                    SgPaymentdetails paymentObj = new SgPaymentdetails();
                                    String invoiceid = GenerateInvoiceId.getDate() + partnerObj.getPartnerId() + GenerateInvoiceId.getRandom();
                                    Double packageAmount = Float.valueOf(packageObject.getPlanAmount()).doubleValue();
                                    paymentObj.setInvoiceNo(invoiceid);
                                    paymentObj.setPaidOn(new Date());
                                    paymentObj.setPaidamount(Float.parseFloat(newAmount));
                                    paymentObj.setSubscriptionId(subscriptionObject.getBucketId());
                                    paymentObj.setPartnerId(partnerObj.getPartnerId());
                                    retValue = new PaymentManagement().createPaymentDetails(SessionId, channel.getChannelid(), paymentObj);
                                    if (retValue > 0) {
                                        subscriptionObject.setStatus(GlobalStatus.PAID);
                                        new PackageSubscriptionManagement().updateDetails(subscriptionObject);
                                    }
                                    // payment detils end
                                    Operators[] operatorObj = new OperatorsManagement().getAllOperators(channel.getChannelid());
                                    String[] operatorEmail = null;
                                    if (operatorObj != null) {
                                        operatorEmail = new String[operatorObj.length];
                                        for (int i = 0; i < operatorObj.length; i++) {
                                            operatorEmail[i] = (String) operatorObj[i].getEmailid();
                                        }
                                    }
                                    JSONObject reqJSONObj = null;
                                    String securityAlert = packageObject.getSecurityAndAlertDetails();
                                    if (securityAlert != null) {
                                        JSONArray alertJson = new JSONArray(securityAlert);
                                        for (int i = 0; i < alertJson.length(); i++) {
                                            JSONObject jsonexists1 = alertJson.getJSONObject(i);
                                            if (jsonexists1.has(packageObject.getBucketName())) {
                                                reqJSONObj = jsonexists1.getJSONObject(packageObject.getBucketName());
                                                if (reqJSONObj != null) {
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    String pdfPassword = "";

                                    if (!reqJSONObj.getString("encryptedPDF").equals("enable")) {
                                        tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.payment");
                                    } else {
                                        tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.payment.pdfPasswordProtect");
                                        if (partnerObj.getPartnerEmailid().length() >= 6) {
                                            pdfPassword = partnerObj.getPartnerEmailid().substring(0, 5);
                                            tmessage = tmessage.replaceAll("#passwordProtectNote#", "Attach invoice is password protected. Open it with your First five letter of your registered email id with us.");
                                        } else {
                                            pdfPassword = partnerObj.getPartnerEmailid();
                                            tmessage = tmessage.replaceAll("#passwordProtectNote#", "Attach invoice is password protected. Open it with your email id, which is registered with us.");
                                        }
                                    }
                                    MSConfig config = (MSConfig) new SettingsManagement().getSetting(SessionId, channel.getChannelid(), SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
                                    String schemes = "http";
                                    if (config != null) {
                                        if (config.apssl.equalsIgnoreCase("yes")) {
                                            schemes = "https";
                                        }
                                    }
                                    String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry");
                                    String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question");
                                    String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea");
                                    String[] enquiryEmailDetails = enquiryId.split(":");
                                    String[] supportEmailDetails = supportId.split(":");
                                    String[] ideaEmailDetails = ideaId.split(":");
                                    String path = request.getContextPath();
                                    String dashboardURL = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path;
                                    String apihref = schemes + "://" + config.aphostIp + ":" + config.aphostPort + "/APIDoc";
                                    String greenbackGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/greenback.gif";
                                    String spadeGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/spade.gif";
                                    String addressbookGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/addressbook.gif";
                                    String penpaperGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/penpaper.gif";
                                    String lightbulbGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/lightbulb.gif";
                                    String packageName = packageObject.getBucketName();
                                    String bucketName = packageName.toLowerCase();
                                    if (bucketName.contains("basic") && bucketName.contains("month")) {
                                        packageName = "Basic";
                                    } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                                        packageName = "Basic";
                                    } else if (bucketName.contains("student") && bucketName.contains("month")) {
                                        packageName = "Student";
                                    } else if (bucketName.contains("student") && bucketName.contains("year")) {
                                        packageName = "Student";
                                    } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                                        packageName = "Standard";
                                    } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                                        packageName = "Standard";
                                    } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                                        packageName = "Enterprise";
                                    } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                                        packageName = "Enterprise";
                                    }
                                    if (tmessage != null) {
                                        tmessage = tmessage.replaceAll("#partnerName#", partnerObj.getPartnerName());
                                        tmessage = tmessage.replaceAll("#channel#", "Ready APIs");
                                        tmessage = tmessage.replaceAll("#datetime#", UtilityFunctions.getTMReqDate(new Date()));
                                        tmessage = tmessage.replaceAll("#package#", packageName);
                                        tmessage = tmessage.replaceAll("#invoiceid#", invoiceid);
                                        tmessage = tmessage.replaceAll("#paidAmount#", df.format(Double.parseDouble(newAmount)));
                                        tmessage = tmessage.replaceAll("#paymentDate#", UtilityFunctions.getTMReqDate(new Date()));
                                        tmessage = tmessage.replaceAll("#email#", partnerObj.getPartnerEmailid());

                                        tmessage = tmessage.replaceAll("#apihref#", apihref);
                                        tmessage = tmessage.replaceAll("#dashboardhref#", dashboardURL);
                                        tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
                                        tmessage = tmessage.replaceAll("#supportId#", supportId);
                                        tmessage = tmessage.replaceAll("#ideaId#", ideaId);
                                        tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
                                        tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
                                        tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);
                                        tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
                                        tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
                                        tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
                                        tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
                                        tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);
                                    }
                                    String appurl = (request.getRequestURL().toString());
                                    URL myAppUrl = new URL(appurl);
                                    int port = myAppUrl.getPort();
                                    if (myAppUrl.getProtocol().equals("https") && port == -1) {
                                        port = 443;
                                    } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
                                        port = 80;
                                    }

                                    String contextPath = request.getContextPath();
                                    String pdfSaveURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + contextPath + "/";
                                    String signImageURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + contextPath + "/";
                                    String pdfSigImg = LoadSettings.pdfSavePath;

                                    String mimeType[] = {"application/octet-stream"};
                                    String invoiceFilePath = new PDFInvoiceManagement().createReadyAPIInvoice(null, null, null, null, null, null, packageAmount, partnerObj, null, invoiceid, subscriptionObject, null, null, null);
                                    // PDF Signing  

                                    String pdfSignchannelId = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.channelId");
                                    String pdfSignloginId = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.loginId");
                                    String pdfSignloginpassword = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.loginPassword");

                                    String signInfoUser = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoUser");
                                    String signInfoEmail = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoEmail");
                                    String pdfSignloginaddress = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoaddress");

                                    String signInfoDesignation = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.designation");
                                    String signInfoReason = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoReason");
                                    if (reqJSONObj != null && reqJSONObj.getString("pdfSigning").equals("enable")) {
                                        logger.info("PDF Signing started at StripeWebHook ");
                                        File f = new File(invoiceFilePath);
                                        String fileName = f.getName();
                                        String pdfURLPath = pdfSaveURL + fileName;
                                        String sessionID = AxiomPDFSignConnector.openSession(pdfSignchannelId, pdfSignloginId, pdfSignloginpassword);
                                        logger.info("PDF Signing sessionID at StripeWebHook >> " + sessionID);
                                        String signImagePath = signImageURL + "Signiture.png";
                                        logger.info("PDF Signing pdfSaveURL physical path at StripeWebHook >> " + pdfSaveURL);
                                        logger.info("PDF Signing signImagePath physical path at StripeWebHook >> " + signImagePath);
                                        AxiomSignerResponse serviceResponse = AxiomPDFSignConnector.sendSignatureRequest(sessionID, pdfURLPath, signInfoUser, signInfoEmail, pdfSignloginaddress, signInfoDesignation, signImagePath, null, null, signInfoReason);
                                        //AxiomSignerResponse serviceResponse = AxiomPDFSignConnector.sendSignatureRequest(sessionID, "https://www.readyapis.com/invoice1.pdf",signInfoUser,signInfoEmail,pdfSignloginaddress,signInfoDesignation,"https://www.readyapis.com/Signiture.png",null,null,signInfoReason);
//                                        String signedPDF = serviceResponse.getBase64SignedPdf();
//                                        if (signedPDF != null) {
//                                            byte[] decodePDF = Base64.decodeBase64(signedPDF);
//                                            File file = new File(invoiceFilePath);
//                                            FileOutputStream fop = new FileOutputStream(file);
//                                            fop.write(decodePDF);
//                                            fop.flush();
//                                            fop.close();
//                                        }          
                                        String pdfResponseURL = serviceResponse.getSingedUrl();
                                        logger.info("PDF Signing pdfResponseURL physical path at StripeWebHook >> " + pdfResponseURL);
                                        if (pdfResponseURL != null && !pdfResponseURL.isEmpty()) {
                                            File signFile = new File(invoiceFilePath);
                                            signFile.delete();
                                            if (reqJSONObj != null && reqJSONObj.getString("encryptedPDF").equals("enable")) {
                                                invoiceFilePath = downloadFile(pdfResponseURL, pdfSigImg, subscriptionObject.getBucketName() + invoiceid);
                                            } else {
                                                invoiceFilePath = downloadFile(pdfResponseURL, pdfSigImg, subscriptionObject.getBucketName() + invoiceid + ".pdf");
                                            }
                                            logger.info("PDF Signing pdfSignDownloadPath physical path at StripeWebHook >> " + invoiceFilePath);
                                        }
                                    }
                                    if (reqJSONObj != null && reqJSONObj.getString("encryptedPDF").equals("enable")) {
                                        String sessionID = AxiomPDFSignConnector.openSession(pdfSignchannelId, pdfSignloginId, pdfSignloginpassword);
                                        logger.info("PDF Encryption started at StripeWebHook ");
                                        logger.info("PDF Encryption pdfSaveURL at StripeWebHook >> " + pdfSaveURL);
                                        logger.info("PDF Encryption sessionID at StripeWebHook >> " + sessionID);
                                        File f = new File(invoiceFilePath);
                                        String fileName = f.getName();
                                        String pdfURLPath = pdfSaveURL + fileName;
                                        AxiomPdfResponse servicePDFResponse = AxiomPDFSignConnector.encryptOrDecryptPdf(sessionID, 1, null, pdfURLPath, pdfPassword, "12345", true, false, true, true, true, true, true, true, true, true, true);
                                        //AxiomPdfResponse servicePDFResponse = AxiomPDFSignConnector.encryptOrDecryptPdf(sessionID, 1, null, "https://www.readyapis.com/invoice1.pdf", pdfPassword, "12345", true, false, true, true, true, true, true, true, true, true, true);
                                        logger.info("#StripeWebHook from #PPortal PDF encryption server response " + servicePDFResponse.getErrormsg() + " with error code " + servicePDFResponse.getErrorcode());
                                        String pdfResponseURL = servicePDFResponse.getPDFDownloadUrl();
                                        System.out.println("pdfEncrResponse >> " + pdfResponseURL);
                                        logger.info("PDF Encryption pdfEncrResponse getPDFDownloadUrl() at StripeWebHook >> " + pdfResponseURL);
                                        System.out.println("serviceEncryResponse >>" + servicePDFResponse.getErrormsg() + " " + servicePDFResponse.getErrorcode());
                                        if (pdfResponseURL != null && !pdfResponseURL.isEmpty()) {
                                            File encryFile = new File(invoiceFilePath);
                                            encryFile.delete();
                                            invoiceFilePath = downloadFile(pdfResponseURL, pdfSigImg, fileName + ".pdf");
                                            System.out.println("encryptionPDF path >> " + invoiceFilePath);
                                            logger.info("PDF Encryption pdfEncrResponse physical path at StripeWebHook >> " + invoiceFilePath);
                                        }
                                    }
                                    Path saveInvoicepath = Paths.get(invoiceFilePath, new String[0]);
                                    byte[] invoiceByteArray = Files.readAllBytes(saveInvoicepath);

                                    SgSapReceiptFile invoiceSave = new SAPBillManagement().getSapReceiptByPartnerIdSubscribeId(partnerObj.getPartnerId(), subscriptionObject.getBucketId());
                                    if (invoiceSave != null) {
                                        invoiceSave.setDocuments(invoiceByteArray);
                                        invoiceSave.setPartnerId(partnerObj.getPartnerId());
                                        invoiceSave.setSubscriptionId(subscriptionObject.getBucketId());
                                        invoiceSave.setUpdatedOn(new Date());
                                        new SAPBillManagement().updateSapReceiptFileDetail(SessionId, invoiceSave);
                                        logger.info("Invoice Updated at StripeWebHook >> ");
                                    } else {
                                        invoiceSave = new SgSapReceiptFile();
                                        invoiceSave.setDocuments(invoiceByteArray);
                                        invoiceSave.setPartnerId(partnerObj.getPartnerId());
                                        invoiceSave.setSubscriptionId(subscriptionObject.getBucketId());
                                        invoiceSave.setCreatedOn(new Date());
                                        new SAPBillManagement().addSAPBillDetails(invoiceSave);
                                        logger.info("Invoice added at StripeWebHook >> ");
                                    }
                                    int productType = 3;
                                    String[] arrInvoicePath = {invoiceFilePath};
                                    SGStatus status = new SendNotification().SendEmail(channel.getChannelid(), partnerObj.getPartnerEmailid(), "Package Payment Successfully Done", tmessage, operatorEmail, null, arrInvoicePath, mimeType, productType);
                                    if (status.iStatus != PENDING && status.iStatus != SEND) {

                                    }
                                    File deleteFile = new File(invoiceFilePath);
                                    deleteFile.delete();
                                }
                                response.sendError(200);
                            }
                        }
                    }
                } else if (eventType != null && eventType.equalsIgnoreCase("invoice.payment_failed")) {
                    EventData eventData = event.getData();
                    StripeObject stripeObj = eventData.getObject();
                    Invoice invoiceevent = Invoice.GSON.fromJson(stripeObj.toJson(), Invoice.class);
                    logger.info("webhook invoiceevent " + invoiceevent.toString());
                    if (invoiceevent != null) {
                        String custId = invoiceevent.getCustomer();
                        Long grossTotalAmt = invoiceevent.getAmountDue();
                        logger.info("webhook stripeCustId " + custId);
                        logger.info("webhook grossTotalAmt " + grossTotalAmt);
                        PartnerDetails partnerObj = new PartnerManagement().getPartnerDetailsByStripeCustomerId(custId);

                        InvoiceLineItemCollection invoiceLineItemColl = invoiceevent.getLines();
                        String tmessage = (String) LoadSettings.g_templateSettings.getProperty("package.payment.failed");
                        List invoiceList = invoiceLineItemColl.getData();
                        InvoiceLineItem invoiceLineItemObj = (InvoiceLineItem) invoiceList.get(0);
                        Plan planObject = invoiceLineItemObj.getPlan();

                        String strPlanAmount = String.valueOf(grossTotalAmt);
                        String newAmount = strPlanAmount.substring(0, strPlanAmount.length() - 2);
                        String lastTwo = strPlanAmount.substring(Math.max(strPlanAmount.length() - 2, 0));
                        String packageName = "";
                        newAmount += "." + lastTwo;
                        logger.info("planId " + planObject.getId());
                        SgReqbucketdetails packageObject = new RequestPackageManagement().getBucketRequestsbyStripePlanId(planObject.getId());
                        if (packageObject != null) {
                            packageName = packageObject.getBucketName();
                        }
                        Operators[] operatorObj = new OperatorsManagement().getAllOperators(channel.getChannelid());
                        String[] operatorEmail = null;
                        if (operatorObj != null) {
                            operatorEmail = new String[operatorObj.length];
                            for (int i = 0; i < operatorObj.length; i++) {
                                operatorEmail[i] = (String) operatorObj[i].getEmailid();
                            }
                        }
                        MSConfig config = (MSConfig) new SettingsManagement().getSetting(SessionId, channel.getChannelid(), SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
                        String schemes = "http";
                        if (config != null) {
                            if (config.apssl.equalsIgnoreCase("yes")) {
                                schemes = "https";
                            }
                        }
                        String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry");
                        String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question");
                        String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea");
                        String[] enquiryEmailDetails = enquiryId.split(":");
                        String[] supportEmailDetails = supportId.split(":");
                        String[] ideaEmailDetails = ideaId.split(":");
                        String path = request.getContextPath();
                        String greenbackGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/greenback.gif";
                        String spadeGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/spade.gif";
                        String addressbookGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/addressbook.gif";
                        String penpaperGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/penpaper.gif";
                        String lightbulbGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/lightbulb.gif";
                        
                        String bucketName = packageName.toLowerCase();
                        if (bucketName.contains("basic") && bucketName.contains("month")) {
                            packageName = "Basic";
                        } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                            packageName = "Basic";
                        } else if (bucketName.contains("student") && bucketName.contains("month")) {
                            packageName = "Student";
                        } else if (bucketName.contains("student") && bucketName.contains("year")) {
                            packageName = "Student";
                        } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                            packageName = "Standard";
                        } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                            packageName = "Standard";
                        } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                            packageName = "Enterprise";
                        } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                            packageName = "Enterprise";
                        }
                        if (tmessage != null) {
                            tmessage = tmessage.replaceAll("#partnerName#", partnerObj.getPartnerName());
                            tmessage = tmessage.replaceAll("#package#", packageName);
                            tmessage = tmessage.replaceAll("#paidAmount#", df.format(Double.parseDouble(newAmount)));

                            tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
                            tmessage = tmessage.replaceAll("#supportId#", supportId);
                            tmessage = tmessage.replaceAll("#ideaId#", ideaId);
                            tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
                            tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
                            tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);
                            tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
                            tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
                            tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
                            tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
                            tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);
                        }
                        int productType = 3;
                        SGStatus status = new SendNotification().SendEmail(channel.getChannelid(), partnerObj.getPartnerEmailid(), "Package payment failed", tmessage, operatorEmail, null, null, null, productType);
                        if (status.iStatus != PENDING && status.iStatus != SEND) {
                            logger.info("WebHook, Email notification failed to sent for Package Payment Failed");
                        }
                        response.sendError(200);
                    }
                }
                //response.sendError(200);
            } else {
                response.sendError(500);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String downloadFile(String fileURL, String saveDir, String fileName) throws IOException {
        String saveFilePath = null;
        try {
            URL url = new URL(fileURL);
            int BUFFER_SIZE = 4096;
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            int responseCode = httpConn.getResponseCode();
            // always check HTTP response code first
            if (responseCode == HttpURLConnection.HTTP_OK) {
                //String fileName = "";
                String disposition = httpConn.getHeaderField("Content-Disposition");
                String contentType = httpConn.getContentType();
                int contentLength = httpConn.getContentLength();

//            if (disposition != null) {
//                // extracts file name from header field
//                int index = disposition.indexOf("filename=");
//                if (index > 0) {
//                    fileName = disposition.substring(index + 10,
//                            disposition.length() - 1);
//                }
//            } else {
//                // extracts file name from URL
//                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
//                        fileURL.length());
//            }
                System.out.println("Content-Type = " + contentType);
                System.out.println("Content-Disposition = " + disposition);
                System.out.println("Content-Length = " + contentLength);
                System.out.println("fileName = " + fileName);

                // opens input stream from the HTTP connection
                InputStream inputStream = httpConn.getInputStream();
                saveFilePath = saveDir + fileName;

                // opens an output stream to save into file
                FileOutputStream outputStream = new FileOutputStream(saveFilePath);

                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
                outputStream.close();
                inputStream.close();
                System.out.println("File downloaded");

            } else {
                System.out.println("No file to download. Server replied HTTP code: " + responseCode);
                return null;
            }
            httpConn.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return saveFilePath;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
