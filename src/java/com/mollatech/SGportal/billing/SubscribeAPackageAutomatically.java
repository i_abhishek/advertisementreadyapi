/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.mollatech.GenerateInvoiceId;
import static com.mollatech.SGportal.billing.SubscribePackage.logger;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgCreditInfo;
import com.mollatech.serviceguard.nucleus.db.SgPaymentdetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgSapReceiptFile;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PDFInvoiceManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SAPBillManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "SubscribeAPackageAutomatically", urlPatterns = {"/SubscribeAPackageAutomatically"})
public class SubscribeAPackageAutomatically extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        logger.info("Request servlet is #SubscribePackage from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String channelName = "Ready API";
        String result = "success";
        String message = "Package have been subscriped successfully";
        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");

        String _promocodeId = (String) request.getSession().getAttribute("_promocodeId");
        String changeOnPackageCharge = (String) request.getSession().getAttribute("_changeOnPackageCharge");
        String _reactivationCharge = (String) request.getSession().getAttribute("_reactivationCharge");
        String _cancellationCharge = (String) request.getSession().getAttribute("_cancellationCharge");
        String _latePenaltyCharge = (String) request.getSession().getAttribute("_latePenaltyCharge");
        String packageName = request.getParameter("packageName");
        String invoiceId =  GenerateInvoiceId.getDate() + partnerObj.getPartnerId() + GenerateInvoiceId.getRandom();
        String paidAmount = request.getParameter("grossPaymentAmount");
        
        DecimalFormat df = new DecimalFormat("#0.00");
        int retValue = -1;
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
        SgCreditInfo info = null;
        float pAmount = 0;
        
        Double packageAmount = null;
        Double changePackageCharge = null;
        Double subscribedserviceCharge = null;
        Double subscribedreactivationCharge = null;
        Double subscribedcancellationCharge = null;
        Double subscribedlatePenaltyCharge = null;
        String billingPeriodMonthly = LoadSettings.g_sSettings.getProperty("billing.period.month");
        String billingPeriodQuaterly = LoadSettings.g_sSettings.getProperty("billing.period.quarterly");
        String billingPeriodHalfYearly = LoadSettings.g_sSettings.getProperty("billing.period.halfYearly");
        String billingPeriodYearly = LoadSettings.g_sSettings.getProperty("billing.period.yearly");
        int productType = 3;
        int billingDays = 0;                
            try {
                if (paidAmount != null) {
                    pAmount = Float.parseFloat(paidAmount);
                }
                SgSubscriptionDetails subscriptionObject = new SgSubscriptionDetails();               
                SgUsers userObj = new UsersManagement().getSgUsersByPartnerId(partnerObj.getPartnerId());

                // add the subscription package details of a developer
                System.out.println("packageName" + packageName);
                SgReqbucketdetails packageObject = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, packageName);
                subscriptionObject.setTierRateDetails(packageObject.getTierRateDetails());
                subscriptionObject.setApRateDetails(packageObject.getApRateDetails());
                subscriptionObject.setBucketDuration(packageObject.getBucketDuration());
                subscriptionObject.setBucketName(packageObject.getBucketName());
                subscriptionObject.setCancellationRate(packageObject.getCancellationRate());
                subscriptionObject.setChangePackageRate(packageObject.getChangePackageRate());
                subscriptionObject.setChannelId(packageObject.getChannelId());
                subscriptionObject.setCreationDate(new Date());
                subscriptionObject.setCreatedOn(new Date());
                subscriptionObject.setBucketId(packageObject.getBucketId());
                subscriptionObject.setDaysForFreeTrial(packageObject.getDaysForFreeTrial());
                subscriptionObject.setFeatureList(packageObject.getFeatureList());
                subscriptionObject.setFreeCredits(packageObject.getFreeCredits());
                subscriptionObject.setLatePenaltyRate(packageObject.getLatePenalitesRate());
                subscriptionObject.setMinimumBalance(packageObject.getMinimumBalance());
                subscriptionObject.setPartnerId(partnerObj.getPartnerId());
                subscriptionObject.setPaymentMode(packageObject.getPaymentMode());
                subscriptionObject.setPlanAmount(packageObject.getPlanAmount());
                subscriptionObject.setReActivationCharge(packageObject.getReActivationCharge());
                subscriptionObject.setSecurityAndAlertDetails(packageObject.getSecurityAndAlertDetails());
                subscriptionObject.setServiceCharge(packageObject.getServiceCharge());
                subscriptionObject.setSlabApRateDetails(packageObject.getSlabApRateDetails());
                subscriptionObject.setApiThrottling(packageObject.getApiThrottling());
                subscriptionObject.setStatus(GlobalStatus.UNPAID);
                subscriptionObject.setTax(packageObject.getTax());
                subscriptionObject.setMainCredits(packageObject.getMainCredits());
                subscriptionObject.setFlatPrice(packageObject.getFlatPrice());
                subscriptionObject.setPackageDescription(packageObject.getPackageDescription());
                int days = 0;
                int multiple = 1;
                if (packageObject.getBucketDuration().equalsIgnoreCase("daily")) {
                    days = 1;
                } else if (packageObject.getBucketDuration().equalsIgnoreCase("weekly")) {
                    days = 7;
                } else if (packageObject.getBucketDuration().equalsIgnoreCase("biMonthly")) {
                    days = 15;
                } else if (packageObject.getBucketDuration().equalsIgnoreCase("monthly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodMonthly);
                    }
                    days = billingDays;
                    multiple = Integer.parseInt(LoadSettings.g_sSettings.getProperty("first.time.discount.mothly"));
                } else if (packageObject.getBucketDuration().equalsIgnoreCase("quarterly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodQuaterly);
                    }
                    days = billingDays;
                } else if (packageObject.getBucketDuration().equalsIgnoreCase("halfYearly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodHalfYearly);
                    }
                    days = billingDays;
                } else if (packageObject.getBucketDuration().equalsIgnoreCase("yearly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodYearly);
                    }
                    days = billingDays;
                    multiple = Integer.parseInt(LoadSettings.g_sSettings.getProperty("first.time.discount.yearly"));
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DATE, days);
                subscriptionObject.setExpiryDateNTime(calendar.getTime());
                retValue = new PackageSubscriptionManagement().CreateSubscriptionDetails(SessionId, channelId, subscriptionObject);
                // addtion of subscription package details end
                if (retValue > 0) {
                    // assign a credit to developer
                    info = new CreditManagement().getDetails(subscriptionObject.getPartnerId());
                    if (info != null) {
                        info.setStatus(GlobalStatus.UPDATED);
                        info.setUpdatedFreeCredits(subscriptionObject.getFreeCredits());
                        info.setUpdatedMainCredits(info.getUpdatedMainCredits() + subscriptionObject.getMainCredits());
                        info.setApiRates(subscriptionObject.getApRateDetails());
                        new CreditManagement().updateDetails(info);
                    } else {
                        info = new SgCreditInfo();
                        info.setStatus(GlobalStatus.SUCCESS);
                        info.setApiRates(subscriptionObject.getApRateDetails());
                        info.setFreeCredit(subscriptionObject.getFreeCredits());
                        info.setMainCredit(subscriptionObject.getMainCredits() * multiple);
                        info.setPartnerId(subscriptionObject.getPartnerId());
                        new CreditManagement().addDetails(info);
                    }
                    // credit assignment end

                    // add payment details
                    SgPaymentdetails paymentObj = new SgPaymentdetails();
                    
                    if (changeOnPackageCharge != null && !changeOnPackageCharge.isEmpty()) {
                        paymentObj.setChangeOnPackageCharge(Float.parseFloat(changeOnPackageCharge));
                        changePackageCharge = Double.parseDouble(changeOnPackageCharge);
                    }
                    if (_reactivationCharge != null && !_reactivationCharge.isEmpty()) {
                        paymentObj.setReactivationPackageCharge(Float.parseFloat(_reactivationCharge));
                        subscribedreactivationCharge = Double.parseDouble(_reactivationCharge);
                    }
                    if (_cancellationCharge != null && !_cancellationCharge.isEmpty()) {
                        paymentObj.setCancellationCharge(Float.parseFloat(_cancellationCharge));
                        subscribedcancellationCharge = Double.parseDouble(_cancellationCharge);
                    }
                    if (_latePenaltyCharge != null && !_latePenaltyCharge.isEmpty()) {
                        paymentObj.setLatePenaltyCharge(Float.parseFloat(_latePenaltyCharge));
                        subscribedlatePenaltyCharge = Double.parseDouble(_latePenaltyCharge);
                    }
                    packageAmount = Float.valueOf(packageObject.getPlanAmount()).doubleValue();
                    paymentObj.setInvoiceNo(invoiceId);
                    paymentObj.setPaidOn(new Date());
                    paymentObj.setPaidamount(pAmount);
                    paymentObj.setSubscriptionId(subscriptionObject.getBucketId());
                    paymentObj.setPartnerId(partnerObj.getPartnerId());
                    retValue = new PaymentManagement().createPaymentDetails(SessionId, channelId, paymentObj);
                    if (retValue > 0) {
                        subscriptionObject.setStatus(GlobalStatus.PAID);
                        new PackageSubscriptionManagement().updateDetails(subscriptionObject);
                    }
                    // payment detils end
                    if (userObj != null && userObj.getFirstLoginFlag() == GlobalStatus.FIRSTTIMELOGIN) {
                        userObj.setFirstLoginFlag(GlobalStatus.FIRSTTIMELOGINDONE);
                        new UsersManagement().editSgUsers(SessionId, channelId, userObj);
                    }                    
                    
                    JSONObject reqJSONObj = null;
                    String securityAlert = packageObject.getSecurityAndAlertDetails();
                    if (securityAlert != null) {
                        JSONArray alertJson = new JSONArray(securityAlert);
                        for (int i = 0; i < alertJson.length(); i++) {
                            JSONObject jsonexists1 = alertJson.getJSONObject(i);
                            if (jsonexists1.has(packageObject.getBucketName())) {
                                reqJSONObj = jsonexists1.getJSONObject(packageObject.getBucketName());
                                if (reqJSONObj != null) {
                                    break;
                                }
                            }
                        }
                    }
                    //String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.payment");
//                    String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry");
//                    String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question");
//                    String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea");
//                    String[] enquiryEmailDetails = enquiryId.split(":");
//                    String[] supportEmailDetails = supportId.split(":");
//                    String[] ideaEmailDetails = ideaId.split(":");
//                    String pdfPassword = "";
                    
//                    MSConfig config = (MSConfig) new SettingsManagement().getSetting(SessionId, channelId, SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
//                    String schemes = "http";
//                    if (config != null) {
//                        if (config.apssl.equalsIgnoreCase("yes")) {
//                            schemes = "https";
//                        }
//                    }
//                    String path = request.getContextPath();
//                    String dashboardURL = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path;
//                    String apihref = schemes + "://" + config.aphostIp + ":" + config.aphostPort + "/APIDoc";
//                    String greenbackGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/greenback.gif";
//                    String spadeGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/spade.gif";
//                    String addressbookGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/addressbook.gif";
//                    String penpaperGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/penpaper.gif";
//                    String lightbulbGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/lightbulb.gif";
//                    String bucketName = packageName.toLowerCase();
//                    if (bucketName.contains("basic") && bucketName.contains("month")) {
//                        packageName = "Basic";
//                    } else if (bucketName.contains("basic") && bucketName.contains("year")) {
//                        packageName = "Basic";
//                    } else if (bucketName.contains("student") && bucketName.contains("month")) {
//                        packageName = "Student";
//                    } else if (bucketName.contains("student") && bucketName.contains("year")) {
//                        packageName = "Student";
//                    } else if (bucketName.contains("standard") && bucketName.contains("month")) {
//                        packageName = "Standard";
//                    } else if (bucketName.contains("standard") && bucketName.contains("year")) {
//                        packageName = "Standard";
//                    } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
//                        packageName = "Enterprise";
//                    } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
//                        packageName = "Enterprise";
//                    }
//                    if (tmessage != null) {
//                        tmessage = tmessage.replaceAll("#partnerName#", partnerObj.getPartnerName());
//                        tmessage = tmessage.replaceAll("#channel#", channelName);
//                        tmessage = tmessage.replaceAll("#datetime#", UtilityFunctions.getTMReqDate(d));
//                        tmessage = tmessage.replaceAll("#package#", packageName);
//                        tmessage = tmessage.replaceAll("#invoiceid#", invoiceId);
//                        tmessage = tmessage.replaceAll("#paidAmount#", String.valueOf(df.format(pAmount)));
//                        tmessage = tmessage.replaceAll("#paymentDate#", UtilityFunctions.getTMReqDate(new Date()));
//                        tmessage = tmessage.replaceAll("#email#", partnerObj.getPartnerEmailid());
//
//                        tmessage = tmessage.replaceAll("#apihref#", apihref);
//                        tmessage = tmessage.replaceAll("#dashboardhref#", dashboardURL);
//                        tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
//                        tmessage = tmessage.replaceAll("#supportId#", supportId);
//                        tmessage = tmessage.replaceAll("#ideaId#", ideaId);
//                        tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
//                        tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
//                        tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);
//                        tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
//                        tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
//                        tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
//                        tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
//                        tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);
//                    }
                    String appurl = (request.getRequestURL().toString());
                    URL myAppUrl = new URL(appurl);
                    int port = myAppUrl.getPort();
                    if (myAppUrl.getProtocol().equals("https") && port == -1) {
                        port = 443;
                    } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
                        port = 80;
                    }

                    String contextPath = request.getContextPath();
//                    String pdfSaveURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + contextPath + "/";
//                    String signImageURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + contextPath + "/";
//                    String pdfSigImg = LoadSettings.pdfSavePath;
                    String mimeType[] = {"application/octet-stream"};
                    String invoiceFilePath = new PDFInvoiceManagement().createReadyAPIInvoice(null, changePackageCharge, subscribedserviceCharge, subscribedreactivationCharge, subscribedcancellationCharge, subscribedlatePenaltyCharge, packageAmount, partnerObj, null, invoiceId, subscriptionObject, null, null, null);
                    //String invoiceFilePath = new PDFInvoiceManagement().htmlToPDF(htmlSourceCode, partnerObj,subscriptionObject, invoiceId);
                    
                    String[] arrInvoicePath = {invoiceFilePath};
                    Path saveInvoicepath = Paths.get(invoiceFilePath, new String[0]);
                    byte[] invoiceByteArray = Files.readAllBytes(saveInvoicepath);

                    SgSapReceiptFile invoiceSave = new SAPBillManagement().getSapReceiptByPartnerIdSubscribeId(partnerObj.getPartnerId(), subscriptionObject.getBucketId());
                    if (invoiceSave != null) {
                        invoiceSave.setDocuments(invoiceByteArray);
                        invoiceSave.setPartnerId(partnerObj.getPartnerId());
                        invoiceSave.setSubscriptionId(subscriptionObject.getBucketId());
                        invoiceSave.setUpdatedOn(new Date());
                        new SAPBillManagement().updateSapReceiptFileDetail(SessionId, invoiceSave);
                    } else {
                        invoiceSave = new SgSapReceiptFile();
                        invoiceSave.setDocuments(invoiceByteArray);
                        invoiceSave.setPartnerId(partnerObj.getPartnerId());
                        invoiceSave.setSubscriptionId(subscriptionObject.getBucketId());
                        invoiceSave.setCreatedOn(new Date());
                        new SAPBillManagement().addSAPBillDetails(invoiceSave);
                    }

//                    SendPaymentNotification signupNotification = new SendPaymentNotification(tmessage, channelId, SessionId, partnerObj.getPartnerEmailid(), null, arrInvoicePath, invoiceFilePath);
//                    Thread signupNotificationThread = new Thread(signupNotification);
//                    signupNotificationThread.start();

                    File deleteFile = new File(invoiceFilePath);
                    deleteFile.delete();
                    json.put("result", result);
                    json.put("message", message);                    
//                new UpdateReneval().runPerRenevalSchedular();
                } else {
                    result = "error";
                    message = "Package subscription failed.";
                    json.put("result", result);
                    logger.debug("Response of #SubscribePackage from #PPortal Servlet's Parameter  result is " + result);
                    json.put("message", message);
                    logger.debug("Response of #SubscribePackage from #PPortal Servlet's Parameter  message is " + message);
                }
                out.print(json);
                out.flush();
                out.close();
                return;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                logger.info("Response of #SubscribePackage from #PPortal " + json.toString());
                logger.info("Response of #SubscribePackage from #PPortal Servlet at " + new Date());
                out.print(json);
                out.flush();
            }
        }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
