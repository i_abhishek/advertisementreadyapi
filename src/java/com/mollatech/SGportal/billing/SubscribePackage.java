package com.mollatech.SGportal.billing;

import blue.bricks.documentsigner.AxiomPdfResponse;
import blue.bricks.documentsigner.AxiomSignerResponse;
import com.mollatech.pdfsigning.AxiomPDFSignConnector;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.AdToDeveloperUtils;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MSConfig;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdCreditInfo;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiseSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiserAdDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdvertisertracking;
import com.mollatech.serviceguard.nucleus.db.SgCreditInfo;
import com.mollatech.serviceguard.nucleus.db.SgPaymentdetails;
import com.mollatech.serviceguard.nucleus.db.SgPromocode;
import com.mollatech.serviceguard.nucleus.db.SgPromocodeUsage;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgSapReceiptFile;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertisementTrackingManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserAdManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserCreditManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PDFInvoiceManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeUsageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SAPBillManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import com.stripe.model.Subscription;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import java.nio.file.Path;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "SubscribePackage", urlPatterns = {"/SubscribePackage"})
public class SubscribePackage extends HttpServlet {

    static final Logger logger = Logger.getLogger(SubscribePackage.class);

    public static final int PENDING = 2;
    public static final int SEND = 0;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #SubscribePackage from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String channelName = "Ready API";
        String result = "success";
        String message = "Package have been subscriped successfully";
        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");

        String _promocodeId = (String) request.getSession().getAttribute("_promocodeId");
        String changeOnPackageCharge = (String) request.getSession().getAttribute("_changeOnPackageCharge");
        String _reactivationCharge = (String) request.getSession().getAttribute("_reactivationCharge");
        String _cancellationCharge = (String) request.getSession().getAttribute("_cancellationCharge");
        String _latePenaltyCharge = (String) request.getSession().getAttribute("_latePenaltyCharge");
        String packageName = (String) request.getSession().getAttribute("_originalPackageName");
        String invoiceId = (String) request.getSession().getAttribute("_invoiceId");
        String paidAmount = (String) request.getSession().getAttribute("_grossAmount");

        String htmlSourceCode = (String) request.getSession().getAttribute("htmlSourceCode");
        DecimalFormat df = new DecimalFormat("#0.00");
        int retValue = -1;
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
        SgCreditInfo info = null;
        float pAmount = 0;
        float loan = 0.0f;
        float loanWithInterest = 0.0f;
        Double packageAmount = null;
        Double changePackageCharge = null;
        Double subscribedserviceCharge = null;
        Double subscribedreactivationCharge = null;
        Double subscribedcancellationCharge = null;
        Double subscribedlatePenaltyCharge = null;
        String billingPeriodMonthly = LoadSettings.g_sSettings.getProperty("billing.period.month");
        String billingPeriodQuaterly = LoadSettings.g_sSettings.getProperty("billing.period.quarterly");
        String billingPeriodHalfYearly = LoadSettings.g_sSettings.getProperty("billing.period.halfYearly");
        String billingPeriodYearly = LoadSettings.g_sSettings.getProperty("billing.period.yearly");
        int productType = 3;
        int billingDays = 0;
        Subscription charge = (Subscription) request.getSession().getAttribute("chargeObj");
        if (charge != null) {
            try {
                if (paidAmount != null) {
                    pAmount = Float.parseFloat(paidAmount);
                }
                SgSubscriptionDetails subscriptionObject = new SgSubscriptionDetails();
                SgSubscriptionDetails packageSubscribed = new PackageSubscriptionManagement().getSubscriptionbyPartnerId(SessionId, channelId, partnerObj.getPartnerId());
                SgUsers userObj = new UsersManagement().getSgUsersByPartnerId(partnerObj.getPartnerId());

                // add the subscription package details of a developer
                System.out.println("packageName" + packageName);
                SgReqbucketdetails packageObject = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, packageName);
                subscriptionObject.setTierRateDetails(packageObject.getTierRateDetails());
                subscriptionObject.setApRateDetails(packageObject.getApRateDetails());
                subscriptionObject.setBucketDuration(packageObject.getBucketDuration());
                subscriptionObject.setBucketName(packageObject.getBucketName());
                subscriptionObject.setCancellationRate(packageObject.getCancellationRate());
                subscriptionObject.setChangePackageRate(packageObject.getChangePackageRate());
                subscriptionObject.setChannelId(packageObject.getChannelId());
                subscriptionObject.setCreationDate(new Date());
                subscriptionObject.setCreatedOn(new Date());
                subscriptionObject.setBucketId(packageObject.getBucketId());
                subscriptionObject.setDaysForFreeTrial(packageObject.getDaysForFreeTrial());
                subscriptionObject.setFeatureList(packageObject.getFeatureList());
                subscriptionObject.setFreeCredits(packageObject.getFreeCredits());
                subscriptionObject.setLatePenaltyRate(packageObject.getLatePenalitesRate());
                subscriptionObject.setMinimumBalance(packageObject.getMinimumBalance());
                subscriptionObject.setPartnerId(partnerObj.getPartnerId());
                subscriptionObject.setPaymentMode(packageObject.getPaymentMode());
                subscriptionObject.setPlanAmount(packageObject.getPlanAmount());
                subscriptionObject.setReActivationCharge(packageObject.getReActivationCharge());
                subscriptionObject.setSecurityAndAlertDetails(packageObject.getSecurityAndAlertDetails());
                subscriptionObject.setServiceCharge(packageObject.getServiceCharge());
                subscriptionObject.setSlabApRateDetails(packageObject.getSlabApRateDetails());
                subscriptionObject.setApiThrottling(packageObject.getApiThrottling());
                subscriptionObject.setStatus(GlobalStatus.UNPAID);
                subscriptionObject.setTax(packageObject.getTax());
                subscriptionObject.setMainCredits(packageObject.getMainCredits());
                subscriptionObject.setFlatPrice(packageObject.getFlatPrice());
                subscriptionObject.setPackageDescription(packageObject.getPackageDescription());
                int days = 0;
                int multiple = 1;
                if (packageObject.getBucketDuration().equalsIgnoreCase("daily")) {
                    days = 1;
                } else if (packageObject.getBucketDuration().equalsIgnoreCase("weekly")) {
                    days = 7;
                } else if (packageObject.getBucketDuration().equalsIgnoreCase("biMonthly")) {
                    days = 15;
                } else if (packageObject.getBucketDuration().equalsIgnoreCase("monthly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodMonthly);
                    }
                    days = billingDays;
                    multiple = Integer.parseInt(LoadSettings.g_sSettings.getProperty("first.time.discount.mothly"));
                } else if (packageObject.getBucketDuration().equalsIgnoreCase("quarterly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodQuaterly);
                    }
                    days = billingDays;
                } else if (packageObject.getBucketDuration().equalsIgnoreCase("halfYearly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodHalfYearly);
                    }
                    days = billingDays;
                } else if (packageObject.getBucketDuration().equalsIgnoreCase("yearly")) {
                    if (billingPeriodMonthly != null && !billingPeriodMonthly.isEmpty()) {
                        billingDays = Integer.parseInt(billingPeriodYearly);
                    }
                    days = billingDays;
                    multiple = Integer.parseInt(LoadSettings.g_sSettings.getProperty("first.time.discount.yearly"));
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DATE, days);
                subscriptionObject.setExpiryDateNTime(calendar.getTime());
                retValue = new PackageSubscriptionManagement().CreateSubscriptionDetails(SessionId, channelId, subscriptionObject);
                // addtion of subscription package details end
                if (retValue > 0) {
                    // assign a credit to developer
                    info = new CreditManagement().getDetails(subscriptionObject.getPartnerId());
                    if (info != null) {
                        info.setStatus(GlobalStatus.UPDATED);
                        info.setUpdatedFreeCredits(subscriptionObject.getFreeCredits());
                        info.setUpdatedMainCredits(info.getUpdatedMainCredits() + subscriptionObject.getMainCredits());
                        info.setApiRates(subscriptionObject.getApRateDetails());
                        new CreditManagement().updateDetails(info);
                    } else {
                        info = new SgCreditInfo();
                        info.setStatus(GlobalStatus.SUCCESS);
                        info.setApiRates(subscriptionObject.getApRateDetails());
                        info.setFreeCredit(subscriptionObject.getFreeCredits());
                        info.setMainCredit(subscriptionObject.getMainCredits() * multiple);
                        info.setPartnerId(subscriptionObject.getPartnerId());
                        new CreditManagement().addDetails(info);
                    }
                    // credit assignment end

                    // add payment details
                    SgPaymentdetails paymentObj = new SgPaymentdetails();
                    if (_promocodeId != null && !_promocodeId.isEmpty() && !_promocodeId.equalsIgnoreCase("null")) {// new change
                        int promoId = Integer.parseInt(_promocodeId);
                        SgPromocode promoObj = new PromocodeManagement().getPromocodeById(SessionId, channelId, promoId);
                        SgPromocodeUsage promoUsageObj = new SgPromocodeUsage();
                        promoUsageObj.setChannelId(channelId);
                        promoUsageObj.setPartnerId(partnerObj.getPartnerId());
                        promoUsageObj.setPromoCodeId(promoId);
                        promoUsageObj.setPromocodeUsedOn(new Date());
                        promoUsageObj.setSubscriptionId(subscriptionObject.getBucketId());      // new change
                        retValue = new PromocodeUsageManagement().addPromoUsageDetails(promoUsageObj);
                        String promocodeDetails = null;
                        if (promoObj.getDiscountType().equalsIgnoreCase("flat")) {
                            promocodeDetails = promoObj.getDiscount() + " ,In price";
                        } else {
                            promocodeDetails = promoObj.getDiscount() + " ,In percentage";
                        }
                        paymentObj.setPromocodeId(promoId);
                        paymentObj.setPromoCodeDetails(promocodeDetails);
                        paymentObj.setPromocodeId(promoId);
                    }
                    if (changeOnPackageCharge != null && !changeOnPackageCharge.isEmpty()) {
                        paymentObj.setChangeOnPackageCharge(Float.parseFloat(changeOnPackageCharge));
                        changePackageCharge = Double.parseDouble(changeOnPackageCharge);
                    }
                    if (_reactivationCharge != null && !_reactivationCharge.isEmpty()) {
                        paymentObj.setReactivationPackageCharge(Float.parseFloat(_reactivationCharge));
                        subscribedreactivationCharge = Double.parseDouble(_reactivationCharge);
                    }
                    if (_cancellationCharge != null && !_cancellationCharge.isEmpty()) {
                        paymentObj.setCancellationCharge(Float.parseFloat(_cancellationCharge));
                        subscribedcancellationCharge = Double.parseDouble(_cancellationCharge);
                    }
                    if (_latePenaltyCharge != null && !_latePenaltyCharge.isEmpty()) {
                        paymentObj.setLatePenaltyCharge(Float.parseFloat(_latePenaltyCharge));
                        subscribedlatePenaltyCharge = Double.parseDouble(_latePenaltyCharge);
                    }
                    packageAmount = Float.valueOf(packageObject.getPlanAmount()).doubleValue();
                    paymentObj.setInvoiceNo(invoiceId);
                    paymentObj.setPaidOn(new Date());
                    paymentObj.setPaidamount(pAmount);
                    paymentObj.setSubscriptionId(subscriptionObject.getBucketId());
                    paymentObj.setPartnerId(partnerObj.getPartnerId());
                    retValue = new PaymentManagement().createPaymentDetails(SessionId, channelId, paymentObj);
                    if (retValue > 0) {
                        subscriptionObject.setStatus(GlobalStatus.PAID);
                        new PackageSubscriptionManagement().updateDetails(subscriptionObject);
                    }
                    // payment detils end
                    if (userObj != null && userObj.getFirstLoginFlag() == GlobalStatus.FIRSTTIMELOGIN) {
                        userObj.setFirstLoginFlag(GlobalStatus.FIRSTTIMELOGINDONE);
                        new UsersManagement().editSgUsers(SessionId, channelId, userObj);
                    }
                    String alertandsecurity = subscriptionObject.getSecurityAndAlertDetails();
                    String alertChangeOnService = null;

                    Operators[] operatorObj = new OperatorsManagement().getAllOperators(channelId);
                    String[] operatorEmail = null;
                    if (operatorObj != null) {
                        operatorEmail = new String[operatorObj.length];
                        for (int i = 0; i < operatorObj.length; i++) {
                            operatorEmail[i] = (String) operatorObj[i].getEmailid();
                        }
                    }

                    if (packageSubscribed != null && !packageSubscribed.getBucketName().contains(subscriptionObject.getBucketName())) {
                        alertChangeOnService = packageSubscribed.getSecurityAndAlertDetails();
                        if (alertChangeOnService != null) {
                            JSONObject reqJSONObj1 = null;
                            String changeOnPackageMessage = null;
                            JSONArray alertJson1 = new JSONArray(alertChangeOnService);
                            for (int i = 0; i < alertJson1.length(); i++) {
                                JSONObject jsonexists1 = alertJson1.getJSONObject(i);
                                if (jsonexists1.has(packageSubscribed.getBucketName())) {
                                    reqJSONObj1 = jsonexists1.getJSONObject(packageSubscribed.getBucketName());
                                    if (reqJSONObj1 != null) {
                                        changeOnPackageMessage = reqJSONObj1.getString("changeOnPackageAlertMesssage");
                                        break;
                                    }
                                }
                            }
                            if (changeOnPackageMessage != null && !changeOnPackageMessage.equals("")) {
                                String cmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.changeOnPackage");
                                if (cmessage != null) {
                                    cmessage = cmessage.replaceAll("#name#", partnerObj.getPartnerName());
                                    cmessage = cmessage.replaceAll("#channel#", channelName);
                                    cmessage = cmessage.replaceAll("#datetime#", sdf.format(d));
                                    cmessage = cmessage.replaceAll("#oldPackage#", packageSubscribed.getBucketName());
                                    cmessage = cmessage.replaceAll("#newPackage#", subscriptionObject.getBucketName());
                                    cmessage = cmessage.replaceAll("#messageBody#", changeOnPackageMessage);
                                    cmessage = cmessage.replaceAll("#email#", partnerObj.getPartnerEmailid());
                                }
                                SGStatus status = new SendNotification().SendEmail(channelId, partnerObj.getPartnerEmailid(), "Package Change  Successfully", cmessage, operatorEmail, null, null, null, productType);
                                if (status.iStatus == PENDING || status.iStatus == SEND) {

                                }
                            }
                        }
                    } else if (packageSubscribed != null && packageSubscribed.getBucketName().contains(subscriptionObject.getBucketName())) {
                        JSONObject reqJSONObj3 = null;
                        String reactivationMessage = null;
                        if (alertandsecurity != null) {
                            JSONArray alertJson = new JSONArray(alertandsecurity);
                            for (int i = 0; i < alertJson.length(); i++) {
                                JSONObject jsonexists1 = alertJson.getJSONObject(i);
                                if (jsonexists1.has(packageName)) {
                                    reqJSONObj3 = jsonexists1.getJSONObject(packageName);
                                    if (reqJSONObj3 != null) {
                                        reactivationMessage = reqJSONObj3.getString("reActivationAlertMesssage");
                                        break;
                                    }
                                }
                            }
                            if (reactivationMessage != null && !reactivationMessage.equals("")) {
                                String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.reactivate");
                                reactivationMessage = reactivationMessage.replaceAll("#package#", packageName);
                                if (tmessage != null) {
                                    tmessage = tmessage.replaceAll("#name#", partnerObj.getPartnerName());
                                    tmessage = tmessage.replaceAll("#channel#", channelName);
                                    tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                                    tmessage = tmessage.replaceAll("#messageBody#", reactivationMessage);
                                    tmessage = tmessage.replaceAll("#email#", partnerObj.getPartnerEmailid());
                                }
                                SGStatus status = new SendNotification().SendEmail(channelId, partnerObj.getPartnerEmailid(), "Reactivate package successfully", tmessage, operatorEmail, null, null, null, productType);
                                if (status.iStatus == PENDING || status.iStatus == SEND) {

                                }
                            }
                        }
                    }

                    JSONObject reqJSONObj = null;
                    String securityAlert = packageObject.getSecurityAndAlertDetails();
                    if (securityAlert != null) {
                        JSONArray alertJson = new JSONArray(securityAlert);
                        for (int i = 0; i < alertJson.length(); i++) {
                            JSONObject jsonexists1 = alertJson.getJSONObject(i);
                            if (jsonexists1.has(packageObject.getBucketName())) {
                                reqJSONObj = jsonexists1.getJSONObject(packageObject.getBucketName());
                                if (reqJSONObj != null) {
                                    break;
                                }
                            }
                        }
                    }
                    String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.payment");
                    String divEmailAdMessage = (String) LoadSettings.g_templateSettings.getProperty("email.ad.extraHTML");
                    String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry");
                    String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question");
                    String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea");
                    String[] enquiryEmailDetails = enquiryId.split(":");
                    String[] supportEmailDetails = supportId.split(":");
                    String[] ideaEmailDetails = ideaId.split(":");
                    String pdfPassword = "";
                    if (reqJSONObj != null && !reqJSONObj.getString("encryptedPDF").equals("enable")) {
                        tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.payment");
                    } else {
                        tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.payment.pdfPasswordProtect");
                        if (partnerObj.getPartnerEmailid().length() >= 6) {
                            pdfPassword = partnerObj.getPartnerEmailid().substring(0, 5);
                            tmessage = tmessage.replaceAll("#passwordProtectNote#", "Attach invoice is password protected. Open it with your First five letter of your registered email id with us.");
                        } else {
                            pdfPassword = partnerObj.getPartnerEmailid();
                            tmessage = tmessage.replaceAll("#passwordProtectNote#", "Attach invoice is password protected. Open it with your email id, which is registered with us.");
                        }
                    }
                    MSConfig config = (MSConfig) new SettingsManagement().getSetting(SessionId, channelId, SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
                    String schemes = "http";
                    if (config != null) {
                        if (config.apssl.equalsIgnoreCase("yes")) {
                            schemes = "https";
                        }
                    }
                    String path = request.getContextPath();
                    String dashboardURL = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path;
                    String apihref = schemes + "://" + config.aphostIp + ":" + config.aphostPort + "/APIDoc";
                    String greenbackGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/greenback.gif";
                    String spadeGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/spade.gif";
                    String addressbookGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/addressbook.gif";
                    String penpaperGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/penpaper.gif";
                    String lightbulbGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/lightbulb.gif";
                    
                    String bucketName = packageName.toLowerCase();
                    if (bucketName.contains("basic") && bucketName.contains("month")) {
                        packageName = "Basic";
                    } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                        packageName = "Basic";
                    } else if (bucketName.contains("student") && bucketName.contains("month")) {
                        packageName = "Student";
                    } else if (bucketName.contains("student") && bucketName.contains("year")) {
                        packageName = "Student";
                    } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                        packageName = "Standard";
                    } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                        packageName = "Standard";
                    } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                        packageName = "Enterprise";
                    } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                        packageName = "Enterprise";
                    }
                    if (tmessage != null) {
                        tmessage = tmessage.replaceAll("#partnerName#", partnerObj.getPartnerName());
                        tmessage = tmessage.replaceAll("#channel#", channelName);
                        tmessage = tmessage.replaceAll("#datetime#", UtilityFunctions.getTMReqDate(d));
                        tmessage = tmessage.replaceAll("#package#", packageName);
                        tmessage = tmessage.replaceAll("#invoiceid#", invoiceId);
                        tmessage = tmessage.replaceAll("#paidAmount#", String.valueOf(df.format(pAmount)));
                        tmessage = tmessage.replaceAll("#paymentDate#", UtilityFunctions.getTMReqDate(new Date()));
                        tmessage = tmessage.replaceAll("#email#", partnerObj.getPartnerEmailid());

                        tmessage = tmessage.replaceAll("#apihref#", apihref);
                        tmessage = tmessage.replaceAll("#dashboardhref#", dashboardURL);
                        tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
                        tmessage = tmessage.replaceAll("#supportId#", supportId);
                        tmessage = tmessage.replaceAll("#ideaId#", ideaId);
                        tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
                        tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
                        tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);
                        tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
                        tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
                        tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
                        tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
                        tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);
                    }
                    String appurl = (request.getRequestURL().toString());
                    URL myAppUrl = new URL(appurl);
                    int port = myAppUrl.getPort();
                    if (myAppUrl.getProtocol().equals("https") && port == -1) {
                        port = 443;
                    } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
                        port = 80;
                    }

                    String contextPath = request.getContextPath();
                    String pdfSaveURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + contextPath + "/";
                    String emailSaveURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + contextPath + "/images/email/";
                    String signImageURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + contextPath + "/";
                    String pdfSigImg = LoadSettings.pdfSavePath;
                    String emailAddSavePath = LoadSettings.emailAddSavePath;
                    String emailAdLogoPath = emailAddSavePath+ "images/email/";
                    String mimeType[] = {"application/octet-stream"};
                    String invoiceFilePath = new PDFInvoiceManagement().createReadyAPIInvoice(null, changePackageCharge, subscribedserviceCharge, subscribedreactivationCharge, subscribedcancellationCharge, subscribedlatePenaltyCharge, packageAmount, partnerObj, null, invoiceId, subscriptionObject, null, null, null);
                    //String invoiceFilePath = new PDFInvoiceManagement().htmlToPDF(htmlSourceCode, partnerObj,subscriptionObject, invoiceId);

                    String pdfSignchannelId = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.channelId");
                    String pdfSignloginId = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.loginId");
                    String pdfSignloginpassword = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.loginPassword");

                    String signInfoUser = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoUser");
                    String signInfoEmail = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoEmail");
                    String pdfSignloginaddress = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoaddress");

                    String signInfoDesignation = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.designation");
                    String signInfoReason = LoadSettings.g_sSettings.getProperty("axiom.pdfsigning.signInfoReason");
                    if (reqJSONObj != null && reqJSONObj.getString("pdfSigning").equals("enable")) {
                        logger.info("#SubscribePackage from #PPortal PDF Signing started ");
                        String sessionID = AxiomPDFSignConnector.openSession(pdfSignchannelId, pdfSignloginId, pdfSignloginpassword);
                        String signImagePath = signImageURL + "Signiture.png";
                        logger.info("#SubscribePackage from #PPortal sessionID pdfSign >> " + sessionID);
                        logger.info("#SubscribePackage from #PPortal pdfSaveURL  >> " + pdfSaveURL);
                        logger.info("#SubscribePackage from #PPortal signImagePath  >> " + signImagePath);
                        System.out.println("pdfSigImg >> " + pdfSigImg);
                        // PDF Signing   
                        File f = new File(invoiceFilePath);
                        String fileName = f.getName();
                        String pdfURLPath = pdfSaveURL + fileName;
                        AxiomSignerResponse serviceResponse = AxiomPDFSignConnector.sendSignatureRequest(sessionID, pdfURLPath, signInfoUser, signInfoEmail, pdfSignloginaddress, signInfoDesignation, signImagePath, null, null, signInfoReason);
                        //AxiomSignerResponse serviceResponse = AxiomPDFSignConnector.sendSignatureRequest(sessionID, "https://www.readyapis.com/invoice1.pdf",signInfoUser,signInfoEmail,pdfSignloginaddress,signInfoDesignation,"https://www.readyapis.com/Signiture.png",null,null,signInfoReason);
                        //String signedPDF = serviceResponse.getBase64SignedPdf();
                        String signedPDFPath = serviceResponse.getSingedUrl();
                        logger.info("#SubscribePackage from #PPortal PDF Signing signedPDFPath " + signedPDFPath);
                        logger.info("#SubscribePackage from #PPortal PDF Signing server response " + serviceResponse.getErrormsg() + " with error code " + serviceResponse.getErrorcode());
//                        if (signedPDF != null) {
//                            byte[] decodePDF = Base64.decodeBase64(signedPDF);
//                            File file = new File(invoiceFilePath);
//                            FileOutputStream fop = new FileOutputStream(file);
//                            fop.write(decodePDF);
//                            fop.flush();
//                            fop.close();
//                            System.out.println("After signed invoiceFilePath >> "+invoiceFilePath);
//                        }   

                        if (signedPDFPath != null && !signedPDFPath.isEmpty()) {
                            File signFile = new File(invoiceFilePath);
                            signFile.delete();
                            if (reqJSONObj != null && reqJSONObj.getString("encryptedPDF").equals("enable")) {
                                invoiceFilePath = downloadFile(signedPDFPath, pdfSigImg, subscriptionObject.getBucketName() + invoiceId);
                            } else {
                                invoiceFilePath = downloadFile(signedPDFPath, pdfSigImg, subscriptionObject.getBucketName() + invoiceId + ".pdf");
                            }
                            logger.info("#SubscribePackage from #PPortal PDF Signing invoiceFilePath >> " + invoiceFilePath);
                        }

                    }
                    if (reqJSONObj != null && reqJSONObj.getString("encryptedPDF").equals("enable")) {
                        logger.info("#SubscribePackage from #PPortal PDF Encryption started");
                        String sessionID = AxiomPDFSignConnector.openSession(pdfSignchannelId, pdfSignloginId, pdfSignloginpassword);
                        logger.info("#SubscribePackage from #PPortal PDF encryption pdfSaveURL" + pdfSaveURL);
                        logger.info("#SubscribePackage from #PPortal PDF encryption sessionID" + sessionID);
                        File f = new File(invoiceFilePath);
                        String fileName = f.getName();
                        String pdfURLPath = pdfSaveURL + fileName;
                        AxiomPdfResponse servicePDFResponse = AxiomPDFSignConnector.encryptOrDecryptPdf(sessionID, 1, null, pdfURLPath, pdfPassword, "12345", true, false, true, true, true, true, true, true, true, true, true);
                        //AxiomPdfResponse servicePDFResponse = AxiomPDFSignConnector.encryptOrDecryptPdf(sessionID, 1, null, "https://www.readyapis.com/invoice1.pdf", pdfPassword, "12345", true, false, true, true, true, true, true, true, true, true, true);
                        logger.info("#SubscribePackage from #PPortal PDF encryption server response " + servicePDFResponse.getErrormsg() + " with error code " + servicePDFResponse.getErrorcode());
                        String pdfResponseURL = servicePDFResponse.getPDFDownloadUrl();
                        logger.info("#SubscribePackage from #PPortal PDF encryption pdfResponseURL" + pdfResponseURL);

                        if (pdfResponseURL != null && !pdfResponseURL.isEmpty()) {
                            File encryFile = new File(invoiceFilePath);
                            encryFile.delete();
                            invoiceFilePath = downloadFile(pdfResponseURL, pdfSigImg, fileName + ".pdf");
                            logger.info("#SubscribePackage from #PPortal PDF encryption encryptionPDF path" + invoiceFilePath);
                        }
                    }
                    String[] arrInvoicePath = {invoiceFilePath};
                    Path saveInvoicepath = Paths.get(invoiceFilePath, new String[0]);
                    byte[] invoiceByteArray = Files.readAllBytes(saveInvoicepath);

                    SgSapReceiptFile invoiceSave = new SAPBillManagement().getSapReceiptByPartnerIdSubscribeId(partnerObj.getPartnerId(), subscriptionObject.getBucketId());
                    if (invoiceSave != null) {
                        invoiceSave.setDocuments(invoiceByteArray);
                        invoiceSave.setPartnerId(partnerObj.getPartnerId());
                        invoiceSave.setSubscriptionId(subscriptionObject.getBucketId());
                        invoiceSave.setUpdatedOn(new Date());
                        new SAPBillManagement().updateSapReceiptFileDetail(SessionId, invoiceSave);
                    } else {
                        invoiceSave = new SgSapReceiptFile();
                        invoiceSave.setDocuments(invoiceByteArray);
                        invoiceSave.setPartnerId(partnerObj.getPartnerId());
                        invoiceSave.setSubscriptionId(subscriptionObject.getBucketId());
                        invoiceSave.setCreatedOn(new Date());
                        new SAPBillManagement().addSAPBillDetails(invoiceSave);
                    }
                    // New changes for advertisement

                        SgAdvertiserAdDetails adDetails = AdToDeveloperUtils.GetEmailAd();
                        boolean adShown = false;
                        String adImagePath = null;
                        if(adDetails != null){
                            SgAdvertiseSubscriptionDetails subscriObject1 = new AdvertiserSubscriptionManagement().getAdvertiserSubscriptionbyAdId(adDetails.getAdvertiserId());                            
                            String emailConf = null;
                            if(subscriObject1 != null){
                                emailConf = subscriObject1.getEmailAdConfiguration();
                                if(emailConf != null){
                                    JSONObject emailjson;                        
                                    emailjson = new JSONObject(emailConf);
                                    String creditDeduction = "0";
                                    if(emailjson.has("emailCreditDeductionPerAd")){
                                        creditDeduction = emailjson.getString("emailCreditDeductionPerAd");
                                    }
                                    SgAdCreditInfo advertiserCredit = new AdvertiserCreditManagement().getDetails(adDetails.getAdvertiserId());
                                    Float credit = advertiserCredit.getMainCredit();
                                    if(credit != 0){
                                    adImagePath = new PDFInvoiceManagement().GenerateImagePath(adDetails,GlobalStatus.EMAIL_AD,emailAdLogoPath);
                                    if(adImagePath != null){                                                                                                                                    
                                        File f1 = new File(adImagePath);
                                        
                                        String imageFileName = f1.getName();
                                        emailAdLogoPath = emailSaveURL + imageFileName;
                                        tmessage = tmessage.replaceAll("#emailAd#", divEmailAdMessage);
                                        tmessage = tmessage.replaceAll("#adLogo#", emailAdLogoPath);
                                        tmessage = tmessage.replaceAll("#adTitle#", adDetails.getEmailAdTitle());
                                        tmessage = tmessage.replaceAll("#emailAdContent#", adDetails.getEmailAdContent());
                                        
                                        Float fCredit = Float.parseFloat(creditDeduction);
                                        SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
                                        adDetails.setEmailAdShownFlag(GlobalStatus.PDF_AD_SHOWN_TODEVELOPER);
                                        new AdvertiserAdManagement().updateDetails(adDetails);
                                        SgAdvertisertracking adTracking = new SgAdvertisertracking();
                                        adTracking.setAdDescription("Email Ad shown at developer sign up");
                                        adTracking.setAdEvent("Developer SignUp");
                                        adTracking.setAdType(GlobalStatus.EMAIL_AD);
                                        adTracking.setAdvertiserId(adDetails.getAdvertiserId());
                                        adTracking.setChannelId(adDetails.getChannelId());
                                        adTracking.setCreationDate(new Date());
                                        adTracking.setPartnerId(userObj.getPartnerid());                                        
                                        adTracking.setSubscriptionId(subscriObject1.getAdSubscriptionId());
                                        adTracking.setCreateDate(sd.format(new Date()));
                                        adTracking.setAdvertiserAdId(adDetails.getAdvertiserAdId());
                                        adTracking.setCreditDeducted(fCredit);
                                        int retValueTracking = new AdvertisementTrackingManagement().addTracking(adTracking);                                                                                                                
                                        Float tempCredit = credit - fCredit;
                                        advertiserCredit.setMainCredit(tempCredit);
                                        new AdvertiserCreditManagement().updateDetails(advertiserCredit);
//                                        File f = new File(adImagePath);
//                                        f.delete();
                                        adShown = true;
                                        }
                                    }                                                
                                }
                            }
                        }
                        if(!adShown){
                             tmessage = tmessage.replaceAll("#emailAd#", "");
                        }
                    SendPaymentNotification signupNotification = new SendPaymentNotification(tmessage, channelId, SessionId, partnerObj.getPartnerEmailid(), null, arrInvoicePath, invoiceFilePath,adImagePath);
                    Thread signupNotificationThread = new Thread(signupNotification);
                    signupNotificationThread.start();

//                File deleteFile = new File(invoiceFilePath);
//                deleteFile.delete();
                    json.put("result", result);
                    json.put("message", message);
                    request.getSession().setAttribute("chargeObj", null);
//                new UpdateReneval().runPerRenevalSchedular();
                } else {
                    result = "error";
                    message = "Package subscription failed.";
                    json.put("result", result);
                    logger.debug("Response of #SubscribePackage from #PPortal Servlet's Parameter  result is " + result);
                    json.put("message", message);
                    logger.debug("Response of #SubscribePackage from #PPortal Servlet's Parameter  message is " + message);
                }
                out.print(json);
                out.flush();
                out.close();
                return;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                logger.info("Response of #SubscribePackage from #PPortal " + json.toString());
                logger.info("Response of #SubscribePackage from #PPortal Servlet at " + new Date());
                out.print(json);
                out.flush();
            }
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    public static String downloadFile(String fileURL, String saveDir, String fileName) throws IOException {
        String saveFilePath = null;
        try {
            URL url = new URL(fileURL);
            int BUFFER_SIZE = 4096;
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            int responseCode = httpConn.getResponseCode();
            // always check HTTP response code first
            if (responseCode == HttpURLConnection.HTTP_OK) {
                //String fileName = "";
                String disposition = httpConn.getHeaderField("Content-Disposition");
                String contentType = httpConn.getContentType();
                int contentLength = httpConn.getContentLength();

//            if (disposition != null) {
//                // extracts file name from header field
//                int index = disposition.indexOf("filename=");
//                if (index > 0) {
//                    fileName = disposition.substring(index + 10,
//                            disposition.length() - 1);
//                }
//            } else {
//                // extracts file name from URL
//                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
//                        fileURL.length());
//            }
                System.out.println("Content-Type = " + contentType);
                System.out.println("Content-Disposition = " + disposition);
                System.out.println("Content-Length = " + contentLength);
                System.out.println("fileName = " + fileName);

                // opens input stream from the HTTP connection
                InputStream inputStream = httpConn.getInputStream();
                saveFilePath = saveDir + fileName;

                // opens an output stream to save into file
                FileOutputStream outputStream = new FileOutputStream(saveFilePath);

                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
                outputStream.close();
                inputStream.close();
                System.out.println("File downloaded");

            } else {
                System.out.println("No file to download. Server replied HTTP code: " + responseCode);
                return null;
            }
            httpConn.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return saveFilePath;
    }
}

class SendPaymentNotification implements Runnable {

    String email;
    String tmessage;
    String channels;
    String sessionId;
    String[] operatorEmail;
    String[] arrInvoicePath;
    String invoiceFilePath;
    String emailAdImage;
    public static final int PENDING = 2;
    public static final int SEND = 0;

    public SendPaymentNotification() {

    }

    public SendPaymentNotification(String message, String channel, String sessionId, String email, String[] operatorEmail, String[] arrInvoicePath, String invoiceFilePath, String emailAdImage) {
        this.email = email;
        this.tmessage = message;
        this.channels = channel;
        this.sessionId = sessionId;
        this.operatorEmail = operatorEmail;
        this.arrInvoicePath = arrInvoicePath;
        this.invoiceFilePath = invoiceFilePath;
        this.emailAdImage = emailAdImage;
    }
    String mimeType[] = {"application/octet-stream"};
    int productType = 3;

    public void run() {
        SGStatus status = new SendNotification().SendEmail(channels, email, "Package Payment has been made successfully", tmessage, operatorEmail, null, arrInvoicePath, mimeType, productType);
        if (status.iStatus == PENDING || status.iStatus == SEND) {
            System.out.println("Package Payment Email sent successfully with retCode " + status.iStatus + " to" + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
        } else {
            System.out.println("Package Payment Email failed to sent with retCode " + status.iStatus + " to " + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
        }
        File deleteFile = new File(invoiceFilePath);
        deleteFile.delete();
        if(emailAdImage != null){
            File deleteAdImage = new File(emailAdImage);
            deleteAdImage.delete();
        }
    }
}
