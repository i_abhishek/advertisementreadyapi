/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.SGportal.reports.ApiDashboard;
import com.mollatech.SortMapByValue;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.ResourceDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestTrackingManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "TopFiveServices", urlPatterns = {"/TopFiveServices"})
public class TopFiveServices extends HttpServlet {

    int ActiveStatus = 0;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Map<String, Long> map = new ConcurrentHashMap<>();
        ArrayList<ApiDashboard> sample = new ArrayList<ApiDashboard>();
        boolean allEmpty = false;
        try {
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
            Integer partnerId = (Integer) request.getSession().getAttribute("_partnerID");
            ResourceManagement resourseObj = new ResourceManagement();
            ResourceDetails[] resoursedetails = resourseObj.getAllResources();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date endDate = new Date();
            endDate.setHours(23);
            endDate.setMinutes(59);
            endDate.setSeconds(59);
            Calendar cal = Calendar.getInstance();
            Date froDate1 = new Date();
            froDate1.setHours(00);
            froDate1.setMinutes(00);
            froDate1.setSeconds(00);
            PartnerManagement part = new PartnerManagement();
            PartnerDetails[] partdetails = part.getAllPartnerDetails(SessionId, ChannelId);
            RequestTrackingManagement requesTr = new RequestTrackingManagement();
            List<Integer> partList = new ArrayList();
            Map<Integer, String> resourceMap = new HashMap();
            Map<String, Long> partnerMap, sortedMap;
            Map<String, Long> tempPartnerMap = new ConcurrentHashMap<String, Long>();

            if (resoursedetails != null) {
                for (PartnerDetails pdetails : partdetails) {
                    if (!Objects.equals(pdetails.getPartnerId(), partnerId)) {
                        partList.add(pdetails.getPartnerId());
                    }
                }

                for (ResourceDetails resData : resoursedetails) {
                    resourceMap.put(resData.getResourceId(), resData.getName());
                    map.put(resData.getName(), 0l);
                    tempPartnerMap.put(resData.getName(), 0l);
                }
                List<Object[]> objects = requesTr.getCountUsingHQL(partnerId, froDate1, endDate, false);
                if (objects != null) {
                    int count = 0;
                    for (Object[] os : objects) {
                        if (count < 5) {
                            String resName = resourceMap.get(os[1]);
                            if (map.get(resName) < (Long) os[0]) {
                                map.put(resName, (Long) os[0]);
//                                count++;
                            }
                        } else {
                            break;
                        }
                    }
                }
                if (map.isEmpty()) {
                    int count = 0;
                    for (int key : resourceMap.keySet()) {
                        if (count < 5) {
                            map.put(resourceMap.get(key), 0l);
//                            count++;
                        } else {
                            break;
                        }
                    }
                }
                map = SortMapByValue.sortByComparator(map, SortMapByValue.DESC);
                List<Object[]> partnerCount = requesTr.getCountUsingHQL(partnerId, froDate1, endDate, true);
                if (partnerCount != null) {
                    int count = 0;
                    for (Object[] os : partnerCount) {
                        if (count < 5) {
                            String resName = resourceMap.get(os[1]);
                            if (tempPartnerMap.get(resName) < (Long) os[0]) {
                                tempPartnerMap.put(resName, (Long) os[0]);
//                                count++;
                            }
                        } else {
                            break;
                        }
                    }
                }
                if (tempPartnerMap.isEmpty()) {
                    int count = 0;
                    for (int key : resourceMap.keySet()) {
                        if (count < 5) {
                            tempPartnerMap.put(resourceMap.get(key), 0l);
//                            count++;
                        } else {
                            break;
                        }
                    }
                }
                sortedMap = SortMapByValue.sortByComparator(tempPartnerMap, SortMapByValue.DESC);

                Map<String, Long> TopFive = new HashMap();
                for (String key : map.keySet()) {
                    if (sortedMap.get(key) == null || map.get(key) >= sortedMap.get(key)) {
                        TopFive.put(key, map.get(key));
                    } else {
                        TopFive.put(key, sortedMap.get(key));
                    }
                }
                TopFive = SortMapByValue.sortByComparator(TopFive, SortMapByValue.DESC);
                int i = 0;

                for (Map.Entry<String, Long> entrySet
                        : TopFive.entrySet()) {
                    if (i >= 5) {
                        break;
                    }
                    String apiname = entrySet.getKey();
                    sample.add(new ApiDashboard(apiname, map.get(apiname), (sortedMap.get(apiname) == null) ? 0l : sortedMap.get(apiname)));
                    i++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(sample, new TypeToken<List<ApiDashboard>>() {
        }.getType());
        JsonArray jsonArray = element.getAsJsonArray();
        out.print(jsonArray);

        out.flush();
        out.close();
        return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ParseException ex) {
            Logger.getLogger(TopFiveServices.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ParseException ex) {
            Logger.getLogger(TopFiveServices.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
}
