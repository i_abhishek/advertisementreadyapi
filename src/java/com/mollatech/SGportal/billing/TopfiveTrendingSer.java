/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.billing;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.SGportal.reports.ApiDashboard;
import com.mollatech.serviceguard.nucleus.db.ResourceDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestTrackingManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author bluebricks
 */
@WebServlet(name = "TopfiveTrendingSer", urlPatterns = {"/TopfiveTrendingSer"})
public class TopfiveTrendingSer extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        int ActiveStatus = 0;
        Map<String, Long> map = new HashMap<String, Long>();
        ArrayList<ApiDashboard> sample = new ArrayList<ApiDashboard>();
        try {
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
            Integer partnerId = (Integer) request.getSession().getAttribute("_partnerID");
            ResourceManagement resourseObj = new ResourceManagement();
            ResourceDetails[] resoursedetails = resourseObj.getAllResources();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date endDate = new Date();
            endDate.setHours(23);
            endDate.setMinutes(59);
            endDate.setSeconds(59);
            Date startDate = new Date();
            startDate.setHours(00);
            startDate.setMinutes(00);
            startDate.setSeconds(00);

            RequestTrackingManagement requesTr = new RequestTrackingManagement();
            Map<Integer, String> resourceMap = new HashMap();
            if (resoursedetails != null) {
                for (ResourceDetails resData : resoursedetails) {
                    resourceMap.put(resData.getResourceId(), resData.getName());
                    map.put(resData.getName(), 0l);
                }
            }
            List<Object[]> partnerCount = requesTr.getTrendingCountUsingHQL(partnerId, startDate, endDate, true);
            if (partnerCount != null) {
                int count = 0;
                for (Object[] os : partnerCount) {
                    if (count < 5) {
                        String resName = resourceMap.get(os[1]);
                        if ((Long) os[0] > map.get(resName)) {
                            map.put(resName, (Long) os[0]);
                        }
                    } else {
                        break;
                    }
                }
            }
            if (map.isEmpty()) {
                int count = 0;
                for (int key : resourceMap.keySet()) {
                    if (count < 5) {
                        map.put(resourceMap.get(key), 0l);
                        count++;
                    } else {
                        break;
                    }
                }
            }
            Object[] a = map.entrySet().toArray();
            Arrays.sort(a, new Comparator() {
                public int compare(Object o1, Object o2) {
                    return ((Map.Entry<String, Long>) o2).getValue()
                            .compareTo(((Map.Entry<String, Long>) o1).getValue());
                }
            });
            for (int i = 0; i < 5; i++) {
                String apiname = ((Map.Entry<String, Long>) a[i]).getKey();
                long apicount = ((Map.Entry<String, Long>) a[i]).getValue();
                sample.add(new ApiDashboard(apiname, apicount));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(sample, new TypeToken<List<ApiDashboard>>() {
        }.getType());
        JsonArray jsonArray = element.getAsJsonArray();
        System.out.println("JSON Array 5:: " + jsonArray);
        out.print(jsonArray);
        out.flush();
        out.close();
        return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
