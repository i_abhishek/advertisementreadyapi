//package com.mollatech.SGportal.billing;
//
//import com.mollatech.service.nucleus.crypto.LoadSettings;
//import com.mollatech.xpay.mockpurchase.AllTrustManager;
//import com.mollatech.xpay.mockpurchase.AllVerifier;
//import com.mollatech.xpay.pathmazing.XpayTransaction;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.net.URL;
//import java.security.KeyManagementException;
//import java.security.NoSuchAlgorithmException;
//import java.util.Date;
//import javax.net.ssl.HttpsURLConnection;
//import javax.net.ssl.SSLContext;
//import javax.net.ssl.TrustManager;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import org.json.JSONObject;
//
///**
// *
// * @author mohanish
// */
//@WebServlet(name = "XpayPayment", urlPatterns = {"/XpayPayment"})
//public class XpayPayment extends HttpServlet {
//
//    /**
//     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
//     * methods.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    private static SSLContext sslContext = null;
//
//    static {
//
//        try {
//            HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
//
//            try {
//                sslContext = SSLContext.getInstance("TLS");
//            } catch (NoSuchAlgorithmException ex) {
//                //Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
//                ex.printStackTrace();
//            }
//            sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
//            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
//
//        } catch (KeyManagementException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }
//
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        JSONObject json = new JSONObject();
//        String result = "success";
//        String message = "Xapy opensession call sucessfully!!!";
//        PrintWriter out = response.getWriter();
//        String packageName = request.getParameter("_packageName");
//        String _originalPackageName = request.getParameter("originalPackageN");
//        String _invoiceId = request.getParameter("_invoiceId");
//        String _totalPaid = request.getParameter("totalBenefitAmount");
//        String _promocodeId = request.getParameter("_promocodeId");
//        String _changeOnPackageCharge = request.getParameter("_changeOnPackageCharge");
//        String _reactivationCharge = request.getParameter("_reactivationCharge");
//        String _cancellationCharge = request.getParameter("_cancellationCharge");
//        String _latePenaltyCharge = request.getParameter("_latePenaltyCharge");
//        
//        // gst, vat, servicetax 
//        String _totalAmountWithoutTax = request.getParameter("_totalAmountWithoutTax");
//        String _gstTax = request.getParameter("_gstTax");
//        String _vatTax = request.getParameter("_vatTax");
//        String _serviceTax = request.getParameter("_serviceTax");
//        
//        int quantity = Integer.parseInt("1");
//        String currency = "USD";
//        String invoiceid = "0";
//        double totalPaid = 0;
//        if (_totalPaid != null && !_totalPaid.isEmpty()) {
//            totalPaid = Double.parseDouble(_totalPaid);
//        }
//        if (_invoiceId != null && !_invoiceId.isEmpty()) {
//            invoiceid = _invoiceId;
//        }
//        if (_changeOnPackageCharge == null) {
//            _changeOnPackageCharge = "";
//        }
//        if (_reactivationCharge == null) {
//            _reactivationCharge = "";
//        }
//        if (_cancellationCharge == null) {
//            _cancellationCharge = "";
//        }
//        if (_latePenaltyCharge == null) {
//            _latePenaltyCharge = "";
//        }
//        String Merchantid = LoadSettings.g_sSettings.getProperty("mock.purchase.merchantID");
//        String Merchantname = LoadSettings.g_sSettings.getProperty("mock.merchant.name");
//        String ChannelId2 = LoadSettings.g_sSettings.getProperty("mock.purchase.channelid");
//        String Secretkey = LoadSettings.g_sSettings.getProperty("mock.purchase.signature");
//        String loginid = LoadSettings.g_sSettings.getProperty("mock.purchase.loginId");
//        String loginpw = LoadSettings.g_sSettings.getProperty("mock.purchase.password");
//        String secure = LoadSettings.g_sSettings.getProperty("mock.purchase.secure");
//        String Xpay_port = LoadSettings.g_sSettings.getProperty("mock.purchase.port");
//        String Xpay_ip = LoadSettings.g_sSettings.getProperty("mock.purchase.ip");
//        String strXPAYPaymentPage = null;
//        URL xpayURL = null;
//        if (secure.equalsIgnoreCase("yes")) {
//            strXPAYPaymentPage = "https://" + Xpay_ip + ":" + Xpay_port;
//        } else {
//            strXPAYPaymentPage = "http://" + Xpay_ip + ":" + Xpay_port;
//        }
//        xpayURL = new URL(strXPAYPaymentPage + "/COOMart/XPAYConnectorServiceInterfaceImpl?wsdl");
//        strXPAYPaymentPage += "/" + Merchantname + "/paymentPage.jsp";
//        String url = (request.getRequestURL().toString());
//        URL myUrl = new URL(url);
//        String intiURL = myUrl.getProtocol() + "://" + myUrl.getHost() + ":" + myUrl.getPort() + "/PPortal/";
//        String successUrlToReturn = intiURL + "SuccessURL.jsp?xpayType=text&_promocodeId=" + _promocodeId + "&_changeOnPackageCharge=" + _changeOnPackageCharge + "&_reactivationCharge=" + _reactivationCharge + "&_cancellationCharge=" + _cancellationCharge + "&_latePenaltyCharge=" + _latePenaltyCharge + "&_originalPackageName=" + _originalPackageName + "&_totalAmountWithoutTax=" + _totalAmountWithoutTax+ "&_gstTax=" + _gstTax+ "&_vatTax=" + _vatTax+ "&_serviceTax=" + _serviceTax;
//        String errorUrl = intiURL + "ErrorURL.jsp?xpayType=text";
//        String transactionID = "1467879015058";
//        String expirytime = "5";
//        String description = packageName + " " ;
//        Date date = new Date();
//        long dt = date.getTime();
//        try {
//            double roundedTotalPaid = (double) Math.round(totalPaid * 100) / 100;
//            com.mollatech.xpay.connector.XPAYConnectorServiceInterfaceImplService service = new com.mollatech.xpay.connector.XPAYConnectorServiceInterfaceImplService(xpayURL);
//            com.mollatech.xpay.connector.Xpayservices port = service.getXPAYConnectorServiceInterfaceImplPort();
//            java.lang.String channelId = ChannelId2;
//            java.lang.String loginId = loginid;
//            java.lang.String password = loginpw;
//            java.lang.String merchantID = Merchantid;
//            java.lang.String signature = Secretkey;
//            java.lang.String integrityCheckString = null;
//            com.mollatech.xpay.pathmazing.XpayTransaction xpayTransaction = new XpayTransaction();
//            xpayTransaction.setExpiryTime(Integer.parseInt(expirytime));
//            xpayTransaction.setIpAddress(myUrl.getHost());
//            xpayTransaction.setTxid(transactionID);
//            xpayTransaction.setQuantity(quantity);
//            xpayTransaction.setPurchaseDate(dt);
//            xpayTransaction.setPurchaseDesc(description.trim());
//            xpayTransaction.setPurchaseCurrency(currency);
//            xpayTransaction.setPurchaseAmount(roundedTotalPaid);
//            xpayTransaction.setPaymentTokenid("");
//            xpayTransaction.setItem("1");
//            xpayTransaction.setInvoiceid(invoiceid);
//            xpayTransaction.setInitialUrl(intiURL);
//            com.mollatech.xpay.pathmazing.XpayResponse openSessionresult = port.openSessionV2(channelId, loginId, password, merchantID, signature, null, xpayTransaction);
//            if (openSessionresult != null) {
//                String sessionid = openSessionresult.getSessionid();
//                request.getSession().setAttribute("sessionId", sessionid);
//                String payTokenid = openSessionresult.getXTran().getPaymentTokenid();
//                json.put("merchantID", merchantID);
//                json.put("sessionid", sessionid);
//                json.put("description", description);
//                json.put("expirytime", expirytime);
//                json.put("payTokenid", payTokenid);
//                json.put("purchaseAmount", roundedTotalPaid);
//                json.put("quantity", quantity);
//                json.put("item", "1");
//                json.put("invoiceid", invoiceid);
//                json.put("currency", currency);
//                json.put("transactionID", transactionID);
//                json.put("successUrlToReturn", successUrlToReturn);
//                json.put("errorUrl", errorUrl);
//                json.put("strXPAYPaymentPage", strXPAYPaymentPage);
//                json.put("result", result);
//                json.put("message", message);
//            } else {
//                json.put("result", "error");
//                json.put("message", "Failed to create xpay open session");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            out.print(json);
//            out.flush();
//            out.close();
//        }
//    }
//
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Handles the HTTP <code>POST</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Returns a short description of the servlet.
//     *
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }
//}
