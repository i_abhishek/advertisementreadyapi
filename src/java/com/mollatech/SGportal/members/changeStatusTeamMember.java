package com.mollatech.SGportal.members;

import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Bluebricks
 */
public class changeStatusTeamMember extends HttpServlet {

    static final int ACTIVE_STATUS = 1;

    static final int SUSPENDED_STATUS = 0;

    static final int PENDING_STATUS = -2;

    public final int PASSWORD = 1;

    public static final int USER_PASSWORD = 1;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Status changed sucessfully";
        PrintWriter out = response.getWriter();
        String memberName = request.getParameter("_userId");
        String memStatus = request.getParameter("_status");
        int imemStatus = 0;
        if (memStatus != null) {
            imemStatus = Integer.parseInt(memStatus);
        }
        int a = -1;
        UsersManagement um = new UsersManagement();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        if (SessionId != null) {
            SgUsers sguser = um.getSgUsersByUserId(SessionId, channelId, memberName);
            if (sguser != null) {
                a = um.ChangeStatus(SessionId, channelId, sguser.getUserid(), imemStatus);
            }
            if (a == 0) {
                result = "success";
                message = "Status changed sucessfully";
            } else if (a == 3) {
                result = "error";
                if (imemStatus == ACTIVE_STATUS) {
                    message = "Status already Active ";
                } else {
                    message = "Status already Suspended  ";
                }
            } else {
                result = "error";
                message = "Status not changed ";
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
