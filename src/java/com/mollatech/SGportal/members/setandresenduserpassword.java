package com.mollatech.SGportal.members;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import static com.mollatech.service.nucleus.crypto.LoadSettings.g_sSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class setandresenduserpassword extends HttpServlet {

    final String itemtype = "USERPASSWORD";

    final String itemTypeAUTH = "AUTHORIZTION";

    public static final int PENDING = 2;

    public static final int SENT = 0;

    public static final int SEND = 0;

    public static final int RESET = 1;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        String channelName = "ServiceGuard Portal";
        String channelid = (String) request.getSession().getAttribute("_ChannelId");
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String _userid = request.getParameter("_userid");
        String result = "success";
        String message = "Password set and send successfully";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        int iapprovalID = -1;
        if (_userid == null) {
            result = "error";
            message = "Fill all Details";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        int retValue = -1;
        UsersManagement um = new UsersManagement();
        SgUsers sguser = um.getSgUsersByUserId(sessionId, channelid, _userid);
        UtilityFunctions u = new UtilityFunctions();
        Date d = new Date();
        String strPassword = u.HexSHA1(sessionId + channelid + _userid + d.toString());
        strPassword = strPassword.substring(0, 9);
        u = null;
        String resultString = "Failure";
        retValue = um.SetPassword(channelid, _userid, strPassword);
        if (retValue != 0) {
            result = "error";
            result = "error";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        if (retValue == 0) {
            resultString = "Success";
        }
        if (retValue == 0) {
        } else {
            result = "error";
            message = "password reset failed";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        strPassword = um.getPassword(sessionId, channelid, _userid);
        int iStatus = -1;
        String strStatus = "Failed";
        int type = RESET;
        String tmessage = (String) LoadSettings.g_sSettings.getProperty("email.partner.password");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
        if (message != null) {
            d = new Date();
            tmessage = tmessage.replaceAll("#name#", sguser.getUsername());
            tmessage = tmessage.replaceAll("#channel#", channelName);
            tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
            tmessage = tmessage.replaceAll("#password#", strPassword);
        }
        if (sguser == null || strPassword == null) {
            result = "error";
            message = "Password Not Sent";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        System.out.println(tmessage);
        int productType = 3;
        SGStatus status = new SendNotification().SendEmail(sessionId, sguser.getEmail(), "RESET PASSWORD", tmessage, null, null, null, null, productType);
        if (status.iStatus == PENDING || status.iStatus == SENT) {
        } else {
            result = "error";
            message = "Password Not Sent";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    public static Object createObject(Constructor constructor, Object[] arguments) {
        Object object = null;
        try {
            object = constructor.newInstance(arguments);
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;
    }
}
