/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.partner;

import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt;
import com.mollatech.serviceguard.nucleus.db.SgPartnerrequest;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Manoj Sherkhane
 */
@WebServlet(name = "EditPartner", urlPatterns = {"/EditPartner"})
public class EditPartner extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Developers details updated successfully";
        PrintWriter out = response.getWriter();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String partnerId = request.getParameter("partnerId");
        String icNo = request.getParameter("icNo");
        String partName = request.getParameter("partnerName");
        String companyName = request.getParameter("companyName");
        String companyRegNo = request.getParameter("companyRegNo");
        String compGstNo = request.getParameter("companyGstNo");
        String compFixed = request.getParameter("companyFixedNo");
        String compAddress = request.getParameter("companyAddress");
        String compState = request.getParameter("companyState");
        String compPostCode = request.getParameter("compnyPcode");
        String compCountry = request.getParameter("companyCountry");
        String billingAddress = request.getParameter("bilingAdd");
        String billingState = request.getParameter("billingState");
        String billingPcode = request.getParameter("bilingPcode");
        String bilingCountry = request.getParameter("bilingCountry");
        try {
            int partid = 0;
            int ic_No = 0;
            if (partnerId != null) {
                partid = Integer.parseInt(partnerId);
            }
            ProductionAccessEnvtManagement prodScEvtObj = new ProductionAccessEnvtManagement();
            SgDeveloperProductionEnvt productionData = prodScEvtObj.getProudDetailsByPartnerId(partid);
            if (icNo != null && !icNo.isEmpty()) {
                ic_No = Integer.parseInt(icNo);
                productionData.setIcNo(icNo);
            }

            productionData.setComapnyName(companyName);
            productionData.setCompanyRegistrationNo(companyRegNo);
            productionData.setGstNo(compGstNo);
            productionData.setFixedLineNumber(compFixed);
            productionData.setAddress(compAddress);
            productionData.setAddrstate(compState);
            productionData.setPostcode(compPostCode);
            productionData.setCountry(compCountry);
            productionData.setBillingAddress(billingAddress);
            productionData.setBillingState(billingState);
            productionData.setBillingPostCode(billingPcode);
            productionData.setBillingCountry(bilingCountry);
            String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
            int prodRes = prodScEvtObj.updateProductionDetail(SessionId, productionData);
            PartnerDetails partDetails = new PartnerManagement().getPartnerDetails(partid);
            partDetails.setPartnerName(partName);
            int res = new PartnerManagement().updateDetails(partDetails);
            SgPartnerrequest partnerReq = new PartnerRequestManagement().getPartnerRequestsPartnerbyId(SessionId, partid);
            partnerReq.setName(partName);
            new PartnerRequestManagement().editPartnerRequest(SessionId, partnerReq);
            SgUsers sp = new UsersManagement().getSgUsersByPartnerId(partnerReq.getPartnerid());
            sp.setUsername(partName);
            new UsersManagement().editSgUsers(SessionId, ChannelId, sp);
            if (res == 0 && prodRes == 0) {
                json.put("_result", result);
                json.put("_message", message);
            } else {
                result = "error";
                message = "Failed to update detail";
                json.put("_result", result);
                json.put("_message", message);
            }
        } catch (NumberFormatException | NullPointerException e) {
            result = "error";
            message = e.getMessage();
            json.put("_result", result);
            json.put("_message", message);
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
