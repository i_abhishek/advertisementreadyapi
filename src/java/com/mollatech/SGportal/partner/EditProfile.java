package com.mollatech.SGportal.partner;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgPartnerrequest;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author Bluebricks
 */
public class EditProfile extends HttpServlet {

    static final Logger logger = Logger.getLogger(EditProfile.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Developer added successfully";
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is EditProfile for PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String pName = request.getParameter("pname");
        logger.debug("value of pName : " + pName);
        String pEmail = request.getParameter("pemail");
        logger.debug("value of pEmail : " + pEmail);
        String pMobileNo = request.getParameter("pmobNo");
        logger.debug("value of pMobileNo : " + pMobileNo);
        String pID = request.getParameter("pID");
        logger.debug("value of pID : " + pID);
        int partnerId = -1;
        if (pID != null) {
            partnerId = Integer.parseInt(pID);
        }
        int pStatus = GlobalStatus.PENDING;   // new change
        String ipAddress = "";
        String iplive = "";
        String[] ipTest = request.getParameterValues("testIpDe");
        logger.debug("value of ipTest : " + ipTest);
        String[] ipLive = request.getParameterValues("liveIpDe");
        logger.debug("value of ipLive : " + ipLive);
        if (ipTest != null) {
            for (int i = 0; i < ipTest.length; i++) {
                ipAddress += ipTest[i] + ",";
            }
        }
        if (ipLive != null) {
            for (int i = 0; i < ipLive.length; i++) {
                iplive += ipLive[i] + ",";
            }
        }
        String pSt = request.getParameter("pStatus");
        logger.debug("value of pStatus : " + pSt);
        if (pSt != null) {
            pStatus = Integer.parseInt(pSt);
        }
        String pWebsite = request.getParameter("pWebsite");
        logger.debug("value of pWebsite : " + pWebsite);
        String pFax = request.getParameter("pFax");
        logger.debug("value of pFax : " + pFax);
        String Landline = request.getParameter("pLandline");
        logger.debug("value of Landline : " + Landline);
        String pCompanyName = request.getParameter("pCompany");
        logger.debug("value of pCompanyName : " + pCompanyName);
        String pAddress = request.getParameter("pAddress");
        logger.debug("value of pAddress : " + pAddress);
        String pPincode = request.getParameter("ppincode");
        logger.debug("value of pPincode : " + pPincode);
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        PartnerDetails pdetails = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        if (!pEmail.matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$")) {
            result = "error";
            message = "Enter valid email address.";
            try {
                json.put("_result", result);
                logger.debug("Response of EditProfile (PPortal) Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of EditProfile (PPortal) Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at EditProfile ", ex);
            }
            out.print(json);
            out.flush();
            return;
        }
        PartnerDetails[] oldObj = new PartnerManagement().getPartnerDetailsByEmail(SessionId, ChannelId, pEmail);
        if (oldObj != null) {
            if (oldObj[0].getStatus() != GlobalStatus.DELETED) {
                if (oldObj.length > 1) {
                    result = "error";
                    message = "Email Id Already In Used.";
                    try {
                        json.put("_result", result);
                        logger.debug("Response of EditProfile (PPortal) Servlet's Parameter  result is " + result);
                        json.put("_message", message);
                        logger.debug("Response of EditProfile (PPortal) Servlet's Parameter  message is " + message);
                    } catch (Exception ex) {
                        logger.error("Exception at EditProfile (PPortal) ", ex);
                    }
                    out.print(json);
                    out.flush();
                    return;
                } else if (oldObj[0].getPartnerId() != partnerId) {
                    result = "error";
                    message = "Email Id Already Used.";
                    try {
                        json.put("_result", result);
                        logger.debug("Response of EditProfile Servlet's Parameter  result is " + result);
                        json.put("_message", message);
                        logger.debug("Response of EditProfile Servlet's Parameter  message is " + message);
                    } catch (Exception ex) {
                        logger.error("Exception at EditProfile ", ex);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            }
        }
        oldObj = new PartnerManagement().getPartnerDetailsByEmail(SessionId, ChannelId, pMobileNo);
        if (oldObj != null) {
            if (oldObj[0].getStatus() != GlobalStatus.DELETED) {
                if (oldObj.length > 1) {
                    result = "error";
                    message = "Mobile Number Already In Used.";
                    try {
                        json.put("_result", result);
                        logger.debug("Response of EditProfile (PPortal) Servlet's Parameter  result is " + result);
                        json.put("_message", message);
                        logger.debug("Response of EditProfile (PPortal) Servlet's Parameter  message is " + message);
                    } catch (Exception ex) {
                        logger.error("Exception at EditProfile (PPortal) ", ex);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            }
        }
        int ps = new PartnerRequestManagement().editPartnerRequest(SessionId, partnerId, pName, pStatus, pMobileNo, pEmail, ipAddress, pWebsite, pFax, Landline, pAddress, pPincode, pCompanyName, iplive);
        logger.debug("value of partnerRequestUpdate : " + ps);
        System.out.println(ps);
        if (ps == 0) {
            if (pdetails != null) {
                int pstatus = pdetails.getStatus();
                String pStatusDe = null;
                if (pstatus == GlobalStatus.ACTIVE) {   // new change
                    pStatusDe = "active";
                } else if (pstatus == GlobalStatus.SUSPEND) {              // new change
                    pStatusDe = "suspended";
                }
                String certStatus = String.valueOf(pdetails.getCertStatus());
                ps = new PartnerManagement().EditPartner(SessionId, ChannelId, pdetails.getPartnerId(), pName, pMobileNo, pEmail, pStatusDe, pdetails.getPartnerGroupId(), ipAddress, certStatus, iplive);
//                SgUsers sp = new UsersManagement().getSgUsersByPartnerId(partnerId);
//                sp.setUsername(pName);
//                new UsersManagement().editSgUsers(SessionId, ChannelId, sp);
            }
            if (ps == 0) {
                result = "success";
                message = "Your details have been updated successfully";
            } else {
                result = "error";
                message = "Failed to update Developer details";
            }
        } else {
            result = "error";
            message = "Failed to update Developer details";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of EditProfile " + json.toString());
            logger.info("Response of EditProfile Servlet at " + new Date());
            out.print(json);
            out.flush();
            out.close();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
