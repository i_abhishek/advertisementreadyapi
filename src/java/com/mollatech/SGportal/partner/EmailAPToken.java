package com.mollatech.SGportal.partner;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "EmailAPToken", urlPatterns = { "/EmailAPToken" })
public class EmailAPToken extends HttpServlet {

    final String itemtype = "USERPASSWORD";

    final String itemTypeAUTH = "AUTHORIZTION";

    public static final int PENDING = 2;

    public static final int SENT = 0;

    public static final int SEND = 0;

    public static final int RESET = 1;

    public static final int completed = 6;

    static final Logger logger = Logger.getLogger(EmailAPToken.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        String channelName = "ServiceGuard Portal";
        String channelid = (String) request.getSession().getAttribute("_ChannelId");
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Token send to your email successfully.";
        String partnerName = request.getParameter("partnerName");
        String partnerEmail = request.getParameter("partnerEmail");
        String token = request.getParameter("apToken");
        String env = request.getParameter("env");
        String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.aplevel.token");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
        if (tmessage != null) {
            Date d   = new Date();
            tmessage = tmessage.replaceAll("#name#", partnerName);
            tmessage = tmessage.replaceAll("#channel#", channelName);
            tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
            tmessage = tmessage.replaceAll("#token#", token);
            tmessage = tmessage.replaceAll("#ENV#", env);
            tmessage = tmessage.replaceAll("#email#", partnerEmail);
        }
        int productType = 3;
        try {
            SGStatus status = new SendNotification().SendEmail(channelid, partnerEmail, "AP token details", tmessage, null, null, null, null, productType);
            if (status.iStatus == PENDING || status.iStatus == SENT) {
                json.put("result", result);
                logger.debug("Response of EmailAPToken from #PPortal Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of EmailAPToken from #PPortal Servlet's Parameter  message is " + message);
            } else {
                result = "error";
                message = "Token details failed to send on email";
                try {
                    json.put("result", result);
                    logger.debug("Response of EmailAPToken from #PPortal Servlet's Parameter  result is " + result);
                    json.put("message", message);
                    logger.debug("Response of EmailAPToken from #PPortal Servlet's Parameter  message is " + message);
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.info("Exception at #EmailAPToken from #PPortal " + e);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Exception at #EmailAPIToken from #PPortal " + e);
        } finally {
            logger.info("Response of #EmailAPIToken from #PPortal " + json.toString());
            logger.info("Response of #EmailAPIToken from #PPortal Servlet at " + new Date());
            out.print(json);
            out.flush();
            out.close();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
