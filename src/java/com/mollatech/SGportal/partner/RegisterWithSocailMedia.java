/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.partner;

import com.itextpdf.text.Image;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.AdToDeveloperUtils;
import com.mollatech.serviceguard.nucleus.commons.AutoAPIKeyAssignment;
import com.mollatech.serviceguard.nucleus.commons.CommonUtility;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MSConfig;
import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.GroupDetails;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdCreditInfo;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiseSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiserAdDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdvertisertracking;
import com.mollatech.serviceguard.nucleus.db.SgPartnerrequest;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.ChannelsUtils;
import com.mollatech.serviceguard.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.serviceguard.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertisementTrackingManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserAdManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserCreditManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PDFInvoiceManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.json.JSONObject;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import java.io.File;

public class RegisterWithSocailMedia extends HttpServlet {

    static final Logger logger = Logger.getLogger(RegisterWithSocailMedia.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("applicaion/json");
        JSONObject json = new JSONObject();
        try {

            String name = request.getParameter("name");
            String email = request.getParameter("email");
            String loggedUsing = request.getParameter("loggedUsing");
            String profileImageUrl = request.getParameter("profileImageUrl");
            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            String _channelName = "ServiceGuard";
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
            Channels channel = cUtil.getChannel(_channelName);
            int pStatus = GlobalStatus.PENDING;
            PartnerDetails[] detailses = new PartnerManagement().getPartnerDetailsByEmail(null, channel.getChannelid(), email);
            boolean activePartner = false;
            SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
            Session sRemoteAcess = suRemoteAcess.openSession();
            RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
            SessionManagement sManagement = new SessionManagement();
            String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
            String SessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
            if (loggedUsing != null && loggedUsing.equalsIgnoreCase("fb")) {
                profileImageUrl = "https://graph.facebook.com/" + profileImageUrl + "/picture?type=normal";
            }
            if (detailses != null) {
                for (int i = 0; i < detailses.length; i++) {
                    PartnerDetails details = detailses[i];

                    if (details.getStatus() == GlobalStatus.ACTIVE) {
                        AssignTokenOperation asignToken = new AssignTokenOperation(details, SessionId);
                        Thread asignTokenThread = new Thread(asignToken);
                        asignTokenThread.start();
                        UsersManagement um = new UsersManagement();
                        SgUsers user = um.getSgUsersByPartnerId(details.getPartnerId());
                        request.getSession(true);
                        request.getSession().setAttribute("_apOprAuth", "yes");
                        request.getSession().setAttribute("_partnerSessionId", SessionId);
                        request.getSession().setAttribute("_ChannelId", channel.getChannelid());
                        request.getSession().setAttribute("_SgUsers", user);
                        request.getSession().setAttribute("_rssuserid", user.getUserid());
                        request.getSession().setAttribute("_partnerDetails", details);
                        request.getSession().setAttribute("_loginType", user.getType());
                        request.getSession().setAttribute("_address", details.getAllowedIps());
                        request.getSession().setAttribute("_addressLive", details.getAllowedIpsForLive());
                        request.getSession().setAttribute("_partnerID", details.getPartnerId());
                        request.getSession().setAttribute("profileImageUrl", profileImageUrl);
                        request.getSession().setAttribute("socialNamed", name);
                        activePartner = true;
                        SgSubscriptionDetails subscriObject1 = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(details.getPartnerId());

                        if (subscriObject1 == null) {
                            json.put("_result", "successWithRedirect");
                            json.put("_message", "You have been successfully sign in and password shared on your mail, Please subscribe package first.");
                        } else {
                            json.put("_result", "success");
                            json.put("_message", "Login Successful.");
                        }
                        PrintWriter out = response.getWriter();
                        out.print(json);
                        return;

                    }
                }
                if (activePartner != true) {
                    json.put("_result", "error");
                    json.put("_message", "Your Account is Not Active.");
                    PrintWriter out = response.getWriter();
                    out.print(json);
                    return;
                }
            } else {
                String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry");
                String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question");
                String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea");
                String[] enquiryEmailDetails = enquiryId.split(":");
                String[] supportEmailDetails = supportId.split(":");
                String[] ideaEmailDetails = ideaId.split(":");
                GroupDetails details = new GroupManagement().checkIsUniqueInGroup(null, channel.getChannelid(), "MPServices");
                if (details == null) {
                    pStatus = GlobalStatus.PENDING;
                }
                String automation = LoadSettings.g_sSettings.getProperty("partner.automation");
                if (automation != null) {
                    if (automation.equalsIgnoreCase("true")) {
                        pStatus = GlobalStatus.ACTIVE;
                    }
                }
                PartnerRequestManagement ppw = new PartnerRequestManagement();
                int poStatus = ppw.addPartnerRequest(null, name, pStatus, "0", email, "*", "https://Example.com", "0", "0", "NA", "0", "Example", null, null, "*", null);
                if (poStatus != -1) {
                    if (pStatus == GlobalStatus.ACTIVE) {
                        final int ADMIN = 0;
                        SgPartnerrequest sgpartner = new PartnerRequestManagement().getPartnerRequestsbyReqId(null, poStatus);
                        String token = sgpartner.getName() + sgpartner.getEmail() + new Date() + null + sgpartner.getPhone();
                        byte[] SHA1hash = CommonUtility.SHA1(token);
                        token = new String(Base64.encode(SHA1hash));
                        int pstatus = new PartnerManagement().CreatePartner(null, channel.getChannelid(), sgpartner.getName(), sgpartner.getEmail(), GlobalStatus.ACTIVE, sgpartner.getPhone(), "" + details.getGroupId(), GlobalStatus.ACTIVE, sgpartner.getIpAddress(), token, sgpartner.getIpLive());
                        PartnerDetails details1 = new PartnerManagement().getPartnerDetails(pstatus);
                        AssignTokenOperation asignToken = new AssignTokenOperation(details1, SessionId);
                        Thread asignTokenThread = new Thread(asignToken);
                        asignTokenThread.start();
                        sgpartner.setPartnerid(pstatus);
                        PartnerDetails partnerDetails = new PartnerManagement().getPartnerDetails(pstatus);
                        new PartnerRequestManagement().editPartnerRequest(null, sgpartner);
                        String developerpassword = token.substring(0, 9);
                        SgUsers sguser = new SgUsers();
                        sguser.setUsername(sgpartner.getName());
                        sguser.setEmail(sgpartner.getEmail());
                        sguser.setPhone(sgpartner.getPhone());
                        sguser.setChannelid(channel.getChannelid());
                        sguser.setAttempts(0);
                        sguser.setPartnerid(pstatus);
                        JSONObject developerDetails = new JSONObject();
                        if (profileImageUrl != null) {
                            URL url = new URL(profileImageUrl);
                            InputStream in = new BufferedInputStream(url.openStream());
                            ByteArrayOutputStream byteArrout = new ByteArrayOutputStream();
                            byte[] buf = new byte[1024];
                            int n = 0;
                            while (-1 != (n = in.read(buf))) {
                                byteArrout.write(buf, 0, n);
                            }
//                            out.close();
                            in.close();
                            byte[] imgFileData = byteArrout.toByteArray();
                            String imgBase64 = Base64.encode(imgFileData);
                            developerDetails.put("profilePic", imgBase64);
                        }
                        sguser.setCreatedon(new Date());
                        sguser.setProfileDetails(developerDetails.toString());
                        sguser.setLastlogindate(new Date());
                        sguser.setStatus(GlobalStatus.ACTIVE);
                        sguser.setType(ADMIN);
                        sguser.setPassword(developerpassword);
                        sguser.setFirstLoginFlag(GlobalStatus.FIRSTTIMELOGIN);
                        new UsersManagement().AddSgUsers(null, channel.getChannelid(), sguser);
                        request.getSession(true);
                        request.getSession().setAttribute("_apOprAuth", "yes");
                        request.getSession().setAttribute("_partnerSessionId", SessionId);
                        request.getSession().setAttribute("_ChannelId", channel.getChannelid());
                        request.getSession().setAttribute("_SgUsers", sguser);
                        request.getSession().setAttribute("_rssuserid", sguser.getUserid());
                        request.getSession().setAttribute("_partnerDetails", partnerDetails);
                        request.getSession().setAttribute("_loginType", sguser.getType());
                        request.getSession().setAttribute("_address", partnerDetails.getAllowedIps());
                        request.getSession().setAttribute("_addressLive", partnerDetails.getAllowedIpsForLive());
                        request.getSession().setAttribute("_partnerID", partnerDetails.getPartnerId());
                        request.getSession().setAttribute("profileImageUrl", profileImageUrl);
                        request.getSession().setAttribute("socialNamed", name);
                        request.getSession().setAttribute("showTourFirstSignUp", "yes");
                        String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.developer.signup");
                        String divEmailAdMessage = (String) LoadSettings.g_templateSettings.getProperty("email.ad.extraHTML");
                        MSConfig config = (MSConfig) new SettingsManagement().getSetting(SessionId, channel.getChannelid(), SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
                        String schemes = "http";
                        if (config != null) {
                            if (config.apssl.equalsIgnoreCase("yes")) {
                                schemes = "https";
                            }
                        }
                        String appurl = (request.getRequestURL().toString());
                        URL myAppUrl = new URL(appurl);
                        int port = myAppUrl.getPort();
                        String contextPath = request.getContextPath();
                        if (myAppUrl.getProtocol().equals("https") && port == -1) {
                            port = 443;
                        } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
                            port = 80;
                        }
                        String username = URLEncoder.encode(new String(Base64.encode(sgpartner.getName().getBytes())), "UTF-8");
                        String encodedemail = URLEncoder.encode(email, "UTF-8");
                        String path = request.getContextPath();
                        String userId = URLEncoder.encode(new String(Base64.encode(String.valueOf(sguser.getUserid()).getBytes())), "UTF-8");
                        String url = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/verifyYourself?username=" + username + "&email=" + encodedemail + "&ref=" + userId;
                        String webSiteURL = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path;
                        String emailSaveURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + contextPath + "/images/email/";    
                        String greenbackGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/greenback.gif";
                        String spadeGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/spade.gif";
                        String addressbookGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/addressbook.gif";
                        String penpaperGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/penpaper.gif";
                        String lightbulbGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/lightbulb.gif";
                        //String emailAdLogoPath = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/";
                        String emailAddSavePath = LoadSettings.emailAddSavePath;
                        String emailAdLogoPath = emailAddSavePath+ "images/email/";
                        if (tmessage != null) {
                            tmessage = tmessage.replaceAll("#name#", name);
                            tmessage = tmessage.replaceAll("#email#", email);
                            tmessage = tmessage.replaceAll("#password#", developerpassword);
                            tmessage = tmessage.replaceAll("#href#", url);
                            tmessage = tmessage.replaceAll("#webSiteURL#", webSiteURL);

                            tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
                            tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
                            tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
                            tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
                            tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);

                            tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
                            tmessage = tmessage.replaceAll("#supportId#", supportId);
                            tmessage = tmessage.replaceAll("#ideaId#", ideaId);

                            tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
                            tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
                            tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);
                            
                            

                        }
                        
                        // New changes for advertisement

                        SgAdvertiserAdDetails adDetails = AdToDeveloperUtils.GetEmailAd();
                        boolean adShown = false;
                        String adImagePath = null;
                        if(adDetails != null){
                            SgAdvertiseSubscriptionDetails subscriObject1 = new AdvertiserSubscriptionManagement().getAdvertiserSubscriptionbyAdId(adDetails.getAdvertiserId());                            
                            String emailConf = null;
                            if(subscriObject1 != null){
                                emailConf = subscriObject1.getEmailAdConfiguration();
                                if(emailConf != null){
                                    JSONObject emailjson;                        
                                    emailjson = new JSONObject(emailConf);
                                    String creditDeduction = "0";
                                    if(emailjson.has("emailCreditDeductionPerAd")){
                                        creditDeduction = emailjson.getString("emailCreditDeductionPerAd");
                                    }
                                    SgAdCreditInfo advertiserCredit = new AdvertiserCreditManagement().getDetails(adDetails.getAdvertiserId());
                                    Float credit = advertiserCredit.getMainCredit();
                                    if(credit != 0){
                                    adImagePath = new PDFInvoiceManagement().GenerateImagePath(adDetails,GlobalStatus.EMAIL_AD,emailAdLogoPath);
                                    if(adImagePath != null){                                                                                                                                    
                                        File f1 = new File(adImagePath);                                        
                                        String imageFileName = f1.getName();
                                        emailAdLogoPath = emailSaveURL + imageFileName;
                                        
                                        tmessage = tmessage.replaceAll("#emailAd#", divEmailAdMessage);
                                        tmessage = tmessage.replaceAll("#adLogo#", emailAdLogoPath);
                                        tmessage = tmessage.replaceAll("#adTitle#", adDetails.getEmailAdTitle());
                                        tmessage = tmessage.replaceAll("#emailAdContent#", adDetails.getEmailAdContent());
                                        
                                        Float fCredit = Float.parseFloat(creditDeduction);
                                        SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
                                        adDetails.setEmailAdShownFlag(GlobalStatus.PDF_AD_SHOWN_TODEVELOPER);
                                        new AdvertiserAdManagement().updateDetails(adDetails);
                                        SgAdvertisertracking adTracking = new SgAdvertisertracking();
                                        adTracking.setAdDescription("Email Ad shown at developer sign up");
                                        adTracking.setAdEvent("Developer SignUp");
                                        adTracking.setAdType(GlobalStatus.EMAIL_AD);
                                        adTracking.setAdvertiserId(adDetails.getAdvertiserId());
                                        adTracking.setChannelId(adDetails.getChannelId());
                                        adTracking.setCreationDate(new Date());
                                        adTracking.setPartnerId(sguser.getPartnerid());                                        
                                        adTracking.setSubscriptionId(subscriObject1.getAdSubscriptionId());
                                        adTracking.setCreateDate(sd.format(new Date()));
                                        adTracking.setAdvertiserAdId(adDetails.getAdvertiserAdId());
                                        adTracking.setCreditDeducted(fCredit);
                                        int retValue = new AdvertisementTrackingManagement().addTracking(adTracking);                                                                                                                
                                        Float tempCredit = credit - fCredit;
                                        advertiserCredit.setMainCredit(tempCredit);
                                        new AdvertiserCreditManagement().updateDetails(advertiserCredit);
//                                        File f = new File(adImagePath);
//                                        f.delete();
                                        adShown = true;
                                        }
                                    }                                                
                                }
                            }
                        }
                        if(!adShown){
                             tmessage = tmessage.replaceAll("#emailAd#", "");
                        }
                        SendSignUpNotification signupNotification = new SendSignUpNotification(tmessage, channel.getChannelid(), SessionId, email,adImagePath);
                        Thread signupNotificationThread = new Thread(signupNotification);
                        signupNotificationThread.start();

                        json.put("_result", "successWithRedirect");
                        json.put("_message", "You have been successfully sign in and password shared on your mail, Please subscribe package first.");
                        PrintWriter out = response.getWriter();
                        out.print(json);
                        System.out.println(json.toString());
                        return;
                    }
                }
            }
        } catch (Exception ex) {
            json.put("_result", "error");
            json.put("_message", ex.getMessage());
            PrintWriter out = response.getWriter();
            out.print(json);
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

class SendSignUpNotification implements Runnable {

    String email;
    String tmessage;
    String channels;
    String sessionId;
    String emailAdPath;
    public static final int PENDING = 2;
    public static final int SEND = 0;

    public SendSignUpNotification() {

    }

    public SendSignUpNotification(String message, String channel, String sessionId, String email, String emailAdPath) {
        this.email = email;
        this.tmessage = message;
        this.channels = channel;
        this.sessionId = sessionId;
        this.emailAdPath=emailAdPath;
    }
    String mimeType[] = {"application/octet-stream"};
    int productType = 3;

    public void run() {
        SGStatus status = new SendNotification().SendEmail(channels, email, "Ready APIs Welcome You, ", tmessage, null, null, null, mimeType, productType);
        if (status.iStatus == PENDING || status.iStatus == SEND) {
            System.out.println("Email sent successfully with retCode " + status.iStatus + " to" + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
        } else {
            System.out.println("Email failed to sent with retCode " + status.iStatus + " to " + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
        }
        if(emailAdPath != null){
            File deleteAdImage = new File(emailAdPath);
            deleteAdImage.delete();
        }
    }
    
}

class AssignTokenOperation implements Runnable {

    String channels;
    PartnerDetails partnerDetails;
    String sessionId;

    public AssignTokenOperation(PartnerDetails partnerDetails, String sessionId) {
        this.partnerDetails = partnerDetails;
        this.sessionId = sessionId;
    }

    public void run() {
        try {
            new AutoAPIKeyAssignment().accessObject(partnerDetails, sessionId);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
