/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.partner;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.nucleus.commons.AdToDeveloperUtils;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MSConfig;
import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.SgAdCreditInfo;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiseSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiserAdDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdvertisertracking;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.ChannelsUtils;
import com.mollatech.serviceguard.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.serviceguard.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertisementTrackingManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserAdManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserCreditManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PDFInvoiceManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "ResetDeveloperPassword", urlPatterns = {"/ResetDeveloperPassword"})
public class ResetDeveloperPassword extends HttpServlet implements Runnable {

    String channelId = null;

    SgUsers developer = null;

    String msg = "";
    
    String adImagePath = null;

    static final Logger logger = Logger.getLogger(ResetDeveloperPassword.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Requested Servlet is ResetOperatorPassword at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        String _channelName = "ServiceGuard";
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        Channels channel = cUtil.getChannel(_channelName);
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        SessionManagement sManagement = new SessionManagement();
        String sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {
//            String _oUsername = request.getParameter("_oUsername");
//            logger.debug("Value of _oUsername  = " + _oUsername);
            String _oEmailId = request.getParameter("_oEmailId");
            logger.debug("Value of _oEmailId  = " + _oEmailId);
//            String _oRandomString = request.getParameter("_oRandomString");
//            logger.debug("Value of _oRandomString  = " + _oRandomString);
            int hour = Integer.parseInt(LoadSettings.g_sSettings.getProperty("password.expiry.minutes"));
            Calendar c = Calendar.getInstance();
            c.add(Calendar.MINUTE, hour);
            Date expiry = c.getTime();
//            if (_oUsername.equals("")) {
//                json.put("result", "error");
//                json.put("message", "Please Enter Your Name");
//                return;
//            }
            if (_oEmailId.equals("")) {
                json.put("result", "error");
                json.put("message", "Please Enter Your Email");
                return;
            }

            SgUsers operators = new UsersManagement().initializeTMResetPassword(sessionId, channel.getChannelid(), null, _oEmailId, expiry);
            if (operators == null) {
                json.put("result", "error");
                json.put("message", "No such Developer is present");
                return;
            }
            channelId = operators.getChannelid();
            developer = operators;
            MSConfig config = (MSConfig) new SettingsManagement().getSetting(sessionId, channelId, SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
            if (config != null) {
                String schemes = "http";
                if (config.apssl.equalsIgnoreCase("yes")) {
                    schemes = "https";
                }
                String path = request.getContextPath();
                SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMM dd, yyyy HH:mm:ss a");
                String username = URLEncoder.encode(new String(Base64.encode(operators.getUsername().getBytes())), "UTF-8");
                String email = URLEncoder.encode(operators.getEmail(), "UTF-8");
                String userId = URLEncoder.encode(new String(Base64.encode(String.valueOf(operators.getUserid()).getBytes())), "UTF-8");
                //String randomString = URLEncoder.encode(new String(Base64.encode(operators.getRandomString().getBytes())), "UTF-8");
                String expiryTime = URLEncoder.encode(new String(Base64.encode(formatter.format(operators.getLinkExpiry()).getBytes())), "UTF-8");
                String url = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/verifyDeveloperDetails.jsp?username=" + username + "&email=" + email + "&expiryTime=" + expiryTime + "&ref=" + userId;
                //randomString = operators.getRandomString().split(",")[1];
                //String message = LoadSettings.g_templateSettings.getProperty("mobile.operator.password.reset");
                //message = message.replace("#rs#", randomString);
                String greenbackGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/greenback.gif";
                String spadeGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/spade.gif";
                String addressbookGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/addressbook.gif";
                String penpaperGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/penpaper.gif";
                String lightbulbGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/lightbulb.gif";
                logger.info("Reset Password lightbulbGIF >> " + lightbulbGIF);
                String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry");
                String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question");
                String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea");
                logger.info("Reset Password enquiryId >> " + enquiryId);
                String[] enquiryEmailDetails = enquiryId.split(":");
                String[] supportEmailDetails = supportId.split(":");
                String[] ideaEmailDetails = ideaId.split(":");
                String appurl = (request.getRequestURL().toString());
                URL myAppUrl = new URL(appurl);
                int port = myAppUrl.getPort();
                if (myAppUrl.getProtocol().equals("https") && port == -1) {
                    port = 443;
                } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
                    port = 80;
                }
                String contextPath = request.getContextPath();
                String emailSaveURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + contextPath + "/images/email/";
                msg = LoadSettings.g_templateSettings.getProperty("email.operator.password.reset");
                msg = msg.replace("#href#", url);
                msg = msg.replace("#name#", operators.getUsername());
                msg = msg.replace("#hour#", "" + hour);
                msg = msg.replace("#greenbackGIF#", greenbackGIF);
                msg = msg.replace("#spadeGIF#", spadeGIF);
                msg = msg.replace("#addressbookGIF#", addressbookGIF);
                msg = msg.replace("#penpaperGIF#", penpaperGIF);
                msg = msg.replace("#lightbulbGIF#", lightbulbGIF);
                msg = msg.replaceAll("#enquiryId#", enquiryId);
                msg = msg.replaceAll("#supportId#", supportId);
                msg = msg.replaceAll("#ideaId#", ideaId);

                msg = msg.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
                msg = msg.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
                msg = msg.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);
                String divEmailAdMessage = (String) LoadSettings.g_templateSettings.getProperty("email.ad.extraHTML");
                logger.info("Reset Password Email \n " + msg);
                String emailAddSavePath = LoadSettings.emailAddSavePath;
                String emailAdLogoPath = emailAddSavePath+ "images/email";
                // New changes for advertisement

                        SgAdvertiserAdDetails adDetails = AdToDeveloperUtils.GetEmailAd();
                        boolean adShown = false;
                       
                        if(adDetails != null){
                            SgAdvertiseSubscriptionDetails subscriObject1 = new AdvertiserSubscriptionManagement().getAdvertiserSubscriptionbyAdId(adDetails.getAdvertiserId());                            
                            String emailConf = null;
                            if(subscriObject1 != null){
                                emailConf = subscriObject1.getEmailAdConfiguration();
                                if(emailConf != null){
                                    JSONObject emailjson;                        
                                    emailjson = new JSONObject(emailConf);
                                    String creditDeduction = "0";
                                    if(emailjson.has("emailCreditDeductionPerAd")){
                                        creditDeduction = emailjson.getString("emailCreditDeductionPerAd");
                                    }
                                    SgAdCreditInfo advertiserCredit = new AdvertiserCreditManagement().getDetails(adDetails.getAdvertiserId());
                                    Float credit = advertiserCredit.getMainCredit();
                                    if(credit != 0){
                                    adImagePath = new PDFInvoiceManagement().GenerateImagePath(adDetails,GlobalStatus.EMAIL_AD,emailAdLogoPath);
                                    if(adImagePath != null){                                                                                                                                    
                                        File f1 = new File(adImagePath);
                                        
                                        String imageFileName = f1.getName();
                                        emailAdLogoPath = emailSaveURL + imageFileName;
                                        msg = msg.replaceAll("#emailAd#", divEmailAdMessage);
                                        msg = msg.replaceAll("#adLogo#", emailAdLogoPath);
                                        msg = msg.replaceAll("#adTitle#", adDetails.getEmailAdTitle());
                                        msg = msg.replaceAll("#emailAdContent#", adDetails.getEmailAdContent());
                                        
                                        Float fCredit = Float.parseFloat(creditDeduction);
                                        SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
                                        adDetails.setEmailAdShownFlag(GlobalStatus.PDF_AD_SHOWN_TODEVELOPER);
                                        new AdvertiserAdManagement().updateDetails(adDetails);
                                        SgAdvertisertracking adTracking = new SgAdvertisertracking();
                                        adTracking.setAdDescription("Email Ad shown at developer sign up");
                                        adTracking.setAdEvent("Developer SignUp");
                                        adTracking.setAdType(GlobalStatus.EMAIL_AD);
                                        adTracking.setAdvertiserId(adDetails.getAdvertiserId());
                                        adTracking.setChannelId(adDetails.getChannelId());
                                        adTracking.setCreationDate(new Date());
                                        adTracking.setPartnerId(operators.getPartnerid());                                        
                                        adTracking.setSubscriptionId(subscriObject1.getAdSubscriptionId());
                                        adTracking.setCreateDate(sd.format(new Date()));
                                        adTracking.setAdvertiserAdId(adDetails.getAdvertiserAdId());
                                        adTracking.setCreditDeducted(fCredit);
                                        int retValueTracking = new AdvertisementTrackingManagement().addTracking(adTracking);                                                                                                                
                                        Float tempCredit = credit - fCredit;
                                        advertiserCredit.setMainCredit(tempCredit);
                                        new AdvertiserCreditManagement().updateDetails(advertiserCredit);
//                                        File f = new File(adImagePath);
//                                        f.delete();
                                        adShown = true;
                                        }
                                    }                                                
                                }
                            }
                        }
                        if(!adShown){
                             msg = msg.replaceAll("#emailAd#", "");
                        }
                if (operators.getEmail() != null) {
                    Thread thread = new Thread(this);
                    thread.start();
                    //new SendNotification().SendOnMobileByPreference(channelId, operators.getPhone(), message, 1, 1, hour);
                    json.put("result", "success");
                    json.put("message", "Password reset link send on your email address");
                    return;
                } else {
                    json.put("result", "error");
                    json.put("message", "Developer email details not found.");
                    return;
                }
            } else {
                json.put("result", "error");
                json.put("message", "Application server configuration missing.");
                return;
            }
        } catch (Exception ex) {
            try {
                json.put("result", "error");
                json.put("message", "Error in reseting password");
                logger.error("Exception at ResetDeveloperPassword ", ex);
                return;
            } catch (Exception e) {
                logger.error("Exception at ResetDeveloperPassword ", ex);
            }
        } finally {
            logger.info("Response of ResetDeveloperPassword " + json.toString());
            logger.info("Response of ResetDeveloperPassword Servlet at " + new Date());
            sChannel.close();
            suChannel.close();
            sRemoteAcess.close();
            suRemoteAcess.close();
            out.print(json);
            out.flush();
            out.close();
        }
    }

    @Override
    public void run() {
        String subject = LoadSettings.g_subjecttemplateSettings.getProperty("email.operator.password.reset.subject");
        new SendNotification().SendEmail(channelId, developer.getEmail(), subject, msg, null, null, null, null, 1);
        if(adImagePath != null){
            File f = new File(adImagePath);
            f.delete();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
