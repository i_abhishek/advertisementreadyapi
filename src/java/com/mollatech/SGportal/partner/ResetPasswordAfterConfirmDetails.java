/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.partner;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.ChannelsUtils;
import com.mollatech.serviceguard.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.serviceguard.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.json.simple.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "ResetPasswordAfterConfirmDetails", urlPatterns = {"/ResetPasswordAfterConfirmDetails"})
public class ResetPasswordAfterConfirmDetails extends HttpServlet {
    static final Logger logger = Logger.getLogger(ResetPasswordAfterConfirmDetails.class);
    public static final int PENDING = 2;
    public static final int SEND = 0;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Requested Servlet is ResetPasswordAfterConfirmDetails at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        String _channelName = "ServiceGuard";
        
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        Channels channel = cUtil.getChannel(_channelName);
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        SessionManagement sManagement = new SessionManagement();
        String sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {
            UtilityFunctions u = new UtilityFunctions();
            Date d = new Date();
            String _oUsername = request.getParameter("_oUsername");
            logger.debug("Value of _oUsername  = " + _oUsername);
            String _oEmailId = request.getParameter("_oEmailId");
            logger.debug("Value of _oEmailId  = " + _oEmailId);
            String _oRandomString = request.getParameter("_oRandomString");
            logger.debug("Value of _oRandomString  = " + _oRandomString);
            _oRandomString += "," + request.getParameter("smsRandomString");
            logger.debug("Value of smsRandomString  = " + _oRandomString);
            String strPassword = u.HexSHA1(_oUsername + _oEmailId + d.toString());
            strPassword = strPassword.substring(0, 9);            
            SgUsers operators = new UsersManagement().resetPassword(sessionId, channel.getChannelid(), _oUsername, _oEmailId, _oRandomString, strPassword);
            if (operators == null) {
                json.put("result", "error");
                json.put("message", "Password Reset Functionality Expired.");
                return;
            }
            String mimeType[] = { "application/octet-stream" };
            String message = LoadSettings.g_templateSettings.getProperty("email.operator.password.reset.and.send.pdf");
            String subject = LoadSettings.g_subjecttemplateSettings.getProperty("email.operator.password.reset.subject");
            File file = File.createTempFile("Password"+ new SimpleDateFormat("dd-MM-yyyy").format(new Date()), ".pdf");            
            String path = file.getCanonicalPath();
            Document doc = new Document();
            FileOutputStream fos = new FileOutputStream(path);
            PdfWriter writer = PdfWriter.getInstance(doc, fos);
            byte[] USER = _oRandomString.split(",")[1].getBytes();
            byte[] OWNER = "ServiceGuard!@#$".getBytes();
            writer.setEncryption(USER, OWNER, PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_256);
            writer.createXmpMetadata();
            doc.open();
            String content = LoadSettings.g_templateSettings.getProperty("pdf.content.partnerPortal");
            content = content.replace("#username#", operators.getUsername());
            content = content.replace("#password#", strPassword);
            doc.add(new Paragraph(content));
            doc.close();
            String filePath[] = { path };
            SGStatus gStatus = new SendNotification().SendEmailWithAttachment(operators.getChannelid(), operators.getEmail(), subject, message, null, null, filePath, mimeType, 1);
            if (gStatus.iStatus != PENDING && gStatus.iStatus != SEND) {
                
                message = "Password reset successfully but failed to sent emil.";
                try {
                    json.put("_result", "error");
                    logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is error");
                    json.put("_message", message);
                    logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                out.print(json);
                out.flush();
                return;
            }
            File deleteFile = new File(path);
            deleteFile.delete();
            
            json.put("result", "success");
            json.put("message", "Password Reseted Successfully and forwarded on your email.");
            return;
        } catch (Exception ex) {
            logger.error("Exception at ResetPasswordAfterConfirmDetails ", ex);
            ex.printStackTrace();
            try {
                json.put("result", "error");
                json.put("message", ex.getMessage());
                return;
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Exception at ResetPasswordAfterConfirmDetails ", e);
            }
        } finally {
            logger.info("Response of ResetPasswordAfterConfirmDetails " + json.toString());
            logger.info("Response of ResetPasswordAfterConfirmDetails Servlet at " + new Date());
            sChannel.close();
            suChannel.close();
            sRemoteAcess.close();
            suRemoteAcess.close();
            out.print(json);
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
