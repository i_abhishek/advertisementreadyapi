package com.mollatech.SGportal.partner;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.RequestedAccessPolicyEntry;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.ChannelsUtils;
import com.mollatech.serviceguard.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.serviceguard.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
public class completePartnerProfile extends HttpServlet {

//    static final int ACTIVE_STATUS = 1;
//
//    static final int SUSPENDED_STATUS = 0;

    static final int PENDING_STATUS = -2;

    public final int PASSWORD = 1;

    public static final int USER_PASSWORD = 1;

    public final int OPERATOR = 1;

    public final int REPORTER = 2;

    public static final int ADMIN = 0;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Request added successfully";
        PrintWriter out = response.getWriter();
        int TPD = -1, TPS = -1;
        SessionManagement sManagement = new SessionManagement();
        String _channelName = "ServiceGuard";
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        Channels channel = cUtil.getChannel(_channelName);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        String SessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
        String pName = request.getParameter("partnerName");
        String pEmail = request.getParameter("partnerEmail");
        String pMobileNo = request.getParameter("partnerMobNo");
        if (pMobileNo != null) {
            pMobileNo = pMobileNo.trim();
        }
        int pStatus = PENDING_STATUS;
        String ipAddress = request.getParameter("partnerIP");
        String ipLive = request.getParameter("partnerIPLive");
        String pWebsite = request.getParameter("partnerWebsite");
        String pLandline = request.getParameter("partnerLandlineNo");
        String pFax = request.getParameter("partnerFax");
        String pCompanyName = request.getParameter("partnerComName");
        String pAddress = request.getParameter("partnerAddress");
        String pPincode = request.getParameter("partnerPincode");
        String pSD = request.getParameter("partnerSD");
        String pED = request.getParameter("partnerED");
        String pST = request.getParameter("partnerST");
        String pET = request.getParameter("partnerET");
        String _pTPD = request.getParameter("partnerTPD");
        String _pTPS = request.getParameter("partnerTPS");
        String _partnerId = request.getParameter("partnerID");
        String zippath = (String) request.getSession().getAttribute("_ZipPath");
        Path path = Paths.get(zippath);
        byte[] compressed_document = Files.readAllBytes(path);
        String Operator = "Admin";
        if (pMobileNo.length() != 13) {
            result = "error";
            message = "Enter 10 digit mobile number with country code";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        } else if (UtilityFunctions.isValidPhoneNumber(pMobileNo) == false) {
            result = "error";
            message = "Enter Valid mobile number";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        if (UtilityFunctions.isValidPhoneNumber(pLandline) == false) {
            result = "error";
            message = "Enter Valid landline number";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        if (UtilityFunctions.isValidPhoneNumber(pFax) == false) {
            result = "error";
            message = "Enter Valid fax number";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        if (UtilityFunctions.isValidPhoneNumber(pPincode) == false) {
            result = "error";
            message = "Enter Valid fax number";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        if (_pTPD != null) {
            if (!_pTPD.isEmpty()) {
                if (UtilityFunctions.isValidPhoneNumber(_pTPD) == false) {
                    result = "error";
                    message = "Enter valid TPD";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
                TPD = Integer.parseInt(_pTPD);
            }
        }
        if (_pTPS != null) {
            if (!_pTPS.isEmpty()) {
                if (UtilityFunctions.isValidPhoneNumber(_pTPS) == false) {
                    result = "error";
                    message = "Enter valid TPS";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
                TPS = Integer.parseInt(_pTPS);
            }
        }
        RequestedAccessPolicyEntry ap = new RequestedAccessPolicyEntry();
        ap.setApEndTime(pET);
        ap.setApStartTime(pST);
        ap.setDayFrom(pSD);
        ap.setDayTo(pED);
        ap.setTpd(TPD);
        ap.setTps(TPS);
        Integer pid = null;
        if (_partnerId != null) {
            pid = Integer.parseInt(_partnerId);
        }
        UsersManagement um = new UsersManagement();
        SgUsers sguser = um.getSgUsers(SessionId, channel.getChannelid(), pName);
        if (SessionId != null) {
            int poStatus = new PartnerRequestManagement().addPartnerRequestWithPid(SessionId, pid, pName, GlobalStatus.ACTIVE, pMobileNo, pEmail, ipAddress, pWebsite, pFax, pLandline, pAddress, pPincode, pCompanyName, ap, compressed_document, ipLive);
            if (poStatus == 0) {
                result = "success";
                message = "Request added Successfully";
            } else {
                result = "error";
                message = "Request not added";
            }
        } else {
            result = "error";
            message = "Request not added.";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
