package com.mollatech.SGportal.partner;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.nucleus.commons.AutoAPIKeyAssignment;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.GroupDetails;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgPartnerrequest;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.ChannelsUtils;
import com.mollatech.serviceguard.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.serviceguard.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.json.JSONObject;

@WebServlet(name = "login", urlPatterns = {"/login"})
public class login extends HttpServlet {

    final String itemType = "LOGIN";

    int SUCCESS = 0;

    int FAILED = -1;

    final int ADMIN = 0;

    final int OPERATOR = 1;

    final int REPORTER = 2;

    final int ACTIVATED = 1;

    String BLOCKED = "BLOCKED_IP";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = null;
        try {
            request.getSession().invalidate();
        } catch (Exception ex) {
        }
        response.setContentType("application/json");
        Object Obj = getServletContext().getAttribute("LoginDetails");
        Map<String, HttpSession> sessionMap = (Map) getServletContext().getAttribute("sessionDetails");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        try {
            String _userName = request.getParameter("_name");
            String _userpassword = request.getParameter("_passwd");
            String otp = request.getParameter("_otp");
            String result = "success";
            String message = "successful credential verification.";
            PartnerDetails pdetails = null;
            SgPartnerrequest partnerReqObj = null;
            SessionManagement sManagement = new SessionManagement();
            String _channelName = "ServiceGuard";
            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
            SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
            Session sRemoteAcess = suRemoteAcess.openSession();
            RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
            Channels channel = cUtil.getChannel(_channelName);
            String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
            String SessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
            if (_userName == null || (_userpassword != null && _userpassword.equals("") && otp.equals(""))) {
                try {
                    request.getSession().invalidate();
                } catch (Exception ex) {
                }
                result = "error";
                message = "Enter Valid Username and Password";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }
            int partnerid = -1;
            UsersManagement um = new UsersManagement();
            SgUsers user = null;
            boolean otpFailed = false;
            boolean passwordFailed = false;
            SgSubscriptionDetails subscriObject1 = null;
            if (_userpassword != null && !_userpassword.equals("")) {
                user = um.VerifyCredential(SessionId, channel.getChannelid(), _userName, _userpassword);
                passwordFailed = true;
            } else if (!otp.equals("")) {
                user = um.VerifyOTP(SessionId, channel.getChannelid(), _userName, otp);
                otpFailed = true;
            }
            if (user != null) {
                partnerid = user.getPartnerid();
                pdetails = new PartnerManagement().getPartnerDetails(SessionId, channel.getChannelid(), partnerid);
                AssignNewRespourceAPITokenOperation asignToken = new AssignNewRespourceAPITokenOperation(pdetails, SessionId);
                Thread asignTokenThread = new Thread(asignToken);
                asignTokenThread.start();
                if (pdetails != null) {
                    GroupDetails details = new GroupManagement().getGroupDetails(Integer.parseInt(pdetails.getPartnerGroupId()));
//                    if (!details.getGroupName().equalsIgnoreCase("MPServices")) {
//                        result = "error";
//                        message = "You Are Not Valid User For MPServices.";
//                        json.put("_result", result);
//                        json.put("_message", message);
//                        out.print(json);
//                        out.flush();
//                        return;
//                    }
                }
                if (pdetails != null) {
                    subscriObject1 = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(pdetails.getPartnerId());
                    if (pdetails.getStatus() == GlobalStatus.SUSPEND) {
                        result = "error";
                        message = "Your account is Suspended, Please contact to Admin";
                        json.put("_result", result);
                        json.put("_message", message);
                        out.print(json);
                        out.flush();
                        return;
                    }
                    if (pdetails.getStatus() == GlobalStatus.DELETED) {
                        result = "error";
                        message = "Your account is removed, Please contact to Admin";
                        json.put("_result", result);
                        json.put("_message", message);
                        out.print(json);
                        out.flush();
                        return;
                    }

                    user.setFirstLoginFlag(GlobalStatus.FIRSTTIMELOGINDONE);
                    um.editSgUsers(SessionId, channel.getChannelid(), user);
                    if (user.getType() == ADMIN) {
                        request.getSession(true);
                        request.getSession().setAttribute("_apOprAuth", "yes");
                        request.getSession().setAttribute("_partnerSessionId", SessionId);
                        request.getSession().setAttribute("_ChannelId", channel.getChannelid());
                        request.getSession().setAttribute("_SgUsers", user);
                        request.getSession().setAttribute("_rssuserid", user.getUserid());
                        request.getSession().setAttribute("_partnerDetails", pdetails);
                        request.getSession().setAttribute("_loginType", user.getType());
                        request.getSession().setAttribute("_address", pdetails.getAllowedIps());
                        request.getSession().setAttribute("_addressLive", pdetails.getAllowedIpsForLive());
                        request.getSession().setAttribute("_partnerID", partnerid);
                    } else if (pdetails.getPartnerEmailid().equalsIgnoreCase(_userName)) {
                        request.getSession().setAttribute("_apOprAuth", "yes");
                        request.getSession().setAttribute("_partnerSessionId", SessionId);
                        request.getSession().setAttribute("_ChannelId", channel.getChannelid());
                        request.getSession().setAttribute("_SgUsers", user);
                        request.getSession().setAttribute("_rssuserid", user.getUserid());
                        request.getSession().setAttribute("_partnerDetails", pdetails);
                        request.getSession().setAttribute("_loginType", user.getType());
                        request.getSession().setAttribute("_partnerID", partnerid);
                    } else {
                        try {
                            request.getSession().invalidate();
                        } catch (Exception ex) {
                        }
                        result = "error";
                        message = "Not Valid User";
                        json.put("_result", result);
                        json.put("_message", message);
                        out.print(json);
                        out.flush();
                        return;
                    }
                    partnerReqObj = new PartnerRequestManagement().getPartnerRequestsPartnerbyId(SessionId, partnerid);
                    if (partnerReqObj == null) {
                        Map map = (Map) Obj;
                        if (map == null) {
                            map = new HashMap();
                        }
                        if (sessionMap == null) {
                            sessionMap = new HashMap();
                        }
                        map.put(user.getUserid(), request.getSession().getId() + ":Logged" + ":" + (new Date().getTime()));
                        getServletContext().setAttribute("LoginDetails", map);
                        sessionMap.put(user.getUserid(), request.getSession());
                        getServletContext().setAttribute("sessionDetails", sessionMap);
                        result = "redirect";
                        message = "successful credential verification, Please fill information first";
                        json.put("_result", result);
                        json.put("_message", message);
                        out.print(json);
                        out.flush();
                        return;
                    }
                } else {
                    try {
                        request.getSession().invalidate();
                    } catch (Exception ex) {
                    }
                    result = "error";
                    message = "Not Valid User";
                    json.put("_result", result);
                    json.put("_message", message);
                    out.print(json);
                    out.flush();
                    return;
                }
            } else {
                if (otpFailed) {
                    try {
                        request.getSession().invalidate();
                    } catch (Exception ex) {
                    }
                    result = "error";
                    message = "OTP expired or invalid, Please try with new one";
                    json.put("_result", result);
                    json.put("_message", message);
                    out.print(json);
                    out.flush();
                    return;
                }
                if (passwordFailed) {
                    try {
                        request.getSession().invalidate();
                    } catch (Exception ex) {
                    }
                    result = "error";
                    message = "Invalid Credentials";
                    json.put("_result", result);
                    json.put("_message", message);
                    out.print(json);
                    out.flush();
                    return;
                }
            }
            if (Obj == null) {
                Map map = new HashMap();
                sessionMap = new HashMap();
                map.put(user.getUserid(), request.getSession().getId() + ":Logged" + ":" + (new Date().getTime()));
                sessionMap.put(user.getUserid(), request.getSession());
                getServletContext().setAttribute("LoginDetails", map);
                getServletContext().setAttribute("sessionDetails", sessionMap);
            } else {
                Map map = (Map) Obj;
                Object value = map.get(user.getUserid());
                if (value == null) {
                    map.put(user.getUserid(), request.getSession().getId() + ":Logged" + ":" + (new Date().getTime()));
                    getServletContext().setAttribute("LoginDetails", map);
                    sessionMap.put(user.getUserid(), request.getSession());
                    getServletContext().setAttribute("sessionDetails", sessionMap);
                } else {
                    String val = value.toString();
                    String sessionIdstr = val.split(":")[0];
                    String flag = val.split(":")[1];
                    if (!request.getSession().getId().equals(sessionIdstr) && !flag.equalsIgnoreCase("Not Logged")) {
                        map.remove(user.getUserid());
                        HttpSession session = sessionMap.get(user.getUserid());
                        try {
                            session.invalidate();
                        } catch (Exception ex) {
                        }
                        sessionMap.remove(user.getUserid());
                        sessionMap.put(user.getUserid(), request.getSession());
                        getServletContext().setAttribute("LoginDetails", map);
                        getServletContext().setAttribute("sessionDetails", sessionMap);
                        json.put("_result", "success");
                        json.put("_message", "Login Successful.");
                        // json.put("_url", "./homePage.jsp");
//                        json.put("_url", "./header.jsp");
                        // check for new resource added and assign auto APIToken
                        if (subscriObject1 != null) {
                            json.put("_url", "./header.jsp");
                        } else {
                            json.put("_url", "./packages.jsp");
                            json.put("_message", "Login Successful, Please subscribe a package first");
                        }

                        out.print(json);
                        out.flush();
                        return;
                    } else {
                        map.put(user.getUserid(), request.getSession().getId() + ":Logged" + ":" + (new Date().getTime()));
                        getServletContext().setAttribute("LoginDetails", map);
                        sessionMap.put(user.getUserid(), request.getSession());
                        getServletContext().setAttribute("sessionDetails", sessionMap);
                    }
                }
            }
            json.put("_result", result);
            if (user != null) {
                if (result.equalsIgnoreCase("success")) {
                    String secondfactor = LoadSettings.g_sSettings.getProperty("axiom.2fa.enable");
                    if (secondfactor != null) {
                        if (secondfactor.equalsIgnoreCase("yes")) {
                            AxiomWrapper axWraper = new AxiomWrapper();
                            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
                            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
                            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
                            String sessionId = axWraper.OpenSession(channelid, remotelogin, password);
                            String strPayLoad = null;
                            String userid = user.getUserid();
                            HttpSession session = request.getSession(true);
                            Boolean securephraseflag = false, isRegistered = false, otpflag = false;
                            RssUserCerdentials userObj = axWraper.getUserCerdentials(sessionId, userid, AxiomWrapper.GET_USER_BY_USERID, strPayLoad);
                            AxiomCredentialDetails axiomCredentialDetails;
                            AxiomCredentialDetails axiomCredentalSecurePhrase = null;
                            if (userObj != null) {
                                if (userObj.getTokenDetails() != null) {
                                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                                        axiomCredentialDetails = userObj.getTokenDetails().get(i);
                                        if (axiomCredentialDetails != null) {
                                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.SECURE_PHRASE) {
                                                if (axiomCredentialDetails.getSecurePhraseImage() != null) {
                                                    securephraseflag = true;
                                                }
                                            } else if (axiomCredentialDetails.getCategory() == AxiomWrapper.SW_MOBILE_TOKEN) {
                                                if (axiomCredentialDetails.getStatus() == ACTIVATED) {
                                                    otpflag = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                session.setAttribute("_rssDemoUserCerdentials", userObj);
                                session.setAttribute("_userSessinId", sessionId);
                                session.setAttribute("_username", userObj.getRssUser().getUserName());
                                session.setAttribute("_user_id", userObj.getRssUser().getUserId());
                            }
                            if (securephraseflag == true) {
                                url = "./verifySecurePhrase.jsp";
                            } else if (otpflag == true) {
                                url = "./verifyOTP.jsp";
                            } else {
                                url = "redirect";
                            }
                        } else //url = "./homePage.jsp";
                        //                            url = "./header.jsp";
                        {
                            if (subscriObject1 != null) {
                                json.put("_url", "./header.jsp");
                            } else {
                                json.put("_url", "./packages.jsp");
                                json.put("_message", "Login Successful, Please subscribe a package first");
                            }
                        }
                    } else //url = "./homePage.jsp";
                    //                        url = "./header.jsp";
                     if (subscriObject1 != null) {
                            json.put("_url", "./header.jsp");
                        } else {
                            json.put("_url", "./packages.jsp");
                            json.put("_message", "Login Successful, Please subscribe a package first");
                        }
                }
            }
            if (subscriObject1 != null) {
                json.put("_url", "./header.jsp");
            } else {
                json.put("_url", "./packages.jsp");
                json.put("_message", "Login Successful, Please subscribe a package first");
            }
            json.put("_message", message);
            out.print(json);
            out.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
            json.put("_url", url);
            json.put("_result", "error");
            json.put("_message", ex.getMessage());
            out.print(json);
            out.flush();
        }
    }

    public static byte[] Bas64SHA1Inner(String message) {
        byte[] base64String = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(message.getBytes());
            byte[] hashValue = md.digest();
            base64String = hashValue;
        } catch (Exception e) {
            return null;
        }
        return base64String;
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}

class AssignNewRespourceAPITokenOperation implements Runnable {

    String channels;
    PartnerDetails partnerDetails;
    String sessionId;

    public AssignNewRespourceAPITokenOperation(PartnerDetails partnerDetails, String sessionId) {
        this.partnerDetails = partnerDetails;
        this.sessionId = sessionId;
    }

    public void run() {
        try {
            new AutoAPIKeyAssignment().accessObject(partnerDetails, sessionId);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
