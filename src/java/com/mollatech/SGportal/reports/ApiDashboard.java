/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.reports;

import java.util.Date;

/**
 *
 * @author bluebricks
 */
public class ApiDashboard {

    String apiName;
    long apiCount;
    double credit;
    long partApiCount;
    Date date;
    double txAmount;

    public ApiDashboard(Date date, double txAmount) {
        this.date = date;
        this.txAmount = txAmount;
    }

    public ApiDashboard(String apiName, long apiCount, long partApiCount) {
        this.apiName = apiName;
        this.apiCount = apiCount;
        this.partApiCount = partApiCount;
    }

    public ApiDashboard(String apiName, long apiCount) {
        this.apiName = apiName;
        this.apiCount = apiCount;
    }

    public ApiDashboard(String apiName, double credit) {
        this.apiName = apiName;
        this.credit = credit;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public long getApiCount() {
        return apiCount;
    }

    public void setApiCount(long apiCount) {
        this.apiCount = apiCount;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public long getPartApiCount() {
        return partApiCount;
    }

    public void setPartApiCount(long partApiCount) {
        this.partApiCount = partApiCount;
    }

}
