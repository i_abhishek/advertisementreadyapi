package com.mollatech.SGportal.reports;

import com.mollatech.serviceguard.nucleus.db.RequestTracking;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestTrackingManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class getReports extends HttpServlet {

    private static int PDF_TYPE = 0;

    private static int CSV_TYPE = 1;

    private static int TEXT_TYPE = 2;

    private static final int BUFSIZE = 4096;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String channelName = "Ready APIs";
            String _apid = request.getParameter("_apid");
            String _grid = request.getParameter("_grid");
            String _resid = request.getParameter("_resid");
            String _pid = request.getParameter("_pid");
            String sdate = request.getParameter("_sdate");
            String stime = request.getParameter("_stime");
            String edate = request.getParameter("_edate");
            String etime = request.getParameter("_etime");
            String format = request.getParameter("_format");
            String _auditUserID = request.getParameter("_auditUserID");
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            DateFormat timeformat = new SimpleDateFormat("hh:mm a");
            int apid = -1, grid = -1, resid = -1, pid = -1;
            String filepath = null;
            if (sdate == null || stime == null || edate == null || etime == null || sdate.length() <= 0 || stime.length() <= 0 || edate.length() <= 0 || etime.length() <= 0) {
                return;
            }
            try {
                try {
                    if (_apid != null && !_apid.isEmpty()) {
                        apid = Integer.parseInt(_apid);
                    }
                    if (_resid != null && !_resid.isEmpty()) {
                        resid = Integer.parseInt(_resid);
                    }
                    if (_grid != null && !_grid.isEmpty()) {
                        grid = Integer.parseInt(_grid);
                    }
                    if (_resid != null && !_resid.isEmpty()) {
                        resid = Integer.parseInt(_resid);
                    }
                    if (_pid != null && !_pid.isEmpty()) {
                        pid = Integer.parseInt(_pid);
                    }
                    Date startDate = null;
                    if (sdate != null && !sdate.isEmpty()) {
                        startDate = (Date) formatter.parse(sdate);
                    }
                    Date startTime = null;
                    if (stime != null && !stime.isEmpty()) {
                        startTime = (Date) timeformat.parse(stime);
                    }
                    Date endDate = null;
                    if (edate != null && !edate.isEmpty()) {
                        endDate = (Date) formatter.parse(edate);
                    }
                    Date endTime = null;
                    if (stime != null && !stime.isEmpty()) {
                        endTime = (Date) timeformat.parse(stime);
                    }
                    int iFormat = -9999;
                    if (format != null && !format.isEmpty()) {
                        iFormat = Integer.parseInt(format);
                    }
                    if (PDF_TYPE == iFormat) {
                        iFormat = PDF_TYPE;
                    } else if (CSV_TYPE == iFormat) {
                        iFormat = CSV_TYPE;
                    } else {
                        iFormat = TEXT_TYPE;
                    }
                    RequestTrackingManagement ppw = new RequestTrackingManagement();
                    String strmsg = "No Record Found";
                    RequestTracking[] requesttracks = ppw.getResponseTimebyDuration(sessionId, null, apid, resid, grid, pid, sdate, edate, stime, etime);
                    filepath = ppw.generateReportReadyAPIs(iFormat, sessionId, null, channelName, requesttracks, startDate, endDate, pid);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                File file = new File(filepath);
                int length = 0;
                ServletOutputStream outStream = response.getOutputStream();
                ServletContext context = getServletConfig().getServletContext();
                String mimetype = context.getMimeType(filepath);
                if (mimetype == null) {
                    mimetype = "application/octet-stream";
                }
                response.setContentType(mimetype);
                response.setContentLength((int) file.length());
                String fileName = (new File(filepath)).getName();
                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                byte[] byteBuffer = new byte[BUFSIZE];
                DataInputStream in = new DataInputStream(new FileInputStream(file));
                while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                    outStream.write(byteBuffer, 0, length);
                }
                in.close();
                outStream.close();
                file.delete();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
