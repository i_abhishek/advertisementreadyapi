/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.SGportal.virtual.number.caas;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgCassNumbers;
import com.mollatech.serviceguard.nucleus.db.connector.management.CaaSManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import java.util.Date;

/**
 *
 * @author Ashu
 */
public class CaaSAutoAssign extends Thread {

    int partnerId;
    String sessionId;

    public CaaSAutoAssign(int partnerId, String sessionId) {
        this.partnerId = partnerId;
        this.sessionId = sessionId;
    }

    @Override
    public void run() {

        PartnerDetails details = new PartnerManagement().getPartnerDetails(partnerId);
        String channeld = details.getChannelId();
        CaaSManagement management = new CaaSManagement();
        SgCassNumbers numbers = null;
        boolean existNumber = false;
        if (details.getCaasNumberAssinged() != null) {
            existNumber = true;
        }
        SgCassNumbers numbersS[] = management.listNumberByPartnerId(channeld, partnerId);
        if (numbersS != null && existNumber == false) {
            existNumber = true;
        }
        if (existNumber == false) {
            SgCassNumbers[] numberses = management.getAllFreeNumbersbyEnv(GlobalStatus.ACTIVE, "Test");
            if (numberses != null) {
                numbers = numberses[0];
                numbers.setPartnerId(partnerId);
                numbers.setChannelId(channeld);
                numbers.setUpdatedOn(new Date());
                management.updateDetails(numbers);
                details.setCaasNumberAssinged("Yes");
                details.setCaasNumber("," + numbers.getNumberId() + ",");
                int result = new PartnerManagement().updateDetails(details);
                System.out.println("result " + result);
            }
        }
    }
}
