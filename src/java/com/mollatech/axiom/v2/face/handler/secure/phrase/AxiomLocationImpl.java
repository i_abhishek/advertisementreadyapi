package com.mollatech.axiom.v2.face.handler.secure.phrase;

import axiom.web.service.AllTrustManager;
import axiom.web.service.AllVerifier;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class AxiomLocationImpl {

    public Location getLocationByIp(String ipAddress) {
        try {
            String url = "http://ipinfo.io/" + ipAddress + "/json";
            String sublocality = null;
            String city = null;
            String state = null;
            String country = null;
            String[] response = SendDataInner(url, null, null, true);
            if (response == null) {
                return null;
            }
            if (response[1].startsWith("application/json") == true) {
                JSONObject jsonResponse = new JSONObject(response[0]);
                city = jsonResponse.getString("city");
                state = jsonResponse.getString("region");
                country = jsonResponse.getString("country");
                Location loc = new Location();
                loc.city = city;
                loc.state = state;
                loc.country = country;
                String location = jsonResponse.getString("loc");
                String[] locationList = location.split(",");
                loc.lattitude = locationList[0];
                loc.longitude = locationList[1];
                return loc;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Location getLocation(String latitude, String longitude) {
        try {
            String latlang = URLEncoder.encode(latitude + "," + longitude, "UTF-8");
            String sensor = URLEncoder.encode("false", "UTF-8");
            String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang + "&sensor=" + sensor;
            String[] response = SendDataInner(url, null, null, true);
            if (response == null && response.length <= 0) {
                return null;
            }
            if (response[1].equals("application/json; charset=UTF-8")) {
                JSONObject jsonResponse = new JSONObject(response[0]);
                String jsonStr = jsonResponse.getString("results");
                JSONArray jsonArrayResponse = new JSONArray(jsonStr);
                String json = jsonArrayResponse.getString(0);
                JSONObject jsonFirst = new JSONObject(json);
                JSONArray jArray = jsonFirst.getJSONArray("address_components");
                String area = null;
                String sublocality = null;
                String policality = null;
                String city = null;
                String cityS = null;
                String state = null;
                String country = null;
                String zipcode = null;
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jsonObj = jArray.getJSONObject(i);
                    JSONArray jsonAinner = jsonObj.getJSONArray("types");
                    for (int j = 0; j < jsonAinner.length(); j++) {
                        String jInnerArray = jsonAinner.getString(j);
                        if (jInnerArray.equalsIgnoreCase("locality")) {
                            cityS = jsonObj.getString("long_name");
                        } else if (jInnerArray.equalsIgnoreCase("sublocality")) {
                            sublocality = sublocality + jsonObj.getString("long_name");
                        } else if (jInnerArray.equalsIgnoreCase("administrative_area_level_2")) {
                            city = jsonObj.getString("long_name");
                        } else if (jInnerArray.equalsIgnoreCase("administrative_area_level_1")) {
                            state = jsonObj.getString("long_name");
                        } else if (jInnerArray.equalsIgnoreCase("country")) {
                            country = jsonObj.getString("long_name");
                        } else if (jInnerArray.equalsIgnoreCase("postal_code")) {
                            zipcode = jsonObj.getString("long_name");
                        }
                    }
                }
                if (city == null) {
                    city = cityS;
                }
                Location loc = new Location();
                loc.area = sublocality;
                loc.city = city;
                loc.state = state;
                loc.country = country;
                loc.zipcode = zipcode;
                return loc;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    protected static String[] SendDataInner(String link, String[][] headers, String[][] NameValue, boolean bSecure) throws Exception {
        SSLContext sslContext;
        String[] returnStrs = null;
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[] { new AllTrustManager() }, null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
        } catch (Exception e) {
            throw e;
        }
        URL url = new URL(link);
        String dataToSend = "";
        if (NameValue != null) {
            for (int i = 0; i < NameValue.length; i++) {
                String[] pair = NameValue[i];
                if (i == 0) {
                    dataToSend = URLEncoder.encode(pair[0], "UTF-8") + "=" + URLEncoder.encode(pair[1], "UTF-8");
                } else {
                    dataToSend = dataToSend + "&" + URLEncoder.encode(pair[0], "UTF-8") + "=" + URLEncoder.encode(pair[1], "UTF-8");
                }
            }
        }
        String text = "";
        BufferedReader reader = null;
        try {
            URLConnection conn = url.openConnection();
            if (headers != null) {
                for (int i = 0; i < headers.length; i++) {
                    String[] header = headers[i];
                    conn.setRequestProperty(header[0], header[1]);
                }
            }
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(dataToSend);
            wr.flush();
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            text = sb.toString();
            String contentType = conn.getHeaderField("Content-Type");
            returnStrs = new String[2];
            returnStrs[0] = text;
            returnStrs[1] = contentType;
        } catch (Exception ex) {
        } finally {
            try {
                reader.close();
            } catch (Exception ex) {
            }
        }
        return returnStrs;
    }
}
