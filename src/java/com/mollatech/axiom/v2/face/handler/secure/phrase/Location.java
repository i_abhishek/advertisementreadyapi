package com.mollatech.axiom.v2.face.handler.secure.phrase;

/**
 *
 * @author mollatech2
 */
public class Location {

    @Override
    public String toString() {
        return "Location{" + "area=" + area + ", city=" + city + ", state=" + state + ", country=" + country + ", zipcode=" + zipcode + '}';
    }

    public String area;

    public String city;

    public String state;

    public String country;

    public String zipcode;

    public String lattitude;

    public String longitude;
}
