package com.mollatech.axiom.v2.face.handler.secure.phrase;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jmimemagic.*;
import java.io.OutputStream;
import javax.servlet.annotation.WebServlet;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

@WebServlet(name = "ShowImage", urlPatterns = { "/ShowImage" })
public class ShowImage extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            response.setContentType("image/gif");
            RssUserCerdentials rssUserObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
            String sessionId = (String) request.getSession().getAttribute("_userSessinId");
            String _lattitude = request.getParameter("_lattitude");
            String _longitude = request.getParameter("_longitude");
            JSONObject payload = new JSONObject();
            payload.put("ip", request.getRemoteAddr());
            payload.put("longi", _longitude);
            payload.put("latti", _lattitude);
            payload.put("txType", AxiomWrapper.GETIMAGE);
            byte[] bytePayload = Base64.encode(payload.toString().getBytes());
            String strPayLoad = new String(bytePayload);
            if (_lattitude.equalsIgnoreCase("latitude")) {
                _lattitude = null;
                _longitude = null;
                strPayLoad = null;
            }
            AxiomWrapper axWraper = new AxiomWrapper();
            AxiomCredentialDetails axiomCredentialDetails = null;
            if (rssUserObj.getTokenDetails() != null) {
                for (int i = 0; i < rssUserObj.getTokenDetails().size(); i++) {
                    axiomCredentialDetails = rssUserObj.getTokenDetails().get(i);
                    if (axiomCredentialDetails != null) {
                        if (AxiomWrapper.SECURE_PHRASE == axiomCredentialDetails.getCategory()) {
                            byte[] base64Image = axiomCredentialDetails.getSecurePhraseImage();
                            if (base64Image == null) {
                                return;
                            }
                            byte[] plainImage = Base64.decode(base64Image);
                            MagicMatch match = Magic.getMagicMatch(plainImage, true);
                            String mimeType = match.getMimeType();
                            if (mimeType.startsWith("image")) {
                                response.setContentType(mimeType);
                                response.setContentLength(plainImage.length);
                                OutputStream out = response.getOutputStream();
                                out.write(plainImage, 0, plainImage.length);
                            } else {
                                return;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
