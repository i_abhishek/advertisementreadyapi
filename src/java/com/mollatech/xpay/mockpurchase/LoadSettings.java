package com.mollatech.xpay.mockpurchase;

import java.io.File;
import java.util.Date;
import java.util.Properties;

public class LoadSettings {

    public static Properties g_sSettings = null;

    public static Properties g_eSettings = null;

    public static String e_strPath = null;

    public static String g_strPath = null;

    public static Properties g_templateSettings = null;

    public static String g_templatestrPath = null;

    public static Properties g_subjecttemplateSettings = null;

    public static String g_subjecttemplatestrPath = null;

    static {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = System.getenv("SGHOME");
        }
        if (usrhome == null) {
            String path = sep + "root" + sep + "worker";
            File f = new File(path);
            if (f.exists() && f.isDirectory()) {
                usrhome = path;
            } else {
                path = sep + "sguard" + sep + "worker";
                f = new File(path);
                if (f.exists() && f.isDirectory()) {
                    usrhome = path;
                } else {
                    path = sep + "app" + sep + "worker";
                    f = new File(path);
                    if (f.exists() && f.isDirectory()) {
                        usrhome = path;
                    } else {
                        path = "C:\\Users\\Ash\\Desktop\\apache-tomcat-8.0.24";
                        f = new File(path);
                        if (f.exists() && f.isDirectory()) {
                            usrhome = path;
                        }
                    }
                }
            }
        }
        if (usrhome == null) {
            usrhome = "/home/axiom/admin";
        }
        usrhome += sep + "serviceguard-settings";
        g_strPath = usrhome + sep;
        String filepath = usrhome + sep + "dbsetting.conf";
        PropsFileUtil p = new PropsFileUtil();
        if (p.LoadFile(filepath) == true) {
            g_sSettings = p.properties;
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "dbsetting setting file failed to load >> " + filepath);
        }
        g_templatestrPath = usrhome + sep + "templates";
        filepath = g_templatestrPath + sep + "templates.conf";
        p = new PropsFileUtil();
        if (p.LoadFile(filepath) == true) {
            g_templateSettings = p.properties;
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "templates setting file failed to load >> " + filepath);
        }
        g_subjecttemplatestrPath = usrhome + sep + "templates";
        filepath = g_subjecttemplatestrPath + sep + "subject.templates.conf";
        p = new PropsFileUtil();
        if (p.LoadFile(filepath) == true) {
            g_subjecttemplateSettings = p.properties;
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "subject templates setting file failed to load >> " + filepath);
        }
    }

    public static void LoadManually(String path) {
        DBSettingManual(path);
    }

    private static void DBSettingManual(String path) {
        String sep = System.getProperty("file.separator");
        String usrhome = path;
        usrhome += sep + "serviceguard-settings";
        g_strPath = usrhome + sep;
        String filepath = usrhome + sep + "dbsetting.conf";
        PropsFileUtil p = new PropsFileUtil();
        if (p.LoadFile(filepath) == true) {
            g_sSettings = p.properties;
        } else {
            System.out.println(new Date() + ">>" + "manually loaded dbsetting setting file failed to load >> " + filepath);
        }
    }
}
