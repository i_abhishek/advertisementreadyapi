//package com.mollatech.xpay.mockpurchase;
//
//import com.mollatech.xpay.pathmazing.XpayResponse;
//import com.mollatech.xpay.pathmazing.XpayTransaction;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.security.cert.X509Certificate;
//import java.util.Date;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import org.json.JSONObject;
//
///**
// *
// * @author mohanish
// */
//@WebServlet(name = "OpenSessionPurchase", urlPatterns = { "/OpenSessionPurchase" })
//public class OpenSessionPurchase extends HttpServlet {
//
//    /**
//     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
//     * methods.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        response.setContentType("application/json");
//        X509Certificate[] certs = (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");
//        if (null != certs && certs.length > 0) {
//            System.out.println("Null");
//        }
//        PrintWriter out = response.getWriter();
//        Date d = new Date();
//        String txid = request.getParameter("txnid");
//        String paymentTokenID = null;
//        String sessionID = null;
//        String channelid = LoadSettings.g_sSettings.getProperty("mock.purchase.channelid");
//        java.lang.String loginId = LoadSettings.g_sSettings.getProperty("mock.purchase.loginId");
//        java.lang.String password = LoadSettings.g_sSettings.getProperty("mock.purchase.password");
//        String merchantID = LoadSettings.g_sSettings.getProperty("mock.purchase.merchantID");
//        java.lang.String signature = LoadSettings.g_sSettings.getProperty("mock.purchase.signature");
//        String paymentTokenId = LoadSettings.g_sSettings.getProperty("mock.purchase.paymentTokenId");
//        try {
//            String description = request.getParameter("description");
//            String amount = request.getParameter("amount");
//            String quanitiy = request.getParameter("quantity");
//            String invoiceid = request.getParameter("invoiceid");
//            String currencytype = request.getParameter("currencytype");
//            XpayTransaction xtransa = new XpayTransaction();
//            xtransa.setTxid(txid);
//            xtransa.setPurchaseAmount(Double.parseDouble(amount));
//            xtransa.setPurchaseDesc(description);
//            xtransa.setExpiryTime(80);
//            xtransa.setQuantity(Integer.parseInt(quanitiy));
//            xtransa.setInvoiceid(invoiceid);
//            xtransa.setPurchaseDate(new Date().getTime());
//            xtransa.setPurchaseCurrency(currencytype);
//            xtransa.setPaymentTokenid(paymentTokenId);
//            XpayResponse result = XPAYConnector.openSessionV2(channelid, loginId, password, merchantID, signature, null, xtransa);
//            System.out.println("Paymention token = " + result.getSessionid());
//            sessionID = result.getSessionid();
//            paymentTokenID = result.getXTran().getPaymentTokenid();
//            JSONObject jsonobj = new JSONObject();
//            jsonobj.put("sessionId", sessionID);
//            jsonobj.put("ptk", paymentTokenID);
//            out.print(jsonobj);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Handles the HTTP <code>POST</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Returns a short description of the servlet.
//     *
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }
//}
