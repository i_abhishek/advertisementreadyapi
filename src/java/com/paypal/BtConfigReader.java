package com.paypal;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BtConfigReader {

    private String environment;
    private String btMerchantKey;
    private String btPublicKey;
    private String btPrivateKey;
    private String btChannel;
    private String btJavaScriptClient;
    private String btJavaScriptHostedFields;
    private String btJavaScriptPayPal;
	
	public BtConfigReader() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			String filename = "config/config.properties";
			
			input = this.getClass().getClassLoader().getResourceAsStream(filename);
			
			if (input == null) {
				System.out.println("Sorry, unable to find  "+ filename);
				return;
			}

			// load a properties file from class path, inside static method
			prop.load(input);
			
			// get environment
			String envPrefix = "LIVE_";
			environment = "production";
			
			if (prop.getProperty("SANDBOX_FLAG").equals("true")) {
				envPrefix="SANDBOX_";
        		environment = "sandbox";
			}
	        
			// get property values from config.properties file based on environment
			btMerchantKey = prop.getProperty(envPrefix + "BT_MERCHANT_ID");
			btPublicKey = prop.getProperty(envPrefix + "BT_PUBLIC_KEY");
			btPrivateKey = prop.getProperty(envPrefix + "BT_PRIVATE_KEY");
			
			// general values
			btChannel	= prop.getProperty("SBN_CODE");
			btJavaScriptClient	= prop.getProperty("BT_JAVASCRIPT_CLIENT");
			btJavaScriptHostedFields	= prop.getProperty("BT_JAVASCRIPT_HOSTED_FIELDS");
			btJavaScriptPayPal	= prop.getProperty("BT_JAVASCRIPT_PAYPAL");

		} 
		catch (IOException e) {
			e.printStackTrace();
		} 
		finally {
	    	if (input!=null){
	    		try {
	    			input.close();
	    		} 
	    		catch (IOException e) {
	    			e.printStackTrace();
	    		}
	    	}
		}
	}

	
	public String getEnvironment() {
		return environment;
	}
	
	public void setEnvironment(String environment) {
		this.environment = environment;
	}	
	
	public String getBtMerchantKey() {
		return btMerchantKey;
	}
	
	public String getBtPublicKey() {
		return btPublicKey;
	}
	
	public String getBtPrivateKey() {
		  return btPrivateKey;
	}
	
	public String getBtChannel() {
		return btChannel;
	}
	
	public String getBtJavaScriptClient() {
		return btJavaScriptClient;
	}
		
	public String getBtJavaScriptHostedFields() {
		return btJavaScriptHostedFields;
	}
	
	public String getBtJavaScriptPayPal() {
		return btJavaScriptPayPal;
	}
	

}
