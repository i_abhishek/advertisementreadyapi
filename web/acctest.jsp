<%@include file="header.jsp"%>
<div id="questionAndAnswerREST">
                            <div class="hpanel panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel-body">
                                    <h4 class="m-t-none m-b-none">Need Help </h4>
                                    <small class="text-muted">All general questions about our API.</small>&nbsp;&nbsp;&nbsp;
                                    <a  class="btn btn-primary btn-xs ladda-button" data-style="zoom-in" type="button" href="./compose_email_technical.jsp" style="margin-left: 1px" target="_blank"><i class="fa fa-check"></i> Raise Ticket</a> <br>
                                </div>
                                <div class="panel-body" id="_apiLevelTokenRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q1" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Where can I get APITokenId?
                                    </a>
                                    <div id="q1" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div>                                                               <!--
-->                             <div class="panel-body" id="_headerErrorRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q2" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Why need Headers and How to add?
                                    </a>
                                    <div id="q2" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div>
                                <div class="panel-body" id="connectionrefuseRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q3" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Connection refused (Connection refused)?
                                    </a>
                                    <div id="q3" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
                                
-->                                <div class="panel-body" id="_noPartnerAvailableRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q4" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        No Such Partner is Available.
                                    </a>
                                    <div id="q4" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                             <div class="panel-body">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q5" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        No Such Webservice is Available.
                                    </a>
                                    <div id="q5" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_groupDisableRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q6" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Your Associated Group Is Not Active. Contact Administrator.
                                    </a>
                                    <div id="q6" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
                                --> <div class="panel-body" id="_resourseUnAvaiRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q7" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Internal Resource IS Not Active. Contact Administrator.
                                    </a>
                                    <div id="q7" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
                                
                                --><div class="panel-body" id="_accDiasableRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q8" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Your Account Is Not Active. Contact Administrator.
                                    </a>
                                    <div id="q8" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_partnerDisableRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q9" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Partner Is Not Active.
                                    </a>
                                    <div id="q9" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="initializeApiTokenRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q10" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Service Guard Error: Please Initialise API Level Token.
                                    </a>
                                    <div id="q10" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_parameterMismatchRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q11" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Service Guard Error: Parameter Mismatch
                                    </a>
                                    <div id="q11" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div>
                                
                                <div class="panel-body">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q12" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Service Guard Error: Please Contact Administrator.
                                    </a>
                                    <div id="q12" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div><!--
-->                             </div><!--
-->                                <div class="panel-body" id="_unAuthorizedPartRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q13" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        You Are Not Autherized Partner.
                                    </a>
                                    <div id="q13" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_reachedLimitRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q14" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Service Guard Error: You have reached your Daily limit.
                                    </a>
                                    <div id="q14" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
                                
-->                                <div class="panel-body" id="_transactionsecRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q15" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Service Guard Error: Transaction Per Second Error.
                                    </a>
                                    <div id="q15" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
                                --><div class="panel-body" id="_accessinvalidIpRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q16" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                         You are accessing from invalid IP.
                                    </a>
                                    <div id="q16" class="panel-collapse collapse">
                                        <hr>please check if you are behind proxy or using dynamic IP.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_serNotaccesibleRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q17" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        You cannot access service now as it is not permitted for this day in the week.
                                    </a>
                                    <div id="q17" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="serNOtPermitedRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q18" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        You cannot access service now as it is not permitted for this time of the day.
                                    </a>
                                    <div id="q18" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="invalidCertRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q20" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        You are offering invalid/empty certificate.
                                    </a>
                                    <div id="q20" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_invalidTokenRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q21" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                       You are offering invalid/empty Token.
                                    </a>
                                    <div id="q21" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div>
                                <div class="panel-body" id="_ipblockRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q22" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        IP BLOCK.
                                    </a>
                                    <div id="q22" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div>
                                <div class="panel-body" id="_invalidTokenRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q23" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        You are offering invalid/empty Token For API.
                                    </a>
                                    <div id="q23" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                             <div class="panel-body" id="_signFailedRest" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q24" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Signature Verification Failed.
                                    </a>
                                    <div id="q24" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div>
                                <div class="panel-body" id="_operationTimeOutRest">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q25" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Operation timed out (Connection timed out).
                                    </a>
                                    <div id="q25" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div>    
<!--                            </div>                            -->
                            </div>
                        </div>
 <%@include file="footer.jsp"%>