<%@include file="header.jsp"%> 

<!-- Main Wrapper -->
<div id="wrapper">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Ala Carte</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Ala Carte
                </h2>
                <small>You may subscribe API server by ala-carte</small>
            </div>
        </div>
    </div>

<div class="content animate-panel">

    <div class="row">
        <div class="col-lg-12">

            <div class="text-center m-b-xl">
                <h3>Available APIs</h3>
            </div>

            <hr class="m-b-xl"/>
            <div class="row">
            <% 
                String apiPreferred = productObj.getPreferedResource();                
                boolean recordFound = true;
                if(apiPreferred != null){
                    int count = 0;                    
                    String[] apiList = apiPreferred.split(",");
                    if(apiList != null){ 
                        for(int i=0; i< apiList.length; i++){
                            String apiName = apiList[i];
            %>
            
                <%if(apiName.equalsIgnoreCase("sms")){%>
                <div class="col-sm-3">
                    <div class="hpanel plan-box">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">SMS</h4>
                        </div>
                        <div class="panel-body text-center">
                            <i class="pe pe-7s-phone big-icon text-warning"></i>
                            <h4 class="font-bold">
                                RM 0.12/sms
                            </h4>

                            <p class="text-muted">
                                Send and receive SMS to mobile numbers with virtual TM number (xxxxxxx). Able to support two (2) way communication.
                            </p>
                            <a href="ala-carte-package.jsp?api=sms" class="btn btn-danger btn-sm">Processed</a>
                        </div>
                    </div>
                </div>
                <%      count++;
                        recordFound = false;
                        continue;
                    }else if(apiName.equalsIgnoreCase("maps")){
                %>
                <div class="col-sm-3">
                    <div class="hpanel plan-box">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Map</h4>
                        </div>
                        <div class="panel-body text-center">
                            <i class="pe pe-7s-map-marker big-icon text-success"></i>
                            <h4 class="font-bold">
                                RM 0.02/page/view
                            </h4>

                            <p class="text-muted">
                                A map-based API provided through Internet service that allow customer to embed TM map into their web based or client application.
                            </p>
                            <a href="ala-carte-package.jsp?api=maps" class="btn btn-danger btn-sm">Processed</a>
                        </div>
                    </div>
                </div>
                <%      count++;
                        recordFound = false ;
                        continue;
                    }else if(apiName.equalsIgnoreCase("multifactorauth")){
                %>
                <div class="col-sm-3">
                    <div class="hpanel plan-box">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">MFA</h4>
                        </div>
                        <div class="panel-body text-center">
                            <i class="pe pe-7s-key big-icon text-info"></i>
                            <h4 class="font-bold">
                                RM 150/mth/unlimited
                            </h4>

                            <p class="text-muted">
                                A method of computer access control which a user can pass by successfully presenting several separate authentication stages. Support for user ID, password, One Time Password (OTP), geographic location and others.
                            </p>
                            <a href="ala-carte-package.jsp?api=multifactorauth" class="btn btn-danger btn-sm">Processed</a>
                        </div>
                    </div>
                </div>
                <%    count++;  
                      recordFound = false ;
                      continue;
                    }else if(apiName.equalsIgnoreCase("digitalsign")){  
                %>
                <div class="col-sm-3">
                    <div class="hpanel plan-box">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Digital Signing</h4>
                        </div>
                        <div class="panel-body text-center">
                            <i class="pe pe-7s-note big-icon text-danger"></i>
                            <h4 class="font-bold">
                                RM 150/mth/unlimited
                            </h4>
                            <p class="text-muted">
                                Increase trust by utilizing digital signing for communication need. Able to digitally sign documents, so recipients can confirm the validity of the received documents without any tampering.
                            </p>
                            <a href="ala-carte-package.jsp?api=digitalsign" class="btn btn-danger btn-sm">Processed</a>
                        </div>
                    </div>
                </div>
                <%
                      count++;  
                      recordFound = false ;
                      continue;
                }%>

                <%if(apiName.equalsIgnoreCase("CAAS")){%>
                <div class="col-sm-3">
                    <div class="hpanel plan-box">
                         <div class="panel-heading hbuilt text-center">
                             <h4 class="font-bold">CAAS</h4>
                         </div>
                         <div class="panel-body text-center">
                             <i class="pe pe-7s-monitor big-icon text-warning"></i>
                             <h4 class="font-bold">
                                 RM 0.03/minute
                             </h4>
                             <p class="text-muted">
                                 Basic APIs provide basic telecommunications functions, such as voice, video, and conference.
                             </p>
                             <a href="ala-carte-package.jsp?api=caas" class="btn btn-warning btn-sm">Processed</a>
                         </div>
                    </div>
                </div>    
            </div>
            <%  count++;
                recordFound = false ;
                }
                        }
                    }
                }
                if(recordFound){
            %>
                <div class="col-md-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <i class="pe-7s-way text-success big-icon"></i>
                            <h1>Not found</h1>
                            <strong>No package found</strong>
                            <p>
                                Sorry, but the package list as per your production request has not been found.

                            </p>
                            <a href="#" class="btn btn-xs btn-success ladda-button" data-style="zoom-in" id="previousButton" onclick="goBack()">Go back to previous page</a>
                        </div>
                    </div>
                </div>
            
            <%}%>
        </div>
    </div>
</div>
    <!-- Footer-->
    <%@include file="footer.jsp"%>
</div>
