<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MSConfig"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement"%>
<%@include file="header.jsp"%>
<script src="scripts/reports.js" type="text/javascript"></script>
<script src="vendor/toastr/build/toastr.min.js"></script>
<script src="vendor/ladda/dist/spin.min.js"></script>
<script src="vendor/ladda/dist/ladda.min.js"></script>
<script src="vendor/ladda/dist/ladda.jquery.min.js"></script>

<!--<script src="scripts/apiCallReport.js" type="text/javascript"></script>-->
<script src="scripts/apiCallReportV2.js" type="text/javascript"></script>
<script src="scripts/operatorsTextReports.js" type="text/javascript"></script>
<%    PartnerDetails pdetails = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
%>
<!-- Main Wrapper -->
<div id="wrapper">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">                            
                            <span>API Call Report</span>                            
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">                    
                    API Call Report                    
                </h2>
                <small>Analyse Your API Call By Month Period</small>
            </div>
        </div>
    </div>
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        Choose API And Timeframe To Generate Report
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-sm-2">
                                <select class="form-control" id="_resource" name="_resource" onchange="apiCallReportAccesspoint(this.value)">
                                    <option value="-1" selected>Select</option>
                                    <%
                                        String aps = ",";
                                        Accesspoint Res[] = null;
                                        Res = new AccessPointManagement().getAllAccessPoint(SessionId, ChannelId);
                                        if (Res != null) {
                                            int count = 0;
                                            for (int i = 0; i < Res.length; i++) {
                                                if (Res[i].getStatus() == GlobalStatus.ACTIVE && Res[i].getGroupid() == Integer.parseInt(parObj.getPartnerGroupId())) {
                                                    int ap = Res[i].getApId();
                                                    Accesspoint apdetails = Res[i];
                                                    if (apdetails != null) {
                                                        int resID = -1;
                                                        String resourceList = apdetails.getResources();
                                                        String[] resids = resourceList.split(",");
                                                        for (int j = 0; j < resids.length; j++) {
                                                            if (!resids[j].isEmpty() && resids[j] != null) {
                                                                resID = Integer.parseInt(resids[j]);
                                                                ResourceDetails rsName = new ResourceManagement().getResourceById(SessionId, ChannelId, resID);
                                                                TransformDetails tmdetail = new TransformManagement().getTransformDetails(SessionId, ChannelId, apdetails.getApId(), resID);
                                                                if (rsName != null && tmdetail != null) {
                                                                    int g = Integer.parseInt(pdetails.getPartnerGroupId());
                                                                    if (g == apdetails.getGroupid()) {
                                                                        if (!aps.contains("," + apdetails.getName() + ",")) {
                                                                            aps += apdetails.getName() + ",";
                                                                            count++;
                                    %>
                                    <option value="<%=apdetails.getName()%>:<%=rsName.getName()%>"><%=apdetails.getName()%></option>

                                    <%                                  }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    %>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <select id="_VersionForAPICallReport" name="_VersionForAPICallReport"  class="form-control" onchange="apiCallReportAPI(this.value)">
                                    <option value="-1" selected>Select Version</option>                                                                                  
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <select id="_APIForAPICallReport" name="_APIForAPICallReport"  class="form-control">
                                    <option value="-1" selected>Select API</option>                                                                                  
                                </select>
                            </div>     
                            <div class="col-sm-2">
                                <select class="form-control" id="_apiCallMonth" name="_apiCallMonth">
                                    <option value="Jan">Jan</option>
                                    <option value="Feb">Feb</option>
                                    <option value="Mar">Mar</option>
                                    <option value="Apr">Apr</option>
                                    <option value="May">May</option>
                                    <option value="Jun">Jun</option>
                                    <option value="Jul">Jul</option>
                                    <option value="Aug">Aug</option>
                                    <option value="Sep">Sep</option>
                                    <option value="Oct">Oct</option>
                                    <option value="Nov">Nov</option>
                                    <option value="Dec">Dec</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control" id="_apiCallYear" name="_apiCallYear">
                                    <%
                                        SimpleDateFormat format = new SimpleDateFormat("yyyy");
                                        for (int year = (Integer.parseInt(format.format(new Date())) - 10); year <= Integer.parseInt(format.format(new Date())); year++) {%>
                                    <option value="<%=year%>"><%=year%></option>
                                    <% }

                                    %>
                                </select>
                                <script>
                                    document.getElementById('_apiCallYear').value = '<%=format.format(new Date())%>';
                                </script>
                            </div>
                            <div class="col-sm-2"><button id="generatePerformanceByMonth" class="btn btn-success btn-sm ladda-button btn-block" data-style="zoom-in" onclick="generateAPIPerformanceByMonth()"><i class="fa fa-bar-chart"></i> Generate</button></div>
                        </div>

                        <hr class="m-b-xl"/>
                        <div  id="report_data">
                        </div>                        
                        <hr class="m-b-xl"/>

                        <div class="col-md-12" id="report">
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <jsp:include page="footer.jsp" />               
    </div>
</div>

<script>
    $(function () {
        // Initialize Example 2
        //$('#api').dataTable();
    });
    $(function () {
        // Initialize Example 2
        //$('#data_type').dataTable();
    });
</script>
<script>

    $(function () {
        document.getElementById("report").style.display = "none";


    });

</script>
