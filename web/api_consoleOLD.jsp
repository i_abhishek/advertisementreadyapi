<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp"%> 
<%    
    Integer smsAccessPointId = 0;
    Integer smsResourceId = 0;

    Integer mapAccessPointId = 0;
    Integer mapResourceId = 0;

    Integer mfaAccessPointId = 0;
    Integer mfaResourceId = 0;

    Integer dsAccessPointId = 0;
    Integer dsResourceId = 0;
    
    Integer caasAccessPointId = 0;
    Integer caasResourceId  = 0;

    Accesspoint[] accesspoints1 = null;
    Warfiles warfiles = null;
    accesspoints1 = new AccessPointManagement().getAllAccessPoint(SessionId, ChannelId);
    if (accesspoints1 != null) {
        for (int i = 0; i < accesspoints1.length; i++) {
            if (accesspoints1[i].getStatus() != GlobalStatus.DELETED) {
                Accesspoint apdetails = accesspoints1[i];
                TransformDetails transformDetails = new TransformManagement().getTransformDetails(SessionId, ChannelId, apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                if (transformDetails != null) {
                    warfiles = new WarFileManagement().getWarFile(SessionId, ChannelId, apdetails.getApId());
                    if (warfiles != null) {
                        String acesspointName = apdetails.getName();
                        if (acesspointName != null && acesspointName.equalsIgnoreCase("multifactorauth")) {
                            mfaAccessPointId = apdetails.getApId();
                        }
                        if (acesspointName != null && acesspointName.equalsIgnoreCase("maps")) {
                            mapAccessPointId = apdetails.getApId();
                        }
                        if (acesspointName != null && acesspointName.equalsIgnoreCase("sms")) {
                            smsAccessPointId = apdetails.getApId();
                        }
                        if (acesspointName != null && acesspointName.equalsIgnoreCase("digitalsign")) {
                            dsAccessPointId = apdetails.getApId();
                        }
                        if(acesspointName != null && acesspointName.equalsIgnoreCase("Caas")) {
                            caasAccessPointId = apdetails.getApId();
                        }
                        String resources = apdetails.getResources();
                        if (resources != null) {
                            String[] resourcesDetails = resources.split(",");
                            if (resourcesDetails != null) {
                                for (int rs = 0; rs < resourcesDetails.length; rs++) {
                                    int resourceId = Integer.parseInt(resourcesDetails[rs]);
                                    ResourceDetails rsDe = new ResourceManagement().getResourceById(resourceId);
                                    if (rsDe != null && rsDe.getName().equalsIgnoreCase("multifactorauth")) {
                                        mfaResourceId = rsDe.getResourceId();
                                    }
                                    if (rsDe != null && rsDe.getName().equalsIgnoreCase("maps")) {
                                        mapResourceId = rsDe.getResourceId();
                                    }
                                    if (rsDe != null && rsDe.getName().equalsIgnoreCase("infoblast")) {
                                        smsResourceId = rsDe.getResourceId();
                                    }
                                    if (rsDe != null && rsDe.getName().equalsIgnoreCase("digitalsign")) {
                                        dsResourceId = rsDe.getResourceId();
                                    }
                                    if (rsDe != null && rsDe.getName().equalsIgnoreCase("CaasClicktocall")) {
                                        caasResourceId = rsDe.getResourceId();
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
    }
%>
<!-- Main Wrapper -->
<div id="wrapper">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>API Console</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    API Console
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center m-b-xl">
                    <h3>Available APIs</h3>
                </div>
                <hr class="m-b-xl"/>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="hpanel plan-box">
                            <div class="panel-heading hbuilt text-center">
                                <h4 class="font-bold">SMS</h4>
                            </div>
                            <div class="panel-body text-center">
                                <i class="pe pe-7s-phone big-icon text-warning"></i>
                                <h4 class="font-bold">
                                    RM 0.12/sms
                                </h4>

                                <p class="text-muted">
                                    Send and receive SMS to mobile numbers with virtual TM number (xxxxxxx). Able to support two (2) way communication.
                                </p>
                                <a href="api_console_access.jsp?_apid=<%=smsAccessPointId%>&_resId=<%=smsResourceId%>&_envt=Test&ver=1" class="btn btn-warning btn-sm">Select SMS API</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="hpanel plan-box">
                            <div class="panel-heading hbuilt text-center">
                                <h4 class="font-bold">Map</h4>
                            </div>
                            <div class="panel-body text-center">
                                <i class="pe pe-7s-map-marker big-icon text-success"></i>
                                <h4 class="font-bold">
                                    RM 0.02/page/view
                                </h4>

                                <p class="text-muted">
                                    A map-based API provided through Internet service that allow customer to embed TM map into their web based or client application.
                                </p>
                                <a href="api_console_access.jsp?_apid=<%=mapAccessPointId%>&_resId=<%=mapResourceId%>&_envt=Test&ver=1" class="btn btn-success btn-sm">Select Map API</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="hpanel plan-box">
                            <div class="panel-heading hbuilt text-center">
                                <h4 class="font-bold">MFA</h4>
                            </div>
                            <div class="panel-body text-center">
                                <i class="pe pe-7s-key big-icon text-info"></i>
                                <h4 class="font-bold">
                                    RM 150/mth/unlimited
                                </h4>

                                <p class="text-muted">
                                    A method of computer access control which a user can pass by successfully presenting several separate authentication stages. Support for user ID, password, One Time Password (OTP), geographic location and others.
                                </p>
                                <a href="api_console_access.jsp?_apid=<%=mfaAccessPointId%>&_resId=<%=mfaResourceId%>&_envt=Test&ver=1" class="btn btn-info btn-sm">Select MFA API</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="hpanel plan-box">
                            <div class="panel-heading hbuilt text-center">
                                <h4 class="font-bold">Digital Signing</h4>
                            </div>
                            <div class="panel-body text-center">
                                <i class="pe pe-7s-note big-icon text-danger"></i>
                                <h4 class="font-bold">
                                    RM 150/mth/unlimited
                                </h4>

                                <p class="text-muted">
                                    Increase trust by utilizing digital signing for communication need. Able to digitally sign documents, so recipients can confirm the validity of the received documents without any tampering.
                                </p>
                                <a href="api_console_access.jsp?_apid=<%=dsAccessPointId%>&_resId=<%=dsResourceId%>&_envt=Test&ver=1" class="btn btn-danger btn-sm">Select Digital Signing API</a>
                            </div>
                        </div>
                    </div>                         
                </div>
                 <div class="col-sm-3">
                        <div class="hpanel plan-box">
                            <div class="panel-heading hbuilt text-center">
                                <h4 class="font-bold">CAAS</h4>
                            </div>
                            <div class="panel-body text-center">
                                <i class="pe pe-7s-monitor big-icon text-warning"></i>
                                <h4 class="font-bold">
                                    RM 0.03/minute
                                </h4>

                                <p class="text-muted">
                                    Basic APIs provide basic telecommunications functions, such as voice, video, and conference.
                                </p>
                                <a href="api_console_access.jsp?_apid=<%=caasAccessPointId%>&_resId=<%=caasResourceId%>&_envt=Test&ver=1" class="btn btn-warning btn-sm">Select CAAS API</a>
                            </div>
                        </div>
                    </div>              
            </div>
        </div>


    </div>

    <!-- Footer-->
    <%@include file="footer.jsp"%>