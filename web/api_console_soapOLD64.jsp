<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MSConfig"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.AccessPolicy"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@include file="header.jsp"%>
<script src="scripts/apiTest.js" type="text/javascript"></script>
<script src="scripts/tokenManagement.js" type="text/javascript"></script>
<%    
    String _resId = request.getParameter("_resId");
    int resId = Integer.parseInt(_resId);
    //String value = request.getParameter("value");
    String _apid = request.getParameter("_apid");
    String envmt = request.getParameter("envt");
    int apId = Integer.parseInt(_apid);
    String mName = request.getParameter("methodName");
    
    String requestType = request.getParameter("type");
    String restType = request.getParameter("restType");
    
    Accesspoint ap = new AccessPointManagement().getAccessPointById(SessionId, ChannelId, apId);
    String apName = ap.getName();
    TransformDetails tf = new TransformManagement().getTransformDetails(SessionId, ChannelId, apId, resId);
    Warfiles warFiles = new WarFileManagement().getWarFile(SessionId, ChannelId, ap.getApId());
    Map map = (Map) Serializer.deserialize(warFiles.getWfile());
    ResourceDetails details = new ResourceManagement().getResourceById(Integer.parseInt(_resId));
    String ver = "1";

    for (Object key : map.keySet()) {
        if (key.toString().split(":")[1].equalsIgnoreCase("Running")) {
            ver = key.toString().split(":")[0].replace("SB", "");
        }
    }
    Map methodMap  = (Map)request.getSession().getAttribute("apiConsolemethods");
    Methods methods = (Methods)methodMap.get(apName);

    int version = Integer.parseInt(ver);
%>
<!-- Main Wrapper -->
<div id="wrapper">	
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li><a href="api_console.jsp">API Console</a></li>
                        <li><a href="api_console_access.jsp"><%=apName%></a></li>
                        <li class="active">
                            <span>Console</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Console for <%=mName%>
                </h2>
                <small>Try the api </small>
            </div>
        </div>
    </div>	
    <div class="normalheader transition animated fadeIn">	
        <%if( envmt.equalsIgnoreCase("test") || (envmt.equalsIgnoreCase("Production") && productObj != null && productObj.getStatus() == GlobalStatus.APPROVED)){%>
        <div class="col-lg-12">
            <div class="col-md-9">
                <a href="https://developer.tm.com.my/APIDoc/apidocs.jsp#accessPointSMS" class="btn btn-success m-b-lg run-tour" target="_blank">Documentation</a>
                <a href="compose_email_technical.jsp" class="btn btn-danger m-b-lg run-tour" target="_blank">Helpdesk</a>
                <a href="compose_production.jsp" class="btn btn-primary m-b-lg run-tour">Subscribe to Production</a>
            </div>
            <form class="form-horizontal" id="apiConsoleData" name="apiConsoleData" role="form">
                <input type="hidden" name="_resId" id="_resId" value="<%=_resId%>">
                <input type="hidden" name="_apid" id="_apid" value="<%=_apid%>">
                <input type="hidden" name="envt" id="envt" value="<%=envmt%>">
                <input type="hidden" name="type" id="type" value="<%=requestType%>">
                <input type="hidden" name="restType" id="restType" value="<%=restType%>">                
            </form>
            <div class="col-md-3 pull-right">
                <div class="form-group">
                    <select class="js-source-states form-control" style="width: 100%"  onchange="getAPIConsoleWindow(this.value)">
                        <optgroup label="Select API End Point">
                            <option value="<%=mName%>" selected><%=mName%></option>
                        <%
                            String apiName = "";
                            List list = methods.methodClassName.methodNames;                             
                            for (int i = 0; i < list.size(); i++) {                                
                                MethodName methodName = (MethodName) list.get(i);
                                if (methodName.visibility.equalsIgnoreCase("yes")) {                                    
                                    apiName = methodName.methodname.split("::")[0];
                                    if (!methodName.transformedname.equals("")) {
                                        apiName = methodName.transformedname.split("::")[0];
                                    }
                                    if(apiName.equalsIgnoreCase(mName)){
                                        continue;
                                    }
                        %>                                                    
                            <option value="<%=apiName%>"><%=apiName%></option>                            
                        <%
                                }
                            }
                        %>
                        </optgroup>                        
                    </select>
                    <!-- Jack Remark: Shortcut to open SOAP/RESTFUL Console when select from list so that developer no need go back previous page -->
                </div>
            </div>
        </div>
        <%}%>
        <div id="apiConsoleWindow">
            
        </div>                
        <form class="form-horizontal" id="assign_token_form" name="assign_token_form" role="form">
            <input type="hidden" name="apName" id="apName" value="<%=apName%>">
            <input type="hidden" name="resourceName" id="resourceName" value="<%=details.getName()%>">
            <input type="hidden" name="version" id="version" value="<%=version%>">
        </form>
        <script>
            getAPIConsoleWindow('<%=mName%>');
        </script>
    </div>
                                          
    <!-- Footer-->
    <%@include file="footer.jsp"%>