<%@include file="header.jsp"%> 

<!-- Main Wrapper -->
<div id="wrapper">

	<div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
						<li><a href="api_console.jsp">API Console</a></li>
						<li><a href="api_console_access.jsp">SMS</a></li>
                        <li class="active">
                            <span>Data Type</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    API Console - Data Type
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>


<div class="content animate-panel">

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    Service Description
                </div>
                <div class="panel-body">
                <table id="api" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
					<th></th>
                    <th></th>
					<th>Description</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Class Name</td>
                    <td>SgHttpStatus</td>
					<td></td>
                </tr>
                <tr>
                    <td>Input Parameter</td>
                    <td>Object responseResult</td>
					<td></td>
                </tr>
                <tr>
                    <td>Input Parameter</td>
                    <td>int responseCode</td>
					<td></td>
                </tr>
                </tbody>
                </table>

                </div>
            </div>
        </div>
		
    </div>

    <%@include file="footer.jsp"%> 


<script>

    $(function () {

        // Initialize Example 2
        $('#api').dataTable();

    });
	
	$(function () {

        // Initialize Example 2
        $('#data_type').dataTable();

    });

</script>