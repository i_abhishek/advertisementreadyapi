<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp"%> 
<script src="scripts/billingReport.js" type="text/javascript"></script>
<!-- Main Wrapper -->
<div id="wrapper">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>API Usage</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    API Usage
                </h2>
                <small>Know your API usage</small>
            </div>
        </div>
    </div>
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        Check My API Usage
                    </div>
                    <div class="panel-body">
                        <form id="api_usage" class="form-horizontal">
                            <div class="col-md-12">
                                <div class="col-sm-2">
                                    <select class="form-control m-b" name="apiUsageEnvironment" id="apiUsageEnvironment">
                                        <!--<option value="0">All</option>-->
                                        <option value="2">Production</option>
                                    </select>
                                </div>						
                                <div class="col-sm-3">
                                    <select class="js-source-states-2" id="apiUsageAccesspoint" name="apiUsageAccesspoint" multiple="multiple" style="width: 100%">                                                        
                                        <%
                                            Accesspoint[] accesspoints1 = null;
                                            Warfiles warfiles = null;
                                            accesspoints1 = new AccessPointManagement().getAllAccessPoint(SessionId, ChannelId);
                                            if (accesspoints1 != null) {
                                                for (int i = 0; i < accesspoints1.length; i++) {
                                                    if (accesspoints1[i].getStatus() == GlobalStatus.ACTIVE && accesspoints1[i].getGroupid() == Integer.parseInt(parObj.getPartnerGroupId())) {
                                                        Accesspoint apdetails = accesspoints1[i];
                                                        TransformDetails transformDetails = new TransformManagement().getTransformDetails(SessionId, ChannelId, apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                                                        if (transformDetails != null) {
                                                            warfiles = new WarFileManagement().getWarFile(SessionId, ChannelId, apdetails.getApId());
                                                            if (warfiles != null) {%>
                                        <option value="<%=apdetails.getName()%>"><%=apdetails.getName()%></option>
                                        <%}
                                                        }
                                                    }
                                                }
                                            }
                                        %>
                                    </select>
                                </div>
                                <div class="col-sm-2"><input id="_startdate" name="_startdate" type="text" class="form-control" placeholder="from"></div>
                                <div class="col-sm-2"><input id="_enddate" name="_enddate" type="text" class="form-control" placeholder="to"></div>
                                <div class="col-sm-3"><button class="btn btn-success ladda-button btn-sm" data-style="zoom-in" id="generateButton" onclick="generatereAPIreport()" type="submit"><i class="fa fa-bar-chart"></i> Generate</button> </div>
                            </div>
                        </form>
                        <hr class="m-b-xl"/>
                        </br>
                        <div id="report_data"></div>
                    </div>
                </div>
            </div>

        </div>

        <%@include file="footer.jsp"%> 


        <script>
            $(function () {

                $('#_startdate').datepicker();
                $('#_enddate').datepicker();

                $(".js-source-states-2").select2();

            });
        </script>
