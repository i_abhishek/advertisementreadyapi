<%@page import="java.util.Set"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.APICallManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgApicallDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PgDailyTxManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PgHourlyTXManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDailyPgtx"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgHourlyPgtx"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CaaSManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCassNumbers"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CAASCDRCalculationManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CDRTxManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCaastransactiondetails"%>
<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SlabPriceCalculationManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCreditInfo"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.HourlyTxManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgHourlytransactiondetails"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.SGportal.billing.SubscribePackage"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.DailyTxManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDailytransactiondetails"%>
<!--<script src="./js/partnerRequest.js"></script>-->
<%
    String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
    Integer partnerId = (Integer) request.getSession().getAttribute("_partnerID");
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");

    String startDate = request.getParameter("_sdate");
    String endDate = request.getParameter("_edate");
    String stime = request.getParameter("_stime");
    String etime = request.getParameter("_etime");
    String apiUsageEnvironment = request.getParameter("_apiUsageEnvironment");
    String apiUsageAccesspoint = request.getParameter("_apiUsageAccesspoint");
    //System.out.println("apiUsageAccesspoint "+apiUsageAccesspoint);
    String[] accessPointList = null;
    if (apiUsageAccesspoint != null) {
        accessPointList = apiUsageAccesspoint.split(",");
    }
    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat tmDateFormat = new SimpleDateFormat("dd/MM/yyyy");
//    Date sDate = format.parse(startDate);
//    Date eDate = format.parse(endDate);    
    Date cdate = format.parse(format.format(new Date()));
    //boolean flag = false;
    SgHourlytransactiondetails[] shs = null;
    SgDailytransactiondetails[] dailyTransObj = null;
    SgSubscriptionDetails subcriptionDetails = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(partnerId);

    // If partner select start date before the date of his subscription of current package that time,
    // we can change his start date to his package subscription package.
    // because in case if partner previousely use some services in such scnario calculation mismatch
    dailyTransObj = new DailyTxManagement().getTxDetails(ChannelId, partnerId, startDate, endDate, stime, etime);

    SgApicallDetails apiCallObj = new SgApicallDetails();
    apiCallObj.setDeveloperId(partnerId);
    apiCallObj.setSubscriptionId(subcriptionDetails.getBucketId());
    SgApicallDetails callDetails = new APICallManagement().getDetails(apiCallObj);

%>
<div class="col-lg-12 col-md-12 col-sm-12">
    <form class="form-horizontal " method="get">       
        <div class="dataTable_wrapper table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead id="apiUsageHeader">
                    <tr>
                        <th>Sr.No.</th>
                        <th>Date</th>
                        <th>API</th>
                        <th>Service</th>
                        <th>Version</th>                                        
                        <th>Usage</th>
                        <th>Credit Used</th>
                    </tr>
                </thead>
                <tbody>
                    <%                        float totalAmount = 0.0f;
                        String apName = "NA";
                        String resName = "NA";
                        String envt = "NA";
                        String apiName = "NA";
                        int version = 0;
                        int callCount = 0;
                        float amount = 0;
                        float totalCharge = 0.0f;
                        JSONObject partnerObj = new JSONObject();
                        SortedMap sortedMap = new TreeMap();
                        Map sortMap = new HashMap();
                        if (shs != null) {
                            for (SgHourlytransactiondetails transcationdetailse : shs) {
                                try {
                                    JSONObject data = new JSONObject(transcationdetailse.getRawData());
                                    if (partnerObj.isNull("" + cdate)) {
                                        partnerObj.put("" + cdate, data.toString());
                                    } else {
                                        Iterator itr = data.keys();
                                        while (itr.hasNext()) {
                                            Object key = itr.next();
                                            JSONObject oldData = new JSONObject(partnerObj.getString("" + cdate));
                                            if (oldData.isNull((String) key)) {
                                                oldData.put((String) key, data.getString((String) key));
                                                partnerObj.put("" + cdate, oldData.toString());
                                            } else {
                                                String value = data.getString((String) key);
                                                String value1 = oldData.getString((String) key);
                                                int count = Integer.parseInt(value.split(":")[0]);
                                                count = count + Integer.parseInt(value1.split(":")[0]);;
                                                value = count + ":" + value.split(":")[1];
                                                oldData.put((String) key, value);
                                                partnerObj.put("" + cdate, oldData.toString());
                                            }
                                        }
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                        if (dailyTransObj != null) {
                            for (int i = 0; i < dailyTransObj.length; i++) {
                                JSONObject data = new JSONObject(dailyTransObj[i].getRawData());
                                partnerObj.put("" + dailyTransObj[i].getExecuutionDate(), data.toString());
                                sortedMap.put("" + dailyTransObj[i].getExecuutionDate(), data.toString());
                            }
                        }
                        //session.setAttribute("partnerBill", partnerObj);
                        if (subcriptionDetails.getSlabApRateDetails() != null) {
                            partnerObj = new SlabPriceCalculationManagement().slabPriceCalculation(partnerObj, subcriptionDetails.getSlabApRateDetails(), "Live", "slab");
                        } else if (subcriptionDetails.getTierRateDetails() != null) {
                            partnerObj = new SlabPriceCalculationManagement().slabPriceCalculation(partnerObj, subcriptionDetails.getTierRateDetails(), "Live", "tier");
                        }
                        session.setAttribute("partnerBill", sortedMap);
                        int count = 0;
                        DecimalFormat df = new DecimalFormat();
                        df.setMaximumFractionDigits(2);
                        if (!sortedMap.isEmpty()) {
                            Set<String> Keys = sortedMap.keySet();
                            for (String keyMap : Keys) {
                                //Object keyS = itrS.next();
                                String apiCallDate = (String) keyMap;
                                Date apiCallDateObj = formatter.parse(apiCallDate);
                                JSONObject data = new JSONObject((String) sortedMap.get(apiCallDate));
                                Iterator itr = data.keys();
                                while (itr.hasNext()) {
                                    Object key = itr.next();
                                    String keyData = (String) key;
                                    String value = data.getString((String) key);
                                    int acc = Integer.parseInt(keyData.split(":")[0]);
                                    int res = Integer.parseInt(keyData.split(":")[1]);
                                    version = Integer.parseInt(keyData.split(":")[2]);
                                    envt = (keyData.split(":")[3]);
                                    String apiNameTemp = (keyData.split(":")[4]);
                                    if (apiNameTemp != null && !apiNameTemp.equalsIgnoreCase("null")) {
                                        apiName = apiNameTemp;
                                    }
                                    Accesspoint ap = new AccessPointManagement().getAccessPointById(ChannelId, acc);
                                    ResourceDetails rs = new ResourceManagement().getResourceById(res);
                                    if (ap != null) {
                                        apName = ap.getName();
                                    }
                                    if (rs != null) {
                                        resName = rs.getName();
                                    }
                                    callCount = Integer.parseInt(value.split(":")[0]);
                                    amount = Float.parseFloat(value.split(":")[1]);
                                    if (amount != -99 && !apiName.equalsIgnoreCase("DoTransaction")) {
                                        int index = -1;
                                        if (accessPointList != null) {
                                            for (int i = 0; i < accessPointList.length; i++) {
                                                if (accessPointList[i].equals(apName) || accessPointList[i].equals("null")) {
                                                    index = i;
                                                    break;
                                                }
                                            }
                                        }

                                        if (index != -1) {
                                            if (apiUsageEnvironment != null && apiUsageEnvironment.equals("1")) {
                                                if (envt.equalsIgnoreCase("Live")) {
                                                    continue;
                                                }
                                            } else if (apiUsageEnvironment != null && apiUsageEnvironment.equals("2")) {
                                                if (envt.equalsIgnoreCase("test")) {
                                                    continue;
                                                }
                                            }

                                            if (envt.equalsIgnoreCase("test")) {
                                                envt = "Sandbox";
                                            }
                                            if (envt.equalsIgnoreCase("Live")) {
                                                envt = "Production";
                                            }

                                            totalCharge = callCount * amount;
                    %>
                    <tr>
                        <td ><font><%=count + 1%></font></td>
                        <td ><font><%=tmDateFormat.format(apiCallDateObj)%></font>
                        <td ><font ><%=apiName%></font></td>                        
                        <td ><font><%=apName%> </font></td>                        
<!--                        <td ><font><%=resName%></font></td>-->
<!--                        <td ><font><%=envt%></font></td>-->
                        <td ><font ><%=version%></font></td>                        
                        <td ><font><%=callCount%></font></td>
<!--                        <td ><font><%=df.format(amount)%></font></td>-->
                        <td ><font><%=df.format(totalCharge)%></font></td>
<!--                        <td ><font style="font-size: 15px"><%=key%></font></td>                                                                                 -->
                    </tr>
                    <%
                                        count++;
                                        totalAmount = totalAmount + totalCharge;
                                    }
                                }
                            }
                        }

                    %>



                    <%                        }
                        if (count >= 1) {
                    %>                                                      
                    <tr>                        
                        <td colspan="6" style="text-align:  right"><strong>TOTAL</strong></td>
                        <td ><%=df.format(totalAmount)%></td>
                    </tr> 
                    <% }
                        if (count < 1) {%>
                <img src="images/no_record_found.png" alt="No record found" width="400px" height="300px"/>
                <script>
                    document.getElementById("apiUsageHeader").style.display = 'none';
                </script>
                <%    }
                %>
                </tbody>
            </table>            
        </div>
        <%if (count != 0) {%>
        <a  style="float: left"class="btn btn-success btn-xs"  onclick="downloadBillingPDF('<%=startDate%>', '<%=endDate%>', '<%=stime%>', '<%=etime%>', '<%=apiUsageEnvironment%>', '<%=apiUsageAccesspoint%>')">Download PDF <i class="fa fa-file-pdf-o"></i> </a>
        <%}%>
    </form>
</div>
<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });

</script>

