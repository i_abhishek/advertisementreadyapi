<%@include file="header.jsp" %>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<script src="./js/partnerRequest.js"></script>
<script src="<%= new com.paypal.BtConfigReader().getBtJavaScriptClient() %>"></script>
<script src="<%= new com.paypal.BtConfigReader().getBtJavaScriptHostedFields() %>"></script>
<script src="<%= new com.paypal.BtConfigReader().getBtJavaScriptPayPal() %>"></script>
<script src="https://www.paypalobjects.com/api/checkout.js" data-version-4></script>
<%
    SgDeveloperProductionEnvt productionEnvt = new ProductionAccessEnvtManagement().getProudDetailsByPartnerId(parObj.getPartnerId());
        String billingAddress       = "NA";
        String billingState         = "NA";
        String billingPostCode      = "NA";
        String billingCountry       = "NA";
        
        if (productionEnvt != null) {
            billingAddress  = productionEnvt.getBillingAddress();
            billingState    = productionEnvt.getBillingState();
            billingPostCode = productionEnvt.getBillingPostCode();
            billingCountry  = productionEnvt.getBillingCountry();
        }
        
    String paymentAmount = (String) session.getAttribute("totalPaymentAmountWithTax");
    String paymode       = request.getParameter("payMode");
    request.getSession().setAttribute("payPalPackagePayment", paymode);
%>
<div id="wrapper">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Checkout</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Check out
                </h2>
                <small>Payment Methods</small>
            </div>
        </div>
    </div>
        <div class="content animate-panel">
        <div class="row">
            <div class="col-md-12">
                <div class="hpanel email-compose">
                    <div class="panel-heading hbuilt">
                        <div class="p-xs h4">
                            Checkout
                        </div>
                    </div>
                    <div class="panel-heading hbuilt">              
                <div class="p-xs">
                    <form class="form-horizontal" method="post">
                        <div class="form-group has-success">                            
                            <div class="col-sm-4 text-success">
                                <span style="font-size: 15px;"><i class="glyphicon glyphicon-info-sign"></i> Transaction Status</span>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Name        : </label> <%=parObj.getPartnerName()%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Address     : </label> <%=billingAddress%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">State       : </label> <%=billingState%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">PostCode    : </label> <%=billingPostCode%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Country     : </label> <%=billingCountry%>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-3">Amount      : </label> <%=paymentAmount%>
                        </div>
                    </form>
                    <a href="makePayment.jsp" class="btn btn-primary"> Pay</a>    
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>  