<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.operation.SERVICEGUARDOperator"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement"%>
<%--<%@include file="header.jsp"%>--%>
<script src="scripts/ticketmanagement.js" type="text/javascript"></script>
<!-- Main Wrapper -->
<%
    String mailId = LoadSettings.g_sSettings.getProperty("support.email.id");
%>
<div id="wrapper">

    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
<!--                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Technical Support</span>
                        </li>
                    </ol>
                </div>-->
                <h2 class="font-light m-b-xs">
                    Compose Technical Email
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
        <div class="row">
            <div class="col-md-12">
                <div class="hpanel email-compose">
                    <div class="panel-heading hbuilt">
                        <div class="p-xs h4">
                            New message
                        </div>
                    </div>
                    <div class="panel-heading hbuilt">
                        <div class="p-xs">
                            <form  class="form-horizontal" method="POST" id="ticketComposeRT">
                                <div class="form-group"><label class="col-sm-2 control-label text-left">To:</label>
                                    <div class="col-sm-10"><input type="text" class="form-control input-sm" value="<%=mailId%>" disabled></div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label text-left">Contact:</label>
                                    <div class="col-sm-10"><input type="text" id="contact_no" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" class="form-control input-sm" placeholder="Enter your contact number"></div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label text-left">Subject:</label>
                                    <div class="col-sm-10">
                                        <select id="subjectBody" class="js-source-states" style="width: 100%">
                                            <optgroup label="General">
                                                <option value="General Questions">General Questions</option>
                                            </optgroup>
                                            <optgroup label="API">
                                                <option value="SMS">SMS</option>
                                                <option value="Map">Map</option>
                                                <option value="MFA">MFA</option>
                                                <option value="Digital_signing">Digital Signing</option>
                                                <option value="Payment_gateway">Payment Gateway</option>
                                            </optgroup>
                                            <optgroup label="Fault">
                                                <option value="Access_Token_Issue">Access Token Issue</option>
                                                <option value="password">Can't Change Password</option>
                                                <option value="others">Others</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                </form>
                                <div style="background: #fff; height: 200px">
                                    <div class="col-sm-12">
                                        <form action="UploadEmailAttachment" method="post" class="dropzone" id="my-dropzone"></form>	
                                            <small>Upload attachment. Support Image File size limit 5mb</small>
                                    </div>
                                </div>
                                <div class="panel-body no-padding">
                                    <div><textarea class="summernote" id="msgBody"></textarea></div>                                    
                                </div>
                                <div class="panel-footer">
                                    <div class="pull-right">
                                        <div class="btn-group">
                                        </div>
                                    </div>
                                    <a id="submitMail" class="btn btn-primary ladda-button" data-style="zoom-in" onclick="sendEmailToRT()">Send email</a>
                                    <a class="btn btn-danger" tabindex="2" onclick="helpDesk()"><i class="fa fa-close"></i> Clear</a>
                                </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
//    var placeHold = "Hello Jonathan!"+
//    "dummy text of the printing and typesetting industry.dummy text of the printing and typesetting industry."+"\n"+
//    "Lorem Ipsum has been the dustrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more\n\
//All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.";
            $(function () {
        var elem = $(".summernote").summernote({
//            placeholder: "Hello Jonathan!\n\
//dummy text of the printing and typesetting industry.dummy text of the printing and typesetting industry.Lorem Ipsum has been the dustrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more\n\
//All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.",
            callbacks: {
                onInit: function () {
                    var editor = elem.next(),
                            placeholder = editor.find(".note-placeholder");

                    function isEditorEmpty() {
                        var code = elem.summernote("code");
                        return code === "<p><br></p>" || code === "";
                    }

                    editor.on("focusin focusout", ".note-editable", function (e) {
                        if (isEditorEmpty()) {
                            placeholder[e.type === "focusout" ? "show" : "hide"]();
                        }
                    });
                }
            }
        });
        // Initialize summernote plugin
//        $('.summernote').summernote({
//             placeholder: "Enter Text Here...",
//            toolbar: [
//                ['headline', ['style']],
//                ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
//                ['textsize', ['fontsize']],
//                ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
//            ]
//        });
//
    });
    
    
    $(document).ready(function () {
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone("#my-dropzone", {
                url: "UploadZip",
                uploadMultiple: true,
                maxFilesize: 5,
                maxFiles: 1,
                acceptedFiles: "image/*",
                dictInvalidFileType: "You can't upload files of this type, only Image file",
                autoProcessQueue: true,
                parallelUploads: 1,
                addRemoveLinks: true,
            });
        });
        
        
</script>

<!-- Footer-->
<%@include file="footer.jsp"%>
</div>
