<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.operation.SERVICEGUARDOperator"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement"%>
<%--<%@include file="header.jsp"%>--%>
<script src="scripts/ticketmanagement.js" type="text/javascript"></script>
<!-- Main Wrapper -->
<%    
    PartnerDetails _partnerDetails = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
    String mailId = LoadSettings.g_sSettings.getProperty("support.email.id");
    Map apiMap = new HashMap();
    int accessPointId, resourceId;
    Accesspoint[] accesspoints1 = null;
    Warfiles warfiles = null;
    if (true) {
        accesspoints1 = new AccessPointManagement().getAllAccessPoint(SessionId, ChannelId);
        if (accesspoints1 != null) {
            for (int i = 0; i < accesspoints1.length; i++) {
                if (accesspoints1[i].getStatus() != GlobalStatus.DELETED && accesspoints1[i].getStatus() != GlobalStatus.SUSPEND && Integer.parseInt(
                        _partnerDetails.getPartnerGroupId()) == accesspoints1[i].getGroupid()) {
                    Accesspoint apdetails = accesspoints1[i];
                    TransformDetails transformDetails = new TransformManagement().getTransformDetails(SessionId, ChannelId, apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                    if (transformDetails != null) {
                        warfiles = new WarFileManagement().getWarFile(SessionId, ChannelId, apdetails.getApId());
                        if (warfiles != null) {
                            String acesspointName = apdetails.getName();
                            accessPointId = apdetails.getApId();
                            resourceId = Integer.parseInt(apdetails.getResources().split(",")[0]);
                            apiMap.put(acesspointName, accessPointId + ":" + resourceId);
                        }
                    }
                }
            }
        }
    }
    String body = "";
    String urlData   = (String) session.getAttribute("urlAPIConsole");
    String bodyData  = (String) session.getAttribute("bodyAPIConsole");
    String responseData  = (String) session.getAttribute("responseAPIConsole");
    Integer paramsLength = (Integer) session.getAttribute("paramsLengthAPIConsole");
    String error = (String) session.getAttribute("errorAPIConsole");
    //PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
    boolean ticketFormed = false;
    if(urlData != null){
        body = "Dear Support Team, <br><br> Please find my ticket information <br/><br/>";
        body += "My service end point - "+urlData+"<br/><br/>";
        ticketFormed = true;
    }
    if(bodyData != null){
        //body += "My request body - <xmp>"+bodyData+"</xmp><br>"; 
        body += "<textarea rows='10' cols='1000' disabled style='border: hidden'>My request body -  "+bodyData+"\n I get response as -  "+responseData+"</textarea><br/>";
        ticketFormed = true;
    }
//    if(paramsLength != null){
//        body += "Header Details - <br><br>";
//        for(int i= 0 ;i<= paramsLength; i++){
//            String headerKey   = (String)session.getAttribute("headerKeyAPIConsole"+i);
//            String headerValue = (String)session.getAttribute("headerValueAPIConsole"+i);
//            body += headerKey + " - "+ headerValue +"<br><br>";            
//        }
//        ticketFormed = true;
//    }

//    if(responseData != null){
//        body += "I get response as - "+responseData+"</textarea><br/><br/>";
//        ticketFormed = true;
//    }
    if(error != null){
        body += "I get the error as - "+error+"<br/><br/>";
        ticketFormed = true;
    }
    if(ticketFormed){
        body += "<span style='margin-left:3%'></span> Warm regards, <br/><span style='margin-left:3%'></span>"+_partnerDetails.getPartnerName()+"&nbsp;&nbsp;";        
    }
    
%>
<!--<div id="wrapper">-->
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">                
                <h2 class="font-light m-b-xs">
                    Compose Ticket
                </h2>
                <small>Send Your Query To Support Team</small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
        <div class="row">
            <div class="col-md-12">
                <div class="hpanel email-compose">
                    <div class="panel-heading hbuilt">
                        <div class="p-xs h4">
                            New message
                        </div>
                    </div>
                    <div class="panel-heading hbuilt">
                        <div class="p-xs">
                            <form  class="form-horizontal" method="POST" id="ticketComposeRT">
                                <div class="form-group"><label class="col-sm-2 control-label text-left">To:</label>
                                    <div class="col-sm-10"><input type="text" class="form-control input-sm" value="<%=mailId%>" disabled></div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label text-left">Contact:</label>
                                    <div class="col-sm-10"><input type="text" id="contact_no" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || ((event.keyCode == 8 || event.keyCode == 46
 || event.keyCode == 37 || event.keyCode == 39))" class="form-control input-sm" placeholder="Enter your contact number"></div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label text-left">Subject:</label>
                                    <div class="col-sm-10">
                                        <select id="subjectBody" class="js-source-states" style="width: 100%">
                                            <optgroup label="General">
                                                <option value="General Questions">General Questions</option>
                                            </optgroup>
                                            <optgroup label="API">
                                                <%for (Object key : apiMap.keySet()) {%>
                                                <option value="<%=key%>"><%=key%></option>
                                                <%}%>
                                            </optgroup>
                                            <optgroup label="Fault">
                                                <option value="Access_Token_Issue">Access Token Issue</option>
                                                <option value="password">Can't Change Password</option>
                                                <option value="others">Others</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group"> <label class="col-sm-2 control-label text-left">Body :</label></div>            
                            </form>
                            <div class="panel-body no-padding">
                                <div>
                                    <%if(body.isEmpty()){%>
                                    <textarea class="summernote" id="msgBody" placeholder="Enter Message Body Here." rows="10" cols="1000" style='width:515px !important'></textarea>
                                    <%}else{%>
                                    <textarea class="summernote" id="msgBody" placeholder="Enter Message Body Here."  style='width:515px !important'><%=body%></textarea>
                                    <%}%>
                                </div>                                    
                            </div><br><br>                
                            <div style="background: #fff; height: 200px">
                                <div class="col-sm-12">
                                    <form action="UploadEmailAttachment" method="post" class="dropzone" id="my-dropzone"></form>	
                                    <small>Upload attachment. Support Zip File size limit 5mb</small>
                                </div>
                            </div>

                            <br/><br/>
                            <div class="panel-footer">
                                <div class="pull-right">
                                    <div class="btn-group">
                                    </div>
                                </div>
                                <a id="submitMail" class="btn btn-primary ladda-button" data-style="zoom-in" onclick="sendEmailToRT()">Send email</a>
                                <a class="btn btn-danger" tabindex="2" onclick="helpDesk()"><i class="fa fa-close"></i> Clear</a>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        //    var placeHold = "Hello Jonathan!"+
        //    "dummy text of the printing and typesetting industry.dummy text of the printing and typesetting industry."+"\n"+
        //    "Lorem Ipsum has been the dustrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more\n\
        //All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.";
        $(function () {
            var elem = $(".summernote").summernote({
                //            placeholder: "Hello Jonathan!\n\
                //dummy text of the printing and typesetting industry.dummy text of the printing and typesetting industry.Lorem Ipsum has been the dustrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more\n\
                //All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.",
                callbacks: {
                    onInit: function () {
                        var editor = elem.next(),
                                placeholder = editor.find(".note-placeholder");

                        function isEditorEmpty() {
                            var code = elem.summernote("code");
                            return code === "<p><br></p>" || code === "";
                        }

                        editor.on("focusin focusout", ".note-editable", function (e) {
                            if (isEditorEmpty()) {
                                placeholder[e.type === "focusout" ? "show" : "hide"]();
                            }
                        });
                    }
                }
            });
            // Initialize summernote plugin
            //        $('.summernote').summernote({
            //             placeholder: "Enter Text Here...",
            //            toolbar: [
            //                ['headline', ['style']],
            //                ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
            //                ['textsize', ['fontsize']],
            //                ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
            //            ]
            //        });
            //
        });


        $(document).ready(function () {

            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone("#my-dropzone", {
                url: "UploadEmailAttachment",
                uploadMultiple: true,
                maxFilesize: 5,
                maxFiles: 1,
                acceptedFiles: "application/zip",
                dictInvalidFileType: "You can't upload files of this type, only Zip file",
                autoProcessQueue: true,
                parallelUploads: 1,
                addRemoveLinks: true,
                removedfile: function(file) {
                removeFromSession(file.name);    
                var _ref;
                if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                        _ref.parentNode.removeChild(file.previewElement);
                    }
                }
                return this._updateMaxFilesReachedClass();
            }
            });
        });


    </script>

    <!-- Footer-->
    <%--<%@include file="footer.jsp"%>--%>
<!--</div>-->
