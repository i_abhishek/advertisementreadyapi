<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt"%>
<%@include file="header.jsp"%>
<script src="scripts/composeProduction.js" type="text/javascript"></script>
<!-- Main Wrapper -->
<div id="wrapper">
    <%
        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        SgDeveloperProductionEnvt productionEnvt = new ProductionAccessEnvtManagement().getProudDetailsByPartnerId(partnerObj.getPartnerId());
    %>
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Production Request</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Request to Production Environment
                </h2>
                <small>Please fill below form for the enabling production environment</small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
        <div class="hpanel">	
            <ul class="nav nav-tabs">
                <li class=""><a data-toggle="tab" href="#individual" style="display: none"> Individual</a></li>
                <li class="active"><a data-toggle="tab" href="#sole_partner"> Sole Proprietorship / Partnership</a></li>
                <li class=""><a data-toggle="tab" href="#enterprise"> Enterprise</a></li>
            </ul>

            <div class="tab-content">
                <div id="individual" class="tab-pane" style="display: none">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="hpanel email-compose">
                                <div class="panel-heading hbuilt">
                                    <div class="p-xs h4">
                                        Submit request
                                    </div>
                                </div>
                                <div class="panel-heading hbuilt">
                                    <div class="p-xs">
                                        <form method="get" class="form-horizontal">
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">To:</label>
                                                <div class="col-sm-10"><input type="text" class="form-control input-sm" value="support@mollatech.com" name="production@tab.com.my" disabled></div><!-- Jack Remark: Email only for sample -->
                                            </div>
                                            <!-- Remark: Field with "*" is compulsory -->
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">I/C No. *:</label>
                                                <div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
                                            </div>
                                            <h3>Billing Details</h3>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Address *</label>
                                                <div class="col-sm-10"><textarea class="form-control input-sm"></textarea></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">State *</label>
                                                <div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Postcode *</label>
                                                <div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Country *</label>
                                                <div class="col-sm-10"><input type="text" class="form-control input-sm" value=""></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Select APIs *:</label>
                                                <div class="col-sm-10">
                                                    <div class="checkbox">
                                                        <input id="sms" type="checkbox">
                                                        <label for="sms">
                                                            SMS
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <input id="map" type="checkbox" checked="">
                                                        <label for="map">
                                                            Map
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <input id="mfa" type="checkbox">
                                                        <label for="mfa">
                                                            MFA
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <input id="digital_sign" type="checkbox">
                                                        <label for="digital_sign">
                                                            Digital Signing
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>								
                                        </form>
                                        <div style="background: #fff; height: 200px">
                                            <div class="col-sm-12">
                                                <form action="/upload-target" id="my-dropzone2" class="dropzone"></form>								
                                                <small>Upload personal I/C.<br/> Support jpeg, gif, png, pdf. File size limit 5mb</small><!-- Jack Remark: Compulsory -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-footer">

                                    <div class="checkbox">
                                        <input id="tncPersonalInfo"  name="tncPersonalInfo" type="checkbox">
                                        <label for="tncPersonalInfo">
                                            Read and agree to <a href="#">Terms of Service</a> to continue subscription <!-- Jack Remark: Compulsory -->
                                        </label>
                                    </div>
                                    <div class="pull-right">
                                        <div class="btn-group">
                                        </div>
                                    </div>
                                    <button class="btn btn-primary">Subscribe</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="sole_partner" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="hpanel email-compose">
                                <div class="panel-heading hbuilt">
                                    <div class="p-xs h4">
                                        Submit request
                                    </div>
                                </div>
                                <div class="panel-heading hbuilt">
                                    <div class="p-xs">
                                        <form id="register_partnership" name="register_partnership"  class="form-horizontal">
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">To:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control input-sm" value="support@mollatech.com" name="productionReqToEmail" id="productionReqToEmail" disabled></div><!-- Jack Remark: Email only for sample -->
                                            </div>
                                            <!-- Remark: Field with "*" is compulsory -->
                                            <div class="form-group"><label class="col-sm-2 control-label text-left"></label>
                                                <div class="col-sm-10">
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="partnership" value="<%=GlobalStatus.Sole_Proprietorship%>" name="partnership" checked="">
                                                        <label for="sole"> Sole Proprietorship </label> <!-- Jack Remark: Compulsory -->
                                                    </div>
                                                    <div class="radio radio-inline">
                                                        <input type="radio" id="partnership" value="<%=GlobalStatus.Partnership%>" name="partnership">
                                                        <label for="partnership"> Partnership </label> <!-- Jack Remark: Compulsory -->
                                                    </div>
                                                </div>
                                            </div>
                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>            
                                            <script>
                                                var elements = document.getElementsByName('partnership');
                                                for (i = 0; i < elements.length; i++) {
                                                    if (elements[i].value == <%=GlobalStatus.Sole_Proprietorship%>) {
                                                        elements[i].checked = true;
                                                    } else if (elements[i].value == <%=GlobalStatus.Partnership%>) {
                                                        elements[i].checked = true;
                                                    }
                                                }
                                            </script>
                                            <%}%>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Company Name *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerCompanyName" name="partnerCompanyName" value="<%=productionEnvt.getComapnyName()%>">
                                                    <%} else {%>
                                                    <input type="text" class="form-control input-sm" id="partnerCompanyName" name="partnerCompanyName">
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Company Registration No. *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerCompanyRegistrationNo" name="partnerCompanyRegistrationNo" value="<%=productionEnvt.getCompanyRegistrationNo()%>">
                                                    <%} else {%>
                                                    <input type="text" class="form-control input-sm" id="partnerCompanyRegistrationNo" name="partnerCompanyRegistrationNo">
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">GST No.</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>                                               
                                                    <input type="text" class="form-control input-sm" id="partnerGSTNo" name="partnerGSTNo" value="<%=productionEnvt.getGstNo()%>">
                                                    <%} else {%>
                                                    <input type="text" class="form-control input-sm" id="partnerGSTNo" name="partnerGSTNo">
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Fixed Line Number *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>  
                                                    <input type="text" class="form-control input-sm" id="partnerFixedLineNumber" name="partnerFixedLineNumber" value="<%=productionEnvt.getFixedLineNumber()%>">
                                                    <%} else {%>
                                                    <input type="text" class="form-control input-sm" id="partnerFixedLineNumber" name="partnerFixedLineNumber">
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Address *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>
                                                    <textarea class="form-control input-sm" id="partnerAddress" name="partnerAddress"><%=productionEnvt.getAddress()%></textarea>
                                                    <%} else {%>
                                                    <textarea class="form-control input-sm" id="partnerAddress" name="partnerAddress"></textarea>
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">State *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerState" name="partnerState" value="<%=productionEnvt.getAddrstate()%>">
                                                    <%} else {%>
                                                    <input type="text" class="form-control input-sm" id="partnerState" name="partnerState">
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Postcode *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerPostcode" name="partnerPostcode" value="<%=productionEnvt.getPostcode()%>">
                                                    <%} else {%>
                                                    <input type="text" class="form-control input-sm" id="partnerPostcode" name="partnerPostcode">
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Country *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerCountry" name="partnerCountry" value="<%=productionEnvt.getCountry()%>">
                                                    <%} else {%>
                                                    <input type="text" class="form-control input-sm" id="partnerCountry" name="partnerCountry" >
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Industry *:</label>
                                                <div class="col-sm-10">
                                                    <select class="js-source-states" id="partnerIndustry" name="partnerIndustry" style="width: 100%">
                                                        <optgroup label="IT">
                                                            <option value="Software">Software house</option>
                                                            <option value="Web development">Web development</option>
                                                        </optgroup>
                                                        <optgroup label="Service">
                                                            <option value="Banking">Banking</option>
                                                            <option value="Finance">Finance</option>
                                                            <option value="Insurance">Insurance</option>
                                                            <option value="Medical">Medical</option>
                                                        </optgroup>
                                                    </select><!-- Jack Remark: Temporary, will add-on later -->
                                                </div>
                                                <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>
                                                <script>
                                                    $("#partnerIndustry").val('<%=productionEnvt.getIndustry()%>');
                                                </script>
                                                <%}%>
                                            </div>
                                            <h3>Billing Details</h3>
                                            <small>Fill in if different from office address</small>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Address</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>
                                                    <textarea class="form-control input-sm" id="partnerBillingAddress" name="partnerBillingAddress"><%=productionEnvt.getBillingAddress()%></textarea>
                                                    <%} else {%>
                                                    <textarea class="form-control input-sm" id="partnerBillingAddress" name="partnerBillingAddress"></textarea>
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">State</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerBillingState" name="partnerBillingState" value="<%=productionEnvt.getBillingState()%>">
                                                    <%} else {%>
                                                    <input class="form-control input-sm" id="partnerBillingState" name="partnerBillingState">
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Postcode</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerBillingPostcode" name="partnerBillingPostcode" value="<%=productionEnvt.getBillingPostCode()%>"/>
                                                    <%} else {%>                                                    
                                                    <input class="form-control input-sm" id="partnerBillingPostcode" name="partnerBillingPostcode"/>
                                                    <%}%>                                                
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Country</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerBillingCountry" name="partnerBillingCountry" value="<%=productionEnvt.getBillingCountry()%>">
                                                    <%} else {%>                                                                                                                                                           
                                                    <input class="form-control input-sm" id="partnerBillingCountry" name="partnerBillingCountry"/>
                                                    <%}%>

                                                </div>
                                            </div>
<!--                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Select APIs:</label>
                                                <div class="col-sm-10">
                                                    <%
                                                        int accessPointId, resourceId;

                                                        Accesspoint[] accesspoints1 = null;
                                                        Warfiles warfiles = null;
                                                        accesspoints1 = new AccessPointManagement().getAllAccessPoint(SessionId, ChannelId);
                                                        if (accesspoints1 != null) {
                                                            for (int i = 0; i < accesspoints1.length; i++) {
                                                                if (accesspoints1[i].getStatus() != GlobalStatus.DELETED && Integer.parseInt(
                                                                        parObj.getPartnerGroupId()) == accesspoints1[i].getGroupid()) {
                                                                    accessPointId = accesspoints1[i].getApId();
                                                                    resourceId = Integer.parseInt(accesspoints1[i].getResources().split(",")[0]);
                                                                    Accesspoint apdetails = accesspoints1[i];
                                                                    TransformDetails transformDetails = new TransformManagement().getTransformDetails(SessionId, ChannelId, apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                                                                    if (transformDetails != null) {
                                                                        warfiles = new WarFileManagement().getWarFile(SessionId, ChannelId, apdetails.getApId());
                                                                        if (warfiles != null) {
                                                                            String acesspointName = apdetails.getName();

                                                    %>
                                                    <div class="checkbox">
                                                        <input id="selectedAPI" class="SMSPartnership" name ="selectedAPI" value = "<%=acesspointName%>" type="checkbox">
                                                        <label for="<%=acesspointName%>">
                                                            <%=acesspointName%>
                                                        </label>
                                                    </div>
                                                    <%}
                                                                    }
                                                                }
                                                            }
                                                        }%>

                                                    <%                                                        boolean paymentFlagPartnership = false;
                                                        if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership)) {
                                                            String apiSelected = productionEnvt.getPreferedResource();
                                                            String[] apiArr = apiSelected.split(",");
                                                            for (int k = 0; k < apiArr.length; k++) {
                                                                if (apiArr[k].equalsIgnoreCase("PaymentGateway")) {
                                                                    paymentFlagPartnership = true;
                                                                }

                                                    %>
                                                    <script>
                                                        $(".<%=apiArr[k]%>Partnership").prop("checked", true);
                                                    </script>
                                                    <%}

                                                        }
                                                    %>
                                                </div>
                                            </div>-->
                                            <hr/>

                                            <!-- Remark: Following field only display when developer choose payment gateway, all compulsory -->
                                            <%if (paymentFlagPartnership) {%>
                                            <div class="PaymentGateway">
                                                <%} else {%>    
                                                <div class="PaymentGateway" style="display: none">
                                                    <%}%>

                                                    <div class="form-group"><label class="col-sm-2 control-label text-left">Product Description</label>
                                                        <div class="col-sm-10">
                                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership) && productionEnvt.getPaymentProductDescription() != null) {%>
                                                            <textarea class="form-control input-sm" id="partnerProductionDescription" name="partnerProductionDescription"><%=productionEnvt.getPaymentProductDescription()%></textarea>
                                                            <%} else {%>
                                                            <textarea class="form-control input-sm" id="partnerProductionDescription" name="partnerProductionDescription"></textarea>
                                                            <%}%> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label class="col-sm-2 control-label text-left">Website URL</label>
                                                        <div class="col-sm-10">
                                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership) && productionEnvt.getPaymentWebsiteUrl() != null) {%>
                                                            <input type="text" class="form-control input-sm" id="partnerWebsiteURL" name="partnerWebsiteURL" value="<%=productionEnvt.getPaymentWebsiteUrl()%>">
                                                            <%} else {%>
                                                            <input type="text" class="form-control input-sm" id="partnerWebsiteURL" name="partnerWebsiteURL">
                                                            <%}%>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label class="col-sm-2 control-label text-left">Bank Name</label>
                                                        <div class="col-sm-10">
                                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership) && productionEnvt.getPaymentBankName() != null) {%>
                                                            <input type="text" class="form-control input-sm" id="partnerBankName" name="partnerBankName" value="<%=productionEnvt.getPaymentBankName()%>">
                                                            <%} else {%>
                                                            <input type="text" class="form-control input-sm" id="partnerBankName" name="partnerBankName">
                                                            <%}%>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label class="col-sm-2 control-label text-left">Bank Branch</label>                                                    
                                                        <div class="col-sm-10">
                                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership) && productionEnvt.getPaymentBankBranch() != null) {%>
                                                            <input type="text" class="form-control input-sm" id="partnerBankBranch" name="partnerBankBranch" value="<%=productionEnvt.getPaymentBankBranch()%>">
                                                            <%} else {%>
                                                            <input type="text" class="form-control input-sm" id="partnerBankBranch" name="partnerBankBranch">
                                                            <%}%>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label class="col-sm-2 control-label text-left">Bank Account No.</label>
                                                        <div class="col-sm-10">
                                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership) && productionEnvt.getPaymentBankAcctNo() != null) {%>
                                                            <input type="text" class="form-control input-sm" id="partnerBankAccountNo" name="partnerBankAccountNo" value="<%=productionEnvt.getPaymentBankAcctNo()%>">
                                                            <%} else {%>
                                                            <input type="text" class="form-control input-sm" id="partnerBankAccountNo" name="partnerBankAccountNo">
                                                            <%}%>
                                                        </div>
                                                    </div>	
                                                    <div class="form-group"><label class="col-sm-2 control-label text-left">SWIFT Code</label>
                                                        <div class="col-sm-10">
                                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Sole_Proprietorship || productionEnvt.getCompanyType() == GlobalStatus.Partnership) && productionEnvt.getPaymentSwiftcode() != null) {%>
                                                            <input type="text" class="form-control input-sm" id="partnerSwiftCode" name="partnerSwiftCode" value="<%=productionEnvt.getPaymentSwiftcode()%>">
                                                            <%} else {%>
                                                            <input type="text" class="form-control input-sm" id="partnerSwiftCode" name="partnerSwiftCode">
                                                            <%}%>
                                                        </div>
                                                    </div>
                                                    <!-- Remark: Following field only display when developer choose payment gateway (END) -->
                                                </div>
                                        </form>
                                        <div style="background: #fff; height: 200px">
                                            <div class="col-sm-12">
                                                <form action="UploadZip" method="POST" id="my-dropzone" class="dropzone dz-clickable">
                                                </form>								
                                                <small>Upload Form D/f document. Support zip file format. File size limit 5mb</small> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="checkbox">
                                        <input id="tnc" name="tnc" type="checkbox" value="true">
                                        <label for="tnc">
                                            Read and agree to <a href="t&c.jsp" target="_blank">Terms of Service</a> and <a href="privacy.jsp" target="_blank"> Privacy policy </a>to continue subscription <!-- Jack Remark: Compulsory -->
                                        </label>
                                    </div>
                                    <div class="pull-right">
                                        <div class="btn-group">
                                        </div>
                                    </div>
                                    <button class="btn btn-primary ladda-button" data-style="zoom-in" id="registerDeveloperDetailButton" onclick="addDeveloperProductionDetail()">Subscribe</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="enterprise" class="tab-pane">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="hpanel email-compose">
                                <div class="panel-heading hbuilt">
                                    <div class="p-xs h4">
                                        Submit request
                                    </div>
                                </div>
                                <div class="panel-heading hbuilt">
                                    <div class="p-xs">
                                        <form id="register_partnership_enterprise" name="register_partnership_enterprise"  class="form-horizontal">
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">To:</label>
                                                <div class="col-sm-10"><input type="text" class="form-control input-sm" value="support@mollatech.com" disabled></div><!-- Jack Remark: Email only for sample -->
                                            </div>
                                            <!-- Remark: Field with "*" is compulsory -->
                                            <div class="form-group"><label class="col-sm-2 control-label text-left"></label>
                                                <div class="col-sm-10">
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="partnership" value="<%=GlobalStatus.Sdn_Bhd%>" name="partnership" checked="">
                                                        <label for="sdn_bhd"> Sdn Bhd </label>
                                                    </div>
                                                    <div class="radio radio-inline">
                                                        <input type="radio" id="partnership" value="<%=GlobalStatus.Berhad%>" name="partnership">
                                                        <label for="berhad"> Berhad </label>
                                                    </div>
                                                    <div class="radio radio-inline">
                                                        <input type="radio" id="partnership" value="<%=GlobalStatus.Foreign%>" name="partnership">
                                                        <label for="foreign_company"> Foreign Company </label> <!-- Jack Remark: Compulsory -->
                                                    </div>
                                                </div>
                                            </div>
                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Foreign || productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd)) {%>            
                                            <script>
                                                var elements = document.getElementsByName('partnership');
                                                for (i = 0; i < elements.length; i++) {
                                                    if (elements[i].value == <%=GlobalStatus.Foreign%>) {
                                                        elements[i].checked = true;
                                                    } else if (elements[i].value == <%=GlobalStatus.Berhad%>) {
                                                        elements[i].checked = true;
                                                    } else if (elements[i].value == <%=GlobalStatus.Sdn_Bhd%>) {
                                                        elements[i].checked = true;
                                                    }
                                                }
                                            </script>
                                            <%}%>            
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Company Name *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerCompanyName" name="partnerCompanyName" value="<%=productionEnvt.getComapnyName()%>">
                                                    <%} else {%>
                                                    <input type="text" class="form-control input-sm" id="partnerCompanyName" name="partnerCompanyName">
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Company Registration No. *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerCompanyRegistrationNo" name="partnerCompanyRegistrationNo" value="<%=productionEnvt.getCompanyRegistrationNo()%>">
                                                    <%} else {%>
                                                    <input type="text" class="form-control input-sm" id="partnerCompanyRegistrationNo" name="partnerCompanyRegistrationNo">
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">GST No.</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {%>                                               
                                                    <input type="text" class="form-control input-sm" id="partnerGSTNo" name="partnerGSTNo" value="<%=productionEnvt.getGstNo()%>">
                                                    <%} else {%>
                                                    <input type="text" class="form-control input-sm" id="partnerGSTNo" name="partnerGSTNo">
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Fixed Line Number *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {%>  
                                                    <input type="text" class="form-control input-sm" id="partnerFixedLineNumber" name="partnerFixedLineNumber" value="<%=productionEnvt.getFixedLineNumber()%>">
                                                    <%} else {%>
                                                    <input type="text" class="form-control input-sm" id="partnerFixedLineNumber" name="partnerFixedLineNumber">
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Address *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {%>
                                                    <textarea class="form-control input-sm" id="partnerAddress" name="partnerAddress"><%=productionEnvt.getAddress()%></textarea>
                                                    <%} else {%>
                                                    <textarea class="form-control input-sm" id="partnerAddress" name="partnerAddress"></textarea>
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">State *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerState" name="partnerState" value="<%=productionEnvt.getAddrstate()%>">
                                                    <%} else {%>
                                                    <input type="text" class="form-control input-sm" id="partnerState" name="partnerState">
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Postcode *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerPostcode" name="partnerPostcode" value="<%=productionEnvt.getPostcode()%>"/>
                                                    <%} else {%>                                                    
                                                    <input class="form-control input-sm" id="partnerPostcode" name="partnerPostcode"/>
                                                    <%}%> 
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Country *</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerCountry" name="partnerCountry" value="<%=productionEnvt.getCountry()%>">
                                                    <%} else {%>
                                                    <input type="text" class="form-control input-sm" id="partnerCountry" name="partnerCountry" >
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Industry *:</label>
                                                <div class="col-sm-10">
                                                    <select class="js-source-states developerIndustry" id="partnerIndustry" name="partnerIndustry" style="width: 100%">
                                                        <optgroup label="IT">
                                                            <option value="Software">Software house</option>
                                                            <option value="Web development">Web development</option>
                                                        </optgroup>
                                                        <optgroup label="Service">
                                                            <option value="Banking">Banking</option>
                                                            <option value="Finance">Finance</option>
                                                            <option value="Insurance">Insurance</option>
                                                            <option value="Medical">Medical</option>
                                                        </optgroup>
                                                    </select><!-- Jack Remark: Temporary, will add-on later -->
                                                </div>
                                                <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {%>
                                                <script>
                                                    //$("#partnerIndustry").val('<%=productionEnvt.getIndustry()%>');
                                                    $(".developerIndustry").val('<%=productionEnvt.getIndustry()%>');
                                                </script>
                                                <%}%>
                                            </div>
                                            <h3>Billing Details</h3>
                                            <small>Fill in if different from office address</small>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Address</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {%>
                                                    <textarea class="form-control input-sm" id="partnerBillingAddress" name="partnerBillingAddress"><%=productionEnvt.getBillingAddress()%></textarea>
                                                    <%} else {%>
                                                    <textarea class="form-control input-sm" id="partnerBillingAddress" name="partnerBillingAddress"></textarea>
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">State</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerBillingState" name="partnerBillingState" value="<%=productionEnvt.getBillingState()%>">
                                                    <%} else {%>
                                                    <input class="form-control input-sm" id="partnerBillingState" name="partnerBillingState">
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Postcode</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerBillingPostcode" name="partnerBillingPostcode" value="<%=productionEnvt.getBillingPostCode()%>"/>
                                                    <%} else {%>                                                    
                                                    <input class="form-control input-sm" id="partnerBillingPostcode" name="partnerBillingPostcode"/>
                                                    <%}%>                                                
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Country</label>
                                                <div class="col-sm-10">
                                                    <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {%>
                                                    <input type="text" class="form-control input-sm" id="partnerBillingCountry" name="partnerBillingCountry" value="<%=productionEnvt.getBillingCountry()%>">
                                                    <%} else {%>                                                                                                                                                           
                                                    <input class="form-control input-sm" id="partnerBillingCountry" name="partnerBillingCountry"/>
                                                    <%}%>                                                    
                                                </div>
                                            </div>
<!--                                            <div class="form-group"><label class="col-sm-2 control-label text-left">Select APIs:</label>
                                                <div class="col-sm-10">
                                                    <%

                                                        accesspoints1 = new AccessPointManagement().getAllAccessPoint(SessionId, ChannelId);
                                                        if (accesspoints1 != null) {
                                                            for (int i = 0; i < accesspoints1.length; i++) {
                                                                if (accesspoints1[i].getStatus() != GlobalStatus.DELETED && Integer.parseInt(
                                                                        parObj.getPartnerGroupId()) == accesspoints1[i].getGroupid()) {
                                                                    accessPointId = accesspoints1[i].getApId();
                                                                    resourceId = Integer.parseInt(accesspoints1[i].getResources().split(",")[0]);
                                                                    Accesspoint apdetails = accesspoints1[i];
                                                                    TransformDetails transformDetails = new TransformManagement().getTransformDetails(SessionId, ChannelId, apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                                                                    if (transformDetails != null) {
                                                                        warfiles = new WarFileManagement().getWarFile(SessionId, ChannelId, apdetails.getApId());
                                                                        if (warfiles != null) {
                                                                            String acesspointName = apdetails.getName();

                                                    %>
                                                    <div class="checkbox">
                                                        <input id="selectedAPI" class="SMSEnterprise" name ="selectedAPI" value = "<%=acesspointName%>" type="checkbox">
                                                        <label for="<%=acesspointName%>">
                                                            <%=acesspointName%>
                                                        </label>
                                                    </div>
                                                    <%}
                                                                    }
                                                                }
                                                            }
                                                        }%>
                                                </div>
                                                <%                                                    boolean paymentFlag = false;
                                                    if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign)) {
                                                        String apiSelected = productionEnvt.getPreferedResource();
                                                        String[] apiArr = apiSelected.split(",");
                                                        for (int k = 0; k < apiArr.length; k++) {
                                                            if (apiArr[k].equalsIgnoreCase("PaymentGateway")) {
                                                                paymentFlag = true;
                                                            }
                                                %>
                                                <script>
                                                    $(".<%=apiArr[k]%>Enterprise").prop("checked", true);
                                                </script>
                                                <%}

                                                    }
                                                %>
                                            </div>-->
                                            <hr/>
                                            <!-- Remark: Following field only display when developer choose payment gateway, all compulsory -->
                                            <%if (paymentFlag) {%>
                                            <div class="PaymentGatewayEnterprise">
                                                <%} else {%>    
                                                <div class="PaymentGatewayEnterprise" style="display: none">
                                                    <%}%>
                                                    <div class="form-group"><label class="col-sm-2 control-label text-left">Product Description</label>
                                                        <div class="col-sm-10">
                                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign) && productionEnvt.getPaymentProductDescription() != null) {%>
                                                            <textarea class="form-control input-sm" id="partnerProductionDescription" name="partnerProductionDescription"><%=productionEnvt.getPaymentProductDescription()%></textarea>
                                                            <%} else {%>
                                                            <textarea class="form-control input-sm" id="partnerProductionDescription" name="partnerProductionDescription"></textarea>
                                                            <%}%> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label class="col-sm-2 control-label text-left">Website URL</label>
                                                        <div class="col-sm-10">
                                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign) && productionEnvt.getPaymentWebsiteUrl() != null) {%>
                                                            <input type="text" class="form-control input-sm" id="partnerWebsiteURL" name="partnerWebsiteURL" value="<%=productionEnvt.getPaymentWebsiteUrl()%>">
                                                            <%} else {%>
                                                            <input type="text" class="form-control input-sm" id="partnerWebsiteURL" name="partnerWebsiteURL">
                                                            <%}%>
                                                        </div>
                                                    </div>

                                                    <div class="form-group"><label class="col-sm-2 control-label text-left">Bank Name</label>
                                                        <div class="col-sm-10">
                                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign) && productionEnvt.getPaymentBankName() != null) {%>
                                                            <input type="text" class="form-control input-sm" id="partnerBankName" name="partnerBankName" value="<%=productionEnvt.getPaymentBankName()%>">
                                                            <%} else {%>
                                                            <input type="text" class="form-control input-sm" id="partnerBankName" name="partnerBankName">
                                                            <%}%>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label class="col-sm-2 control-label text-left">Bank Branch</label>
                                                        <div class="col-sm-10">
                                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign) && productionEnvt.getPaymentBankBranch() != null) {%>
                                                            <input type="text" class="form-control input-sm" id="partnerBankBranch" name="partnerBankBranch" value="<%=productionEnvt.getPaymentBankBranch()%>">
                                                            <%} else {%>
                                                            <input type="text" class="form-control input-sm" id="partnerBankBranch" name="partnerBankBranch">
                                                            <%}%>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label class="col-sm-2 control-label text-left">Bank Account No.</label>
                                                        <div class="col-sm-10">
                                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign) && productionEnvt.getPaymentBankAcctNo() != null) {%>
                                                            <input type="text" class="form-control input-sm" id="partnerBankAccountNo" name="partnerBankAccountNo" value="<%=productionEnvt.getPaymentBankAcctNo()%>">
                                                            <%} else {%>
                                                            <input type="text" class="form-control input-sm" id="partnerBankAccountNo" name="partnerBankAccountNo">
                                                            <%}%>
                                                        </div>
                                                    </div>	
                                                    <div class="form-group"><label class="col-sm-2 control-label text-left">SWIFT Code</label>
                                                        <div class="col-sm-10">
                                                            <%if (productionEnvt != null && (productionEnvt.getCompanyType() == GlobalStatus.Berhad || productionEnvt.getCompanyType() == GlobalStatus.Sdn_Bhd || productionEnvt.getCompanyType() == GlobalStatus.Foreign) && productionEnvt.getPaymentSwiftcode() != null) {%>
                                                            <input type="text" class="form-control input-sm" id="partnerSwiftCode" name="partnerSwiftCode" value="<%=productionEnvt.getPaymentSwiftcode()%>">
                                                            <%} else {%>
                                                            <input type="text" class="form-control input-sm" id="partnerSwiftCode" name="partnerSwiftCode">
                                                            <%}%>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Remark: Following field only display when developer choose payment gateway (END) -->											
                                        </form>
                                        <div style="background: #fff; height: 200px">
                                            <div class="col-sm-12">
                                                <form action="#" method="post" class="dropzone"></form>								
                                                <small>Upload Form 9/49 (Sdn Bhd), Form 8 & 23 (Berhad) / Form 79,90/80A/83/83A (Foreign Company) document within one zip file. Support zip. File size limit 5mb</small> <!-- Jack Remark: Compulsory -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="checkbox">
                                        <input id="tncEnterprise" name="tncEnterprise" value="true" type="checkbox">
                                        <label for="tncEnterprise">
                                            Read and agree to <a href="t&c.jsp" target="_blank">Terms of Service</a> and <a href="privacy.jsp" target="_blank"> Privacy policy </a>to continue subscription <!-- Jack Remark: Compulsory -->
                                        </label>
                                    </div>
                                    <div class="pull-right">
                                        <div class="btn-group">
                                        </div>
                                    </div>
                                    <button class="btn btn-primary ladda-button" data-style="zoom-in" id="registerDeveloperEnterpriceDetailButton" onclick="addDeveloperEnterpriceProductionDetail()">Subscribe</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>


        $(document).ready(function () {

            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone("#my-dropzone", {
                url: "UploadZip",
                uploadMultiple: true,
                maxFilesize: 5,
                maxFiles: 1,
                acceptedFiles: ".zip",
                dictInvalidFileType: "You can't upload files of this type, only Zip file",
                autoProcessQueue: true,
                parallelUploads: 1,
                addRemoveLinks: true
            });
        });
    </script>
    <!-- Footer-->
    <%@include file="footer.jsp"%>
