<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="java.net.URL"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt"%>
<%@page import="java.math.BigDecimal"%>
<!DOCTYPE html>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgProductionAccess"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.EmailManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgEmailticket"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPaymentdetails"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.LoanManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgLoanDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCreditInfo"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgUsers"%>
<%@page import="java.util.List"%>
<html>
    <%response.addHeader("X-FRAME-OPTIONS", "DENY");%>    
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>Ready API</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" type="images/front-logo.png" href="images/front-logo.png" />

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="vendor/bootstrap-star-rating/css/star-rating.css" />
        <link rel="stylesheet" href="vendor/datatables.net-bs/css/dataTables.bootstrap.min.css" />
        <link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
        <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
        <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
        <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
        <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
        <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css" />
        <link rel="stylesheet" href="vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
        <link rel="stylesheet" href="vendor/summernote/dist/summernote.css" />
        <link rel="stylesheet" href="vendor/summernote/dist/summernote-bs3.css" />
        <link rel="stylesheet" href="vendor/jquery-ui/themes/base/all.css" />
        <link rel="stylesheet" href="vendor/dropzone/dropzone.css">
        <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
        <link rel="stylesheet" href="vendor/ladda/dist/ladda-themeless.min.css" />
        <link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
        <link rel="stylesheet" href="vendor/codemirror/style/codemirror.css" />
        <link rel="stylesheet" href="vendor/fooTable/css/footable.core.min.css" />
        <!--         <link rel="stylesheet" href="vendor/c3/c3.min.css" />-->
        <!-- App styles -->
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="styles/static_custom.css">
        <link rel="stylesheet" href="styles/style.css">
        <link rel="stylesheet" href="vendor/bootstrap-tour/build/css/bootstrap-tour.min.css" />

        <!-- Need call first -->
        <script src="scripts/header.js" type="text/javascript"></script>
        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/summernote/dist/summernote.min.js"></script>
        <script src="vendor/codemirror/script/codemirror.js"></script>
        <script src="vendor/codemirror/javascript.js"></script> 
        <link rel="stylesheet" href="vendor/c3/c3.min.css" />
        <script src="vendor/d3/d3.min.js"></script>
        <script src="vendor/c3/c3.min.js"></script>
        <link rel="stylesheet" href="vendor/chartist/custom/chartist.css" />
        <script src="vendor/chartist/dist/chartist.min.js"></script>
        <script src="scripts/homepage.js" type="text/javascript"></script>
        <script src="vendor/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
        <!--<script src="vendor/jquery-flot/jquery.flot.pie.js" type="text/javascript"></script>-->
    </head>
    <%
        final int ADMIN = 0;
        final int OPERATOR = 1;
        final int REPORTER = 2;
        SgUsers rUser = null;
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String profileImageUrl = (String) request.getSession().getAttribute("profileImageUrl");
        String socialNamed = (String) request.getSession().getAttribute("socialNamed");
        if (SessionId == null) {
            response.sendRedirect("logout.jsp");
            return;
        }
        if (socialNamed == null) {
            socialNamed = parObj.getPartnerName();
        }
        String PartnerType = "ADMIN";
        rUser = (SgUsers) request.getSession().getAttribute("_SgUsers");
        if (rUser.getType() == OPERATOR) {
            PartnerType = "OPERATOR";
        } else if (rUser.getType() == REPORTER) {
            PartnerType = "REPORTER";
        }
        if (rUser.getType() == ADMIN) {
            PartnerType = "ADMIN";
        }
        //String mailBoxFunctionality = LoadSettings.g_sSettings.getProperty("functionalityOf.ticket.email.booleanValue");

        SgDeveloperProductionEnvt productObj = new ProductionAccessEnvtManagement().getProudDetailsByPartnerId(parObj.getPartnerId());
        int notificationCount = 0;
        String appurl = (request.getRequestURL().toString());
        URL myAppUrl = new URL(appurl);
        int port = myAppUrl.getPort();
        if (myAppUrl.getProtocol().equals("https") && port == -1) {
            port = 443;
        } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
            port = 80;
        }
        String apiDocsURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + "/APIDoc" + "/";

        // find the Service Count
        int serviceCount = 0;
        Accesspoint Res1[] = null;
        Res1 = new AccessPointManagement().getAllAccessPoint(SessionId, ChannelId);
        if (Res1 != null) {
            for (int i = 0; i < Res1.length; i++) {
                if (Res1[i].getStatus() == GlobalStatus.ACTIVE) {
                    Accesspoint apdetails = Res1[i];
                    if (apdetails != null) {
                        int resID = -1;
                        String resourceList = apdetails.getResources();
                        String[] resids = resourceList.split(",");
                        for (int j = 0; j < resids.length; j++) {
                            if (!resids[j].isEmpty() && resids[j] != null) {
                                resID = Integer.parseInt(resids[j]);
                                ResourceDetails rsName = new ResourceManagement().getResourceById(SessionId, ChannelId, resID);
                                TransformDetails tmdetail = new TransformManagement().getTransformDetails(SessionId, ChannelId, apdetails.getApId(), resID);
                                if (rsName != null && tmdetail != null) {
                                    int g = Integer.parseInt(parObj.getPartnerGroupId());
                                    if (g == apdetails.getGroupid()) {
                                        serviceCount++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    %>

    <body class="fixed-navbar">
        <style>
            span.capitalize {
                text-transform: capitalize;
            }
        </style>        
        <!-- Simple splash screen-->
        <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Header -->
        <div id="header">
            <div class="color-line">
            </div>
            <div id="logo" class="light-version">
                <span>
                    Ready API
                </span>
            </div>
            <nav role="navigation">
                <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
                <div class="small-logo">
                    <span class="text-primary">Ready API</span>
                </div>
                <div class="mobile-menu">
                    <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
                        <i class="fa fa-chevron-down"></i>
                    </button>
                    <div class="collapse mobile-navbar" id="mobile-collapse">
                        <ul class="nav navbar-nav">
                            <li>
                                <a class="" href="login.jsp">Login</a>
                            </li>
                            <li>
                                <a class="" href="login.jsp">Logout</a>
                            </li>
                            <li>
                                <a class="" href="profile.jsp">Profile</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="navbar-right">
                    <ul class="nav navbar-nav no-borders">
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                <i class="pe-7s-keypad" title="My Menu"></i>
                            </a>

                            <div class="dropdown-menu hdropdown bigmenu animated flipInX">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a onclick="changePassword()">
                                                    <i class="pe pe-7s-key text-info"></i>
                                                    <h5>Change Password</h5>
                                                </a>
                                            </td>
                                            <!--                                            <td>
                                                                                            <a href="my_profile.jsp">
                                                                                                <i class="pe pe-7s-users text-success"></i>
                                                                                                <h5>My Profile</h5>
                                                                                            </a>
                                                                                        </td>-->
                                            <td>
                                                <a onclick="helpDesk()">
                                                    <i class="pe pe-7s-mail text-warning"></i>
                                                    <h5>Raise Ticket</h5>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>" target="_blank">
                                                    <i class="pe pe-7s-help2 text-danger"></i>
                                                    <h5>API User Guide</h5>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a onclick="subscription()">
                                                    <i class="pe pe-7s-shopbag text-info"></i>
                                                    <h5>Subscription</h5>
                                                </a>
                                            </td> 
                                            <td>
                                                <a onclick="myInvoice()">
                                                    <i class="pe pe-7s-cash text-warning"></i>
                                                    <h5>Invoice</h5>
                                                </a>
                                            </td>
                                            <td>
                                                <a class="run-tour">
                                                    <i class="pe pe-7s-info text-danger"></i>
                                                    <h5>Quick Tour</h5>
                                                </a>
                                            </td> 

                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                <!-- Jack Remark: Temporary hide it as no need from now -->
                                                <a href="access_point_policy.jsp">
                                                    <i class="pe pe-7s-speaker text-warning"></i>
                                                    <h5>Access Point Policy</h5>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <!--                        <li class="dropdown">
                                                    <a class="dropdown-toggle label-menu-corner" href="#" data-toggle="dropdown">
                                                        <i class="pe-7s-mail"></i><div style="font-size:9px"></div>
                                                        <span class="label label-success" id="notificationSpan"><%=notificationCount%></span>
                                                    </a>
                                                    <ul class="dropdown-menu hdropdown animated flipInX">-->
                        <!--                        <div class="title">
                                                    You have new messages
                                                </div>-->
                        <%
                            SgPaymentdetails pDetailsObj = null;
                            double mainCredit = 0;
                            SgCreditInfo creditObj1 = null;

                            SgSubscriptionDetails subscriObject1 = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(parObj.getPartnerId());
                            if (subscriObject1 != null) {
                                PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
                                pDetailsObj = new PaymentManagement().getPaymentDetailsbyPartnerAndSubscriptionID(subscriObject1.getBucketId(), partnerObj.getPartnerId());
                                creditObj1 = new CreditManagement().getDetails(parObj.getPartnerId());
                                if (subscriObject1 != null && subscriObject1.getPaymentMode().equalsIgnoreCase("prepaid")) {
                                    String lowBalance1 = LoadSettings.g_sSettings.getProperty("prepaid.low.balance");
                                    float lBalance1 = 0.0f;
                                    int subscripaidId = subscriObject1.getBucketId();
                                    if (lowBalance1 != null) {
                                        lBalance1 = Float.parseFloat(lowBalance1);
                                    }
                                    if (creditObj1 != null) {
                                        if (creditObj1.getMainCredit() < lBalance1 && creditObj1.getStatus() != GlobalStatus.UPDATED) {
                                            String hefeature = subscriObject1.getFeatureList();
                                            JSONObject hefeatJSONObj = null;
                                            String heloanLimit = "0";
                                            String heinterestRate = "0";
                                            if (hefeature != null) {
                                                JSONArray alertJson = new JSONArray(hefeature);
                                                for (int i = 0; i < alertJson.length(); i++) {
                                                    JSONObject jsonexists1 = alertJson.getJSONObject(i);
                                                    if (jsonexists1.has(subscriObject1.getBucketName())) {
                                                        hefeatJSONObj = jsonexists1.getJSONObject(subscriObject1.getBucketName());
                                                        if (hefeatJSONObj != null) {
                                                            heloanLimit = hefeatJSONObj.getString("loanLimit");
                                                            heinterestRate = hefeatJSONObj.getString("interestRate");
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if (Float.parseFloat(heloanLimit) > 0 && Float.parseFloat(heinterestRate) > 0) {
                                                SgLoanDetails loandetailObj1 = new LoanManagement().getLoanDetails(SessionId, ChannelId, parObj.getPartnerId(), subscriObject1.getBucketId());
                                                if ((loandetailObj1 != null && loandetailObj1.getStatus() == GlobalStatus.PAID) || loandetailObj1 == null) {
                        %> 

                        <!--                                <li>
                                                            <a href="getLoan.jsp">
                                                                Your balance <%=creditObj1.getMainCredit()%>, is very low. Get Loan
                                                            </a>
                                                        </li>-->
                        <%
                                    }
                                }
                            }
                            if (subscriObject1.getStatus() == GlobalStatus.PAID) {%>
                        <!--                                <li>
                                                            <a onclick="myInvoice()">
                                                                Get Invoice
                                                            </a>
                                                        </li>-->
                        <%  }
                            }
                        } else if (subscriObject1 != null && subscriObject1.getPaymentMode().equalsIgnoreCase("postpaid")) {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss a");
                            String todayDate = dateFormat.format(new Date());
                            String expiryDate = dateFormat.format(subscriObject1.getExpiryDateNTime());
                            Date curDate = dateFormat.parse(todayDate);
                            Date exDate = dateFormat.parse(expiryDate);
                            if (curDate.after(exDate) && pDetailsObj == null && subscriObject1.getStatus() == GlobalStatus.UNPAID) {
                                notificationCount++;
                        %>         
                        <!--                                <li>
                                                            <a>
                                                                Your package is expired
                                                            </a>
                                                        </li>-->
                        <script>
                            document.getElementById("notificationSpan").innerHTML = '<%=notificationCount%>';
                        </script>
                        <%
                                    }
                                }
                            }
                            int productionAccessRejectioncount = 0;
                            SgProductionAccess[] productionAccessdetails = new ProductionAccessManagement().getAllRejectedDetailsByPartnerId(SessionId, ChannelId, parObj.getPartnerId(), GlobalStatus.REJECTED);
                            if (productionAccessdetails != null) {
                                productionAccessRejectioncount = productionAccessdetails.length;
                            }
                            if (productionAccessRejectioncount >= 1) {
                        %>    
                        <!--                                <li>
                                                            <a href="productionAccessRejected.jsp">
                                                                Production Access
                                                            </a>
                                                        </li>-->
                        <%
                            }
                        %>
                        <!--                            </ul>
                                                </li>-->
                        <li class="dropdown">
                            <a href="logout.jsp">
                                <i class="pe-7s-upload pe-rotate-90" title="Logout"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <!-- Navigation -->
        <aside id="menu">
            <div id="navigation">
                <div class="profile-picture">
                    <a onclick="home()">
                        <%if (profileImageUrl != null) {%>
                        <img src="<%=profileImageUrl%>" class="img-circle m-b" alt="profile">
                        <%} else {%>                        
                        <img src="images/images.png" class="img-circle m-b" alt="profile" width="90"/>                        
                        <%}%>
                    </a>
                    <div class="stats-label text-color">
                        <span class="font-extra-bold font-uppercase"><%=socialNamed%></span>
                    </div>
                    <div class="tour-1">
                        <div id="sparkline1" class="small-chart m-t-sm"></div>
                    </div>
                    <hr/>
                    <div class="tour-2">
                        <h4 class="font-extra-bold m-b-xs">
                            <%
                                String expiry = "NA";
                                if (creditObj1 != null) {
                                    mainCredit = creditObj1.getMainCredit();
                                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a");
                                    expiry = dateFormatter.format(subscriObject1.getExpiryDateNTime());
                                }
                            %>
                            <font id="mainCreditOfDev"> <%=new BigDecimal(mainCredit).doubleValue()%></font>       
                        </h4>
                        <small class="text-muted">Remaining Credits</small></br>
                        <small class="text-muted">Expires On <%=expiry%></small>
                    </div>
                </div>

                <ul class="nav" id="side-menu">
                    <!--                    <li class="active">
                                            <a href="home.jsp"> <span class="capitalize">Dashboard</span> </a>
                                        </li>                    -->

                    <li class="tour-3">
                        <!--<a href="apiConsole.jsp"><span class="capitalize">Services <span class="label label-success pull-right"><%=serviceCount%></span> </a>-->
                        <a  onclick="apiServices()"><span class="capitalize">Services <span class="label label-success pull-right"><%=serviceCount%></span></span> </a>
                    </li>
                    <li class="tour-4">
                        <a onclick='reports()'><span class="capitalize">Reports </span> </a>
                    </li>
                    <li class="tour-5">
                        <a onclick='myInvoice()'> <span class="capitalize">My Invoice</span></a>
                    </li>
                    <li class="tour-6">
                        <%if (parObj.getStripeSubscriptionId() != null) {%>
                        <a onclick='subscribe()'><span class="capitalize">Subscribe Package</span></a>
                        <%} else {%>
                        <a onclick='subscribeNew()'><span class="capitalize">Subscribe Package</span></a>
                        <%}%>
                    </li>
                    <li class="tour-7">
                        <a onclick="helpDesk()"> <span class="capitalize">Helpdesk</span></a>
                    </li>
                </ul>
            </div>
        </aside>
        
        <div id="wrapper4">
            <div id="wrapper">    
                <div id="myInvoiceReport"></div>
            </div>
        </div>
        <div id="wrapper3">
            <div id="wrapper">
                <div id="testApi"></div>
            </div>
        </div>
        <div id="wrapper5">
            <div id="wrapper">
                <div id="reports"></div>
            </div>
        </div>
        <div id="wrapper6">
            <div id="wrapper">
                <div id="subscribePackage"></div>
            </div>
        </div>
        <div id="wrapper7">
            <div id="helpdesk"></div>
        </div>
        <div id="wrapper8">
            <div id="changePassWord"></div>
        </div>                 
        <div id="wrapper1">
            <div id="wrapper">
                <div id="homePage">
                    <div class="content animate-panel">
                        <div class="row">
                            <div class="col-lg-3 tour-8">
                                <div class="hpanel stats">
                                    <div class="panel-heading">
                                        <div class="panel-tools">
                                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                            <a class="closebox"><i class="fa fa-times"></i></a>
                                        </div>
                                        Today's total credit used
                                    </div>
                                    <div class="panel-body h-200">
                                        <div class="stats-icon text-center ">
                                            <i class="pe-7s-share fa-4x"></i>
                                        </div>
                                        <div class="m-t-xl">
                                            <div class="progress m-t-xs full progress-small">
                                                <div id="percentage" aria-valuemax="100" style="width: 10%" aria-valuemin="0" aria-valuenow="0" role="progressbar" class=" progress-bar progress-bar-success">
                                                    <span class="sr-only">100%</span>
                                                </div>
                                            </div> 
                                            <div class="row" style="height: 60px">
                                                <div class="col-xs-6">
                                                    <small class="stats-label">Credit used</small>
                                                    <h4>50</h4>
                                                </div>
                                                <div class="col-xs-6">
                                                    <small class="stats-label">% Credit used</small>
                                                    <h4 class="text-center">10%</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 tour-9">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        <div class="panel-tools">
                                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                            <a class="closebox"><i class="fa fa-times"></i></a>
                                        </div>
                                        Last 7 days total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-graph1 fa-4x"></i>
                                        <h1 class="m-b-xs text-success" id="lastWeekAmount">300</h1>
                                        <small id="weekMsg">You have used total 300 credits in last week for all services</small>
                                        <h3 class="font-bold no-margins lead text-success row">
                                            Weekly expense
                                        </h3> <!--<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit</small>-->
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 tour-10">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        <div class="panel-tools">
                                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                            <a class="closebox"><i class="fa fa-times"></i></a>
                                        </div>
                                        Last month total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-global fa-4x"></i>
                                        <h1 class="m-b-xs text-success" id="monthCredit">1000</h1>

                                        <small id="monthMsg">You have used total 1000 credits in last week for all services</small>
                                        <h3 class="font-bold no-margins lead text-success row">
                                            Monthly usage
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 tour-12" id="topmostServices" data-child="hpanel" data-effect="fadeInLeft">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        <div class="panel-tools">
                                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                            <a class="closebox"><i class="fa fa-times"></i></a>
                                        </div>
                                        <span id="mostlyFive"></span>
                                    </div>
                                    <div class="panel-body" style="height: 213px">
                                        <div class="flot-chart text-center" style="width: 200;height:250">
                                            <div class="flot-chart-content"  id="flot-pie-chart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3" id = "mostlyservices"  style="display: none">
                                <div class="hpanel">
                                    <div class="panel-heading">
                                        <div class="panel-tools">
                                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                            <a class="closebox"><i class="fa fa-times"></i></a>
                                        </div>
                                        Your mostly used services
                                    </div>
                                    <div class="panel-body">                                        
                                        <img src="images/no_record_found.jpg" alt="No record found" width="200" height="170"/>
                                        <br>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row tour-11">
                            <div class="col-lg-12 ">
                                <div class="hpanel ">
                                    <div class="panel-heading">
                                        <div class="panel-tools">
                                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                            <a class="closebox"><i class="fa fa-times"></i></a>
                                        </div>
                                        Services Information And Statistics
                                    </div>
                                    <div class="panel-body" >
                                        <div class="text-left">
                                            <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Last 7 day expenditure</span>
                                        </div>
                                        <div class="btn-group" style="padding-left: 60%">
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in">Top five service</button>
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" >Today total expenditure</button>
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in">Todays trending services</button>
                                        </div>
                                        <div  id="topFiveSer"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a onclick = "apiServices()" class="text-success" data-toggle="tooltip" data-placement="right" title="" style="display:none" id="apiServicesWindow"><i class="pe pe-7s-science pe-3x"></i></a>
                <%@include file="footer.jsp"%>
            </div>
        </div>
        
        <div id="wrapper2">
            <div id="wrapper">
                <div id="apiconsole" ></div>
            </div>
        </div>
            <div id="wrapper">
        <div class="content animate-panel">
                <div class="row">
                    <div class="col-lg-6 tour-21">				
                        <div class="hpanel">		
                            <div class="panel-heading">
                                <h3>Your request</h3>
                            </div>				
                            <div class="panel-body">					                                        
                                <form  role="form" class="form-horizontal" id="getBody" name="getBody">
                                    <h3>Header</h3>
                                    <table id="_headerTable" CELLSPACING="15" class="tour-23">                                                
                                        <tr>                                                    
                                            <td width="35%">
                                                <input type="text" class="form-control" id="_myNameRM" name="_myNameRM" value="APITokenId">                                                        
                                            </td>
                                            <td width="90%">
                                                <div style='padding: 1%'>
                                                    <input type="text" class="form-control" id="_myResType" name="_myResType" value="vEBDceyF4TRRkZq1qBo+NaYRgV4=">                                                        
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="35%">
                                                <input type="text" class="form-control" id="_myNameRM" name="_myNameRM" value="PartnerId">                                                        
                                            </td>
                                            <td width="90%">
                                                <div style='padding: 1%'>
                                                    <input type="text" class="form-control" id="_myResType" name="_myResType" value="XteIg8dYAYwRiHGz0A300Lwog+w=">                                                        
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="35%">
                                                <input type="text" class="form-control" id="_myNameRM" name="_myNameRM" value="PartnerTokenId">                                                        
                                            </td>
                                            <td width="90%">
                                                <div style='padding: 1%'>
                                                    <input type="text" class="form-control" id="_myResType" name="_myResType" value="wkV52UiG64dweuOFX+5UoJXNfG4=">                                                        
                                                </div>
                                            </td>
                                            <td >                                                        
                                                <a class='btn btn-info btn-sm'  onclick="addHeader()" id="addUserButton"><span class='fa fa-plus-circle'></span></a>
                                            </td>  
                                            <td style="padding: 1%">
                                                <a class='btn btn-warning btn-sm' onclick="removeHeader()" id="minusUserButton"><span class='fa fa-minus-circle'></span></a>
                                            </td>                                              
                                        </tr>                                                   
                                    </table>
                                    <div class="hr-line-dashed"></div>
                                    <div style="display: none">
                                        <h3>Basic Authentication</h3>
                                        <div class="form-group">
                                            <div class="col-sm-6"><input type="text" id="_userNameAuth" name="_userNameAuth"  class="form-control" placeholder="Username"></div>
                                            <div class="col-sm-6"><input type="text" id="_passwordAuth" name="_passwordAuth" class="form-control" placeholder="Password"></div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                    </div>
                                    <div class="tour-24">
                                        <h3>Body</h3>
                                        <textarea class="form-control" id="_APIbody" name="_APIbody" style="min-height: 500px;">
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.wsdl.wadl.sg.mollatech.com/">
        <soapenv:Header/>
        <soapenv:Body>
            <web:createChatRoom>
                <!--Optional:-->
                <developerEmail>abhishek@mollatech.com</developerEmail>
                <!--Optional:-->
                <numberOfUsers>2</numberOfUsers>
                <!--Optional:-->
                <groupName>Buzz</groupName>
                <!--Zero or more repetitions:-->
                <userEmailIds>jerry@noemail.com</userEmailIds>
                <userEmailIds>tom@noemail.com</userEmailIds>
                <!--Optional:-->
                <urlexpiryTimeInMinutes>15</urlexpiryTimeInMinutes>
            </web:createChatRoom>
        </soapenv:Body>
    </soapenv:Envelope>
                                        </textarea>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <button class="btn btn-primary ladda-button" data-style="zoom-in" onclick="SendRequest()" id="apiConsoleRequest" type="button"><i class="fa fa-terminal"></i> Submit</button>                                    
                                </form>								
                            </div>					
                        </div>				
                    </div>
                    <div class="col-lg-6 tour-22">
                        <div class="hpanel">
                            <div class="panel-heading">
                                <h3>Your response</h3>
                            </div>
                            <div class="panel-body">
                                <form>
                                    <textarea style="min-height: 220px;" class="form-control" id="responseID" name="responseID">
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:createChatRoomResponse xmlns:ns2="http://webservice.wsdl.wadl.sg.mollatech.com/" xmlns:ns3="http://com.mollatech.sg.sgholder">
            <return>
                <chatURL>https://www.readyapis.com:9443/WebChat/index.jsp?3a4e8f81-f33d-4c81-8b04-54a162de020c</chatURL>
                <resultCode>0</resultCode>
                <result>3a4e8f81-f33d-4c81-8b04-54a162de020c Chat Room Is Created Successfully.</result>
                <password>55f0a7</password>
            </return>
        </ns2:createChatRoomResponse>
    </S:Body>
</S:Envelope>
                                    </textarea>
                                </form>
                                <br>
                                <div id="startValuesAndTargetExample">
<p class="text-info"><i class="fa fa-check-square"></i> Congratulations, Your API takes  00:06 sec to get response. </p>
                                </div>
                            </div>                            
                        </div>
                        <div id="questionAndAnswer">
                            <div class="hpanel panel-group tour-25" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel-body">
                                    <h4 class="m-t-none m-b-none">Need Help </h4>
                                    <small class="text-muted">All general questions about our API.</small>&nbsp;&nbsp;&nbsp;
                                    <a  class="btn btn-primary btn-xs ladda-button" data-style="zoom-in" type="button" href="#" onclick="apiHelpDesk()" style="margin-left: 1px"><i class="fa fa-check"></i> Raise Ticket</a> <br>
                                </div>
                                <div class="panel-body" id="_apiLevelToken" >
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q1" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Where can I get APITokenId?
                                    </a>
                                    <div id="q1" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div>                                                               <!--
-->                                <div class="panel-body" id="_headerError">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q2" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Why need Headers and How to add?
                                    </a>
                                    <div id="q2" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                             <div class="panel-body" id="connectionrefuse" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q3" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Connection refused (Connection refused)?
                                    </a>
                                    <div id="q3" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
                                
-->                                <div class="panel-body" id="_noPartnerAvailable" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q4" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        No Such Partner is Available.
                                    </a>
                                    <div id="q4" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q5" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        No Such Webservice is Available.
                                    </a>
                                    <div id="q5" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_groupDisable" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q6" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Your Associated Group Is Not Active. Contact Administrator.
                                    </a>
                                    <div id="q6" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
                                --> <div class="panel-body" id="_resourseUnAvai" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q7" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Internal Resource IS Not Active. Contact Administrator.
                                    </a>
                                    <div id="q7" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
                                
                                --><div class="panel-body" id="_accDiasable" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q8" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Your Account Is Not Active. Contact Administrator.
                                    </a>
                                    <div id="q8" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_partnerDisable" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q9" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Partner Is Not Active.
                                    </a>
                                    <div id="q9" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="initializeApiToken" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q10" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Service Guard Error: Please Initialise API Level Token.
                                    </a>
                                    <div id="q10" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_parameterMismatch" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q11" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Service Guard Error: Parameter Mismatch
                                    </a>
                                    <div id="q11" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div>                                
                                <div class="panel-body">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q12" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Service Guard Error: Please Contact Administrator.
                                    </a>
                                    <div id="q12" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>                                
                                </div><!--
-->                                <div class="panel-body" id="_unAuthorizedPart" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q13" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        You Are Not Autherized Partner.
                                    </a>
                                    <div id="q13" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_reachedLimit" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q14" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Service Guard Error: You have reached your Daily limit.
                                    </a>
                                    <div id="q14" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div>
                    <div class="panel-body" id="_transactionsec" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q15" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Service Guard Error: Transaction Per Second Error.
                                    </a>
                                    <div id="q15" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
                                --><div class="panel-body" id="_accessinvalidIp" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q16" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                         You are accessing from invalid IP.
                                    </a>
                                    <div id="q16" class="panel-collapse collapse">
                                        <hr>please check if you are behind proxy or using dynamic IP.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_serNotaccesible" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q17" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        You cannot access service now as it is not permitted for this day in the week.
                                    </a>
                                    <div id="q17" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="serNOtPermited" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q18" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        You cannot access service now as it is not permitted for this time of the day.
                                    </a>
                                    <div id="q18" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="invalidCert" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q20" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        You are offering invalid/empty certificate.
                                    </a>
                                    <div id="q20" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_invalidToken" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q21" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                       You are offering invalid/empty Token.
                                    </a>
                                    <div id="q21" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_ipblock" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q22" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        IP BLOCK.
                                    </a>
                                    <div id="q22" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                                <div class="panel-body" id="_invalidToken" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q23" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        You are offering invalid/empty Token For API.
                                    </a>
                                    <div id="q23" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div><!--
-->                             <div class="panel-body" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q24" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Signature Verification Failed.
                                    </a>
                                    <div id="q24" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div> 
                                <div class="panel-body" id="_operationTimeOut" style="display: none">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#q25" aria-expanded="true">
                                        <i class="fa fa-chevron-down pull-right text-muted"></i>
                                        Operation timed out (Connection timed out).
                                    </a>
                                    <div id="q25" class="panel-collapse collapse">
                                        <hr>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    </div>
                                </div>     
<!--                            </div>                            -->
                            </div>
                        </div>
                    </div>             
                </div>
            </div>    
        </div>
        <style>
            .tour-step-background{background: none!important;}
        </style>     

        <script type="text/javascript">
            apiServicesTour();
            $('#testFirstAPI').click();
            document.onreadystatechange = function () {
                if (document.readyState === 'complete') {
                    $('#topFiveSer').ready(function () {
                        generateTopServiceTour();
                    });

                    $('#flot-pie-chart').ready(function () {
                        yourMostlyServiceTour();
                    });
                }
            };
            $('#wrapper2').hide();
            $('#wrapper3').hide();
            $('#wrapper4').hide();
        </script>


        <!--</div>-->
        <!--<div id="apiconsole"></div>-->
        <script>/*<![CDATA[*/window.zEmbed || function (e, t) {
                var n, o, d, i, s, a = [], r = document.createElement("iframe");
                window.zEmbed = function () {
                    a.push(arguments)
                }, window.zE = window.zE || window.zEmbed, r.src = "javascript:false", r.title = "", r.role = "presentation", (r.frameElement || r).style.cssText = "display: none", d = document.getElementsByTagName("script"), d = d[d.length - 1], d.parentNode.insertBefore(r, d), i = r.contentWindow, s = i.document;
                try {
                    o = s
                } catch (e) {
                    n = document.domain, r.src = 'javascript:var d=document.open();d.domain="' + n + '";void(0);', o = s
                }
                o.open()._l = function () {
                    var e = this.createElement("script");
                    n && (this.domain = n), e.id = "js-iframe-async", e.src = "https://assets.zendesk.com/embeddable_framework/main.js", this.t = +new Date, this.zendeskHost = "mollatechhelp.zendesk.com", this.zEQueue = a, this.body.appendChild(e)
                }, o.write('<body onload="document._l();">'), o.close()
            }();
            /*]]>*/
                </script>
        <script>
            //    document.onreadystatechange = function(){
            //     if(document.readyState === 'complete'){
            //         $('#homepage').ready(function() {
            //       sethomepage();
            //        });
            //    }};
            
            $(document).ready(function () {
                //var creditUsedData = fifteenDaysCreditExpenditure();
                var obj = [5, 6, 7, 2, 0, 4, 2, 4, 5, 7, 2, 4, 12, 11, 4];
//                for (var key in creditUsedData) {
//                    var value = creditUsedData[key];
//                    obj.push(value.txAmount);
//                    //        alert(JSON.stringify(key)+" :: "+JSON.stringify(value.apiName));
//                }
                $("#sparkline1").sparkline(obj, {
                    type: 'bar',
                    barWidth: 7,
                    height: '30px',
                    barColor: '#62cb31',
                    negBarColor: '#53ac2a'
                });
                
                var doc = document.getElementById('_APIbody');
                var dataCode = CodeMirror.fromTextArea(doc, {
                    lineNumbers: true,
                    matchBrackets: true,
                    styleActiveLine: true,
                    tabMode: "indent",
                });
                dataCode.on('change', function ash() {
                    $('#_APIbody').val(dataCode.getValue());
                });

                var doc = document.getElementById('responseID');
                var dataCode = CodeMirror.fromTextArea(doc, {
                    lineNumbers: true,
                    matchBrackets: true,
                    styleActiveLine: true,
                    tabMode: "indent",
                });
                dataCode.on('change', function ash() {
                    $('#responseID').val(dataCode.getValue());
                });
            });
            //    
            function fifteenDaysCreditExpenditure() {
                var s = './DailyTransactionAmount';
                var jsonData = $.ajax({
                    url: s,
                    dataType: "json",
                    async: false
                }).responseText;
                if (jsonData.length === 0) {
                    jsonData = [{"label": "NA", "value": 0, "value5": 0}];
                    return jsonData;
                } else {
                    var myJsonObj = JSON.parse(jsonData);
                    return myJsonObj;
                }
            }

            function getCreditStatus() {
                var s = './GetCredit';
                $.ajax({
                    type: 'POST',
                    url: s,
                    datatype: 'json',
                    data: $("#getBody").serialize(),
                    success: function (data) {
                        if (data.credits !== 'NA') {
                            $('#mainCreditOfDev').html(data.credits);
                        }
                    }});
            }
            getCreditStatus();
            
            
            $(function () {
                // Instance the tour
                var tour = new Tour({
                    backdrop: true,
                    backdropContainer:'body',
                    onShown: function (tour) {

                        // ISSUE    - https://github.com/sorich87/bootstrap-tour/issues/189
                        // FIX      - https://github.com/sorich87/bootstrap-tour/issues/189#issuecomment-49007822

                        // You have to write your used animated effect class
                        // Standard animated class
                        $('.animated').removeClass('fadeIn');
                        // Animate class from animate-panel plugin
                        $('.animated-panel').removeClass('zoomIn');

                    },                    
                    steps: [
                        {
                            element: ".tour-1",
                            title: "15 days credit used",
                            content: "From here you get information of your credit usage from last 15 days.",
                            placement: "bottom"
                        },
                        {
                            element: ".tour-2",
                            title: "Remaining Credit",
                            content: "Your remaining credit and expire date and time of your package.",
                            placement: "top"

                        },
                        {
                            element: ".tour-3",
                            title: "Services",
                            content: "You can get service details with your APIToken key, API description, API today and monthly credit and performance reports also test the API with our API console in SOAP/REST envirnoment.",
                            placement: "right"

                        },
                        {
                            element: ".tour-4",
                            title: "Reports",
                            content: "Reports provide visibility into APIs and services from different perspectives with API performance, API credit used, with dynamic time frame ",
                            placement: "right"

                        },
                        {
                            element: ".tour-5",
                            title: "My Invoice",
                            content: "You can download your payment invoice also get overview of your payment history.",
                            placement: "right"

                        },
                        {
                            element: ".tour-6",
                            title: "Subscribe Package",
                            content: "You can subscribe/unsubscribe to your package from here. To subscribe to new package firstly you need to unsubscribe from your currently subscribed package.",
                            placement: "right"

                        },
                        {
                            element: ".tour-7",
                            title: "Helpdesk",
                            content: "You can raise a ticket or ask a question to support team.  ",
                            placement: "right"

                        },
                        {
                            element: ".tour-8",
                            title: "Today Used Credit",
                            content: "You can analyse your todays credit used in percentage of your total remaining credit",
                            placement: "right"

                        },
                        {
                            element: ".tour-9",
                            title: "Last 7 days ",
                            content: "You can get total credit used in last seven days.",
                            placement: "right"

                        },
                        {
                            element: ".tour-10",
                            title: "Last month ",
                            content: "You can get total credit used in last month.",
                            placement: "right"

                        },
                        {
                            element: ".tour-12",
                            title: "Mostly Used Services",
                            content: "Top view from the basis of services.",
                            placement: "left"

                        },
                        {                            
                            element: ".tour-11",
                            title: "Service Analysis ",
                            content: "Top view from the basis of services.",
                            placement: "top"
                                                                         
                        },
                        {
                            element: ".tour-13",
                            title: "Know API ",
                            content: "Here you get the API details as per the services you selected.",
                            placement: "top"   
                        },
                        {
                            element: ".tour-14",
                            title: "Regenerate your APITokenId",
                            content: "Regenerate your APITokenId from here",
                            placement: "top"      
                        },
                        {
                            element: ".tour-15",
                            title: "Credit Used",
                            content: "Regenerate your APITokenId from here",
                            placement: "top"
                                                                                   
                        },
                        {
                            element: ".tour-16",
                            title: "Performance Report",
                            content: "Regenerate your APITokenId from here",
                            placement: "top"
                                                                                   
                        },
                        {
                            element: ".tour-17",
                            title: "API Console",
                            content: "Regenerate your APITokenId from here",
                            placement: "top"
                        },
                        {
                            element: ".tour-21",
                            title: "Your request window",
                            content: "From here you can set your rerquired headers and pass the parameter in body.",
                            placement: "top"
                        },
                        {
                            element: ".tour-23",
                            title: "Headers",
                            content: "Here you need to set APITokenId which is unique to API, PartnerId which is your unique id, PartnerTokenId which is unique to your services.",
                            placement: "top"   
                        },
                        {
                            element: ".tour-24",
                            title: "Body",
                            content: "Here you can pass the required parameter for the API. ",
                            placement: "top"
                        },
                        {
                            element: ".tour-22",
                            title: "Response Window",
                            content: "From here you can get the response from the server",
                            placement: "top"
                        },
                        {
                            element: ".tour-25",
                            title: "Need Help",
                            content: "Incase in response you get error message, Here you get the possibility of why this error may occur.",
                            placement: "top"
                        }                       
                    ]});
                // Initialize the tour
                tour.init();
                // Restart the tour
                $(document).ready(function () {
                    tour.restart();
                });
                $('.run-tour').click(function () {
                    tour.restart();
                });
            });
            
        </script>
