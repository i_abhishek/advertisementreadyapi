<%-- 
    Document   : downloadInAppPushFiles
    Created on : Sep 20, 2017, 4:00:27 PM
    Author     : abhishekingle
--%>

<%@page import="com.mollatech.serviceguard.nucleus.db.SgUsers"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<script src="scripts/downloadFiles.js" type="text/javascript"></script>
<%@page import="java.net.URL"%>
<%
    String appurl = (request.getRequestURL().toString());
    PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
    SgUsers sgUser = (SgUsers) request.getSession().getAttribute("_SgUsers");
        URL myAppUrl = new URL(appurl);
        int port = myAppUrl.getPort();
        if (myAppUrl.getProtocol().equals("https") && port == -1) {
            port = 443;
        } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
            port = 80;
        }
        String apiDocsURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + "/APIDoc" + "/";
%>
<style>
    .copied::after {
        position: absolute;
        top: 12%;
        right: 110%;
        display: block;
        content: "copied";
        font-size: 0.75em;
        padding: 2px 3px;
        color: #FFF;
        background-color: #2381c4;
        border-radius: 3px;
        opacity: 0;
        will-change: opacity, transform;
        animation: showcopied 1.5s ease;
    }

    @keyframes showcopied {
        0% {
            opacity: 0;
            transform: translateX(100%);
        }
        70% {
            opacity: 1;
            transform: translateX(0);
        }
        100% {
            opacity: 0;
        }
    }
</style>
<div class="small-header transition animated fadeIn" id="invoiceHeader">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                Downloads
            </h2>
            <small>Get supportive file for the InApp Push service</small>
        </div>
    </div>
</div>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="hpanel">               
                <div class="panel-body">
                    <h3><i class="pe-7s-comment text-info fa-1x"></i>&nbsp;&nbsp; InApp Push </h3>
                    <hr>
                    <p><h5>Need to do following two steps before continue InApp Push on any of the below platform. There are specific requirements for each platform which will run InApp Push. Click on specific platform to view the corresponding requirements. </h5></p>
                    <ul>                                    
                        <li> <h5>Step 1] Call the API "createDeveloper" of an InApp Push Service which returns a developer id. Keep that developer id noted for future reference. </h5></li>
                        <li> <h5>Step 2] Call the API "createApplication" of an InApp Push Service which returns an application id. Keep that application id noted for future reference. </h5></li>                        
                        <li> <h5><i class="fa fa-info-circle text-info"></i> For more information about the API follow the <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>" target="_blank"> API Documentation</a></h5></li>
                    </ul>
                    <br/>
                    <ul class="nav nav-tabs">
                        <!--                        <li class="active"><a data-toggle="tab" href="#tab-1"> <i class="fa fa-laptop"></i> </a></li>-->
                        <li class="active"><a data-toggle="tab" href="#tab-2"><i class="fa fa-desktop fa-2x"></i> </a></li>
                        <li class=""><a data-toggle="tab" href="#tab-5"><i class="fa fa-safari fa-2x"></i> </a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"><i class="fa fa-apple fa-2x"></i> </a></li>
                         <li class=""><a data-toggle="tab" href="#tab-4"><i class="fa fa-android fa-2x"></i> </a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-2" class="tab-pane active">
                            <div class="panel-body">
                                <table>
                                    <tr>
                                    <div class="col-lg-8">
                                        <td>
                                            <h3><strong>Code and Implementation for Web </strong></h3>
                                        </td>
                                    </div>
                                    <div class="col-lg-2">
                                        <td>
                                            <a class="btn btn-info btn-sm" onclick="downloadWebAppPush()" role="button" style="margin-left: 250px">Download</a>
                                        </td>
                                    </div>
                                    </tr>
                                </table>
                               
                                <br/>
                                <div class="text-muted font-bold m-b-xs">
                                    <ul>
                                        <li><h5>Step 1] Download the supportive files for InApp Push. Download contains main.js and sw.js which need to add in your web directory.</h5>                                                    
                                        </li><br/>
                                        <li><h5>Step 2] You have the Developer Id that you get by calling the API "createDeveloper" of an InApp Push Service, which returns a developer id. That developer id need to assign to a variable "developerId" in main.js.</h5>                                                    
                                        </li><br/>
                                        <li><h5>Step 3] You have the Application Id that you get by calling the API "createApplication" of an InApp Push Service, which returns an application id. That application id need to assign to a variable "appplicationId" in main.js.</h5>                                                    
                                        </li><br/>
                                        <li><h5>Step 4] Assign value to a variable "userId" according to your users.</h5>                                                    
                                        </li><br/>
                                        <li><h5>Step 5] Place your callback URL to a variable "callBackURL" in main.js, your mention call back URL get the information about the user device token, as response with "userId" which is unique identification id for the user that you have place in main.js w.r.t "userId", "developerId" which is your developer id, "applicationId" which is your application id, "DeviceToken" which give the information about the user device token.</h5>                                                    
                                        </li><br/>                                                                                    
                                                <table>
                                                    <tr>
                                                        <td>
                                                    <li><h5>Step 6] Copy and Paste this code before &lt;/head&gt;</code> tag on your website page.</h5></li>
                                                        </td>
                                                        <td>
                                                            <font class="col-lg-2" id="scriptCopyDiv"></font>
                                                        </td>
                                                        </tr>
                                                </table>
                                            
                                            <br/>
                                            <textarea class="form-control" name="inAppPushScript" id="inAppPushScript" style="min-height: 270px;">
&lt;script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"&gt;&lt;/script&gt;
&lt;script type="text/javascript"&gt;
    window.onload = inject();
    function inject() {(
            function (a, b, c, d) {
                b.type = c;
                b.src = d;
                a.appendChild(b);
            })
    (document.getElementsByTagName('head')[0], document.createElement('script'), 'text/javascript', 'main.js');
    };
&lt;/script&gt;
                                            </textarea>
                                            <br/>
                                            <script>
    var doc = document.getElementById('inAppPushScript');
    var dataCode = CodeMirror.fromTextArea(doc, {
        lineNumbers: true,
        matchBrackets: true,
        styleActiveLine: true,
        tabMode: "indent",
    });
                                            </script>
                                            <li><h5>Step 7] Allow the push setting and send the push notification to your user using "broadcast", "multicast" and "testMessage" APIs. Below is screen of Push Notification. For more information follow the <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>" target="_blank"> API Documentation</a></h5><br/><br/>                                                    
                                                 <span style="margin-left: 10%"><img class="img-responsive img-thumbnail" src="images/webPush/pushPermission.png" alt="Confirm APP Id" width="450" height=""/></span><br/><br/>
                                                 <span style="margin-left: 10%"><img class="img-responsive img-thumbnail" src="images/webPush/notificationComes.png" alt="Confirm APP Id" width="450" height=""/></span><br/><br/>
                                            </li><br/>                                             
                                        <li><h5>Browser Supports</h5>
                                            <ul>
                                                <li style="font-weight: 400"><i class="fa fa-chrome text-info"></i> Chrome 49+  &nbsp;&nbsp;Supported on Windows, Android, Mac OS X and Linux</li>
                                                <li style="font-weight: 400"><i class="fa fa-firefox text-info"></i> Firefox  &nbsp;&nbsp;52+ &nbsp;&nbsp;Supported on Windows, Mac OS X and Linux</li>                                                
                                                <li style="font-weight: 400"><i class="fa fa-opera text-info"></i> Opera &nbsp;&nbsp;&nbsp;42+&nbsp;&nbsp;&nbsp;Supported on Windows, Mac OS X and Linux</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>                                
                            </div>                                                                                                           
                        </div>
                        <div id="tab-5" class="tab-pane">
                            <div class="panel-body">
                                <table>
                                    <tr>
                                    <div class="col-lg-8">
                                        <td>
                                            <h3><strong>Code and Implementation for Safari </strong></h3>
                                        </td>
                                    </div>
                                    <div class="col-lg-2">
                                        <td>
                                            <%if(sgUser != null && sgUser.getSafariSiteName()!=null){%>
                                            <a class="btn btn-info btn-sm" onclick="downloadWebAppPush()" id="safariJSDownload" role="button" style="margin-left: 250px" >Download</a>
                                            <%}else{%>
                                            <a class="btn btn-info btn-sm disabled" onclick="downloadWebAppPush()" id="safariJSDownload" role="button" style="margin-left: 250px" >Download</a>
                                            <%}%>
                                        </td>
                                    </div>
                                    </tr>
                                </table>
                               
                                <br/>
                                <div class="text-muted font-bold m-b-xs">
                                    <ul>
                                        <li><h4>Step 1] The Certificate Signing Request</h4>
                                            <ul>                                                
                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingOne">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                    A) Introduction
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                            <div class="panel-body">
                                                                Now that you have created the demo project, put it aside for a few minutes, and get ready for the first step in the whole preparation process. Here, we are going to produce a Certificate Signing Request (CSR) file that we’ll use later on to create a SSL certificate for push notifications. For this step you’ll need the Keychain Access Mac app, so either use the Launchpad or the Spotlight search to find it and open it. If you’re new to Keychain Access, make sure you don’t delete any existing stuff accidentally.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingTwo">
                                                            <h4 class="panel-title">
                                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                    B) Request a certificate
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                            <div class="panel-body">
                                                                Once you get there, open the Keychain Access &gt; Certificate Assistant &gt; Request a Certificate From a Certificate Authority… menu, as shown next:  <br/><br/>                                                                      
                                                                <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/requestForCert2.png" alt="Request a Certificate" width="550" height=""/></span><br/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingThree">
                                                            <h4 class="panel-title">
                                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                    C) Fill Certificate Information
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                            <div class="panel-body">
                                                                In the window that comes up, you need to mandatorily fill in the User Email Address and Common Name fields. Besides that, click to the Saved to disk option so you are able to save the CSR file to disk, and use it later in the Apple Developer web site.<br/><br/>
                                                                <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/fillCertInfo.png" alt="Fill a Certificate information" width="" height=""/></span><br/><br/>
                                                                Click Continue and optionally choose a folder and a name for the CSR file right before you save it. I have created a folder to keep together all the files we’re about to produce in this tutorial (called PNDemo Files, I encourage you to do the same), and I’ve left the default file name for the CSR file. Once you see a message saying that your certificate request has been created on disk, click Done and you’re… done. The certificate we just requested for and saved to disk will be used to sign other certificates in the Apple Developer website.
                                                            </div>
                                                        </div>
                                                    </div>          
                                                </div>
                                            </ul>
                                        </li>
                                        
                                        <li><h4>Step 2] Create an Web ID</h4>
                                            <ul>                                                        
                                                <div class="panel-group" id="accordionSafari" role="tablist" aria-multiselectable="true">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingStep2a">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordionSafari" href="#collapseIntro" aria-expanded="true" aria-controls="collapse1">
                                                                    A) Introduction
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseIntro" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingStep2a">
                                                            <div class="panel-body">
                                                                Our next step is to create a new Web ID in the Apple Developer website. This Web ID is going to uniquely identify our web amongst others, and it will help APN servers to properly route the notifications. Actually, as you’ll see next, we’ll connect the Web ID to a couple of other things: To a new certificate for push notifications, First thing, so begin by going to the <a href="https://developer.apple.com/membercenter/" target="_blank">Apple Developer Member Center</a>. Provide your credentials, and get connected. Then, click to the Certificates, Identifiers & Profiles link, so you’re navigated to the proper page. <br/><br/>
                                                                <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/welcomeToAppCenter.png" alt="Welcome to Apple Developer Center" width="" height=""/></span><br/><br/>
                                                            </div>                                                            
                                                        </div>
                                                    </div>                                                                                                                
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingStepSafari">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordionSafari" href="#collapsecreateWebId" aria-expanded="false" aria-controls="collapsecreateAppId">
                                                                    B) Select Option Create Web ID
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapsecreateWebId" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingStepSafari">
                                                            <div class="panel-body">
                                                                On the next screen, under identifiers section, select “Website Push Ids”.<br/><br/>
                                                                <span style="margin-left: 5%"><img class="img-responsive img-thumbnail" src="images/safariPush/webid.png" alt="Create Web Id" width="" height=""/></span><br/><br/>                                                                
                                                                On the next screen, click on the + button in top right.</br></br>
                                                                <span style="margin-left: 5%"><img class="img-responsive img-thumbnail" src="images/safariPush/webidplussign.png" alt="Create Web Id" width="" height=""/></span><br/><br/>
                                                                It’s time now to configure a new App ID for our application. That explain in next step :
                                                            </div>                                                            
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="crateAPPID2">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordionSafari" href="#crateWebstep2" aria-expanded="false" aria-controls="collapsecreateAppId">
                                                                    C) Create Web ID
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="crateWebstep2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="crateAPPID">
                                                            <div class="panel-body">
                                                                For starters, we need to fill in two fields.<br/>
                                                                1. A description about the new Web ID. you can simply use your website title for the description field.<br/>
                                                                2. And for the identifier field, enter a string in the form of com.[company name].[website title], you can choose not to enter company name, but it is generally a good practice (as you type, web followed by a dot will be automatically added to your identifier). After adding the identifier, note down the identifier, we are gonna need it in a later step.
                                                                <br/> <br>                                                              
                                                                <span style="margin-left: 5%"><img class="img-responsive img-thumbnail" src="images/safariPush/registerWebPushId.png" alt="Create Web Id" width="" height=""/></span><br/><br/>                                                                
                                                                <br/>
                                                                After you click continue, you will be asked to confirm the details of your push id, simply click register and than on the next screen click Done to finish the Push Id creation process.
                                                            </div>
                                                        </div>
                                                    </div>                                                                                                                                                                                                                        
                                                </div>
                                            </ul>
                                        </li>
                                        
                                        <li><h4>Step 3] Configure the SSL Certificate</h4>
                                                    <ul>                                                        
                                                        <div class="panel-group" id="accordionSafari3" role="tablist" aria-multiselectable="true">                                                                                                                        
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordionSafari3" href="#collapseStepSafari3B" aria-expanded="true" aria-controls="collapse2">
                                                                            b) Configure SSL Certificate
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseStepSafari3B" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">                                                                        
                                                                        Let’s get going now by clicking to the newly created Web ID in the list so it gets expanded. now there’s a button titled Edit. Click it to Edit. <br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/safariPush/EditPushId.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        Let’s keep going with that process, and in the first step of the guide just click to the Create Certificate button.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/safariPush/createCert.png" alt="Configure SSL Cert" width="470" height=""/></span><br/><br/>
                                                                        It’s about time now to make use of the Certificate Signing Request file we created through the Keychain Access app a couple of parts ago. So, let’s keep going with that process, and in the first step of the guide just click to the Continue button. Here you get just some instructions on how to generate the CSR, in case you haven’t done so already.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/configureSSLCert2.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        Next, click to the Choose File… button so you can search for the CSR file that you generated in the first step. If you hadn’t changed the default file name, then you’re looking for a file titled CertificateSigningRequest.certSigningRequest Finally, click to the Generate blue button that you can see in the following screenshot.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/configureSSLCert3.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        Great, you just created a new certificate that will enable you to send push notifications. Now, you must download it and add it to the keychain (the Keychain Access app on Mac), so proceed by clicking to the Download button.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/configureSSLCert4.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        The file you just downloaded is named aps_development.cer. Spot it in your Downloads folder and double click on it, so it’s added to the collection of certificates in the Keychain Access app. That is explain in next step.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordionSafari3" href="#collapseStepSafari3c" aria-expanded="true" aria-controls="collapse2">
                                                                            c) Add certificate in the keychain access app.
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseStepSafari3c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        <span class="text-danger">An important note</span><br/><br/>                                                                        
                                                                        When the Keychain Access app opens by double clicking to the .cer file, make sure that the new certificate is added to the login keychain, and not to the System or any other keychain. If that happens, then just drag and drop the certificate to the login kaychain. That's important for what we'll do right next.  <br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/keychainAccessApp1.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        Once you locate the new certificate in the Keychain, right click on it, and then click to the Export “…” option.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/exportCert.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        Make sure that the .p12 file format is selected before you click to the Save button.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/exportCert1.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        You can skip setting a password simply by clicking to the OK button in the next window. If you set one, make sure to remember it or write it somewhere, because the file becomes unusable if you forget it. We recommend that set a password as it is needed for "createGateway" API of an InApp push service.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/certPassword.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        Note that in this tutorial we are not going to use the file you just exported. You are going to need it, however, in case you want to test push notifications from a server like Parse, where you have to provide the .p12 file before even sending your first notification. So for now just keep that file along with the rest.<br/>
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                        </div>
                                                    </ul>
                                                </li>
                                                
                                        <li><h5> We required some information from you before proceed, Please fill below details and save </h5>                                                    
                                        </li><br/>
                                        
                                        <form id="safariInfo" class="form-horizontal">
                                            <input type="hidden" id="partnerId" name="partnerId">
                                            <h6><i class="fa fa-server fa-2x text-info">  Domain Information</i></h6><br/>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" >Site Name <span style="color: red">*</span></label>
                                                <div class="col-sm-9">
                                                    <%if(sgUser != null && sgUser.getSafariSiteName()!=null){%>
                                                    <input type="text" id="siteName" placeholder="Ready APIs" name="siteName" value="<%=sgUser.getSafariSiteName()%>" class="form-control" required>                                
                                                    <%}else{%>
                                                    <input type="text" id="siteName" placeholder="Ready APIs" name="siteName" class="form-control" required>                                
                                                    <%}%>
                                                    <span class="help-block small">Your Site name which is used by the notification UI.</span>
                                                </div>                                                                                                                       
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" >Domain Name <span style="color: red">*</span></label>
                                                <div class="col-sm-9">
                                                    <%if(sgUser != null && sgUser.getSafariDomainName()!=null){%>
                                                    <input type="text" id="domainName" placeholder="https://readyapis.com" name="domainName" value="<%=sgUser.getSafariDomainName()%>" class="form-control" required>                                
                                                    <%}else{%>
                                                    <input type="text" id="domainName" placeholder="https://readyapis.com" name="domainName" class="form-control" required>                                
                                                    <%}%>
                                                    <span class="help-block small">For example, If your application running on 8080 port, enter domain name like https://yourdomainname.com:8080 or http://yourdomainname.com:8080</span>
                                                </div>                                                                                                                       
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" >Website Push Id <span style="color: red">*</span></label>
                                                <div class="col-sm-9">
                                                    <%if(sgUser != null && sgUser.getSafariWebPushId()!=null){%>
                                                    <input type="text" id="websitePushID" placeholder="Your Website Push Id" name="websitePushID" value="<%=sgUser.getSafariWebPushId()%>" class="form-control" required>                                
                                                    <%}else{%>
                                                    <input type="text" id="websitePushID" placeholder="Your Website Push Id" name="websitePushID" class="form-control" required>                                
                                                    <%}%>
                                                    <span class="help-block small">Your website push id generated at Apple Development Center as shown in Step2]</span>
                                                </div>                                                                                                                       
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" >p12 Password<span style="color: red">*</span></label>
                                                <div class="col-sm-9">
                                                    <%if(sgUser != null && sgUser.getSafariP12Password()!=null){%>
                                                    <input type="password" id="p12Password" placeholder="Your .p12 file password" name="p12Password" value="<%=sgUser.getSafariP12Password()%>" class="form-control" required>                                
<!--                                                    <input type="checkbox" id="show-hide1" name="show-hide1" value="" />-->
                                                    <%}else{%>
                                                    <input type="password" id="p12Password" placeholder="Your .p12 file password" name="p12Password" class="form-control" required>                                
<!--                                                    <input type="checkbox" id="show-hide1" name="show-hide1" value="" />-->
                                                    <%}%>
                                                    <span class="help-block small">Your p12 file password, Please make sure your password is same as that you have put in Step 3] under C] section</span>
                                                </div>                                                                                                                       
                                            </div>         
                                            <button class="btn btn-primary ladda-button validateSafariForm" data-style="zoom-in" onclick="validateSafariForm()" style="display: none">Save changes</button>    
                                        </form>
                                        
                                        <h6><i class="fa fa-file-o fa-2x text-info"> p12 <span style="color: red">*</span></h6></i><br/>
                                        
                                        <br>
                                        <div style="background: #fff; height: 200px">
                                            <div class="col-sm-7 col-lg-7 col-md-7" style="width: 77%;margin-left: 16%">
                                                <form action="UploadEmailAttachme" method="post" class="dropzone" id="my-dropzonep12"></form>	
                                                <small>Upload p12 file. Which you have created in step 3]</small>
                                            </div>
                                        </div>
                                        
                                        <h6><i class="fa fa-image fa-2x text-info"> Icon Set</h6></i><br/>
                                        <p>1] Create the icon set that is used by the notification UI. You need to add six image in png format with size of 16x16 with image name icon_16x16.png, 32x32 with image name icon_16x16@2x.png, 32x32 with image name icon_32x32.png, 64x64 with image name icon_32x32@2x.png, 128x128 with image name icon_128x128.png, 256x256 with image name icon_128x128@2x.png respectively and place it in Folder <br>2] Zip that Folder and upload it <br>If you dose not upload icon set we use default icon set.</p>
                                        <br>
                                        <div style="background: #fff; height: 200px">
                                            <div class="col-sm-7 col-lg-7 col-md-7" style="width: 77%;margin-left: 16%">
                                                <form action="UploadEmailAttachme" method="post" class="dropzone" id="my-dropzone"></form>	
                                                <small>Upload zip file. Which contains your icon for push notification.</small>
                                            </div>
                                        </div>
                                        
                                        
                                        <br>
                                        <div class="form-group">
                                            <div class="col-sm-8" style="margin-left: 16%">                                                                           
                                                <a class="btn btn-info ladda-button" id="safariUpdate" data-style="zoom-in"  onclick="callValidateSafariForm()">Save</a>
                                            </div>
                                        </div><br/><br/>
                                        <script>
                                            $(document).ready(function () {
                                                Dropzone.autoDiscover = false;
                                                var myDropzone = new Dropzone("#my-dropzone", {
                                                    url: "UploadIconSet",
                                                    uploadMultiple: false,
                                                    maxFilesize: 5,
                                                    maxFiles: 1,
                                                    acceptedFiles: "application/zip",
                                                    dictInvalidFileType: "You can't upload files of this type, only zip file",
                                                    autoProcessQueue: true,
                                                    parallelUploads: 1,
                                                    addRemoveLinks: true,
                                                    dictDefaultMessage: "Drop icon set here to upload",
                                                    removedfile: function(file) {
                                                        removeIconFileFromSession(file.name);    
                                                        var _ref;
                                                        if (file.previewElement) {
                                                            if ((_ref = file.previewElement) != null) {
                                                                _ref.parentNode.removeChild(file.previewElement);
                                                            }
                                                        }
                                                        return this._updateMaxFilesReachedClass();
                                                    }
                                                });
                                            });
                                        </script>
                                        <script>
                                            $(document).ready(function () {
                                                Dropzone.autoDiscover = false;
                                                var myDropzone = new Dropzone("#my-dropzonep12", {
                                                    url: "UploadP12",
                                                    uploadMultiple: false,
                                                    maxFilesize: 5,
                                                    maxFiles: 1,
                                                    acceptedFiles: ".p12",
                                                    dictInvalidFileType: "You can't upload files of this type, only p12 file",
                                                    autoProcessQueue: true,
                                                    parallelUploads: 1,
                                                    addRemoveLinks: true,
                                                    dictDefaultMessage: "Drop p12 here to upload",
                                                    removedfile: function(file) {
                                                        removeIconFileFromSession(file.name);    
                                                        var _ref;
                                                        if (file.previewElement) {
                                                            if ((_ref = file.previewElement) != null) {
                                                                _ref.parentNode.removeChild(file.previewElement);
                                                            }
                                                        }
                                                        return this._updateMaxFilesReachedClass();
                                                    }
                                                });
                                            });
                                        </script>
                                        <li><h5>Step 2] Your domain name without https or http protocol i.e. if your domain is https://www.readyapis.com then your domain name would be www.readyapis.com, that domain name need to assign to a variable host in main.js.</h5>                                                    
                                        </li><br/>
                                        <li><h5>Step 3] You have the Developer Id that you get by calling the API "createDeveloper" of an InApp Push Service, which returns a developer id. That developer id need to assign to a variable "developerId" in main.js.</h5>                                                    
                                        </li><br/>
                                        <li><h5>Step 4] You have the Application Id that you get by calling the API "createApplication" of an InApp Push Service, which returns an application id. That application id need to assign to a variable "appplicationId" in main.js.</h5>                                                    
                                        </li><br/>
                                        <li><h5>Step 5] Assign value to a variable "userId" according to your users.</h5>                                                    
                                        </li><br/>
                                        <li><h5>Step 6] Place your callback URL to a variable "callBackURL" in main.js, your mention call back URL get the information about the user device token, as response with "userId" which is unique identification id for the user that you have place in main.js w.r.t "userId", "developerId" which is your developer id, "applicationId" which is your application id, "DeviceToken" which give the information about the user device token.</h5>                                                    
                                        </li><br/>                                                                                    
                                                <table>
                                                    <tr>
                                                        <td>
                                                    <li><h5>Step 7] Copy and Paste this code before &lt;/head&gt;</code> tag on your website page.</h5></li>
                                                        </td>
                                                        <td>
                                                            <font class="col-lg-2" id="safariScriptCopyDiv"></font>
                                                        </td>
                                                        </tr>
                                                </table>
                                            
                                            <br/>
                                            <textarea class="form-control" name="inAppPushScriptSafari" id="inAppPushScriptSafari" style="min-height: 270px;">
&lt;script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"&gt;&lt;/script&gt;
&lt;script type="text/javascript"&gt;
    window.onload = inject();
    function inject() {(
            function (a, b, c, d) {
                b.type = c;
                b.src = d;
                a.appendChild(b);
            })
    (document.getElementsByTagName('head')[0], document.createElement('script'), 'text/javascript', 'main.js');
    };
&lt;/script&gt;
                                            </textarea>
                                            <br/>
                                            <script>
    var doc = document.getElementById('inAppPushScriptSafari');
    var dataCode = CodeMirror.fromTextArea(doc, {
        lineNumbers: true,
        matchBrackets: true,
        styleActiveLine: true,
        tabMode: "indent",
    });
                                            </script>
                                            <li><h5>Step 8] Firstly you need to create you gateway for safari push by calling the API "createGateWay", In that API fill the parameter for object androidAndWebPushSettings, where certpassword is your p12 password and p12CertFileURL is your p12 file. Send the push notification to your user using "broadcast", "multicast" and "testMessage" APIs. Below is screen of Push Notification. For more information follow the <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>" target="_blank"> API Documentation</a></h5><br/><br/>                                                    
                                                 <span style="margin-left: 10%"><img class="img-responsive img-thumbnail" src="images/safariPush/image.png" alt="Confirm APP Id" width="750" height=""/></span><br/><br/>
                                                 <span style="margin-left: 10%"><img class="img-responsive img-thumbnail" src="images/safariPush/image(1).png" alt="Confirm APP Id" width="750" height=""/></span><br/><br/>
                                            </li><br/>                                             
                                        <li><h5>Browser Supports</h5>
                                            <ul>                                                                                                
                                                <li style="font-weight: 400"><i class="fa fa-safari text-info"></i> Safari  &nbsp;&nbsp;&nbsp;10+    &nbsp;&nbsp;&nbsp;Supported on Mac OS X</li>                                                
                                            </ul>
                                        </li>
                                    </ul>
                                </div>                                
                            </div>                                                                                                           
                        </div>
                                                 
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                 <table>
                                    <tr>
                                        <div class="col-lg-8">
                                        <td>
                                            <h3><strong>Code and Implementation for iOS </strong></h3>
                                        </td>
                                        </div>
                                        <div class="col-lg-2">
                                        <td>
                                            <a class="btn btn-info btn-sm" onclick="downloadiOSAppPush()" role="button" style="margin-left: 260px">Download</a>
                                        </td>
                                        </div>
                                    </tr>
                                </table>                               
                                <br/>
                                <div class="text-muted font-bold m-b-xs">
                                    <div class="text-muted font-bold m-b-xs">
                                            <ul>                                    
                                                <li><h4>Step 1] The Certificate Signing Request</h4>
                                                    <ul>                                                
                                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                            A) Introduction
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        Now that you have created the demo project, put it aside for a few minutes, and get ready for the first step in the whole preparation process. Here, we are going to produce a Certificate Signing Request (CSR) file that we’ll use later on to create a SSL certificate for push notifications. For this step you’ll need the Keychain Access Mac app, so either use the Launchpad or the Spotlight search to find it and open it. If you’re new to Keychain Access, make sure you don’t delete any existing stuff accidentally.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                            B) Request a certificate
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                    <div class="panel-body">
                                                                        Once you get there, open the Keychain Access &gt; Certificate Assistant &gt; Request a Certificate From a Certificate Authority… menu, as shown next:  <br/><br/>                                                                      
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/requestForCert2.png" alt="Request a Certificate" width="550" height=""/></span><br/>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingThree">
                                                                    <h4 class="panel-title">
                                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                            C) Fill Certificate Information
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                    <div class="panel-body">
                                                                        In the window that comes up, you need to mandatorily fill in the User Email Address and Common Name fields. Besides that, click to the Saved to disk option so you are able to save the CSR file to disk, and use it later in the Apple Developer web site.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/fillCertInfo.png" alt="Fill a Certificate information" width="" height=""/></span><br/><br/>
                                                                        Click Continue and optionally choose a folder and a name for the CSR file right before you save it. I have created a folder to keep together all the files we’re about to produce in this tutorial (called PNDemo Files, I encourage you to do the same), and I’ve left the default file name for the CSR file. Once you see a message saying that your certificate request has been created on disk, click Done and you’re… done. The certificate we just requested for and saved to disk will be used to sign other certificates in the Apple Developer website.

                                                                    </div>
                                                                </div>
                                                            </div>          
                                                        </div>
                                                    </ul>
                                                </li>
                                                <li><h4>Step 2] Create an App ID</h4>
                                                    <ul>                                                        
                                                        <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingStep2a">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                                                            A) Introduction
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingStep2a">
                                                                    <div class="panel-body">
                                                                        Our next step is to create a new App ID in the Apple Developer website. This App ID is going to uniquely identify our app amongst others, and it will help APN servers to properly route the notifications. Actually, as you’ll see next, we’ll connect the App ID to a couple of other things: To a new certificate for push notifications, and to a provisioning profile that will let us run the app on a test device.First things first, so begin by going to the <a href="https://developer.apple.com/membercenter/" target="_blank">Apple Developer Member Center</a>. Provide your credentials, and get connected. Then, click to the Certificates, Identifiers & Profiles link, so you’re navigated to the proper page. <br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/welcomeToAppCenter.png" alt="Welcome to Apple Developer Center" width="" height=""/></span><br/><br/>
                                                                    </div>
                                                                </div>
                                                            </div>                                                                                                                
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingStep2b">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapsecreateAppId" aria-expanded="false" aria-controls="collapsecreateAppId">
                                                                            B) Select Option Create App ID
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapsecreateAppId" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingStep2b">
                                                                    <div class="panel-body">
                                                                        By landing to the new webpage, click to the Identifiers link under the iOS Apps section. You’ll see that the App IDs option is pre-selected (in the Identifiers category to the left menu), and a list with all the existing App ID values is shown to the main window area. The new App ID that we’re about to create is going to be added to that list, but first, let’s click to the plus button existing in the top-right side<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/createAppId.png" alt="Create APP Id" width="" height=""/></span><br/><br/>
                                                                        It’s time now to configure a new App ID for our demo application. That explain in next step :
                                                                    </div>
                                                                </div>
                                                            </div>
                                                             <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="crateAPPID2">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion1" href="#crateAPPIDstep2" aria-expanded="false" aria-controls="collapsecreateAppId">
                                                                            C) Create App ID
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="crateAPPIDstep2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="crateAPPID">
                                                                    <div class="panel-body">
                                                                        For starters, we need to fill in two fields.<br/>
                                                                        1. A description about the new App ID. In this sample it’s not really important what you will type here, but you’d better pick a good and precise description in real-world cases.<br/>
                                                                        2. The Bundle ID of the app. Just copy that value from your Xcode project, and paste it here.<br/>
                                                                        As you can see, there’s also one more value between the above two. It’s the App ID Prefix, and usually you don’t really need to change the default value. However, don’t hesitate to do so if it’s necessary to pick a different prefix value. For the purpose of this tutorial, I just leave the default value.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/createAppIdStep2.png" alt="Create APP Id" width="" height=""/></span><br/><br/>
                                                                        It’s time now to configure a new App ID for our demo application. That explain in next step :
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="crateAPPID2">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion1" href="#crateAPPIDstep3" aria-expanded="false" aria-controls="collapsecreateAppId">
                                                                            D) Configure App ID
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="crateAPPIDstep3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="crateAPPID2">
                                                                    <div class="panel-body">
                                                                        For starters, we need to fill in two fields.<br/>
                                                                        1. A description about the new App ID. In this sample it’s not really important what you will type here, but you’d better pick a good and precise description in real-world cases.<br/>
                                                                        2. The Bundle ID of the app. Just copy that value from your Xcode project, and paste it here.<br/>
                                                                        As you can see, there’s also one more value between the above two. It’s the App ID Prefix, and usually you don’t really need to change the default value. However, don’t hesitate to do so if it’s necessary to pick a different prefix value. For the purpose of this tutorial, I just leave the default value.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/createAppIdStep2.png" alt="Create APP Id" width="" height=""/></span><br/><br/>
                                                                        There’s an important detail you should keep in mind: Setting an explicit App ID is necessary when you’re about to enable push notifications, as the App ID must match to specific app Bundle ID. Using a wildcard App ID (an App ID ending with an asterisk *) is something that you’re not allowed to do in this case. Personally, I always find it way better to set an explicit App ID than using the wildcard, no matter what the features of the app are meant to be. Doing that, it’s also easy to distinguish each App ID when you see it listed in the IDs summary page. Now to configure a App service area. That explain in next step :
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="crateAPPID3">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion1" href="#crateAPPIServiceArea" aria-expanded="false" aria-controls="collapsecreateAppId">
                                                                            E) App Services area
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="crateAPPIServiceArea" class="panel-collapse collapse" role="tabpanel" aria-labelledby="crateAPPID">
                                                                    <div class="panel-body">
                                                                        After having set the above values, scroll down until you reach the App Services area. At the bottom of the provided services, you’ll find the Push Notifications option. Click to the checkbox to mark it as selected, and make sure twice that it is checked before you continue.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/configureAppServiceArea.png" alt="Configure APP Id" width="" height=""/></span><br/><br/>
                                                                        Next, click to the Continue button, and wait for a confirmation page to show up. 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="crateAPPID4">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion1" href="#confirmAppId" aria-expanded="false" aria-controls="collapsecreateAppId">
                                                                            F) Confirm App Id
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="confirmAppId" class="panel-collapse collapse" role="tabpanel" aria-labelledby="crateAPPID">
                                                                    <div class="panel-body">
                                                                        Verify that everything is okay, and then click to the Submit button to get finished. If you feel that something is wrong, then you can go back and change any of the previously set values.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/confirmAppId.png" alt="Confirm APP Id" width="" height=""/></span><br/><br/>
                                                                        The last step here is to see the Registration Complete page. Just click to the Donebutton, and you’ll see the new App ID being listed to the App IDs collection.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/appIdlist.png" alt="APP Id" width="" height=""/></span><br/><br/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ul>
                                                </li>
                                                <li><h4>Step 3] Configure the App ID for Push Notifications</h4>
                                                    <ul>                                                        
                                                        <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion3" href="#collapseStep3A" aria-expanded="true" aria-controls="collapse2">
                                                                            a) Introduction
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseStep3A" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        You might have noticed in the previous part that the status for both Development and Distribution mode is not marked as Enabled but as Configurable, even though we checked (enabled) the Push Notifications service during the App ID creation. That means that we need to take some extra actions so we can manage to switch to the proper state.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/configAppPush.png" alt="APP Id" width="" height=""/></span><br/><br/>
                                                                        Note that for the sake of the tutorial we’ll configure the push notifications for the Development mode only. As we are not going to test any push notifications in production stage, we won’t touch the Distribution mode at all. However, whatever you’ll see next applies for that case as well. In a real app you should definitely configure the Distribution mode, otherwise push notifications are not going to work when your app will go live to the App Store.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion3" href="#collapseStep3B" aria-expanded="true" aria-controls="collapse2">
                                                                            b) Configure SSL Certificate
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseStep3B" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        Let’s get going now by clicking to the newly created App ID in the list so it gets expanded. Right after all the displayed services there’s a button titled Edit. Click it to proceed. In the next step scroll down until you see the Push Notifications section. You’ll find two buttons there which you can use to create SSL Certificates for both the Development and Production stage. As we’re interested in the Development mode only, click to the first button as shown below.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/configureSSLCert.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        It’s about time now to make use of the Certificate Signing Request file we created through the Keychain Access app a couple of parts ago. So, let’s keep going with that process, and in the first step of the guide just click to the Continue button. Here you get just some instructions on how to generate the CSR, in case you haven’t done so already.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/configureSSLCert2.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        Next, click to the Choose File… button so you can search for the CSR file that you generated in the first step. If you hadn’t changed the default file name, then you’re looking for a file titled CertificateSigningRequest.certSigningRequest Finally, click to the Generate blue button that you can see in the following screenshot.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/configureSSLCert3.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        Great, you just created a new certificate that will enable you to send push notifications in development (sandbox) mode. Now, you must download it and add it to the keychain (the Keychain Access app on Mac), so proceed by clicking to the Download button.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/configureSSLCert4.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        The file you just downloaded is named aps_development.cer. Spot it in your Downloads folder and double click on it, so it’s added to the collection of certificates in the Keychain Access app. That is explain in next step.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion3" href="#collapseStep3c" aria-expanded="true" aria-controls="collapse2">
                                                                            c) Add certificate in the keychain access app.
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseStep3c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        <span class="text-danger">An important note</span><br/><br/>                                                                        
                                                                        When the Keychain Access app opens by double clicking to the .cer file, make sure that the new certificate is added to the login keychain, and not to the System or any other keychain. If that happens, then just drag and drop the certificate to the login kaychain. That's important for what we'll do right next.  <br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/keychainAccessApp1.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        Once you locate the new certificate in the Keychain, right click on it, and then click to the Export “…” option.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/exportCert.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        Make sure that the .p12 file format is selected before you click to the Save button.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/exportCert1.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        You can skip setting a password simply by clicking to the OK button in the next window. If you set one, make sure to remember it or write it somewhere, because the file becomes unusable if you forget it. We recommend that set a password as it is needed for "createGateway" API of an InApp push service.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/certPassword.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                        Note that in this tutorial we are not going to use the file you just exported. You are going to need it, however, in case you want to test push notifications from a server like Parse, where you have to provide the .p12 file before even sending your first notification. So for now just keep that file along with the rest. At this point it’s important to realise that knowing how to generate a .p12 file for development mode enables you to do the same for the production stage as well.<br/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </ul>
                                                </li>
                                                <li><h4>Step 4] Register Your Device</h4>
                                                    If you have already done so in the past and your device is already listed there, then you can just skip this step.<br/><br/>
                                                    <ul>                                                        
                                                        <div class="panel-group" id="accordion4" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingstep4a">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion4" href="#collapsestep4a" aria-expanded="false" aria-controls="collapsestep4a">
                                                                           a) Introduction
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapsestep4a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep4a">
                                                                    <div class="panel-body">
                                                                        First of all, let me say that this step is useful for testing push notification only (in sandbox mode), and it’s not necessary for the production mode. Here we are going to register the device (or devices) that the app is going to be tested to in the Apple Developer website. If you have already done so in the past and your device is already listed there, then you can just skip this step.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingstep4b">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion4" href="#collapsestep4b" aria-expanded="false" aria-controls="collapsestep4b">
                                                                           b) Know your device Identifier
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapsestep4b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep4b">
                                                                    <div class="panel-body">
                                                                        Supposing that you are going to add your device for first time here, what you have to initially do is to plug it to your Mac and then go to Xcode. Open the Window > Devices menu, so a new window with all devices and simulators to appear. Continue by clicking to your device to the left, so you see its details on the main window area. You’ll notice that there is a field called Identifier, containing a long string with letters and digits as its value. Double click on that value, and then copy it.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/addYourDevice.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingstep4c">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion4" href="#collapsestep4c" aria-expanded="false" aria-controls="collapsestep4c">
                                                                        c) Know your device Identifier
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapsestep4c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep4c">
                                                                <div class="panel-body">
                                                                    Supposing that you are going to add your device for first time here, what you have to initially do is to plug it to your Mac and then go to Xcode. Open the Window > Devices menu, so a new window with all devices and simulators to appear. Continue by clicking to your device to the left, so you see its details on the main window area. You’ll notice that there is a field called Identifier, containing a long string with letters and digits as its value. Double click on that value, and then copy it.<br/><br/>
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/addYourDevice.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingstep4d">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion4" href="#collapsestep4d" aria-expanded="false" aria-controls="collapsestep4d">
                                                                        d) Register device on Apple Developer Website
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapsestep4d" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep4d">
                                                                <div class="panel-body">
                                                                    Now go back to the Apple Developer website, and click to the All option under the Devices category. Any existing registered devices will be listed to the main window area. To add a new device, click to the button with the plus icon (+) to the top-right side. In the new form that appears, first set a name for your device in the Name field (for example, Gabriel’s iPhone 6S, or My lovely iPad). Then, paste the device identifier you copied before in the UDID field, and you’re done.<br/><br/>
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/registerDeviceOnStore.png" alt="Register YOur Device" width="" height=""/></span><br/><br/>
                                                                    Click Continue, and in the next step make sure that the information you just provided is accurate. Once you are ready, click to the Register button to get finished with the guide. You can verify that your device has been added indeed to the list with the registered devices simply by clicking once again to the All option in the Devices category. Go through all entries until you find the one with the name you just entered.

                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </ul>
                                                </li>
                                                <li><h4>Step 5] Create a Provisioning Profile for Development</h4>
                                                    <ul>                                                        
                                                        <div class="panel-group" id="accordion5" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingstep4a">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion5" href="#collapsestep5a" aria-expanded="false" aria-controls="collapsestep5a">
                                                                           a) Introduction
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapsestep5a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep5a">
                                                                    <div class="panel-body">
                                                                        As a last job in the Apple Developer website, let’s create a provisioning profile for Development so we can code sign our app later on. Note that you have to create a Distribution provisioning profile and use it similarly to what you’ll learn here, prior to uploading your app to iTunesConnect either for use on TestFlight or on the App Store.<br/>
                                                                        On the Apple Developer page, click to the Development link in the Provisioning Profiles category. Any existing profiles will be shown to the main window side, and in a few minutes our new one will be added there as well.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingstep4b">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion5" href="#collapsestep5b" aria-expanded="false" aria-controls="collapsestep4b">
                                                                           b) Select type of provisiong profile
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapsestep5b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep5b">
                                                                    <div class="panel-body">
                                                                        To create a new provisioning profile, click to the plus (+) button to the top-right side of the window. In the new form that appears, and for the sake of our example, select the iOS App Development option (the first one) by clicking on it. Note that you have to select an option from the second (lower) section (most probably the App Store) when you’ll be creating a provisioning profile for Distribution. Anyway, once you select the proper option, use the Continue button to move to the next step.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/profileProvisiong1.png" alt="Profile Provisiong" width="" height=""/></span><br/><br/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingstep5c">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion5" href="#collapsestep5c" aria-expanded="false" aria-controls="collapsestep5c">
                                                                        c) Match with your APP ID
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapsestep5c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep5c">
                                                                <div class="panel-body">
                                                                    Now it’s time to connect the profile we’re creating to the App ID matching to our app. Locate and select the proper App ID value in the dropdown control and then move on.<br/><br/>
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/provisioningProfile2.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingstep4d">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion5" href="#collapsestep5d" aria-expanded="false" aria-controls="collapsestep5d">
                                                                        d) Select a certificate
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapsestep5d" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep5d">
                                                                <div class="panel-body">
                                                                    Next, you must include your iOS Development certificate to the provisioning profile (supposing that you have at least one). If there are more than one certificates like it’s shown in the screenshot below and you are unsure which one you should select, then simply check the Select All checkbox and you are covered.<br/><br/>
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/profileProvision3.png" alt="Register YOur Device" width="" height=""/></span><br/><br/>
                                                                    Continue, and get ready to select the device or devices that the app is going to be run to. Make sure that you won’t forget to select any devices which you want to test push notifications to. Once again, click Continue when you’re ready.

                                                                </div>
                                                            </div>
                                                        </div>                                                        
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingstep4d">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion5" href="#collapsestep5e" aria-expanded="false" aria-controls="collapsestep5d">
                                                                        e) Select a device
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapsestep5e" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep5d">
                                                                <div class="panel-body">
                                                                    Select the device you wish to include in this provisiong profile.<br/><br/>
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/provisiongProfile4.png" alt="Register YOur Device" width="" height=""/></span><br/><br/>                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingstep4d">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion5" href="#collapsestep5f" aria-expanded="false" aria-controls="collapsestep5d">
                                                                        f) Name your profile
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapsestep5f" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep5d">
                                                                <div class="panel-body">
                                                                    Lastly, give a name to your provisioning profile so you can distinguish it amongst others. I named it “PNDemo Development Profile“, but feel free to give any other name you desire.<br/><br/>
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/provisingProfile6.png" alt="Register YOur Device" width="" height=""/></span><br/><br/>                                                                    
                                                                    Click Generate and wait for the next screen. When the new profile gets ready, you’ll be able to download it as shown next<br/><br/>
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/provisionProfile7.png" alt="Register YOur Device" width="" height=""/></span><br/><br/>                                                                    
                                                                    All you have to do now is to follow the instructions of the above image, and double click to the file you just downloaded so as to be installed. If you named it just like I did, then your file is called PNDemo_Development_Profile.mobileprovision.
                                                                </div>
                                                            </div>
                                                        </div>       
                                                        </div>
                                                    </ul>
                                                </li>
                                                <li><h4>Step 6] Configure the Project</h4>
                                                    <ul>                                                        
                                                        <div class="panel-group" id="accordion6" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingstep6a">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion6" href="#collapsestep6a" aria-expanded="false" aria-controls="collapsestep6a">
                                                                           a) Introduction
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapsestep6a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep6a">
                                                                    <div class="panel-body">
                                                                        Starting from this part, we’re going to abandon the Apple Developer website, and to focus on our project. We have two goals to achieve here<br/>
                                                                        1. Initially, we must enable the Push Notifications capability in the project, so we can actually get notifications on any device. This step is basic, but trust me, even though it is a simple prerequisite, a lot of developers simply forget to turn a simple switch on and activate that capability.<br/>
                                                                        2. We’ll properly set the values for both code signing and provisioning profiles that must be used by the app. Please note that whatever we’re about to do regards only the Development mode, as we’re not dealing with the Production stage at all. However, everything is quite similar in both cases, so you can easily follow the same steps for the Distribution mode in a real app before it goes live.<br/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingstep4b">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion6" href="#collapsestep6b" aria-expanded="false" aria-controls="collapsestep6b">
                                                                           b) Select team for provisiong profile
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapsestep6b" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep6b">
                                                                    <div class="panel-body">
                                                                        So, bring the app in Xcode in front of you and select the project in the Project Navigator. Make sure that you are in the General tab. What you have to do is to go to the Team dropdown control, and select the proper team that will be used for provisioning.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/configureProject1.png" alt="Register Your Device" width="" height=""/></span><br/><br/>                                                                    
                                                                        If you see no entries in the Team, then you must go to Xcode > Preferences… menu, in the Accounts tab and add a new Apple ID. Provide the Apple ID and password matching to your Developer account, and click to the Add button to get finished.<br/>
                                                                        Going into the details of this process is out of our scope, therefore if you are unsure on what you should do please take a look at <a href="https://developer.apple.com/library/ios/documentation/IDEs/Conceptual/AppStoreDistributionTutorial/AddingYourAccounttoXcode/AddingYourAccounttoXcode.html" target="_blank"> this link </a> for a step by step guide. After you have successfully added your Apple ID, close the Preferences window and return to General tab so you select the proper team value.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingstep6c">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion6" href="#collapsestep6c" aria-expanded="false" aria-controls="collapsestep6c">
                                                                        c) Set the Capabilities
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapsestep6c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep6c">
                                                                <div class="panel-body">
                                                                    Next, click to the Capabilities tab and go to the Push Notifications section. All you have to do here is to just turn the switch on.<br/><br/>
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/selectCapabilities.png" alt="Configure SSL Cert" width="" height=""/></span><br/><br/>
                                                                    As the message in the above screenshot says, by enabling the Push Notifications capability the proper entitlement is automatically added to the Info.plist file.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingstep6d">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion6" href="#collapsestep6d" aria-expanded="false" aria-controls="collapsestep6d">
                                                                        d) Build Setting
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapsestep6d" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep6d">
                                                                <div class="panel-body">
                                                                    Now open the Build Settings tab, and locate the Code Signing section. Expand the Provisioning Profile entry, and in the row titled Debug click to the Automatic value so as to display a list with all the existing provisioning profiles in your Developer account. Select the one with the name matching to the profile you downloaded and installed in the previous step.<br/><br/>
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/buildSetting2.png" alt="Register YOur Device" width="" height=""/></span><br/><br/>
                                                                    There is no need to set a provisioning profile in the Release row right now, as we haven’t created one for Distribution. However, you mandatorily have to do that in the same fashion when you create and download the proper provisioning profile from the Apple Developer website.

                                                                </div>
                                                            </div>
                                                        </div>                                                        
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingstep6d">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion6" href="#collapsestep6e" aria-expanded="false" aria-controls="collapsestep6d">
                                                                        e) Code Signing Identity
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapsestep6e" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingstep6d">
                                                                <div class="panel-body">
                                                                    Right above the Provisioning Profile you will find the Code Signing Identity entry. Click to the arrow in the left side to expand it, if it’s not already expanded. Similarly to what we just did, click to the iOS Developer (or iPhone Developer) value in the Debug row, and select a proper identity as shown next.<br/><br/>
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/codeSiging1.png" alt="Register Your Device" width="" height=""/></span><br/><br/>  
                                                                    
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/codeSigning3.png" alt="Register Your Device" width="" height=""/></span><br/><br/>                                                                    
                                                                    Do not forget that your identity matching to Distribution should be set in the Releaserow in a real app as well.
                                                                    <br/>
                                                                    Now click to the Target to the left side of the General tab, and select the Project.<br/><br/>
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/builtApp4.png" alt="Register Your Device" width="" height=""/></span><br/><br/>                                                                    
                                                                    Go to the Code Signing section, and follow the exact same steps as above. First select the correct provisioning profile for the Debug mode, and then the proper Code Signing Identity.<br/><br/>
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/buildProject8.png" alt="Register Your Device" width="" height=""/></span><br/><br/>                                            
                                                                </div>
                                                            </div>
                                                        </div>                                                        
                                                    </div>
                                                </ul>
                                            </li>
                                            
                                            <li><h4>Step 7] Register for Push Notifications</h4>
                                                    <ul>                                                        
                                                        <div class="panel-group" id="accordion7" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion7" href="#collapseStep7A" aria-expanded="true" aria-controls="collapse2">
                                                                            a) Introduction
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseStep7A" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        All the required configuration to the project has been done, so it’s time to write some lines of code. We are going to begin by making the app register itself to iOS for push notifications and to explicitly specify the notification types we desire (like for example badge, sound and alert message).<br/>
                                                                        You can download sample code file and copy from below, AppDelegate.h and AppDelegate.m
                                                                                                                                                
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion7" href="#collapseStep7B" aria-expanded="true" aria-controls="collapse2">
                                                                            b) Sample Code
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseStep7B" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                            <h5>AppDelegate.h</h5>
                                                                                </td>
                                                                                <td>
                                                                                    <font class="col-lg-2" id="AppDelegateHCopyDiv"></font>
                                                                                </td>
                                                                                </tr>
                                                                        </table>
                                                                        <br/>                                                                      
                                                                        <textarea class="form-control" name="inAppPushiOSHFile" id="inAppPushiOSHFile" style="min-height: 270px;">
//
//  AppDelegate.h
//  Notifications10ObjC
//
//  Created by SYS004 on 9/24/16.
//  Copyright © 2016 Kode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *strDeviceToken;

@end
                                                                        </textarea><br/>  
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                            <h5>AppDelegate.m</h5>
                                                                                </td>
                                                                                <td>
                                                                                    <font class="col-lg-2" id="AppDelegateMCopyDiv"></font>
                                                                                </td>
                                                                                </tr>
                                                                        </table><br/>  
                                                                        <textarea class="form-control" name="inAppPushiOMSHFile" id="inAppPushiOMSHFile" style="min-height: 270px;">
//
//  AppDelegate.m
//  Notifications10ObjC
//
//  Created by SYS004 on 9/24/16.
//  Copyright © 2016 Kode. All rights reserved.
//

#import "AppDelegate.h"
#import "UserDetails.h"

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self registerForRemoteNotification];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Remote Notification Delegate // <= iOS 9.x

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString *strDevicetoken = [[NSString alloc]initWithFormat:@"%@",[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    NSLog(@"Device Token = %@",strDevicetoken);
    // Capture UserId And Assign Value to It.
    
    [self registerForPush :strDevicetoken];
     self.strDeviceToken = strDevicetoken;
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"Push Notification Information : %@",userInfo);
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"%@ = %@", NSStringFromSelector(_cmd), error);
    NSLog(@"Error = %@",error);
}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    NSLog(@"User Info = %@",notification.request.content.userInfo);
    
    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
    NSLog(@"User Info = %@",response.notification.request.content.userInfo);
    completionHandler();
}

#pragma mark - Class Methods

/**
 Notification Registration
 */
- (void)registerForRemoteNotification {
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
            if(!granted){
                [self registerForPush:nil];
            }
        }];
    }
    else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

-(void) registerForPush:(NSString *)strDevicetoken{
    UserDetails* details =[self setDetails];
    NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded"};
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[[NSString stringWithFormat: @"DeviceToken=%@",strDevicetoken] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"&userId=%@",[details userId]]  dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"&developerId=%@",[details developerId]] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"&appplicationId=%@",[details appplicationId]] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"&callBackURL=%@",[details callBackURL]] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&type=iOS" dataUsingEncoding:NSUTF8StringEncoding]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://www.readyapis.com:9443/ReadiAPIWrapper/registerUser"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:100.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSLog(@"dataAsString %@", [NSString stringWithUTF8String:[data bytes]]);
                                                    }
                                                }];
    [dataTask resume];
}

-(UserDetails*) setDetails{
    NSString* userId=@"PlaceYourUserID";//Please set Your UserId Here
    NSString* developerId=@"PlaceYourDeveloperId";//Please set Your UserId Here
    NSString* appplicationId=@"PlaceYOurApplicationId";//Please set Your UserId Here
    NSString* callBackURL=@"PlaceYourCallBackURL";//Please set Your UserId Here
    UserDetails* details=[[UserDetails alloc] init];
    [details assign:userId :developerId  :appplicationId :callBackURL];
    return  details;
}

@end
                                                                        </textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion7" href="#collapseStep7c" aria-expanded="true" aria-controls="collapse2">
                                                                            c) Note
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseStep7c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        <span class="text-danger">Need to place your Developer Id</span><br/><br/>                                                                        
                                                                        You have the Developer Id that you get by calling the API "createDeveloper" of an InApp Push Service which returns a developer id. If you already created the developer id Add that your developer id at AppDelegate.m in setDetails()<br/><br/>
                                                                        <span class="text-danger">Need to place your Application Id</span><br/><br/>                                                                        
                                                                        You have the Application Id that you get by calling the API "createApplication" of an InApp Push Service which returns a application id. If you already created the application Add that your application id at AppDelegate.m in setDetails()<br/><br/>
                                                                        <span class="text-danger">Need to place your Callback URL</span><br/><br/>                                                                        
                                                                        Place your callback URL to a variable "callBackURL" in AppDelegate.m, your mention call back URL get the information about the user device token, as response with "userId" which is unique identification id for the user that you have place in AppDelegate.m w.r.t "userId", "developerId" which is your developer id, "applicationId" which is your application id, "DeviceToken" which give the information about the user device token. If you already created the application using "createApplication" API Add that your application id at AppDelegate.m in setDetails()<br/><br/>
                                                                        <span class="text-danger">Need to place User Id</span><br/><br/>                                                                                                                                                
                                                                        Assign value to a variable "userId" according to your users at AppDelegate.m in setDetails()<br/><br/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion7" href="#collapseStep7d" aria-expanded="true" aria-controls="collapse2">
                                                                            d) Enjoy InApp push 
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseStep7d" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        After successfully done the all steps your App is configured with Push Service. Allow the Push Service like below image so you get the push messages.<br/><br/>
                                                                    <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/allowPushMsg.png" alt="Register YOur Device" width="200" height=""/></span><br/><br/>                                                                    
                                                                        Before send the Push message to your user you need to create your gateway with InApp Push service by calling the API "createGateway" and fill the parameter for iOS.
                                                                        <br/><br/> and send the push notification to your user using "broadcast", "multicast" and "testMessage" APIs. For more information follow the <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>" target="_blank"> API Documentation</a><br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/iosPushMsg/pushDeliveried.PNG" alt="Register YOur Device" width="200" height=""/></span><br/><br/>                                                                    
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </ul>
                                                </li>
                                        </ul>
                                    </div>      
                                </div>
                            </div>
                        </div>
                        
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                 <table>
                                    <tr>
                                        <td>
                                            <h3><strong>Code and Implementation for Android </strong></h3>
                                        </td>
                                        <td>
                                            <a class="btn btn-info btn-sm" onclick="downloadAndroidAppPush()" role="button" style="margin-left: 210px">Download</a>
                                        </td>
                                    </tr>
                                </table>                               
                                <br/>
                                <div class="text-muted font-bold m-b-xs">
                                    <div class="text-muted font-bold m-b-xs">
                                            <ul>                                    
                                                <li><h4>Step 1] Creating an Android Studio Project </h4>
                                                    <ul>                                                
                                                        <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion3" href="#tab3Step1" aria-expanded="true" aria-controls="collapseOne">
                                                                            A) Introduction
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="tab3Step1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        We need to create a new Android Studio Project. So open Android Studio and create a new project. I have created AndroidGCM.<br/>
                                                                        Now once your project is loaded, copy the package name of your project. You can get the package name by going to the manifest file. 
                                                                    </div>
                                                                </div>
                                                            </div>                                                                                                                             
                                                        </div>
                                                    </ul>
                                                </li>
                                                <li><h4>Step 2] Creating an App in Google Developers</h4>
                                                    <ul>                                                        
                                                        <div class="panel-group" id="accordion31" role="tablist" aria-multiselectable="true">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingStep2a">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion31" href="#collapseAndroidStep2a" aria-expanded="true" aria-controls="collapse1">
                                                                            A) Introduction
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseAndroidStep2a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingStep2a">
                                                                    <div class="panel-body">
                                                                        Here is tools from Google for developing great apps. In this step we need to get google-services.json file. This file contains the configuration specific to your application.<br/><br/>                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>                                                                                                                
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingStepAndroidC">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion31" href="#collapsecreateFirebase" aria-expanded="false" aria-controls="collapsecreateAppId">
                                                                            B) Create Project at Google Development
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapsecreateFirebase" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingStepAndroidC">
                                                                    <div class="panel-body">
                                                                        Firstly you need to create a firebase project for that please <a href="https://console.firebase.google.com" target="_blank"> click here</a><br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/androidPush/firebaseCreateProject.png" alt="" width="" height=""/></span><br/><br/>
                                                                        Click on  ADD PROJECT. After clicking on add project give your FCM project name. and click on create project.<br/>
                                                                        After you create project, below window will appear.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/androidPush/selectAndriodPlatform.png" alt="" width="" height=""/></span><br/><br/>
                                                                        Click on Add Firebase to your android app. <br/>
                                                                        Now you will be asked to enter an app name, and your application’s package name. Just put an app name (I have written readyapiPushDemo) . click on Register App.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/androidPush/addFirebase.png" alt="" width="" height=""/></span><br/><br/>
                                                                        Now in the next screen you will see a button to Download google-service.json file, click on that.<br/><br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/androidPush/googleJSONFile.png" alt="" width="" height=""/></span><br/><br/>
                                                                        Now you will get a button to download the configuration file. Click on the button and you will get your goolge-services.json file.<br/><br/>                                                                        
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/androidPush/googleCreateProjectStepThree.png" alt="" width="480" height=""/></span><br/><br/>
                                                                        Now you will need to add a firebase in your android app just mention in above screen.<br/><br/>
                                                                        Your Google App creation part over here.
                                                                    </div>
                                                                </div>
                                                            </div>                                                                                                                        
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="crateAPPID2">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion31" href="#crateAndroidAPPIDstep2" aria-expanded="false" aria-controls="collapsecreateAppId">
                                                                            C) Implementing Android Push Notification 
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="crateAndroidAPPIDstep2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="crateAPPID">
                                                                    <div class="panel-body">
                                                                        Project Configuration.<br/>
                                                                        1. Follow the steps from here very carefully. The first thing we need is to add the google-services.json to our project.<br/>
                                                                        2. Click on the project explorer and select project. </br><br/>
                                                                        <br/>
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/androidPush/createProject.png" alt="Create APP Id" width="" height=""/></span><br/><br/>
                                                                        Right click on the app folder and paste the json file you just downloaded.<br/><br/>                                                                        
                                                                        <span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/androidPush/placeJSONFile.png" alt="Create APP Id" width="270" height=""/></span><br/><br/>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    Come inside your app level build.gradle file and in dependencies add the following lines.
                                                                                </td>
                                                                                <td>
                                                                                    <font class="col-lg-2" id="AppAndroidPluginCopyDiv"></font>
                                                                                </td>
                                                                                </tr>
                                                                        </table><br/>                                                                          
                                                                        <textarea class="form-control" name="inAppPushAndroidPlugin" id="inAppPushAndroidPlugin" style="min-height: 270px;">compile 'com.google.android.gms:play-services:9.0.0'</textarea><br/>
                                                                        <script>
                                                                            var inAppPushAndroidPlugin = document.getElementById('inAppPushAndroidPlugin');   
                                                                            var dataCode = CodeMirror.fromTextArea(inAppPushAndroidPlugin, {
                                                                                lineNumbers: true,
                                                                                matchBrackets: true,
                                                                                styleActiveLine: true,
                                                                                tabMode: "indent",
                                                                            });
                                                                            dataCode.on('change', function ash() {
                                                                                $('#inAppPushAndroidPlugin').val(dataCode.getValue());
                                                                            });
                                                                            $("#inAppPushAndroidPlugin").focus();
                                                                            $("#inAppPushAndroidPlugin").trigger("click");
                                                                        </script> 
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    In AndroidManifest.xml add below permission.
                                                                                </td>
                                                                                <td>
                                                                                    <font class="col-lg-2" id="AndroidDependenciesCopyDiv"></font>
                                                                                </td>
                                                                                </tr>
                                                                        </table><br/>
                                                                        
                                                                        <textarea class="form-control" name="inAppPushAndroidDependencies" id="inAppPushAndroidDependencies" style="min-height: 270px;">
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.WAKE_LOCK" />
<permission
    android:name="net.simplifiedcoding.androidgcm.permission.C2D_MESSAGE"
    android:protectionLevel="signature" />
<uses-permission android:name="net.simplifiedcoding.androidgcm.permission.C2D_MESSAGE" />
                                                                        </textarea>
                                                                         <script>
                                                                            var inAppPushAndroidDependencies = document.getElementById('inAppPushAndroidDependencies');   
                                                                            var dataCode = CodeMirror.fromTextArea(inAppPushAndroidDependencies, {
                                                                                lineNumbers: true,
                                                                                matchBrackets: true,
                                                                                styleActiveLine: true,
                                                                                tabMode: "indent",
                                                                            });
                                                                            dataCode.on('change', function ash() {
                                                                                $('#inAppPushAndroidDependencies').val(dataCode.getValue());
                                                                            });
                                                                            $("#inAppPushAndroidDependencies").focus();
                                                                            $("#inAppPushAndroidDependencies").trigger("click");
                                                                        </script><br/>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    You also need to add below lines inside your AndroidManifest.xml inside application element
                                                                                </td>
                                                                                <td>
                                                                                    <font class="col-lg-2" id="AndroidProjectDependenciesCopyDiv"></font>
                                                                                </td>
                                                                                </tr>
                                                                        </table><br/>
                                                                        <textarea class="form-control" name="inAppPushAndroidProjectDependencies" id="inAppPushAndroidProjectDependencies" style="min-height: 270px;">
<!--
    GCM Receiver
-->
<receiver
    android:name="com.google.android.gms.gcm.GcmReceiver"
    android:exported="true"
    android:permission="com.google.android.c2dm.permission.SEND">
    <intent-filter>
        <action android:name="com.google.android.c2dm.intent.RECEIVE"/>
        <category android:name="com.gnirt69.gcmexample"/>
    </intent-filter>
</receiver>

<!--
    GCM Receiver Service
-->
<service android:name=".GCMPushReceiverService" android:exported="false">
    <intent-filter>
        <action android:name="com.google.android.c2dm.intent.RECEIVE"/>
    </intent-filter>
</service>

<!--
    GCM Registration Intent Service
-->
<service android:name=".GCMRegistrationIntentService" android:exported="false">
    <intent-filter>
        <action android:name="com.google.android.gms.iid.InstanceID"/>
    </intent-filter>
</service>                                                                        </textarea>
                                                                         <script>
                                                                            var inAppPushAndroidProjectDependencies = document.getElementById('inAppPushAndroidProjectDependencies');   
                                                                            var dataCode = CodeMirror.fromTextArea(inAppPushAndroidProjectDependencies, {
                                                                                lineNumbers: true,
                                                                                matchBrackets: true,
                                                                                styleActiveLine: true,
                                                                                tabMode: "indent",
                                                                            });
                                                                            dataCode.on('change', function ash() {
                                                                                $('#inAppPushAndroidProjectDependencies').val(dataCode.getValue());
                                                                            });
                                                                            $("#inAppPushAndroidProjectDependencies").focus();
                                                                            $("#inAppPushAndroidProjectDependencies").trigger("click");
                                                                        </script><br/>
                                                                        Download the files in download contains MainActivity.java, GCMPushReceiverService.java, GCMRegistrationIntentService.java, GCMTokenRefreshListenerService.java that files need to add under src folder with appropriate package.<br/>                                                                                                                                                
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                             <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion31" href="#collapseStepAnd7c" aria-expanded="true" aria-controls="collapse2">
                                                                            D) Note
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseStepAnd7c" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        <span class="text-danger">Need to place your Developer Id</span><br/><br/>                                                                        
                                                                        You have the Developer Id that you get by calling the API "createDeveloper" of an InApp Push Service which returns a developer id. If you already created the developer id Add that your developer id at GCMRegistrationIntentService.java<br/><br/>
                                                                        <span class="text-danger">Need to place your Application Id</span><br/><br/>                                                                        
                                                                        You have the Application Id that you get by calling the API "createApplication" of an InApp Push Service which returns a application id. If you already created the application Add that your application id at GCMRegistrationIntentService.java<br/><br/>
                                                                        <span class="text-danger">Need to place your Callback URL</span><br/><br/>                                                                        
                                                                        Place your callback URL to a variable "callBackURL" in AppDelegate.m, your mention call back URL get the information about the user device token, as response with "userId" which is unique identification id for the user that you have place in AppDelegate.m w.r.t "userId", "developerId" which is your developer id, "applicationId" which is your application id, "DeviceToken" which give the information about the user device token. If you already created the application using "createApplication" API Add that your application id at GCMRegistrationIntentService.java<br/><br/>
                                                                        <span class="text-danger">Need to place User Id</span><br/><br/>                                                                                                                                                
                                                                        Assign value to a variable "userId" according to your users at GCMRegistrationIntentService.java<br/><br/>
<!--                                                                        <span class="text-danger">Version Support</span><br/><br/>                                                                                                                                                
                                                                        above 23<br/><br/>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="crateAPPID2">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion31" href="#crateAndroidGatewaystep2" aria-expanded="false" aria-controls="collapsecreateAppId">
                                                                            E) Create Gateway
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="crateAndroidGatewaystep2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="crateAPPID">
                                                                    <div class="panel-body">
                                                                        After creating project at Google Development Center you get Server Key And Sender ID from Firbase, Settings >> Cloud Messaging. As shown in below image<br/>                                                                        
                                                                        <br/>
                                                                        <span style="margin-left: 10%"><img class="img-responsive img-thumbnail" src="images/androidPush/APIAndSenderKey.png" alt="APIAndSenderKey" width="600" height="500"/></span><br/><br/>
                                                                       <br/><br/>                                                                        
                                                                        Now you need to create a gateway for android application using "createGateway" API.                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" role="tab" id="headingOne">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion31" href="#collapseAndroidStep3A" aria-expanded="true" aria-controls="collapse2">
                                                                            F) Enjoy the InApp Push
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseAndroidStep3A" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="panel-body">
                                                                        You have done the all the steps that needed for the InApp Push service for Android platform. <br/>
                                                                        Before send the Push message to your user you need to create your gateway with InApp Push service by calling the API "createGateway" and fill the parameter for android.<br/>
                                                                        and send the push notification to your user using "broadcast", "multicast" and "testMessage" APIs. Below is the screen for push notification. For more information follow the <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>" target="_blank"> API Documentation</a>
                                                                        <br/><br/><span style="margin-left: 20%"><img class="img-responsive img-thumbnail" src="images/androidPush/pushDelivered.jpg" alt="Push notification" width="270" height=""/></span><br/><br/>
                                                                    </div>
                                                                </div>
                                                            </div>   
                                                        </div>
                                                    </ul>
                                                </li>
<!--                                                <li><h4>Step 3] Enjoy the InApp Push</h4>
                                                    <ul>                                                        
                                                        <div class="panel-group" id="accordion31" role="tablist" aria-multiselectable="true">
                                                                                                                                                                                                                                                  
                                                        </div>
                                                    </ul>
                                                </li>-->
                                        </ul>
                                    </div>      
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                            
    </div>
    <textarea id="myPushScript" style="display: none"></textarea>
</div>
<script>
    
    var inAppPushiOSHFile = document.getElementById('inAppPushiOSHFile');
    var inAppPushiOMSHFile = document.getElementById('inAppPushiOMSHFile');
   
    var dataCode = CodeMirror.fromTextArea(inAppPushiOSHFile, {
        lineNumbers: true,
        matchBrackets: true,
        styleActiveLine: true,
        tabMode: "indent",
    });
    $("#inAppPushiOSHFile").focus();
    $("#inAppPushiOSHFile").trigger("click");
    var dataCode = CodeMirror.fromTextArea(inAppPushiOMSHFile, {
        lineNumbers: true,
        matchBrackets: true,
        styleActiveLine: true,
        tabMode: "indent",
    });
    $("#inAppPushiOMSHFile").focus();
    $("#inAppPushiOMSHFile").trigger("click");
    window.setTimeout(function(){
       var copyScriptBody = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copy()><b>Copy</b></a>";
       console.log("copyScriptBody >>> " + copyScriptBody);
       document.getElementById('scriptCopyDiv').innerHTML = copyScriptBody; 
       
       var copySafariScriptBody = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copySafari()><b>Copy</b></a>";
       console.log("copyScriptBody >>> " + copySafariScriptBody);
       document.getElementById('safariScriptCopyDiv').innerHTML = copySafariScriptBody; 
       
       var AppDelegateHCopyDiv = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copyAppDelegateH()><b>Copy</b></a>";
       console.log("AppDelegateHCopyDiv >>> " + AppDelegateHCopyDiv);
       document.getElementById('AppDelegateHCopyDiv').innerHTML = AppDelegateHCopyDiv;
       
       var AppDelegateMCopyDiv = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copyAppDelegateM()><b>Copy</b></a>";
       console.log("AppDelegateHCopyDiv >>> " + AppDelegateMCopyDiv);
       document.getElementById('AppDelegateMCopyDiv').innerHTML = AppDelegateMCopyDiv;
       
       var AppAndroidPluginCopyDiv = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copyAndroidPlugin()><b>Copy</b></a>";
       console.log("AppAndroidPluginCopyDiv >>> " + AppAndroidPluginCopyDiv);
       document.getElementById('AppAndroidPluginCopyDiv').innerHTML = AppAndroidPluginCopyDiv;
       
       var AndroidDependenciesCopyDiv = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copyAndroidDependencies()><b>Copy</b></a>";
       console.log("AndroidDependenciesCopyDiv >>> " + AndroidDependenciesCopyDiv);
       document.getElementById('AndroidDependenciesCopyDiv').innerHTML = AndroidDependenciesCopyDiv;
       
       var AndroidProjectDependenciesCopyDiv = "<a href='#/' style='font-size:10px; text-decoration:none' class='btn btn-info btn-xs' onclick=copyAndroidProjectDependencies()><b>Copy</b></a>";
       console.log("AndroidProjectDependenciesCopyDiv >>> " + AndroidProjectDependenciesCopyDiv);
       document.getElementById('AndroidProjectDependenciesCopyDiv').innerHTML = AndroidProjectDependenciesCopyDiv;
    },1000);
    function copy() {                
        var t;
        t = document.getElementById("scriptCopyDiv");
        t.classList.add('copied');
        setTimeout(function () {
            t.classList.remove('copied');
        }, 1500);
        var scriptData = document.getElementById('inAppPushScript').value;
        console.log("scriptData >> "+scriptData);
        $("#myPushScript").val("");
        document.getElementById("myPushScript").style.display = "block";
        $("#myPushScript").val(scriptData);
        $("#myPushScript").select();
        document.execCommand('copy');
        document.getElementById("myPushScript").style.display = "none";
    }
    
    function copySafari() {                
        var t;
        t = document.getElementById("safariScriptCopyDiv");
        t.classList.add('copied');
        setTimeout(function () {
            t.classList.remove('copied');
        }, 1500);
        var scriptData = document.getElementById('inAppPushScriptSafari').value;
        console.log("scriptData >> "+scriptData);
        $("#myPushScript").val("");
        document.getElementById("myPushScript").style.display = "block";
        $("#myPushScript").val(scriptData);
        $("#myPushScript").select();
        document.execCommand('copy');
        document.getElementById("myPushScript").style.display = "none";
    }
    
    function copyAppDelegateH() {                
        var t;
        t = document.getElementById("AppDelegateHCopyDiv");
        t.classList.add('copied');
        setTimeout(function () {
            t.classList.remove('copied');
        }, 1500);
        var scriptData = document.getElementById('inAppPushiOSHFile').value;
        console.log("inAppPushiOSHFile >> "+scriptData);
        $("#myPushScript").val("");
        document.getElementById("myPushScript").style.display = "block";
        $("#myPushScript").val(scriptData);
        $("#myPushScript").select();
        document.execCommand('copy');
        document.getElementById("myPushScript").style.display = "none";
    }
    
    function copyAppDelegateM() {                
        var t;
        t = document.getElementById("AppDelegateMCopyDiv");
        t.classList.add('copied');
        setTimeout(function () {
            t.classList.remove('copied');
        }, 1500);
        var scriptData = document.getElementById('inAppPushiOMSHFile').value;
        console.log("inAppPushiOMSHFile >> "+scriptData);
        $("#myPushScript").val("");
        document.getElementById("myPushScript").style.display = "block";
        $("#myPushScript").val(scriptData);
        $("#myPushScript").select();
        document.execCommand('copy');
        document.getElementById("myPushScript").style.display = "none";
    }
    
    function copyAndroidPlugin(){
        var t;
        t = document.getElementById("AppAndroidPluginCopyDiv");
        t.classList.add('copied');
        setTimeout(function () {
            t.classList.remove('copied');
        }, 1500);
        var inAppPushAndroidPlugin = document.getElementById('inAppPushAndroidPlugin').value;
        console.log("inAppPushAndroidPlugin >> "+inAppPushAndroidPlugin);
        $("#myPushScript").val("");
        document.getElementById("myPushScript").style.display = "block";
        $("#myPushScript").val(inAppPushAndroidPlugin);
        $("#myPushScript").select();
        document.execCommand('copy');
        document.getElementById("myPushScript").style.display = "none";
    }
    function copyAndroidDependencies(){
        var t;
        t = document.getElementById("AndroidDependenciesCopyDiv");
        t.classList.add('copied');
        setTimeout(function () {
            t.classList.remove('copied');
        }, 1500);
        var inAppPushAndroidDependencies = document.getElementById('inAppPushAndroidDependencies').value;
        console.log("inAppPushAndroidDependencies >> "+inAppPushAndroidDependencies);
        $("#myPushScript").val("");
        document.getElementById("myPushScript").style.display = "block";
        $("#myPushScript").val(inAppPushAndroidDependencies);
        $("#myPushScript").select();
        document.execCommand('copy');
        document.getElementById("myPushScript").style.display = "none";
    }
    
    function copyAndroidProjectDependencies(){
        var t;
        t = document.getElementById("AndroidProjectDependenciesCopyDiv");
        t.classList.add('copied');
        setTimeout(function () {
            t.classList.remove('copied');
        }, 1500);
        var inAppPushAndroidDependencies = document.getElementById('inAppPushAndroidProjectDependencies').value;
        console.log("inAppPushAndroidDependencies >> "+inAppPushAndroidDependencies);
        $("#myPushScript").val("");
        document.getElementById("myPushScript").style.display = "block";
        $("#myPushScript").val(inAppPushAndroidDependencies);
        $("#myPushScript").select();
        document.execCommand('copy');
        document.getElementById("myPushScript").style.display = "none";
    }
</script>
