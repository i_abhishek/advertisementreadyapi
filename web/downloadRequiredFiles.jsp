<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<script src="scripts/downloadFiles.js" type="text/javascript"></script>
<%@page import="java.net.URL"%>
<%
    String appurl = (request.getRequestURL().toString());
    PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        URL myAppUrl = new URL(appurl);
        int port = myAppUrl.getPort();
        if (myAppUrl.getProtocol().equals("https") && port == -1) {
            port = 443;
        } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
            port = 80;
        }
        String apiDocsURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + "/APIDoc" + "/";
%>
<div class="small-header transition animated fadeIn" id="invoiceHeader">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                Downloads
            </h2>
            <small>Get supportive file for the services</small>
        </div>
    </div>
</div>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="hpanel">               
                <div class="panel-body">
                    <h3><i class="pe-7s-network text-info fa-1x"></i>&nbsp;&nbsp; Application Server Monitoring <a class="btn btn-info btn-sm" onclick="downloadMobileMonitoring()" role="button" style="margin-left: 55%">Download</a></h3>
                    <hr>
                   
                    <ul>                                    
                        <li><h5> Step 1] Download the file and unzip it.</h5></li>
                        <li><h5> Step 2] After unzip you get file need to add it in your application server.</h5></li>
                        <li><h5> Step 3] After addition of a file in your application you need to configure it at <b>"serverurl"</b> while calling the <b>"addApp API"</b>.</h5></li>
                        <li><h5> Note : For more information follow the <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>" target="_blank"> API Documentation</a></li>
                    </ul>
                </div>
            </div>
        </div>
                    <div class="col-lg-12" style="margin-bottom: 40%"></div>            
    </div>
</div>
