<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
    

    <footer class="footer">
        <span class="pull-right">
            Ready APIs, Platform Owned by BlueBricks Technologies
        </span>
    </footer>
<script>
    setInterval(function () {
        checkValidSession();
    }, 180000);        
</script>
<%if(parObj.getAnalyticScript() == null){%>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107416064-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-107416064-2');
</script>
<%}else{%>
    <%=parObj.getAnalyticScript()%>
<%}%>
</body>
</html>
