<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%> 
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel" style="height: 50%">

                <div class="panel-body" style="text-align: center;">
                    <form id="api_usageV2" class="form-horizontal">
                        <div class="col-md-12">
                            <div style="display: none">
                                <select class="form-control m-b" name="apiUsageEnvironment" id="apiUsageEnvironment">
                                    <!--<option value="0">All</option>-->
                                    <option value="2">Production</option>
                                </select>
                            </div>						
                            <div class="col-sm-3">
                                <select class="js-source-states-2" id="apiUsageAccesspoint" name="apiUsageAccesspoint" multiple="multiple" style="width: 100%">                                                        
                                    <%
                                        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                                        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
                                        PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
                                        Accesspoint[] accesspoints1 = null;
                                        Warfiles warfiles = null;
                                        accesspoints1 = new AccessPointManagement().getAllAccessPoint(SessionId, ChannelId);
                                        if (accesspoints1 != null) {
                                            for (int i = 0; i < accesspoints1.length; i++) {
                                                if (accesspoints1[i].getStatus() == GlobalStatus.ACTIVE && accesspoints1[i].getGroupid() == Integer.parseInt(parObj.getPartnerGroupId())) {
                                                    Accesspoint apdetails = accesspoints1[i];
                                                    TransformDetails transformDetails = new TransformManagement().getTransformDetails(SessionId, ChannelId, apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                                                    if (transformDetails != null) {
                                                        warfiles = new WarFileManagement().getWarFile(SessionId, ChannelId, apdetails.getApId());
                                                        if (warfiles != null) {
                                                            if (i == 0) {
                                    %>
                                    <option value="<%=apdetails.getName()%>" selected><%=apdetails.getName()%></option>
                                    <%} else {%>
                                    <option value="<%=apdetails.getName()%>"><%=apdetails.getName()%></option>
                                    <%}
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    %>
                                </select>
                            </div>
                            <div class="col-sm-3"><input id="_startdate" name="_startdate" type="text" class="form-control" placeholder="from"></div>
                            <div class="col-sm-3"><input id="_enddate" name="_enddate" type="text" class="form-control" placeholder="to"></div>
                            <div class="col-sm-3">
                                <button class="btn btn-success ladda-button btn-sm btn-block" data-style="zoom-in" id="generateButton" onclick="generatereAPIreport()" type="submit"><i class="fa fa-bar-chart"></i> Generate</button> 
                            </div>
                        </div>
                        <div class="col-lg-12" id="fillBlankSpace" style="margin-bottom: 33%">
                        </div>
                    </form>
                    <hr class="m-b-xl"/>
                    </br>
                    <div id="report_data"></div>
                </div>
            </div>
        </div>
       <div class="col-md-12" style="margin-bottom: 22%"></div>
    </div>
</div>
<script>
    $(function () {
        $('#_startdate').datepicker();
        $('#_enddate').datepicker();
        $(".js-source-states-2").select2();
        $("#_enddate").datepicker('setDate', new Date());
        $("#_startdate").datepicker('setDate', new Date().getDay() - 15);

    });
    generatereAPIreport();
</script>
