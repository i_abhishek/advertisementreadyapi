<script src="scripts/getReportsDiv.js" type="text/javascript"></script>
<!--<div class="wrapper">-->
<div class="content animate-panel">        
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">

                <div class="panel-body" style="text-align: center;">     
                    <div class="text-left">
                        <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Last 7 days expenditure</span>
                    </div>
                    <div id="report_data_button">
                        <div class="btn-group" style="float: right;" >
                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="dailyTxReport()"> Daily</button>
                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="lastSevenDayEx()"> Last 7 days</button>
                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="monthlyTxReportByButton()"> Last 30 days</button>
                        </div>
                    </div>
                    <br>
                    <hr class="m-b-xl"/>                         
                    <div id="expenditureReport" style="width:300; height:370">
                    </div>
                    <div id="noRecordFoundData">
                        <img src="images/no_record_found.png" alt="No record found" width="300" height="250" />
                        <br>      
                    </div>                                  
                </div>
            </div>
            <div class="col-md-12" style="margin-bottom: 12%"></div>
        </div>
    </div>       
</div>

<!--</div>-->
