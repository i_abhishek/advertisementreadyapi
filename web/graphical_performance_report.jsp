<%@include file="header.jsp"%> 

<!-- Main Wrapper -->
<div id="wrapper">

	<div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Performance Report</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Performance Report
                </h2>
                <small>Analyse your API performance (milisecond) by time period</small>
            </div>
        </div>
    </div>


<div class="content animate-panel">

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    Choose API and timeframe to generate report
                </div>
                <div class="panel-body">
				<div class="col-md-12">
					<div class="col-sm-3">
						<select class="form-control m-b" name="api">
							<option> - </option>
							<option>SMS</option>
							<option>Map</option>
							<option>MFA</option>
							<option>Digital Signing</option>
						</select>
					</div>
					<div class="col-sm-3"><input id="datapicker1" type="text" class="form-control" placeholder="from"></div>
					<div class="col-sm-3"><input id="datapicker2" type="text" class="form-control" placeholder="to"></div>
					<div class="col-sm-3"><button class="btn btn-success btn-sm" type="submit"><i class="fa fa-bar-chart"></i> Generate</button></div>
				</div>
				
				<hr class="m-b-xl"/>
				
                <div>
                    <canvas id="lineOptions_status" height="140"></canvas>
                </div>
				
				<hr class="m-b-xl"/>
				
				<div class="col-md-12">
					<div class="col-sm-3">
						<select class="form-control m-b" name="api">
							<option> - </option>
							<option>PDF</option>
							<option>CSV</option>
							<option>TXT</option>
						</select>
					</div>
					<div class="col-sm-3"><button class="btn btn-sm btn-primary2 " type="button"><i class="fa fa-cloud-download"></i> <span class="bold">Download Report</span></button></div>
				</div>

                </div>
            </div>
        </div>
		
    </div>

    <%@include file="footer.jsp"%> 


<script>

    $(function () {

        // Initialize Example 2
        $('#api').dataTable();

    });
	
	$(function () {

        // Initialize Example 2
        $('#data_type').dataTable();

    });

</script>
 <script>

	$(function(){
		
		$('#datapicker1').datepicker();
		$('#datapicker2').datepicker();
  
	});

</script>
<script>

    $(function () {


        /**
         * Options for Line chart
         */
        var lineData = {
            labels: ["01/03/2017", "02/03/2017", "03/03/2017", "04/03/2017", "05/03/2017"],
            datasets: [
                {
                    label: "Example dataset",
                    fillColor: "rgba(98,203,49,0.5)",
                    strokeColor: "rgba(98,203,49,0.7)",
                    pointColor: "rgba(98,203,49,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(26,179,148,1)",
                    data: [33, 48, 40, 19, 54]
                }
            ]
        };

        var lineOptions_status = {
            scaleShowGridLines : true,
            scaleGridLineColor : "rgba(0,0,0,.05)",
            scaleGridLineWidth : 1,
            bezierCurve : true,
            bezierCurveTension : 0.4,
            pointDot : true,
            pointDotRadius : 4,
            pointDotStrokeWidth : 1,
            pointHitDetectionRadius : 20,
            datasetStroke : true,
            datasetStrokeWidth : 1,
            datasetFill : true,
            responsive: true
        };


        var ctx = document.getElementById("lineOptions_status").getContext("2d");
        var myNewChart = new Chart(ctx).Line(lineData, lineOptions_status);

        var sharplineOptions_status = {
            scaleShowGridLines : true,
            scaleGridLineColor : "rgba(0,0,0,.05)",
            scaleGridLineWidth : 1,
            bezierCurve : false,
            pointDot : true,
            pointDotRadius : 4,
            pointDotStrokeWidth : 1,
            pointHitDetectionRadius : 20,
            datasetStroke : true,
            datasetStrokeWidth : 1,
            datasetFill : true,
            responsive: true
        };

        var ctx = document.getElementById("sharplineOptions_status").getContext("2d");
        var myNewChart = new Chart(ctx).Line(sharpLineData, sharplineOptions_status);



    });

</script>