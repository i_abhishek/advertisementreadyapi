<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="java.net.URL"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt"%>
<%@page import="java.math.BigDecimal"%>
<!DOCTYPE html>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgProductionAccess"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.EmailManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgEmailticket"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPaymentdetails"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.LoanManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgLoanDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCreditInfo"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgUsers"%>
<%@page import="java.util.List"%>
<html>
    <%response.addHeader("X-FRAME-OPTIONS", "DENY");%>    
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>MP Services</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="vendor/bootstrap-star-rating/css/star-rating.css" />
        <link rel="stylesheet" href="vendor/datatables.net-bs/css/dataTables.bootstrap.min.css" />
        <link rel="stylesheet" href="vendor/select2-3.5.2/select2.css" />
        <link rel="stylesheet" href="vendor/select2-bootstrap/select2-bootstrap.css" />
        <link rel="stylesheet" href="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" />
        <link rel="stylesheet" href="vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
        <link rel="stylesheet" href="vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
        <link rel="stylesheet" href="vendor/clockpicker/dist/bootstrap-clockpicker.min.css" />
        <link rel="stylesheet" href="vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
        <link rel="stylesheet" href="vendor/summernote/dist/summernote.css" />
        <link rel="stylesheet" href="vendor/summernote/dist/summernote-bs3.css" />
        <link rel="stylesheet" href="vendor/jquery-ui/themes/base/all.css" />
        <link rel="stylesheet" href="vendor/dropzone/dropzone.css">
        <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
        <link rel="stylesheet" href="vendor/ladda/dist/ladda-themeless.min.css" />
        <link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
        <link rel="stylesheet" href="vendor/codemirror/style/codemirror.css" />
        <link rel="stylesheet" href="vendor/fooTable/css/footable.core.min.css" />
<!--         <link rel="stylesheet" href="vendor/c3/c3.min.css" />-->
        <!-- App styles -->
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="styles/static_custom.css">
        <link rel="stylesheet" href="styles/style.css">
        <!-- Need call first -->
        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/summernote/dist/summernote.min.js"></script>
        <script src="vendor/codemirror/script/codemirror.js"></script>
        <script src="vendor/codemirror/javascript.js"></script> 
        <link rel="stylesheet" href="vendor/c3/c3.min.css" />
        <script src="vendor/d3/d3.min.js"></script>
        <script src="vendor/c3/c3.min.js"></script>
        <link rel="stylesheet" href="vendor/chartist/custom/chartist.css" />
        <script src="vendor/c3/c3.min.js"></script>
        <script src="vendor/chartist/dist/chartist.min.js"></script>
    </head>
    <%
        final int ADMIN = 0;
        final int OPERATOR = 1;
        final int REPORTER = 2;
        SgUsers rUser = null;
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String profileImageUrl = (String) request.getSession().getAttribute("profileImageUrl");
        String socialNamed = (String) request.getSession().getAttribute("socialNamed");
        if (SessionId == null) {
            response.sendRedirect("logout.jsp");
            return;
        }
        if(socialNamed == null){
           socialNamed =  parObj.getPartnerName();
        }
        String PartnerType = "ADMIN";
        rUser = (SgUsers) request.getSession().getAttribute("_SgUsers");
        if (rUser.getType() == OPERATOR) {
            PartnerType = "OPERATOR";
        } else if (rUser.getType() == REPORTER) {
            PartnerType = "REPORTER";
        }
        if (rUser.getType() == ADMIN) {
            PartnerType = "ADMIN";
        }
        //String mailBoxFunctionality = LoadSettings.g_sSettings.getProperty("functionalityOf.ticket.email.booleanValue");

        SgDeveloperProductionEnvt productObj = new ProductionAccessEnvtManagement().getProudDetailsByPartnerId(parObj.getPartnerId());
        int notificationCount = 0;
        String appurl = (request.getRequestURL().toString());
        URL myAppUrl = new URL(appurl);
        int port = myAppUrl.getPort();
        if (myAppUrl.getProtocol().equals("https") && port == -1) {
            port = 443;
        } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
            port = 80;
        }
        String apiDocsURL = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + "/APIDoc" + "/";

        // find the Service Count
        int serviceCount = 0;
        Accesspoint Res1[] = null;
        Res1 = new AccessPointManagement().getAllAccessPoint(SessionId, ChannelId);
        if (Res1 != null) {
            for (int i = 0; i < Res1.length; i++) {
                if (Res1[i].getStatus() == GlobalStatus.ACTIVE) {
                    Accesspoint apdetails = Res1[i];
                    if (apdetails != null) {
                        int resID = -1;
                        String resourceList = apdetails.getResources();
                        String[] resids = resourceList.split(",");
                        for (int j = 0; j < resids.length; j++) {
                            if (!resids[j].isEmpty() && resids[j] != null) {
                                resID = Integer.parseInt(resids[j]);
                                ResourceDetails rsName = new ResourceManagement().getResourceById(SessionId, ChannelId, resID);
                                TransformDetails tmdetail = new TransformManagement().getTransformDetails(SessionId, ChannelId, apdetails.getApId(), resID);
                                if (rsName != null && tmdetail != null) {
                                    int g = Integer.parseInt(parObj.getPartnerGroupId());
                                    if (g == apdetails.getGroupid()) {
                                        serviceCount++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    %>

    <body class="fixed-navbar fixed-sidebar">
        <style>
            span.capitalize {
                text-transform: capitalize;
            }
        </style>
        <script language="JavaScript" type="text/javascript">
            function changeclass(id) {
                var NAME = document.getElementById(id);
                NAME.className = "active";
            }
        </script>
        <!-- Simple splash screen-->
        <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Header -->
        <div id="header">
            <div class="color-line">
            </div>
            <div id="logo" class="light-version">
                <span>
                    Ready API
                </span>
            </div>
            <nav role="navigation">
                <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
                <div class="small-logo">
                    <span class="text-primary">Ready API</span>
                </div>
                <div class="mobile-menu">
                    <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
                        <i class="fa fa-chevron-down"></i>
                    </button>
                    <div class="collapse mobile-navbar" id="mobile-collapse">
                        <ul class="nav navbar-nav">
                            <li>
                                <a class="" href="login.jsp">Login</a>
                            </li>
                            <li>
                                <a class="" href="login.jsp">Logout</a>
                            </li>
                            <li>
                                <a class="" href="profile.jsp">Profile</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="navbar-right">
                    <ul class="nav navbar-nav no-borders">
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                <i class="pe-7s-keypad" title="My Menu"></i>
                            </a>

                            <div class="dropdown-menu hdropdown bigmenu animated flipInX">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a href="change_password.jsp">
                                                    <i class="pe pe-7s-key text-info"></i>
                                                    <h5>Change Password</h5>
                                                </a>
                                            </td>
<!--                                            <td>
                                                <a href="my_profile.jsp">
                                                    <i class="pe pe-7s-users text-success"></i>
                                                    <h5>My Profile</h5>
                                                </a>
                                            </td>-->
                                             <td>
                                                <a href="<%=apiDocsURL%>apidocs.jsp?g=<%=parObj.getPartnerGroupId()%>" target="_blank">
                                                    <i class="pe pe-7s-help2 text-info"></i>
                                                    <h5>API User Guide</h5>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="compose_email_details.jsp">
                                                    <i class="pe pe-7s-mail text-warning"></i>
                                                    <h5>Raise Ticket</h5>
                                                </a>
                                            </td>                         
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="my_subscription_package_detail.jsp">
                                                    <i class="pe pe-7s-shopbag text-danger"></i>
                                                    <h5>Subscription</h5>
                                                </a>
                                            </td> 
                                            <td>
                                                <a href="my_invoice.jsp">
                                                    <i class="pe pe-7s-cash text-warning"></i>
                                                    <h5>Invoice</h5>
                                                </a>
                                            </td> 
                                           
                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                <!-- Jack Remark: Temporary hide it as no need from now -->
                                                <a href="access_point_policy.jsp">
                                                    <i class="pe pe-7s-speaker text-warning"></i>
                                                    <h5>Access Point Policy</h5>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle label-menu-corner" href="#" data-toggle="dropdown">
                                <i class="pe-7s-mail"></i><div style="font-size:9px"></div>
<!--                                <span class="label label-success" id="notificationSpan"><%=notificationCount%></span>-->
                            </a>
                            <ul class="dropdown-menu hdropdown animated flipInX">
                                <!--                        <div class="title">
                                                            You have new messages
                                                        </div>-->
                                <%
                                    SgPaymentdetails pDetailsObj = null;
                                    double mainCredit = 0;
                                    SgCreditInfo creditObj1 = null;

                                    SgSubscriptionDetails subscriObject1 = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(parObj.getPartnerId());
                                    if (subscriObject1 != null) {
                                        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
                                        pDetailsObj = new PaymentManagement().getPaymentDetailsbyPartnerAndSubscriptionID(subscriObject1.getBucketId(), partnerObj.getPartnerId());
                                        creditObj1 = new CreditManagement().getDetails(parObj.getPartnerId());
                                        if (subscriObject1 != null && subscriObject1.getPaymentMode().equalsIgnoreCase("prepaid")) {
                                            String lowBalance1 = LoadSettings.g_sSettings.getProperty("prepaid.low.balance");
                                            float lBalance1 = 0.0f;
                                            int subscripaidId = subscriObject1.getBucketId();
                                            if (lowBalance1 != null) {
                                                lBalance1 = Float.parseFloat(lowBalance1);
                                            }
                                            if (creditObj1 != null) {
                                                if (creditObj1.getMainCredit() < lBalance1 && creditObj1.getStatus() != GlobalStatus.UPDATED) {
                                                    String hefeature = subscriObject1.getFeatureList();
                                                    JSONObject hefeatJSONObj = null;
                                                    String heloanLimit = "0";
                                                    String heinterestRate = "0";
                                                    if (hefeature != null) {
                                                        JSONArray alertJson = new JSONArray(hefeature);
                                                        for (int i = 0; i < alertJson.length(); i++) {
                                                            JSONObject jsonexists1 = alertJson.getJSONObject(i);
                                                            if (jsonexists1.has(subscriObject1.getBucketName())) {
                                                                hefeatJSONObj = jsonexists1.getJSONObject(subscriObject1.getBucketName());
                                                                if (hefeatJSONObj != null) {
                                                                    heloanLimit = hefeatJSONObj.getString("loanLimit");
                                                                    heinterestRate = hefeatJSONObj.getString("interestRate");
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (Float.parseFloat(heloanLimit) > 0 && Float.parseFloat(heinterestRate) > 0) {
                                                        SgLoanDetails loandetailObj1 = new LoanManagement().getLoanDetails(SessionId, ChannelId, parObj.getPartnerId(), subscriObject1.getBucketId());
                                                        if ((loandetailObj1 != null && loandetailObj1.getStatus() == GlobalStatus.PAID) || loandetailObj1 == null) {
                                %> 

                                <li>
                                    <a href="getLoan.jsp">
                                        Your balance <%=creditObj1.getMainCredit()%>, is very low. Get Loan
                                    </a>
                                </li>
                                <%
                                            }
                                        }
                                    }
                                    if (subscriObject1.getStatus() == GlobalStatus.PAID) {%>
                                <li>
                                    <a href="my_invoice.jsp">
                                        Get Invoice
                                    </a>
                                </li>
                                <%  }
                                    }
                                } else if (subscriObject1 != null && subscriObject1.getPaymentMode().equalsIgnoreCase("postpaid")) {
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                                    String todayDate = dateFormat.format(new Date());
                                    String expiryDate = dateFormat.format(subscriObject1.getExpiryDateNTime());
                                    Date curDate = dateFormat.parse(todayDate);
                                    Date exDate = dateFormat.parse(expiryDate);
                                    if (curDate.after(exDate) && pDetailsObj == null && subscriObject1.getStatus() == GlobalStatus.UNPAID) {
                                        notificationCount++;
                                %>         
                                <li>
                                    <a>
                                        Your package is expired
                                    </a>
                                </li>
                                <script>
                                    document.getElementById("notificationSpan").innerHTML = '<%=notificationCount%>';
                                </script>
                                <%
                                            }
                                        }
                                    }
                                    int productionAccessRejectioncount = 0;
                                    SgProductionAccess[] productionAccessdetails = new ProductionAccessManagement().getAllRejectedDetailsByPartnerId(SessionId, ChannelId, parObj.getPartnerId(), GlobalStatus.REJECTED);
                                    if (productionAccessdetails != null) {
                                        productionAccessRejectioncount = productionAccessdetails.length;
                                    }
                                    if (productionAccessRejectioncount >= 1) {
                                %>    
                                <li>
                                    <a href="productionAccessRejected.jsp">
                                        Production Access
                                    </a>
                                </li>
                                <%
                                    }
                                %>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="logout.jsp">
                                <i class="pe-7s-upload pe-rotate-90" title="Logout"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <!-- Navigation -->
        <aside id="menu">
            <div id="navigation">
                <div class="profile-picture">
                    <a href="homePage.jsp">
                        <%if(profileImageUrl != null){%>
                        <img src="<%=profileImageUrl%>" class="img-circle m-b" alt="profile">
                        <%}else{%>                        
                        <img src="images/images.png" class="img-circle m-b" alt="profile" width="90"/>                        
                        <%}%>
                    </a>
                    <div class="stats-label text-color">
                        <span class="font-extra-bold font-uppercase"><%=socialNamed%></span>
                    </div>
                    <div id="sparkline1" class="small-chart m-t-sm"></div>
                    <hr/>
                    <div>
                        <h4 class="font-extra-bold m-b-xs">
                            <%
                                String expiry = "NA";
                                if (creditObj1 != null) {
                                    mainCredit = creditObj1.getMainCredit();
                                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
                                    expiry = dateFormatter.format(subscriObject1.getExpiryDateNTime());
                                }
                            %>
                            <%=new BigDecimal(mainCredit).doubleValue()%>       
                        </h4>
                        <small class="text-muted">Remaining Credit</small></br>
                        <small class="text-muted">Expires On <%=expiry%></small>
                    </div>
                </div>

                <ul class="nav" id="side-menu">
<!--                    <li class="active">
                        <a href="home.jsp"> <span class="capitalize">Dashboard</span> </a>
                    </li>                    -->
                    <li>
                        <a href="apiConsole.jsp"><span class="capitalize">Services <span class="label label-success pull-right"><%=serviceCount%></span> </a>
                    </li>
                    <li>
                        <a href="reports.jsp"><span class="capitalize">Reports </a>
                    </li>
                    
<!--                    <li>
                        <a href="#"><span class="capitalize">Reports</span><span class="fa arrow"></span> </a>
                        <ul class="nav nav-second-level">
                            <li><a href="api_usage.jsp"> <span class="nav-label">API Usage</span></a></li>
                            <li><a href="apiCallReport.jsp">API Call</span></a></li>
                            <li><a href="apiTranscationForMonth.jsp">API Transaction</span></a></li>
                            <li><a href="graphical_transaction_report.jsp?_type=1&_opType=0">Transaction Report</a></li>
                            <li><a href="graphical_transaction_report.jsp?_type=2&_opType=0">Performance Report</a></li>
                           <li><a href="performanceReport.jsp">Performance Report</a></li>
                        </ul>
                    </li>-->
                    <li id="myInvoice">
                        <a href="my_invoice.jsp" onclick="changeclass('myInvoice')"> <span class="capitalize">My Invoice</span></a>
                    </li>
                    <!--            <li>
                                    <a href="my_service.jsp"> <span class="nav-label">My Service</span></a>
                                </li>-->
                    <li>
                        <a href="ala-carte-package.jsp"><span class="capitalize">Subscribe Package</span></a>
                    </li>

                    <li>
                        <a href="compose_email_details.jsp"> <span class="capitalize">Helpdesk</span></a>
                    </li>
                </ul>
            </div>
        </aside>
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="mollatechhelp.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
/*]]>*/</script>
<script>
    $(document).ready(function () {
        var creditUsedData = fifteenDaysCreditExpenditure();
        
        $("#sparkline1").sparkline([5, 6, 7, 2, 0, 4, 2, 4, 5, 7, 2, 4, 12, 11, 4], {
            type: 'bar',
            barWidth: 7,
            height: '30px',
            barColor: '#62cb31',
            negBarColor: '#53ac2a'
        });
        
    });
    
    function fifteenDaysCreditExpenditure(){        
//        var s = './MonthlyApiTxChart';
//        var jsonData = $.ajax({
//            url: s,
//            dataType: "json",
//            async: false
//        }).responseText;
//        if (jsonData.length === 0) {
//            jsonData = [{"label": "NA", "value": 0, "value5": 0}];
//            return jsonData;
//        } else {
//            var myJsonObj = JSON.parse(jsonData);
//            return myJsonObj;
//        }
    }
</script>

<script>
    $(document).ready(function () {
        var creditUsedData = fifteenDaysCreditExpenditure();
        var obj = [];
        for (var key in creditUsedData) {
        var value = creditUsedData[key];
        obj.push(value.txAmount);
//        alert(JSON.stringify(key)+" :: "+JSON.stringify(value.apiName));
    }
        $("#sparkline1").sparkline(obj, {
            type: 'bar',
            barWidth: 7,
            height: '30px',
            barColor: '#62cb31',
            negBarColor: '#53ac2a'
        });
        
    });
//    
    function fifteenDaysCreditExpenditure(){        
        var s = './DailyTransactionAmount';
        var jsonData = $.ajax({
            url: s,
            dataType: "json",
            async: false
        }).responseText;
        if (jsonData.length === 0) {
            jsonData = [{"label": "NA", "value": 0, "value5": 0}];
            return jsonData;
        } else {
            var myJsonObj = JSON.parse(jsonData);
            return myJsonObj;
        }
    }
</script>