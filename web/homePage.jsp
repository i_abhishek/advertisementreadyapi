<script src="scripts/homepage.js" type="text/javascript"></script>
<%@include file="header.jsp"%>
<!-- Main Wrapper -->
<body>
<div id="wrapper">
        <div class="content animate-panel">
            <div class="row">
            <div class="col-lg-3">
                <div class="hpanel stats">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Today's total credit used
                    </div>
                    <div class="panel-body h-200">
                        <div class="stats-title pull-left">
                            <h3 style="font-weight: 500" class="m-b-xs text-success" id="todayUcredit"></h3>
                            <span class="font-bold no-margins" id="remainingCreditProgressBar">
                                Remaining Credit
                            </span>
                            <br>
                        </div>
                        <div class="stats-icon pull-right">
                            <i class="pe-7s-share fa-4x"></i>
                        </div>
                        <div class="m-t-xl">
                        <br><br>
                        <div class="progress m-t-xs full progress-small">
                            <div id="percentage" style="" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class=" progress-bar progress-bar-success">
                                <span class="sr-only">35% Complete (success)</span>
                            </div>
                        </div> 
                        <div class="row" style="height: 65">
                            <br>
                                <div class="col-xs-6">
                                    <small class="stats-label">Credit used</small>
                                    <h4 id='creditUsed'>7.80</h4>
                                </div>

                                <div class="col-xs-6">
                                    <small class="stats-label">% Credit used</small>
                                    <h4 id='perCreditUsed'>76.43%</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Last week total credit used
                    </div>
                    <div class="panel-body text-center h-200">
                        <i class="pe-7s-graph1 fa-4x"></i>

                        <h1 class="m-b-xs text-success" id="lastWeekAmount"></h1>
                        <small id="weekMsg"></small>
                        <h3 class="font-bold no-margins lead text-success row">
                            Weekly usage
                        </h3> <!--<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit</small>-->
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Last month total credit used
                    </div>
                    <div class="panel-body text-center h-200">
                        <i class="pe-7s-global fa-4x"></i>
                        <h1 class="m-b-xs text-success" id="monthCredit"></h1>
                        
                        <small id="monthMsg"></small>
                        <h3 class="font-bold no-margins lead text-success row">
                            Monthly usage
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3" id="leastUsedService">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        <span id="leastusedTitle"></span>
                        
                    </div>
                    <div class="panel-body">
                        <div class="flot-chart text-center" style="width: 200;height:160">
                            <div class="flot-chart-content"  id="flot-line-chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="row">
            <div class="col-lg-9" data-child="hpanel" data-effect="fadeInLeft">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Service Statistics
                    </div>
                    <div class="panel-body" style="height: 300px">
                        <div class="text-left">
                            <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Last 7 day expenditure</span>
                        </div>
                        <div style="padding-left: 40%" class="btn-group">
                                <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="generateTopServiceV2()">Top five service</button>
                                <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="creditperSer()">Today total expenditure</button>
                                <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" onclick="toptrndingServices()">Todays trending services</button>
                            </div>
<!--                            <div class="text-center" id="homereportImage" style="display: none">
                            <img src="images/oopsss.jpeg" alt=""  width="200" height="150"/>
                            </div>-->
                            <div  id="topFiveSer"></div>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-3" id="topmostServices" data-child="hpanel" data-effect="fadeInLeft">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        <span id="mostlyFive"></span>
                    </div>
                    <div class="panel-body" style="height: 300px">
                        <div class="flot-chart text-center" style="width: 200;height:190">
                            <div class="flot-chart-content"  id="flot-pie-chart"></div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-lg-3" id = "mostlyservices" data-child="hpanel" data-effect="fadeInLeft">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Your mostly used services
                    </div>
                    <div class="panel-body text-center" style="height: 300px">
                        <img src="images/no_record_found.jpg" alt="No record found" width="200" height="150"/><br/>
                        <small style="margin-top: 10%">Sorry you didn't used any service from last month.</small>
                    </div>
                </div>
            </div>
        </div>
<%@include file="footer.jsp"%>
        </div>
    </div>
<script type="text/javascript">
   document.onreadystatechange = function(){
     if(document.readyState === 'complete'){
         $('#flot-line-chart').ready(function() {
        leastUsedService();
        });
     
     $('#todayUcredit').ready(function() {
        todayusedCredit("today");
        }); 
        
       $('#topFiveSer').ready(function() {
        generateTopService();
        });
        $('#lastWeekAmount').ready(function() {
        weeklyusedCredit("lastweek");
        });
        $('#monthCredit').ready(function() {
        monthlyCredit("month");
        });
         $('#flot-pie-chart').ready(function() {
        yourMostlyService();
        });
        }; };
//var chart2 = c3.generate({
//        bindto: '#chart2',
//    data: {
//        // iris data from R
//        columns: [
//            ['Office1', 240],
//            ['Work1', 120],
//            ['School1', 180],
//            ['Water1', 220]
//        ],
//        type : 'pie'
//    },
//    legend: {
//         show: false
//    },
//    size: {
//        width: 200,
//        height: 150
//    },
//    label: {
//       format: function (value, ratio, id) {
//          return d3.format('$')(value);
//        }
//  }
//});

//var chart1 = c3.generate({
//    bindto: '#chart1',
//    data: {
//        // iris data from R
//        columns: [
//            ['Office', 240],
//            ['Work', 120],
//            ['School', 180],
//            ['Water', 220]
//        ],
//        type : 'pie'
//    },
//    legend: {
//         show: false
//    },
//    size: {
//        width: 200,
//        height: 150
//    },
//    label: {
//       format: function (value, ratio, id) {
//          return d3.format('$')(value);
//        }
//  }
//});
//api();
//function api(){
//var apiname = [];
//    var s = './DeveloperLeastService';
//    var jsonData = $.ajax({
//        url: s,
//        dataType: "json",
//        async: false
//    }).responseText;
//    console.log(jsonData);
//    var jsonobj = JSON.parse(jsonData);
//    var i = 0,temp = 5;
//    var obj = {};
//    for (var key in jsonobj) {
//        var value = jsonobj[key];
//        apiname.push(value.apiName);
//        obj[i] = value.apiCount;
//        if(value.apiCount === 0){
//            temp--;
//        }
//        i++;
//    }
//    if(temp === 0){
//        document.getElementById("leastUsedService").style.display='none';
//        return;
//    }
//    document.getElementById("leastusedTitle").innerHTML="Your Least used "+temp+" services";
//    var data1 = apiname[0];
//    var value11 = parseInt(obj[0]);
//    if(obj[0] !== 0){
//     data1 = apiname[0];
//     value11 = parseInt(obj[0]);
//    }
//    if(obj[1] !== 0){
//    var data2 = apiname[1];
//    var value22 = parseInt(obj[1]);
//    }
//    if(obj[2] !== 0){
//    var data3 = apiname[2];
//    var value33 = parseInt(obj[2]);
//    }
//    if(obj[3] !== 0){
//    var data4 = apiname[3];
//    var value44 = parseInt(obj[3]);
//    }
//    if(obj[4] !== 0){
//    var data5 = apiname[4];
//    var value55 = parseInt(obj[4]);
//    }
//    alert(data1+" :: "+value11);
//    var chart1 = c3.generate({
//        bindto: '#chart1',
//    data: {
//        // iris data from R
//        columns: [
//            [data1, value11],
//            [data2, value22],
//            [data3, value33],
//            [data4, value44],
//            [data5, value55]
//        ],
//        type : 'pie'
//    },
//    legend: {
//         show: false
//    },
//    size: {
//        width: 200,
//        height: 150
//    },
//    label: {
//       format: function (value, ratio, id) {
//          return d3.format('$')(value);
//        }
//  }
//});

//}


</script>
</body>