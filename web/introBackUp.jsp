
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>Ready APIs</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />

        <!-- App styles -->
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="styles/style.css">
        <link rel="stylesheet" href="vendor/ladda/dist/ladda-themeless.min.css" />
        <link href="api/flag-icon.css" rel="stylesheet" type="text/css"/>
        <link href="api/flags.css" rel="stylesheet" type="text/css"/>
        <link href="api/flags.min.css" rel="stylesheet" type="text/css"/>

        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <!--<link href="styles/toggleButton/bootstrap-switch.css" rel="stylesheet" type="text/css"/>-->
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <link href="styles/switchUserDefined.css" rel="stylesheet" type="text/css"/>
        <script src="scripts/switchJquery.js" type="text/javascript"></script>
    </head>

    <body class="landing-page">

        <!-- Simple splash screen-->
        <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Ready APIs</h1><p>Loading ... </p><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <h2 class="navbar-brand">Ready APIs</h2>
                    <!--                    <div class="brand-desc" style="margin-left: 60%">
                                            Tag Line 
                                        </div>-->
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a class="page-scroll" href="#page-top">Home</a></li>
                        <li><a class="page-scroll" page-scroll href="#features2">Services</a></li>
                        <li><a class="page-scroll" page-scroll href="#components">Features</a></li>                
                        <li><a class="page-scroll" page-scroll href="#pricing">Pricing </a></li>
                        <!--                        <li><a class="page-scroll" page-scroll href="#billingFAQ">Pricing FAQ</a></li>-->
                        <li><a class="page-scroll" page-scroll href="#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <header id="page-top">
            <div class="container">
                <div class="heading">
                    <div class="row">
                        <div class="col-lg-10">
                            <h1>
                                Welcome to Ready APIs
                            </h1>
                        </div>
                        <div class="col-lg-2 text-center" style="margin-top: 1%;">
                            <a href="./login.jsp" class="btn btn-info btn-sm">Login</a>
                        </div>
                    </div>
                    <span>APIs are bridge between companies, systems and ideas.</span>

                    <p class="small">
                        We invite you to innovate through your ideas and Ready APIs. We are proud to bring complex services as simple APIs to build your products faster, reliable and scalable.
                    </p>
                    <a href="#pricing" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="right" title="There are two step process to sign up, Firstly do free sign up and Secondly subscribe a package.">Sign Up</a>
                </div>
                <div class="heading-image animate-panel" data-child="img-animate" data-effect="fadeInRight">

                    <img class="img-animate" src="images/report2.png" width="155px" height="155px"/>
                    <img class="img-animate" src="images/mostUsedService.png" width="155px" height="155px"/>
                    <img class="img-animate" src="images/developerSigned.png" width="155px" height="155px"/>
                    <img class="img-animate" src="images/todayCredit.png" width="155px" height="155px"/>
                    <br/>                       

                    <img class="img-animate" src="images/totalPercentage.png" width="155px" height="155px"/>            
                    <img class="img-animate" src="images/apiPerformanceBlue.png" width="155px" height="155px"/>
                    <img class="img-animate" src="images/todayTrending.png" width="155px" height="155px"/>
                    <img class="img-animate" src="images/report3.png" width="155px" height="155px">
                </div>
            </div>
        </header>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Our Expertise</h4>
                        <p>We are group of 15+ engineers working hard to solve complex problems into simple APIs. We have experts in cyber security, messaging, big data, artifical intelligance, image processing etc. We have build Ready APIs for you to enjoy with simple APIs exposed.</p>
                        <!--            <p><a class="navy-link btn btn-sm" href="#" role="button">Learn more</a></p>-->
                    </div>
                    <div class="col-md-4">
                        <h4>Inviting Innovators</h4>
                        <p>Companies, colleges, startups, growth hackers and many more are invited to join our platform to bring faster development of new innovative ideas into life. Especially companies that are building software for others companies can enjoy all these APIs to bring great value to their customers and workflows.</p>
                        <!--            <p><a class="navy-link btn btn-sm" href="#" role="button">Learn more</a></p>-->
                    </div>
                    <div class="col-md-4">
                        <h4>Saving Time & Effort</h4>
                        <p>With Ready APIs you can code much faster for all complex items like Security, Authentication, Messaging, Data Manipulation, Automation, Image Processing, Document Processing, WebChat, Tokenization etc. It will save you about 60% time and 40% effort with Ready APIs. In addition you do not need to worry about licensing and hardware server cost.</p>
                        <!--            <p><a class="navy-link btn btn-sm" href="#" role="button">Learn more</a></p>-->
                    </div>
                </div>
            </div>
        </section>

        <section id="features2" class="bg-light">
            <div class="container">
                <div class="row text-center">
                    <div class="col-lg-12">
                        <h2><span class="text-info">Ready to use services</span></h2>
                        <!--                        <p>Lorem Ipsum available, but the majority have suffered alteration euismod. </p>-->
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="pe-7s-server text-info icon-big"></i></h4>
                        <strong>DB As Service</strong>
                        <p>You can expose our database like Oracle, Mysql and Cassandra. You can fire various queries on database as per the requirement and easily access the database.</p>
                    </div>
                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="pe-7s-copy-file text-info icon-big"></i></h4>
                        <strong>Document Utility</strong>
                        <p>You can do wonder operations with PDF documents. Operations cover Sign, Convert to image and back, Watermark, Password Protect, Compress, Merge, Rotate on your documents. Our intention is to give more value to your documents and workflow.</p>
                    </div>
                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="fa fa-google text-info icon-big"></i></h4>
                        <strong>Google 2FA Token</strong>
                        <p>Every app needs security and 2FA (Second Factor Authentication) is getting more popular. You can use this service for manage your users, their OTP tokens and usage. Enjoy free security with Ready APIs and Google.</p>
                    </div>
                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="fa fa-photo text-info icon-big"></i></h4>
                        <strong>Image Utility</strong>
                        <p>You can manipulate images in wonderful way. The APIs can do - Change formats, Hide/Unhide Messages inside Image, Watermark, Fun Stay Calm Creation, Extract meta data from image, Compress Image and Compress Website Images too.</p>
                    </div>


                </div>
                <div class="row text-center">
                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="fa fa-try text-info icon-big"></i></h4>
                        <strong>Tokenization</strong>
                        <p>The token is a reference (i.e. identifier) that maps back to the sensitive data through a tokenisation system. With tokens you do not need to share confidential data between various system. You can enjoy complete end to end token lifecycle through tokenization.</p>
                    </div>
                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="pe-7s-science text-info icon-big"></i></h4>
                        <strong>Value Added Service</strong>
                        <p>We have built a lot of Value Added Services that help in quick rapid development of your workflows. Few of them to mention are EmailId Validation, QR Code And Secure QR Code Generation, Shorten URL with Hit count, ICS Calendar Generation, JSON/CSV/XML Format validator, Credit Card Number Validator, Phone Number Validator and Bank Finder.</p>
                    </div>


                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="pe-7s-map-marker text-info icon-big"></i></h4>
                        <strong>IP2GEO</strong>
                        <p>This service has a goal of providing geolocation service to all users . It provide all details of server location like country, state, latitude, longitude for specific ip address we have pass as input. Every month we update our DB with latets ip2geo data.</p>
                    </div>                    
                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="fa fa-users text-info icon-big"></i></h4>
                        <strong>Instant WebChat</strong>
                        <p>We have build for you to a true value add service for Instant Web Chat (with no client software installation) into your workflow. You can generate conference video call and it works on 97% browsers like chrome, opera, Firefox as well android and iOS browsers. Also you can password protect session for the users joining in.</p>
                    </div>
                </div>
                <div class="row text-center">                    
                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="pe-7s-loop text-info icon-big"></i></h4>
                        <strong>Reverse Proxy</strong>
                        <p>One of trouble point we have tried to address with this service is, ability for your web application to be behind a proper SSL enabled server. Many websites and mobile app need to have secure connection and putting your own SSL certificate is costly and hardwork. You can use this service to put non SSL server behind reverse proxy provides an additional level of abstraction and control. </p>
                    </div>
                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="pe-7s-comment text-info icon-big"></i></h4>
                        <strong>In App Push</strong>
                        <p>Push notification has changed the way we give information out to our customers. You can enjoy end to end device and push notification for all media - Android, iOS and Web. Your mobile native app in android and iOS along with your web app can get messages through Ready APIs. Chrome, Firefox, Opera and Safari are popular browsers that are supported. </p>
                    </div>
                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="pe-7s-network text-info icon-big"></i></h4>
                        <strong>Application Server Monitoring</strong>
                        <p>Monitoring your app servers give in-depth visibility into key performance indicators of application performance. From CPU, Memory, Processes etc get checked on periodic basis at application level, you will get alert if consumption is too high. Currently Java JSP is only supported. PHP and .Net ASPx will be supported soon.</p>
                    </div>
                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="fa fa-file-code-o text-info icon-big"></i></h4>
                        <strong>Digital Certificate Issuer</strong>
                        <p>We are promoted of security for all and this service is true reflection of our commitment to security. You can use these service APIs to issue certificates to your user or your system (application).  Using user inputs or directly CSR the service can generate the certificate for you. Revoke option is also available to remove if needed.</p>
                    </div>                                        
                </div>
                <div class="row text-center">
                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="fa  fa-gears text-info icon-big"></i></h4>
                        <strong>SSL Cert Discovery</strong>
                        <p>Scan multiple networks and their ports to Scan Vulnerabilities (Beast, Weak Cipher, SSLv2 Enabled, SSLv3 Enabled, Heartbleed, Freak Attack, Weak Keys, SHA1 violation, Name Mismatch, Weak Hash Algorithm etc). The system gives your reports that allow you to view the health of your entire network, or drill down into specific trouble areas on bases of certificate information.</p>
                    </div>                    
                    <div class="col-md-3">
                        <h4 class="m-t-lg"><i class="fa fa-file-picture-o text-info icon-big"></i></h4>
                        <strong>Dynamic Image Authentication</strong>
                        <p>Once again we wish to bring security with comfort to you. Dynamic image authentication is two way two factor authentication where user desired and selected TEXT + COLOR based image is created along with user known sweet spot on dynamic image. This approach is very effective to give needed mutual authentication with web and mobile apps.</p>
                    </div>                    
                </div>                
            </div>
        </section>

        <section id="components" >
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h3><span class="text-info"><b>Many features </span>to discover with Ready APIs</b></h3>
                        <p>We have worked hard to build a simple yet powerful set of features for you to enojy. </p>
                    </div>
                </div>
                <div class="row m-t-md">
                    <div class="col-md-8">
                        <h3>Special designed API Console</h3>
                        <p style="font-size: 20px;text-align: justify">The API console is envisioned with features keeping developers at its heart. For new as well as seasoned developers can enjoy all in one dashboard that gives everything that you need from API endpoints, security credentials, reports, analytics and billing along with sandbox testing model. You do not need anything else with this console working for you.</p>
                    </div>
                    <div class="col-md-4">              
                        <!--                <img src="images/SS-Technology.png" class="img-responsive" width="70%" height="70%"/>-->
                        <img src="images/crossplatform.svg" class="img-responsive" width="170px" height="170px" style="margin-left: 60px"></a>
                    </div>
                </div>
                <div class="row m-t-xl">
                    <div class="col-md-3">
                        <!--                <img src="images/seamless.png" class="img-responsive" width="90%" height="70%"/>-->
                        <img src="images/launchpad.svg" class="img-responsive" width="200px" height="200px"></a>
                    </div>
                    <div class="col-md-8">
                        <h3>Integration and Compatibility </h3>
                        <p style="font-size: 20px;text-align: justify">We have language bindings in 10 languages from Ruby, PHP, Java, JS, Obj C, Python and many more! You can view code sample by our experts to suit your business and industry specific needs. Very detailed API documentation gives complete comprehensive working on each API inside this set of services. We have tested all these APIs thoroughly with all the 10 popular languages and all sample code is available for you to use and speed up your development and testing cycle.</p>
                    </div>
                </div>
                <div class="row  m-t-xl">
                    <div class="col-md-8">
                        <h3>Sandbox with SOAP and REST Implementation</h3>
                        <p style="font-size: 20px;text-align: justify">From older generation applications that can consume SOAP protocol to the new & lighter mobile apps that can consume RESTFUL APIs, both are avialable for you to enjoy. You get WSDL and WADL endpoint references for all available Services and their APIs in terms of how they work Ready API endpoints. You can also use the sandbox testing for real time trying and playing with the APIs.</p>
                    </div>
                    <div class="col-md-4">
                        <!--                <img src="images/Protect Website Usability.png" class="img-responsive" width="70%" heigh="70%"/>-->
                        <img src="images/market.svg" class="img-responsive" width="170px" heigh="170px" style="margin-left: 60px"/>
                    </div>
                </div>        
                <div class="row m-t-xl">
                    <div class="col-md-3">                
                        <!--                <img src="images/Anti Bot Service to control cost.png" class="img-responsive" width="70%" heigh="70%" />-->
                        <img src="images/nofees.svg" class="img-responsive" width="170px" heigh="170px"/>
                    </div>
                    <div class="col-md-8">
                        <h3>Pay per Use Monthly Billing</h3>
                        <p style="font-size: 20px;text-align: justify"> The beauty of Ready APIs lies in its costing too. Even though you subscribe to monthly package but there is no lost of credits. You get all the serivce APIS with credit asscoaited and only usage of API leads to credit deduction. Otherwise the remaining credits are rolled to next billing cycle. All the credits are available in the detailed reports.</p>
                    </div>
                </div>
                <div class="row m-t-xl">            
                    <div class="col-md-8">
                        <h3>Analytical Reports</h3>
                        <p style="font-size: 20px;text-align: justify">You can enjoy detailed analytics reports for usage as well as credits that are charged. Our Dashboard with out-of-the-box reports provide visibility into APIs and services from different perspectives. You can see the histroical reports that for the performance, billing as well as API usage. The reports also show you the response time of the various APIs for your understanding and troubleshooting. You can understand API traffic dynamics and take informed decisions accordingly.</p>
                    </div>
                    <div class="col-md-4">
                        <!--                <img src="images/H.png" class="img-responsive" width="70%" heigh="70%"/>-->
                        <img src="images/decentralization.svg" class="img-responsive" width="160px" heigh="160px" style="margin-left: 60px"></a>
                    </div>
                </div>
                <div class="row m-t-xl">

                    <div class="col-md-3">                
                        <!--                <img src="images/Anti Bot Service to control cost.png" class="img-responsive" width="70%" heigh="70%" />-->
                        <img src="images/nocensorship.svg" class="img-responsive" width="170px" heigh="170px"/>
                    </div>
                    <div class="col-md-8">
                        <h3>API Level Security</h3>
                        <p style="font-size: 20px;text-align: justify">We take security very seriously and it clearly reflects in the security that is offered by Ready APIs. We have three levels of security credentials above and beyond HTTP SSL to ensure best grade security between your application and Ready APIs. Credential Keys are three levels - Developer, Service And API level. Each level adds to the confidential and complexity to ensure you are protected.</p>
                    </div>
                </div>
            </div>
        </section>
        <section id="pricing" class="bg-light">
            <div class="container">
                <div class="row text-center">
                    <div class="col-lg-12">
                        <h2><span class="text-info">Best pricing </span>for your work</h2>
                        <!--                        <p>Lorem Ipsum available, but the majority have suffered alteration euismod. </p>-->
                    </div>
                </div>
                <table> 
                    <tr>
                        <td style="padding: 10px!important; "><div id="monthlyLable"><h4>Monthly</h4></div> </td>
                        <td><input class="checkbox" name="checkboxName" type="checkbox"></td>
                        <td style="padding: 10px!important;"> <div id="yearlyLable" style="display: none"><h4>Yearly</h4></div></td>
                    </tr>                  
                </table>
                <div class="row m-t-lg">                            
                    <div id="monthly">
                        <div class="col-sm-3">
                            <div class="hpanel plan-box hyellow active">
                                <div class="panel-heading hbuilt text-center">
                                    <h4 class="font-bold">Basic</h4>
                                    <!--                                <h4 class="font-bold">$5/Month</h4>
                                                                    <h4 class="font-bold">$60/Year&nbsp;</h4>-->
                                </div>
                                <div class="panel-body">
                                    <p class="text-muted">
                                        For first time sign up you get 4x credit.
                                    </p>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>
                                                    Features
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Response Time (5 days)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Resolution Time (14 days)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Support Available (Email)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Product Documentation
                                                </td>
                                            </tr>                                        
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Reports
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   All In One Solution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-square-o"></i>   One on One API Discussion
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Uptime 95%
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   API Calls per Second (3)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits (1000 Credits)
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>                                                          
                                    <h3 class="font-bold">
                                        A$ 5/Month
                                    </h3>
                                    <a href="./registration.jsp?pa=baMo" class="btn btn-warning btn-sm m-t-xs btn-outline">Sign Up</a>        

                                    <!--                                <a href="./registration.jsp?pa=baMo" style="float: left" class="btn btn-warning btn-sm m-t-xs">Monthly</a>
                                                                    <a href="./registration.jsp?pa=baYr" style="float: right"  class="btn btn-warning btn-sm m-t-xs">Yearly</a>                                                                                   -->
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="hpanel plan-box hblue active">
                                <div class="panel-heading hbuilt text-center">
                                    <h4 class="font-bold">Student</h4>
                                    <!--                                <h4 class="font-bold">$15/Month</h4>
                                                                    <h4 class="font-bold">$180/Year&nbsp;</h4>-->
                                </div>
                                <div class="panel-body">
                                    <p class="text-muted">
                                        For first time sign up you get 2x credit.
                                    </p>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>
                                                    Features
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Response Time (3 days)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Resolution Time (3 days)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Support Available (Email)
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Product Documentation
                                                </td>
                                            </tr>                                        
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Reports
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   All In One Solution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-square-o"></i>   One on One API Discussion
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Uptime 98%
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   API Calls per Second (5)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits  (1500)
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <h3 class="font-bold">
                                        A$ 15/Month
                                    </h3>
                                    <a href="./registration.jsp?pa=stMo" class="btn btn-info btn-sm m-t-xs btn-outline disabled">Sign Up</a>                                                               

                                    <!--                                <a href="./registration.jsp?pa=stMo" style="float: left"  class="btn btn-info btn-sm m-t-xs">Monthly</a>
                                                                    <a href="./registration.jsp?pa=stYr" style="float: right"  class="btn btn-info btn-sm m-t-xs">Yearly</a>-->

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="hpanel plan-box hgreen active">
                                <div class="panel-heading hbuilt text-center">
                                    <h4 class="font-bold">Startup</h4>
                                    <!--                                <h4 class="font-bold">$50/Month</h4>
                                                                    <h4 class="font-bold">$575/Year</h4>-->
                                </div>
                                <div class="panel-body">
                                    <p class="text-muted">
                                        For first time sign up you get 2x credit.
                                    </p>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>
                                                    Features
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Response Time (1 day)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Resolution Time (1 day)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Support Available (Email & Call)
                                                </td>
                                            </tr>                                        
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Product Documentation
                                                </td>
                                            </tr>                                        
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Reports
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   All In One Solution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 2/month)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   One on One API Discussion (30mins/month)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Uptime 99%
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   API Calls per Second (10)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits (5000)
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <h3 class="font-bold">
                                        A$ 50/Month
                                    </h3>
                                    <a href="./registration.jsp?pa=stdMo" class="btn btn-success btn-sm m-t-xs btn-outline disabled">Sign Up</a>                                                               
                                    <!--                                <a href="./registration.jsp?pa=stdMo" style="float: left"  class="btn btn-success btn-sm m-t-xs">Monthly</a>
                                                                    <a href="./registration.jsp?pa=stdYr" style="float: right"  class="btn btn-success btn-sm m-t-xs">Yearly</a>-->
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="hpanel plan-box hred active">
                                <div class="panel-heading hbuilt text-center">
                                    <h4 class="font-bold">Enterprise</h4>
                                    <!--                                <h4 class="font-bold">$300/Month</h4>
                                                                    <h4 class="font-bold">$3300/Year</h4>-->
                                </div>
                                <div class="panel-body">
                                    <p class="text-muted">
                                        For first time sign up you get 2x credit.
                                    </p>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>
                                                    Features
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Response Time (2 hours)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Resolution Time (2 hours)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Support Available (Email & Call) 
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Product Documentation
                                                </td>
                                            </tr>                                       
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Reports
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   All In One Solution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 5/month)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   One on One API Discussion (90mins/month)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Uptime 99.90%
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   API Calls per Second (30)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Support Hours 24x7x365 Weekdays
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits (36000)
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <h3 class="font-bold">
                                        A$ 300/Month
                                    </h3>
                                    <a href="./registration.jsp?pa=entMo" class="btn btn-danger btn-sm m-t-xs btn-outline disabled">Sign Up</a>
                                    <!--                                <a href="./registration.jsp?pa=entMo" style="float: left"  class="btn btn-danger btn-sm m-t-xs">Monthly</a>
                                                                    <a href="./registration.jsp?pa=entYr" style="float: right"  class="btn btn-danger btn-sm m-t-xs">Yearly</a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="yearly" style="display: none">
                        <div class="col-sm-3">
                            <div class="hpanel plan-box hyellow active">
                                <div class="panel-heading hbuilt text-center">
                                    <h4 class="font-bold">Basic</h4>
                                </div>
                                <div class="panel-body">
                                    <p class="text-muted">
                                        For first time sign up you get 3x credit.
                                    </p>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>
                                                    Features
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Response Time (5 days)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Resolution Time (14 days)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Support Available (Email)
                                                </td>
                                            </tr>                                        
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Product Documentation
                                                </td>
                                            </tr>                                        
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Reports
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   All In One Solution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-square-o"></i>   One on One API Discussion
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Uptime 95%
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   API Calls per Second  (3)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (50000)
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>                            
                                    <h3 class="font-bold">
                                        A$ 60/Year
                                    </h3>
                                    <a href="./registration.jsp?pa=baYr" class="btn btn-warning btn-sm m-t-xs btn-outline disabled">Sign Up</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="hpanel plan-box hblue active">
                                <div class="panel-heading hbuilt text-center">
                                    <h4 class="font-bold">Student</h4>
                                </div>
                                <div class="panel-body">
                                    <p class="text-muted">
                                        For first time sign up you get 3x credit.
                                    </p>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>
                                                    Features
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Response Time (5 days)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Resolution Time (14 days)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Support Available (Email)
                                                </td>
                                            </tr>                                        
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Product Documentation
                                                </td>
                                            </tr>                                        
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Reports
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   All In One Solution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-square-o"></i>   One on One API Discussion
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Uptime 95%
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   API Calls per Second  (3)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (50000)
                                                </td>
                                            </tr>
                                        </tbody>

                                    </table>                            
                                    <h3 class="font-bold">
                                        A$ 180/Year
                                    </h3>
                                    <a href="./registration.jsp?pa=stYr" class="btn btn-info btn-sm m-t-xs btn-outline disabled">Sign Up</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="hpanel plan-box hgreen active">
                                <div class="panel-heading hbuilt text-center">
                                    <h4 class="font-bold">Startup</h4>
                                </div>
                                <div class="panel-body">
                                    <p class="text-muted">
                                        For first time sign up you get 3x credit.
                                    </p>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>
                                                    Features
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Response Time (1 day)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Resolution Time (1 day)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Support Available (Email & Call)
                                                </td>
                                            </tr>                                        
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Product Documentation
                                                </td>
                                            </tr>                                        
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Reports
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   All In One Solution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 2/month)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   One on One API Discussion (30mins/month)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Uptime 99%
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   API Calls per Second (10)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (500000)
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>                            
                                    <h3 class="font-bold">
                                        A$ 575/Year
                                    </h3>
                                    <a href="./registration.jsp?pa=stYr" class="btn btn-success btn-sm m-t-xs btn-outline disabled">Sign Up</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="hpanel plan-box hred active">
                                <div class="panel-heading hbuilt text-center">
                                    <h4 class="font-bold">Enterprise</h4>
                                </div>
                                <div class="panel-body">
                                    <p class="text-muted">
                                        For first time sign up you get 3x credit.
                                    </p>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>
                                                    Features
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Response Time (2 hours)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Resolution Time (2 hours)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Support Available (Email & Call)
                                                </td>
                                            </tr>                                        
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i> Product Documentation
                                                </td>
                                            </tr>                                        
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Reports
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   All In One Solution
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 5/month)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   One on One API Discussion (90mins/month)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Uptime 99.90%
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   API Calls per Second (30)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Support Hours 24x7x365
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (5400000)
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>                            
                                    <h3 class="font-bold">
                                        A$ 3300/Year
                                    </h3>
                                    <a href="./registration.jsp?pa=entYr" class="btn btn-danger btn-sm m-t-xs btn-outline disabled">Sign Up</a>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
        </section>
        <section id="billingFAQ" >
            <div class="container" >
                <div class="row">
                    <div class="col-lg-12">
                        <h2><span class="text-info"><b>Pricing & Billing  </span>FAQ</b></h2>
                        <!--                        <p>Lorem Ipsum available, but the majority have suffered alteration euismod. </p>-->
                    </div>
                </div>
                <div class="row m-t-md">
                    <div class="col-md-6">
                        <h4>How can I use your services?</h4>
                        <p style="font-size: 14px;text-align: justify">To use service you need to subscribe a package. Choose a package as per your API requirement call volume. After subscribe a package you get a credit and by using this credit you can call the API.</p>
                    </div>
                    <div class="col-md-6">              
                        <h4>From where i get my payment invoice?</h4>
                        <p style="font-size: 14px;text-align: justify">When you subscribe for a package and do payment we will email your invoice and from dashboard you can also download you invoice and view your payment history.</p>
                    </div>
                </div>
                <div class="row m-t-xl">
                    <div class="col-md-6">
                        <h4>How my billing cycle works?</h4>
                        <p style="font-size: 14px;text-align: justify">If your subscription is monthly. You package is auto renew after a month and you will get a payment acknowledgement. Your credit will be updated with newly subscribed package expiry.</p>
                    </div>
                    <div class="col-md-6">              
                        <h4>How my credit deducted?</h4>
                        <p style="font-size: 14px;text-align: justify">Your credit deducted as per API you call. Our credit deduction is upon API usage not service dependent. We provide three types of credit deduction a) Credit/API b) Credit/Renewal c) Credit/Entry</p>
                    </div>
                </div>
                <div class="row  m-t-xl">
                    <div class="col-md-6">
                        <h4>What is Credit/API deduction?</h4>
                        <p style="font-size: 14px;text-align: justify">Your credit deducted as per API usage and each API have their own credit value assign. You can get more info about the API level credit from dashboard.</p>
                    </div>
                    <div class="col-md-6">              
                        <h4> What is Credit/Renewal deduction?</h4>
                        <p style="font-size: 14px;text-align: justify">Your credit deducted as per API when that package is renewed accordingly package plan. Each API have their own per renewal credit value assign. You can get more info about the API level credit from dashboard.</p>
                    </div>
                </div>
                <div class="row  m-t-xl">
                    <div class="col-md-6">
                        <h4>What is Credit/ Entry deduction?</h4>
                        <p style="font-size: 14px;text-align: justify">Your credit deducted as per API in which how much parameter you have passed. For example, In Document Utility service their is API for PDF sign. For that API you pass 3 PDF files for signing. In such scenario that API counted as three time use and your credit deducted as per it.  Each API have their own per entry credit value assign.  You can get more info about the API level credit from dashboard.</p>
                    </div>
                    <div class="col-md-6">              
                        <h4> Where i can see my credit usage?</h4>
                        <p style="font-size: 14px;text-align: justify">From dashboards and out-of-the-box reports provide visibility into APIs and services from different perspectives.</p>
                    </div>
                </div>

                <div class="row  m-t-xl">
                    <div class="col-md-6">
                        <h4>How can I estimate my API requirement ?</h4>
                        <p style="font-size: 14px;text-align: justify">You can log your API call volume for a day. from that you can analyse how much credit you need for a day. And as per a day credit calculation you decide your credit limit and choose a package accordingly.</p>
                    </div>
                    <div class="col-md-6">              
                        <h4> What happens if I use all credit before package expire?</h4>
                        <p style="font-size: 14px;text-align: justify">Means may be  API requirement is high than which package you had been subscribe. In that case you need to subscribe to another plan and upgrade your package to get credit. or you can continue with current package with subscribing it again.</p>
                    </div>
                </div> 

                <div class="row  m-t-xl">
                    <div class="col-md-6">
                        <h4>Can I Upgrade or downgrade later?</h4>
                        <p style="font-size: 14px;text-align: justify">Absolutely! You can change your account up or down whenever you want. You can upgrade or downgrade plan accordingly package you choose.</p>
                    </div>
                    <div class="col-md-6">              
                        <h4> What happen to my previous credit if i upgrade or downgrade?</h4>
                        <p style="font-size: 14px;text-align: justify">No Issue, Your remaining credit of previous package will be added in your next subscribe package.</p>
                    </div>
                </div> 

                <div class="row  m-t-xl">
                    <div class="col-md-6">
                        <h4> What happen if i forget to recharge to my account?</h4>
                        <p style="font-size: 14px;text-align: justify">We will take care of it, Our billing cycle is recurring it will auto recharge to you subscribed package.</p>
                    </div>
                    <div class="col-md-6">              
                        <h4>What happen to my remain credit after package expire?</h4>
                        <p style="font-size: 14px;text-align: justify">We will auto renew to your package and invoice you. And your remaining credit will be added with package renewed credit with renew package expiry date.</p>
                    </div>
                </div> 

                <div class="row m-t-xl">
                    <div class="col-md-6">
                        <h4>What is your full Terms of Service?</h4>
                        <p style="font-size: 14px;text-align: justify">You can read The full Loupe Cloud-Hosted <a href="./t&c.jsp" target="_blank">terms of service here.</a> If you need custom terms of service this can be accomodated with Custom Enterprise plans, just contact us to discuss your requirements.</p>
                    </div>
                    <div class="col-md-6">              
                        <h4>What is your privacy policy?</h4>
                        <p style="font-size: 14px;text-align: justify">Loupe Cloud-Hosted has its own <a href="./privacy.jsp" target="_blank">privacy policy</a> that governs what happens to data you upload to Loupe. You can choose to have your data hosted exclusively within the EU if necessary to meet your data sovereignty requirements.</p>
                    </div>
                </div>

            </div>
        </section>
        <section id="contact" class="bg-light">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-6 col-md-offset-3">
                        <h2><span class="text-info">Contact with us</span> anytime</h2>
                        <p>
                            If you are interested in learning more about our READY APIs or having an authorized partner contact you with more information. Please drop a message with below details.
                        </p>
                    </div>
                </div>

                <div class="row text-center m-t-lg">
                    <div class="col-md-4 col-md-offset-3">
                        <form class="form-horizontal" role="form" method="post" id="sentMsgForm" name="sentMsgForm" action="#">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Name</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Your full name" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Email</label>

                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="user@example.com" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="message" class="col-sm-2 control-label">Message</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="3" name="message" id="message" placeholder="Your message here..."></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <span id="submitButton"><a id="submit" class="btn btn-info ladda-button" data-style="zoom-in" onclick="sendMessage()">Send us a message</a></span>
                                    <span id="sendMsg" style="display: none"><a class="btn btn-info" ><i class="fa fa-thumbs-up"></i> Your message sent successfully.</a></span>
                                    <span id="failedsendMsg" style="display: none"><a class="btn btn-danger" ><i class="fa fa-thumbs-down"></i> <span id="responseMsg">Your message failed to sent.</span></a></span>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="col-md-4 text-left">
                        <strong><span class="navy">Our Office</span></strong><br/>
                        <address>
                            <strong><span class="navy">Ready APIs, Blue Bricks Pty. Ltd.</span></strong><br/>
                            <!--                            Unit 39, 118 Adderton Road,<br/>
                                                        Carlingford 2118 NSW, Australia<br/>
                                                        <abbr title="Phone">P:</abbr> +61 423 394 252 / +61 478 665 482-->
                            <img src="api/blank.gif" class="flag flag-au" alt="Australia"/> 36/118 Adderton Road, Carlingford 2118, Sydney, NSW, Australia</br>
                            <img src="api/blank.gif" class="flag flag-my" alt="Malaysia"/> Suite Ex5, A-5-10 Empire Tower SS16/1, Subang Jaya 47500, Selangor, Malaysia</br>
                            <img src="api/blank.gif" class="flag flag-in" alt="India"/> 321 office number, Amanora chamber ,East ,Hadapsar ,Pune, Maharashtra 411028, India</br>
                            <abbr title="Phone">P:</abbr> +61478665482<br>
                            <abbr title="Email">E:</abbr> support@readyapis.com
                        </address>


                        <!--                        <address>
                                                    <strong><span class="navy">Also we have offices in,</span></strong><br/>                    
                                                    <strong><span class="navy">Malaysia</span></strong><br/>
                                                    <strong><span class="navy">India</span></strong><br/>
                                                    <strong><span class="navy">USA</span></strong><br/>
                                                </address>                -->
                    </div>

                    <!--            <div class=" text-left">
                                <address>
                                    <strong><span class="navy">Blue Bricks Pty. Ltd.</span></strong><br/>
                                    No. 321, Amanora Chambers East and West,<br/>
                                    Hadapsar, Pune - 411028. Maharashtra, India <br/>                                                
                                    <abbr title="Phone">P:</abbr> +917 3500 18936
                                </address>
                                </div>-->
                </div>


            </div>
        </section>

        <!-- Vendor scripts -->

        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>

        <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="vendor/sparkline/index.js"></script>

        <!-- App scripts -->
        <script src="scripts/homer.js"></script>
        <script src="main.js"></script>
        <script src="vendor/ladda/dist/spin.min.js"></script>
        <script src="vendor/ladda/dist/ladda.min.js"></script>
        <script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
        <!-- Local script for menu handle -->
        <!-- It can be also directive -->

        <script>
                                        $(document).ready(function () {

                                            // Page scrolling feature
                                            $('a.page-scroll').bind('click', function (event) {
                                                var link = $(this);
                                                $('html, body').stop().animate({
                                                    scrollTop: $(link.attr('href')).offset().top - 50
                                                }, 500);
                                                event.preventDefault();
                                            });

                                            $('body').scrollspy({
                                                target: '.navbar-fixed-top',
                                                offset: 80
                                            });

                                        });

                                        function sendMessage() {
                                            var s = './sentMessage';
                                            var l;
                                            l = $('#submit').ladda();
                                            l.ladda('start');
                                            $.ajax({
                                                type: 'POST',
                                                url: s,
                                                dataType: 'json',
                                                data: $("#sentMsgForm").serialize(),
                                                success: function (data) {
                                                    if (strCompare(data.result, "error") === 0) {
                                                        l.ladda('stop');
                                                        document.getElementById("failedsendMsg").style.display = 'block';
                                                        document.getElementById("responseMsg").innerHTML = data.message;
                                                        document.getElementById("submitButton").style.display = 'none';
                                                        window.setTimeout(function () {
                                                            document.getElementById("submitButton").style.display = 'block';
                                                            document.getElementById("failedsendMsg").style.display = 'none';
                                                        }, 3000);
                                                    } else if (strCompare(data.result, "success") === 0) {
                                                        l.ladda('stop');
                                                        document.getElementById("sendMsg").style.display = 'block';
                                                        document.getElementById("submitButton").style.display = 'none';
                                                        window.setTimeout(function () {
                                                            document.getElementById("submitButton").style.display = 'block';
                                                            document.getElementById("sendMsg").style.display = 'none';
                                                        }, 3000);
                                                    }
                                                }
                                            });
                                        }
        </script>

        <!--Global site tag (gtag.js) - Google Analytics--> 
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107416064-2"></script>
        <script>
                                        window.dataLayer = window.dataLayer || [];
                                        function gtag() {
                                            dataLayer.push(arguments);
                                        }
                                        gtag('js', new Date());

                                        gtag('config', 'UA-107416064-2');
        </script>


    </body>
</html>
