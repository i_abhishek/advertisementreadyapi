<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TaxCalculationManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.GenerateInvoiceId"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Page title -->
        <title>Ready APIs</title>
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->
        <!-- Vendor styles -->
        <!--    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />-->
        <!--    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />-->
        <!--    <link rel="stylesheet" href="vendor/animate.css/animate.css" />-->
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
        <!-- App styles -->
        <!--    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />-->
        <!--    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />-->
        <link rel="stylesheet" href="styles/style.css">
    </head>

    <body>
        <style>
            .col-centered{
                float: none;
                margin: 0 auto;
            }
        </style>
        <!-- Simple splash screen-->
        <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Ready APIs</h1><p>Loading... </p><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- Main Wrapper -->
        <%
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            if (SessionId == null) {
                response.sendRedirect("logout.jsp");
                return;
            }
            String stripePublishableKey = LoadSettings.g_sSettings.getProperty("stripe.publishableKey");
            PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
            String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
            String packageName = request.getParameter("_packageName");
            String _paymentMode = request.getParameter("_paymentMode");
            String outPackageName = packageName;
            SimpleDateFormat invoiceDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:MM a");
            String invoiceid = GenerateInvoiceId.getDate() + parObj.getPartnerId() + GenerateInvoiceId.getRandom();

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            String invoiceDate = dateFormat.format(new Date());

            SgReqbucketdetails packageObject = new RequestPackageManagement().getReqPackageByName(SessionId, ChannelId, packageName);
            String paymentType = packageObject.getPaymentMode();
            //SgSubscriptionDetails subscriObj = new PackageSubscriptionManagement().getSubscriptionbyPartnerId(SessionId, ChannelId, parObj.getPartnerId());
            NumberFormat form = new DecimalFormat("#0.00");
            Double totalPaid = new Double(0);
            Double changeOnPackageCharge = null;
            Double reActivationCharge = null;
            Double cancellationCharge = null;
            Double latePenaltyCharge = null;

            // variable for gst tax cal
            Double totalAmountWithoutTax = 0.00;
            Double gstTax = 0.00;
            Double vatTax = 0.00;
            Double serviceTax = 0.00;

            Map<String, Double> amount = new LinkedHashMap<String, Double>();
            Map<String, String> taxMap = new LinkedHashMap<String, String>();

            if (packageObject != null) {
                SgSubscriptionDetails subscriObjPrepaid = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(parObj.getPartnerId());
                packageName = packageObject.getBucketName();
                session.setAttribute("_originalPackageName", packageName);
                double planAmount = packageObject.getPlanAmount();
                String bucketName = packageName.toLowerCase();
                if (bucketName.contains("basic") && bucketName.contains("month")) {
                    packageName = "Basic";
                } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                    packageName = "Basic";
                } else if (bucketName.contains("student") && bucketName.contains("month")) {
                    packageName = "Student";
                } else if (bucketName.contains("student") && bucketName.contains("year")) {
                    packageName = "Student";
                } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                    packageName = "Standard";
                } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                    packageName = "Standard";
                } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                    packageName = "Enterprise";
                } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                    packageName = "Enterprise";
                }
                amount.put(packageName, planAmount);
                totalPaid += planAmount;

                double serviceCharge = Float.parseFloat(packageObject.getServiceCharge());
//                amount.put("Service Charge", serviceCharge);
                totalPaid += serviceCharge;
                if (subscriObjPrepaid != null) {

                    if (!packageObject.getBucketName().equals(subscriObjPrepaid.getBucketName())) {
                        double changePackageCharge = subscriObjPrepaid.getChangePackageRate();
                        //amount.put("Change Package Charge", changePackageCharge);
                        //totalPaid += changePackageCharge;
                        changeOnPackageCharge = changePackageCharge;
                        session.setAttribute("_changeOnPackageCharge", String.valueOf(changePackageCharge));
                    }

                    if (packageObject.getBucketName().equals(subscriObjPrepaid.getBucketName())) {
                        double reactivationCharge = Float.parseFloat(String.valueOf(subscriObjPrepaid.getReActivationCharge()));
                        //amount.put("Reactivation Charge", reactivationCharge);
                        //totalPaid += reactivationCharge;
                        reActivationCharge = reactivationCharge;
                        session.setAttribute("_reactivationCharge", String.valueOf(reactivationCharge));
                    }

                    String expiryDate = dateFormat.format(subscriObjPrepaid.getExpiryDateNTime());
                    String strDate = dateFormat.format(new Date());
                    Date curDate = dateFormat.parse(strDate);
                    Date exDate = dateFormat.parse(expiryDate);
                    if (curDate.before(exDate)) {
                        double packageCancellationCharge = subscriObjPrepaid.getCancellationRate();
                        //amount.put("Cancellation Charge", packageCancellationCharge);
                        //totalPaid += packageCancellationCharge;
                        cancellationCharge = packageCancellationCharge;
                        session.setAttribute("_cancellationCharge", String.valueOf(packageCancellationCharge));
                    }
                    // get info about loan
                }
                totalAmountWithoutTax = totalPaid;
                String taxRate = packageObject.getTax();
                taxMap = TaxCalculationManagement.calculateTax(taxRate, totalPaid);

                String calculatedGSTDetail = (String) taxMap.get("GST Tax");
                String calculatedVATDetail = (String) taxMap.get("VAT Tax");
                String calculatedSTDetail = (String) taxMap.get("Service Tax");

                String[] calculatedGST = calculatedGSTDetail.split(":");
                String[] calculatedVAT = calculatedVATDetail.split(":");
                String[] calculatedST = calculatedSTDetail.split(":");

                totalPaid += Double.parseDouble(calculatedGST[1]);
                //                totalPaid += Double.parseDouble(calculatedVAT[1]);
                //                totalPaid += Double.parseDouble(calculatedST[1]);

                gstTax = Double.parseDouble(calculatedGST[0]);
                vatTax = Double.parseDouble(calculatedVAT[0]);
                serviceTax = Double.parseDouble(calculatedST[0]);
            }

            double roundedTMREQAMOUNT = (double) Math.round(totalPaid * 100) / 100;
            String tmWellFormAmount = form.format(roundedTMREQAMOUNT);
            String wellFormTotalBeforeTax = form.format(totalAmountWithoutTax);
            JSONObject partnerObj = new JSONObject();

            session.setAttribute("tierOrSlabUsageDetails", partnerObj);

            Double flatPrice = 0.00;

            //String jsonFlatPriceDetails = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

            String paymode = URLEncoder.encode(new String(Base64.encode(_paymentMode.getBytes())), "UTF-8");
            System.out.println("paymode >>>> " + paymode);

            
            session.setAttribute("_invoiceId", invoiceid);
            session.setAttribute("_packageObject", packageObject);
            String stripeAmount = "0";
            String gstNumber = "NA";
            String strgstNum = (String) LoadSettings.g_sSettings.getProperty("gst.number");
            if (strgstNum != null) {
                gstNumber = strgstNum;
            }
        %>

        <div id="wrapper" style="margin: 0 0 0 0px!important">
            <div class="content animate-panel">
                <div class="row">
                    <div class="col-lg-8 col-centered">
                        <div class="hpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <img src="images/imageedit_1_9895468797.png" width="150" style="padding-left: 20px!important" alt=""/>
                                        <!--                            <h4>Invoice <small>IN-9177283-2016</small></h4>-->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-right">                                
                                            <form action="./StripeCreateCharge" method="POST">

                                                <script
                                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                    data-key="<%=stripePublishableKey%>"
                                                    data-amount="<%=stripeAmount%>"
                                                    data-name="Ready APIs"
                                                    data-description="<%=packageName%>"
                                                    data-image="images/front-logo.png"
                                                    data-locale="auto"                                                    
                                                    data-currency="aud">
                                                </script>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel-body p-xl">
                                <div class="row m-b-xl">
                                    <div class="col-sm-6">
                                        <h4>Invoice No. <%=invoiceid%></h4>

                                        <address>
                                            <strong>Blue Bricks Pty Ltd.</strong><br>
                                            Unit 39, 118 Adderton Road,<br>
                                            Carlingford 2118 NSW,</br>
                                            Australia</br>
                                            <%if (!gstNumber.equalsIgnoreCase("na")) {%>
                                            GST No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=gstNumber%>
                                            <%}%>
                                            <!--                                <abbr title="Phone">P:</abbr> (831) 758-7200-->
                                        </address>
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <span>To:</span>
                                        <address>
                                            <strong><%=parObj.getPartnerName()%></strong><br>
                                            <!--                                60 Mortensen Avenue<br>
                                                                            Salinas, CA 123343<br>-->
                                            <abbr title="Email">E:</abbr> <%=parObj.getPartnerEmailid()%></br>

                                            <p>
                                                <span><strong>Date:</strong> <%=invoiceDateFormat.format(new Date())%></span><br/>                                
                                            </p>
                                        </address>                            

                                    </div>
                                </div>

                                <div class="table-responsive m-t col-md-12">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Item List</th>
                                                <th>Details</th>
                                                <th/>
                                                <th style="text-align: center">Total Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                int count = 1;
                                                Double totalAmount = 0.0;
                                                DecimalFormat df = new DecimalFormat("#0.00");
                                                df.setMaximumFractionDigits(2);
                                                String apName = "NA";
                                                String resName = "NA";
                                                String envt = "NA";
                                                String apiName = "NA";
                                                int version = 0;
                                                int callCount = 0;
                                                float amountS = 0;
                                                if (partnerObj.length() != 0) {
                                                    Iterator itrS = partnerObj.keys();
                                                    while (itrS.hasNext()) {
                                                        Object keyS = itrS.next();
                                                        Date apiCallDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(keyS.toString());
                                                        JSONObject data = new JSONObject(partnerObj.getString((String) keyS));
                                                        Iterator itr = data.keys();
                                                        while (itr.hasNext()) {
                                                            float totalCharge = 0.0f;
                                                            Object key = itr.next();
                                                            String keyData = (String) key;
                                                            String value = data.getString((String) key);
                                                            int acc = Integer.parseInt(keyData.split(":")[0]);
                                                            int res = Integer.parseInt(keyData.split(":")[1]);
                                                            version = Integer.parseInt(keyData.split(":")[2]);
                                                            envt = (keyData.split(":")[3]);
                                                            apiName = (keyData.split(":")[4]);
                                                            Accesspoint ap = new AccessPointManagement().getAccessPointById(ChannelId, acc);
                                                            ResourceDetails rs = new ResourceManagement().getResourceById(res);
                                                            if (ap != null) {
                                                                apName = ap.getName();
                                                            }
                                                            if (rs != null) {
                                                                resName = rs.getName();
                                                            }
                                                            if (!envt.equalsIgnoreCase("Live")) {
                                                                continue;
                                                            }
                                                            callCount = Integer.parseInt(value.split(":")[0]);
                                                            amountS = Float.parseFloat(value.split(":")[1]);

                                                        }
                                                    }
                                                }
                                                if (packageObject != null && packageObject.getFlatPrice() != null) {
                                                    String flatPricede = packageObject.getFlatPrice();
                                                    JSONArray jsOld = new JSONArray(flatPricede);
                                                    for (int j = 0; j < jsOld.length(); j++) {
                                                        JSONObject jsonexists1 = jsOld.getJSONObject(j);
                                                        Iterator<String> keys = jsonexists1.keys();
                                                        if (keys.hasNext()) {
                                                            String key = (String) keys.next();
                                                            String[] keyArr = key.split(":");
                                                            String value = jsonexists1.getString(key);
                                                            String[] valueArr = value.split(":");
                                            %>
                                            <tr>
                                                <td><%=keyArr[0]%></td>

                                                <td></td>
                                                <td/>
                                                <td style="text-align: center">AUD <%=df.format(Double.parseDouble(valueArr[1]))%></td>
                                            </tr>
                                            <%
                                                            totalAmount = totalAmount + Double.parseDouble(valueArr[1]);
                                                        }
                                                    }
                                                }
                                                for (Map.Entry<String, Double> entry : amount.entrySet()) {
                                            %>     
                                            <tr>

                                                <!--<td colspan="2"></td>-->
                                                <td><%=entry.getKey()%></td>
                                                <td></td>
                                                <td/>
                                                <td style="text-align: center">AUD <%=df.format(entry.getValue())%></td>
                                            </tr>
                                            <%
                                                    count++;
                                                    totalAmount = totalAmount + entry.getValue();
                                                }
                                                totalAmountWithoutTax = totalAmount;
                                                String taxRate = packageObject.getTax();
                                                session.setAttribute("totalPaymentAmountWithoutTax", totalAmountWithoutTax);
                                                taxMap = TaxCalculationManagement.calculateTax(taxRate, totalAmountWithoutTax);
                                                if (!taxMap.isEmpty()) {
                                                    for (Map.Entry<String, String> entry : taxMap.entrySet()) {
                                                        if (entry.getKey().equalsIgnoreCase("GST Tax")) {
                                                            String[] taxArr = entry.getValue().split(":");

                                            %>
                                            <tr>
                                                <!--<td colspan="2"></td>-->
                                                <td><%=entry.getKey()%></td>
                                                <td><%=taxArr[0]%> %</td>
                                                <td/>
                                                <td style="text-align: center">AUD <%=df.format(Double.parseDouble(taxArr[1]))%></td>
                                            </tr>
                                            <%
                                                            count++;
                                                            totalAmount = totalAmount + Float.parseFloat(taxArr[1]);
                                                        }
                                                    }
                                                }
                                                session.setAttribute("packageNameStripe", packageName);
                                                session.setAttribute("_grossAmount", tmWellFormAmount);
                                                stripeAmount = tmWellFormAmount.replace(".", "");
                                            %> 
                                            <tr>
                                                <!--<td colspan="2"></td>-->
                                                <td/>
                                                <td colspan="2" style="text-align: right"><strong>TOTAL</strong></td>
                                                <td style="text-align: center" id="totalAmount">AUD <%=df.format(totalAmount)%></td>
                                            </tr>        
                                        </tbody>
                                    </table>
                                </div>                    
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="m-t"><strong>PRIVACY STATEMENT</strong></br></br>
                                            Blue Bricks's PRIVACY STATEMENT In its effort to ensure compliance to the Personal Data Protection Act 2010 (PDPA), Blue Bricks has put in place a personal data protection policy which shall govern the use and protection of your personal data as Blue Bricks's customer. For details of the policy, 
                                            please refer to Blue Bricks's Privacy Statement at http://www.blue-bricks.com, which may be reviewed by Blue Bricks from time to time.
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Vendor scripts -->
        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="vendor/sparkline/index.js"></script>
        <!-- App scripts -->
        <script src="scripts/homer.js"></script>
        <script>

                                                        var htmlSource = document.documentElement.innerHTML;
                                                        console.log(htmlSource);
                                                        function saveHTML(htmlSource) {
                                                            var data = {"htmlSource": htmlSource};
                                                            var s = './SaveInvoice';
                                                            $.ajax({
                                                                type: 'POST',
                                                                url: s,
                                                                datatype: 'html',
                                                                data: data,
                                                                success: function (data) {

                                                                }
                                                            });
                                                        }

                                                        saveHTML(htmlSource);
        </script>
    </body>
</html>
