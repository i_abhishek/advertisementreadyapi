
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement"%>
<%@page import="java.util.Map"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String sessionid = (String) session.getAttribute("_partnerSessionId");
    int loginType = 0;
    boolean sessionFlag = true;
    if (session.getAttribute("_loginType") != null) {
        loginType = (Integer) session.getAttribute("_loginType");

        if (sessionid != null) {
            new SessionManagement().CloseSession(sessionid);

            Map map = (Map) application.getAttribute("LoginDetails");
            String userid = (String) request.getSession().getAttribute("_rssuserid");
            Map<String, HttpSession> sessionMap = (Map) getServletContext().getAttribute("sessionDetails");
            if (map == null) {
                map = new HashMap();
            }
            if (sessionMap == null) {
                sessionMap = new HashMap();
            }
            map.remove(userid);
            HttpSession sesion = sessionMap.get(userid);
            if (sesion != null) {
                sesion.invalidate();
                sessionFlag = false;
            }
            sessionMap.remove(userid);
            getServletContext().setAttribute("LoginDetails", map);
            getServletContext().setAttribute("sessionDetails", sessionMap);
        }
    }
    if (loginType == 0 && sessionFlag) {
        session.invalidate();
%>
<script>
    window.location.href = './login.jsp';
</script>
<%} else {%>
<script>
    window.location.href = './login.jsp';
</script>
<%}%>
