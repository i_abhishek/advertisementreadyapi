'use strict';
const applicationServerPublicKey = 'BHJbjjDO-6F-eXTqtVVu_aHZ_yssYATwe-3bU8pqXwe8Xik0lkq6LI9rOojUbVqh46GQrxoObj4A4zv_T9hBpZ0';
var domain = "web.com.readyapis.safari"; //Replace your website push id incase you are this script for safari
var host = "www.readyapis.com"; // Replace your host incase you are this script for safari

var developerId = '5e24462f-d237-47e9-bddc-ecd1f328a23e'; //Replace your developer ID
var appplicationId = '075661ca-04a5-4e43-beee-12eeb640d030'; //Replace your appplication ID
var callBackURL = ''//Replace this Callback URL with your server URL

var ua = window.navigator.userAgent,
        safariTxt = ua.indexOf("Safari"),
        chrome = ua.indexOf("Chrome"),
        version = ua.substring(0, safariTxt).substring(ua.substring(0, safariTxt).lastIndexOf("/") + 1);
let isSubscribed = false;
let swRegistration = null;

function urlB64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
            .replace(/\-/g, '+')
            .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

function updateBtn() {
    if (Notification.permission === 'denied') {

       // alert('Push Messaging Blocked.');
        isSubscribed = false;
        updateSubscriptionOnServer(null);
        return;
    }

}

function updateSubscriptionOnServerForSafari(subscription) {
    /* TODO: Send the subscription object to application server.
     *       notifications are sent from the server using this object.
     */

    var DeviceToken;
    var s;
    
    
        DeviceToken = JSON.stringify(subscription);
        console.log("Updating deviceToken on server >> "+DeviceToken);
        s = 'https://www.readyapis.com:443/ReadiAPIWrapper/registerUser?userId=' + encodeURIComponent(userId) + '&developerId=' + developerId + '&appplicationId=' + appplicationId + '&DeviceToken=' + subscription.deviceToken + '&callBackURL=' + callBackURL +'&type=web' +'&subtype=readyapis';
    

    var jsonData = $.ajax({
        url:  s,
        dataType: "json",
        async: false,
        crossDomain: true
    }).responseText;
    console.log("TokenUpdation On server >> "+JSON.stringify(jsonData));
    return false;

}

function updateSubscriptionOnServer(subscription) {
    /* TODO: Send the subscription object to application server.
     *       notifications are sent from the server using this object.
     */

    var DeviceToken;
    var s;
    console.log("Updating deviceToken on server >> "+subscription);
    if (isSubscribed === false) {
//        s = 'https://www.readyapis.com:443/ReadiAPIWrapper/unregisterUser?userId=' + encodeURIComponent(userId) + '&developerId=' + developerId + '&appplicationId=' + appplicationId + '&callBackURL=' + callBackURL +'&type=web';
    } else {
        DeviceToken = JSON.stringify(subscription);
        s = 'https://www.readyapis.com:443/ReadiAPIWrapper/registerUser?userId=' + encodeURIComponent(userId) + '&developerId=' + developerId + '&appplicationId=' + appplicationId + '&DeviceToken=' + DeviceToken + '&callBackURL=' + callBackURL +'&type=web&subtype=readyapis';
    }

    var jsonData = $.ajax({
        url:  s,
        dataType: "json",
        async: false,
        crossDomain: true
    }).responseText;
    console.log("DokenUpdation On server >> "+JSON.stringify(jsonData));
    return false;

}

function subscribeUser() {

    const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
    swRegistration.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: applicationServerKey
    }).then(function (subscription) {
        console.log('User is subscribed:', subscription);
        isSubscribed = true;
        updateSubscriptionOnServer(subscription);

       // updateBtn();
        // Show subscription for debug
       // window.prompt('Subscription details:', JSON.stringify(subscription));
    })
            .catch(function (err) {
                console.log('Failed to subscribe the user: ', err);
              //  updateBtn();
            });
}

function unsubscribeUser() {
    swRegistration.pushManager.getSubscription().then(function (subscription) {
        if (subscription) {
         //   alert("You are not subscribed now.")
            return subscription.unsubscribe();
        }
    }).catch(function (error) {
        console.log('Error unsubscribing', error);
    }).then(function () {
        isSubscribed = false;

        updateSubscriptionOnServer(null);
        console.log('User is unsubscribed.');
      //  updateBtn();
    });
}

function subscribeNon() {

    if (isSubscribed) {
        unsubscribeUser();
    } else {
        subscribeUser();
    }
    // Set the initial subscription value
    swRegistration.pushManager.getSubscription().then(function (subscription) {
        isSubscribed = !(subscription === null);
       // updateSubscriptionOnServer(subscription);
    });
}

// For safari
function requestPermissions() {
    window.safari.pushNotification.requestPermission('https://www.readyapis.com/push-api/', domain, {}, function (subscription) {
        console.log(subscription);
        if (subscription.permission === 'granted') {
            updateSubscriptionOnServerForSafari(subscription);
        } else if (c.permission === 'denied') {
            // TODO:
        }
    });
}

function nonSafariInit() {

    if ('serviceWorker' in navigator && 'PushManager' in window) {
        console.log('Service Worker and Push is supported');
        navigator.serviceWorker.register('sw.js').then(function (swReg) {
            console.log('Service Worker is registered', swReg);
            swRegistration = swReg;
            subscribeNon();
        }).catch(function (error) {
            console.error('Service Worker Error', error);
        });
    } else {
        console.warn('Push messaging is not supported');
      //  alert('Push Not Supported');
    }
}

// For safari
function safariIniti() {
   
   safariIPAndHost();

    // var pResult = window.safari.pushNotification.permission(domain);

    // if (pResult.permission === 'default') {
    //     //request permission
    //     requestPermissions();
    // } else if (pResult.permission === 'granted') {
    //     console.log("Permission for " + domain + " is " + pResult.permission);
    //     var token = pResult.deviceToken;
    //     // Show subscription for debug
    //   //  window.prompt('Subscription details:', token);
    // } else if (pResult.permission === 'denied') {
    //   //  alert("Permission for " + domain + " is " + pResult.permission);
    // }
}

function strCompare(a, b){
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function safariIPAndHost(){
    var s = "https://www.readyapis.com:443/push-api/GetIPHost?&hostForSafari="+host
    
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                
            } else if (strCompare(data.result, "success") === 0) {                
                var pResult = window.safari.pushNotification.permission(domain);
    
                if(pResult.permission === 'default') {
                    //request permission
                    requestPermissions();
                } else if (pResult.permission === 'granted') {
                    console.log("Permission for " + domain + " is " + pResult.permission);
                    var token = pResult.deviceToken;
                    // Show subscription for debug
//                    window.prompt('Subscription details:',token);
                } else if(pResult.permission === 'denied') {
//                    alert("Permission for " + domain + " is " + pResult.permission);
                }
            } 
        }
    });
}

/*
 * Call relevant methods.
 */
if (chrome == -1 && safariTxt > 0) {
    if (parseInt(version, 10) >= 7) {
        console.log("Safari browser detected.");
        safariIniti();
    } else {
        console.log("Safari unsupported version detected.");
    }
} else {
    console.log("Non Safari browser detected.");
    nonSafariInit();
}

function register() {
    var userid = getUserid();
    //send Token with Userid
}

function getUserid() {
    return "abc";
}