<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%--<%@include file="header.jsp" %>--%>
<%@ include file="incl.header.jsp" %>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt"%>
<script src="<%= new com.paypal.BtConfigReader().getBtJavaScriptClient()%>"></script>
<script src="<%= new com.paypal.BtConfigReader().getBtJavaScriptHostedFields()%>"></script>
<script src="<%= new com.paypal.BtConfigReader().getBtJavaScriptPayPal()%>"></script>
<script src="https://www.paypalobjects.com/api/checkout.js" data-version-4></script>
<%
    PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
    SgDeveloperProductionEnvt productionEnvt = new ProductionAccessEnvtManagement().getProudDetailsByPartnerId(parObj.getPartnerId());
    String billingAddress = "NA";
    String billingState = "NA";
    String billingPostCode = "NA";
    String billingCountry = "NA";

    if (productionEnvt != null) {
        billingAddress = productionEnvt.getBillingAddress();
        billingState = productionEnvt.getBillingState();
        billingPostCode = productionEnvt.getBillingPostCode();
        billingCountry = productionEnvt.getBillingCountry();
    }

    String paymentAmount = (String) session.getAttribute("totalPaymentAmountWithTax");
%>

<div class="container-fluid" style="text-align: center">
            <div class="row">
            
	            <div class="col-sm-5 colCentered" >
					
					<h4>Payment Methods</h4>
					
					<br />
                            <form  id="bt-payPalButton-form" action="BtCheckoutServlet" method="post">
                                <div id="papbutton"></div>
                                <input type="hidden" name="csrf" value="csrf" />
                                <input type="hidden" name="payment-method-nonce" class="payment-method-nonce" />						
                                <input type="hidden" name="total-amount" value="<%=paymentAmount%>" />
                                <input type="hidden" name="first-name" value="<%=parObj.getPartnerName()%>" />
                                <input type="hidden" name="last-name" value=" " />
                                <input type="hidden" name="line1" value="<%=billingAddress%>" />
                                <input type="hidden" name="line2" value="" />
                                <input type="hidden" name="city" value="<%=billingState%>" />
                                <input type="hidden" name="state" value="<%=billingState%>" />
                                <input type="hidden" name="postal-code" value="431203" />
                                <input type="hidden" name="country-code" value="" />
                            </form>
                            <form id="bt-hsf-checkout-form" action="BtCheckoutServlet" method="post" >
                                <input type="hidden" name="csrf" value="csrf" />
                                <input type="hidden" name="payment-method-nonce" class="payment-method-nonce" />						
                                <input type="hidden" name="total-amount" value="<%=paymentAmount%>" />
                                <input type="hidden" name="first-name" value="<%= parObj.getPartnerName()%>" />
                                <input type="hidden" name="last-name" value=" " />
                                <input type="hidden" name="line1" value="<%=billingAddress%>" />
                                <input type="hidden" name="line2" value=" " />
                                <input type="hidden" name="city" value="<%= billingState%>" />
                                <input type="hidden" name="state" value="<%= billingState%>" />
                                <input type="hidden" name="postal-code" value="431203" />
                                <input type="hidden" name="country-code" value="" />
                                <table>	
                                    <tr>
                                        <td colspan="2">
                                            <div id="invalid-field-error" class="btError" style="display:none; color: red;">
                                                Please review the credit card fields below.
                                            </div>
                                            <div id="error-message"></div>
                                        </td>
                                    </tr>
                                    <tr style="text-align: center">
                                        <td>Card Number</td>
                                        <td>
                                            <div class="hosted-field" id="card-number" ></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>CVV</td>
                                        <td><div class="hosted-field" id="cvv"></div></td>
                                    </tr>
                                    <tr>
                                        <td>Expiration Date</td>
                                        <td><div class="hosted-field" id="expiration-date"></div></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div style="font-size: 11px; right-margin: -80px;">
                                                NOTE: Please enter the values displayed in the credit card fields
                                            </div>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td>			                  		
                                            <br><input class="btn btn-success" id="bt-hsf-checkout-submit" type="submit" value="Pay with Card" disabled>
                                        </td>
                                    </tr>
                                </table>                                
                            </form>                                
                        </div>
                    </div>
                </div>
           
        
        <%@ include file="incl.footer.jsp" %>
<script type="text/javascript">
		
			// authorization from server
			var authorization = "<%= new com.paypal.BtTransactionHelper().getToken() %>";
			
			// Hosted Fields 
			var submit = document.getElementById('bt-hsf-checkout-submit');
			var btForm = document.getElementById('bt-hsf-checkout-form');
			
			// Checkout with PayPal
			var ppForm = document.getElementById('bt-payPalButton-form');
			var btPayPalButton = document.getElementById('papbutton');
			
			
			var recipientName = '<%= request.getParameter("first-name") %> <%= request.getParameter("last-name") %>';
			var line1 = '<%= request.getParameter("line1") %>';
			var line2 = '<%= request.getParameter("line2") %>';
			var city = '<%= request.getParameter("city") %>';
			var state = '<%= request.getParameter("state") %>';
			var postalCode = '<%= request.getParameter("postal-code") %>';
			var countryCode = '<%= request.getParameter("country-code") %>';
			var totalAmount = '<%= request.getParameter("total-amount") %>';
			
			braintree.client.create({
				authorization: authorization
			}, function(clientErr, clientInstance) {
				// Handle error in client creation
				if (clientErr) { 
					console.log('Error creating client instance:: ' + clientErr);
					return;
				}
				/* 
					Braintree - Hosted Fields component 
				*/
				braintree.hostedFields.create({
					client: clientInstance,
					styles: {
						'input': {
							'font-size': '14pt',
							'color': '#6E6D6C'
						},
						'input.invalid': {
							'color': 'red'
						},
						'input.valid': {
							'color': 'green'
						}
					},
					fields: {
						number: {
							selector: '#card-number',
							placeholder: '4111 1111 1111 1111'
						},
						cvv: {
							selector: '#cvv',
							placeholder: '123'
						},
						expirationDate: {
							selector: '#expiration-date',
							placeholder: '10/2019'
						}
					}
				}, function(hostedFieldsErr, hostedFieldsInstance) {
					if (hostedFieldsErr) { /*Handle error in Hosted Fields creation*/
						return;
					}
					submit.removeAttribute('disabled');
					btForm.addEventListener('submit', function(event) {
						event.preventDefault();
						hostedFieldsInstance.tokenize(function(tokenizeErr, payload) {
							if (tokenizeErr) { /* Handle error in Hosted Fields tokenization*/
								document.getElementById('invalid-field-error').style.display = 'inline';
								return;
							} 
							
							// Put `payload.nonce` into the 'payment-method-nonce' input, then submit the form 
							$('.payment-method-nonce').val(payload.nonce);
							
							console.log('hostfield nonce', payload.nonce);

							btForm.method = "post";
							
							
							if ("merchant" === "merchant") {
								btForm.submit();
							}
						});
					}, false);
				});

				/* 
					Braintree - PayPal button component 
				*/
				braintree.paypalCheckout.create({
					client: clientInstance
				}, function (createErr, paypalCheckoutInstance) {
					if (createErr) {
						if (createErr.code === 'PAYPAL_BROWSER_NOT_SUPPORTED') {
							console.error('This browser is not supported.');
						} 
						else {
							console.error('Error!', createErr);
						}
						return;
					}
					
//					paypal.Button.render({
//						env: 'sandbox', // or 'production'
//
//						locale: 'en_US',
//						
////				        style: {
////				            size: 'medium',
////				            color: 'gold',
////				            shape: 'pill',
////				            label: 'checkout'
////				        },
//
//						payment: function () {
//							return paypalCheckoutInstance.createPayment({
//								flow: 'checkout',
//								intent: 'sale',
//								amount: totalAmount,
//								currency: 'USD',
//								locale: 'en_US',
//								displayName: 'Demo Portal Test Store',
//								enableShippingAddress: false,
//								shippingAddressEditable: false,
////								shippingAddressOverride: {
////									recipientName: recipientName,
////									line1: line1,
////									line2: line2,
////									city: city,
////									countryCode: countryCode,
////									postalCode: postalCode,
////									state: state
////								}
//							});
//						},
//						
//						// Optional: show a 'Pay Now' button in the checkout flow rather than Continue
//						commit: true, 
//
//						onAuthorize: function (data, actions) {
//							return paypalCheckoutInstance.tokenizePayment(data, function (err1, payload) {
//								
//								if (err1) {
//									console.error('onAuthorize: ', err1);
//								} 
//								else {
//									console.log('Paypal nonce: ', payload.nonce );
//
//									// 	Put `payload.nonce` into the 'payment-method-nonce' input, then submit the form 
//									$('.payment-method-nonce').val(payload.nonce);
//									
//									ppForm.method = "post";
//									ppForm.submit();
//								}
//							});
//						},
//
//						onCancel: function (data) {
//							console.log('checkout.js payment cancelled', JSON.stringify(data, 0, 2));
//							var currentUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;
//							var cancelUrl = currentUrl.substring(0, currentUrl.lastIndexOf('/')) + '/cancel.jsp';
//							window.location.href = cancelUrl;
//
//						},
//
//						onError: function (err) {
//							console.error('checkout.js error', err.toString());
//							var currentUrl = window.location.href = window.location.protocol + '//' + window.location.host + window.location.pathname;
//							var errorUrl = currentUrl.substring(0, currentUrl.lastIndexOf('/')) + '/error.jsp?message='+err.toString();
//							window.location.href = errorUrl;
//						}
//					}, '#papbutton'); // the PayPal button will be rendered in an html element with the id `papbutton`
				});				

			});

		</script>

