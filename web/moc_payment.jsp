<%@include file="header.jsp"%>
<script src="jshash/sha1-min.js" type="text/javascript"></script>
<script src="jshash/sha1.js" type="text/javascript"></script>       
<!--<script src="scripts/payment.js" type="text/javascript"></script>-->

<!-- Main Wrapper -->
<%
    String env = request.getParameter("_envt");
    String partnerId = parObj.getPartnerUniqueId();
    String partnerTokenId = "";
    if (env.equalsIgnoreCase("Test")) {
        partnerTokenId = parObj.getToken();
    } else {
        partnerTokenId = parObj.getTokenForLive();
    }

%>
<div id="wrapper">

    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Mock Payment</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Api console
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">

        <div>
            <div class="row">
                <%if (env.equalsIgnoreCase("Test")) {%>
                <form action='https://developer.tm.com.my/SGTMPG/SandBox' method="POST" id="apiConsolePay" class="form-horizontal">
                    <%} else {%>
                    <form action='https://developer.tm.com.my/SGTMPG/Production' method="POST" id="apiConsolePay" class="form-horizontal">
                        <%}%> 
                        <div class="col-lg-12">
                            <div class="hpanel">
                                <div class="panel-heading">
                                    <div class="panel-tools">
                                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                    </div>
                                    Api console for payment gateway
                                </div>
                                <div class="panel-body">
                                    <!-- Jack Remark: Those field cannot edit mean profile information not allowed to change by developer -->
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Partner Id</label>
                                        <div class="col-sm-3"><input type="text" id="PartnerId" name="PartnerId" value="<%=partnerId%>" class="form-control" placeholder="eg: TMPG_TEST2"></div>
                                        <span class="help-block small">Form field name should be "PartnerId"</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Partner Token Id</label>
                                        <div class="col-sm-3"><input type="text" id="PartnerTokenId" name="PartnerTokenId" value="<%=partnerTokenId%>" class="form-control" placeholder="eg: TMPG_TEST2"></div>
                                        <span class="help-block small">Form field name should be "PartnerTokenId"</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Merchant Id</label>
                                        <div class="col-sm-3"><input type="text" id="MERCHANTID" name="MERCHANTID" class="form-control" placeholder="eg: TMPG_TEST2"></div>
                                        <span class="help-block small">Form field name should be "MERCHANTID"</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Merchant Password</label>
                                        <div class="col-sm-3"><input type="password" id="PSWD" name="PSWD" class="form-control" placeholder="*****"></div>
                                        <span class="help-block small">Form field name should be "PSWD"</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Merchant Transaction Id</label>
                                        <div class="col-sm-3"><input type="text" id="MERCHANT_TRANID" name="MERCHANT_TRANID" class="form-control" placeholder="eg: 531380883225680"></div>
                                        <span class="help-block small">Form field name should be "MERCHANT_TRANID"</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Currency code</label>
                                        <div class="col-sm-3"><input type="text" id="CURRENCYCODE" name="CURRENCYCODE" class="form-control" placeholder="eg: MYR"></div>
                                        <span class="help-block small">Form field name should be "CURRENCYCODE"</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Amount</label>
                                        <div class="col-sm-3"><input type="text" id="AMOUNT" name="AMOUNT" class="form-control" placeholder="eg: 300.00"></div>
                                        <span class="help-block small">Form field name should be "AMOUNT"</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Customer Name</label>
                                        <div class="col-sm-3"><input type="text" id="CUSTNAME" name="CUSTNAME" class="form-control" placeholder="eg: abc"></div>
                                        <span class="help-block small">Form field name should be "CUSTNAME"</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Customer Email</label>
                                        <div class="col-sm-3"><input type="text" id="CUSTEMAIL" name="CUSTEMAIL" class="form-control" placeholder="eg: abc@gmail.com"></div>
                                        <span class="help-block small">Form field name should be "CUSTEMAIL"</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Return Url</label>
                                        <div class="col-sm-3"><input type="text" id="RETURN_URL" value="https://developer.tm.com.my/DeveloperPortal/response.jsp" name="RETURN_URL" class="form-control" disabled></div>
                                        <span class="help-block small">Form field name should be "RETURN_URL"</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Signature</label>
                                        <div class="col-sm-3"><textarea id="SIGNATURE" name="SIGNATURE" class="form-control"></textarea></div>
                                        <span class="help-block small">Form field name should be "SIGNATURE"</span>
                                        <button class="btn btn-xs btn-primary" type="button" id="genHash" onclick="generateHash()">Hash key</button>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Total transaction</label>
                                        <div class="col-sm-3"><textarea id="TOTAL_CHILD_TRANSACTION" name="TOTAL_CHILD_TRANSACTION" class="form-control"></textarea></div>
                                        <span class="help-block small">Form field name should be "TOTAL_CHILD_TRANSACTION"</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Child transaction</label>
                                        <div class="col-sm-3"><textarea id="CHILD_TRANSACTION" name="CHILD_TRANSACTION" class="form-control"></textarea></div>
                                        <span class="help-block small">Form field name should be "TOTAL_CHILD_TRANSACTION"</span>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <button class="btn btn-default" type="button" onClick="window.location.reload();">Clear</button>
                                            <button class="btn btn-primary" type="submit" id="apiPayment"  disabled>Pay</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
            </div>
        </div>
        <script>
            function generateHash() {
                var merchantid = document.getElementById("MERCHANTID").value.toUpperCase();
                var merchant_pass = document.getElementById("PSWD").value.toUpperCase();
                var merchant_trId = document.getElementById("MERCHANT_TRANID").value.toUpperCase();
                var amount = document.getElementById("AMOUNT").value.toUpperCase();
                var str = "##" + merchantid + "##" + merchant_pass + "##" + merchant_trId + "##" + amount + "##0##";
                var finalStr = hex_sha1(str).toUpperCase();
                console.log(str);
                document.getElementById("SIGNATURE").value = finalStr;
                var childTranscation = "<txn>"
                        + "<child_txn>"
                        + "<sub_merchant_id>" + merchantid + "</sub_merchant_id>"
                        + "<sub_order_id>11100111</sub_order_id>"
                        + "<gross_amount>" + amount + "</gross_amount>"
                        + "<gbt_amount>" + amount + "</gbt_amount>"
                        + //"<gbt_amount>"+_totalAmountWithoutTax+"</gbt_amount>" +
                        "<nett_amount>00.00</nett_amount>"
                        + "<account_no>" + merchantid + "</account_no>"
                        + "<revenue_code>" + 772 + "</revenue_code>"
                        + "<misc_amount>00.00</misc_amount>"
                        + "</child_txn>"
                        + "</txn>";
                document.getElementById("TOTAL_CHILD_TRANSACTION").value = 1;
                document.getElementById("CHILD_TRANSACTION").value = childTranscation;
                document.getElementById("apiPayment").disabled = false;
                document.getElementById("RETURN_URL").disabled = false;
            }
        </script>
        <!-- Footer-->
        <%@include file="footer.jsp"%>
