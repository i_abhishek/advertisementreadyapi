<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgUsers"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt"%>
<script src="scripts/profile.js" type="text/javascript"></script>

<%
    PartnerDetails partnerObjEdit = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
    PartnerManagement partnerDetails = new PartnerManagement();
    PartnerDetails partnerData = partnerDetails.getPartnerDetails(partnerObjEdit.getPartnerId());
    request.getSession().setAttribute("_partnerDetails", partnerData);
    SgUsers editusrObj = (SgUsers) request.getSession().getAttribute("_SgUsers");
    JSONObject usrJSONObj = null;
    String address1 = null; String address2 = null; String address3 = null; String address4 = null;
    String city = null; String state = null; String country = null; String pinCode = null;
    if (editusrObj != null) {
        String usrProfileDesc = editusrObj.getProfileDetails();
            if (usrProfileDesc != null) {
                usrJSONObj = new JSONObject(usrProfileDesc);
                if (usrJSONObj.has("address1")) {
                    address1 = (String) usrJSONObj.get("address1");
                }
                if (usrJSONObj.has("address2")) {
                    address2 = (String) usrJSONObj.get("address2");
                }
                if (usrJSONObj.has("address3")) {
                    address3 = (String) usrJSONObj.get("address3");
                }
                if (usrJSONObj.has("address4")) {
                    address4 = (String) usrJSONObj.get("address4");
                }                
                if (usrJSONObj.has("city")) {
                    city = (String) usrJSONObj.get("city");
                }
                if (usrJSONObj.has("state")) {
                    state = (String) usrJSONObj.get("state");
                }
                if (usrJSONObj.has("country")) {
                    country = (String) usrJSONObj.get("country");
                }
                if (usrJSONObj.has("pinCode")) {
                    pinCode = (String) usrJSONObj.get("pinCode");
                }
            }
        }
%>
<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                My Profile
            </h2>
            <small>View/Update your information </small>
        </div>
    </div>
</div>

<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">                                
                    <form id="personalInfo" class="form-horizontal">
                        <input type="hidden" id="partnerId" name="partnerId" value="<%=partnerData.getPartnerId()%>">
                        <h6><i class="fa fa-user fa-2x text-info">  Personal Information</i></h6><br/>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Name <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (partnerData != null && partnerData.getPartnerName() != null && !partnerData.getPartnerName().isEmpty()) {%>
                                <input type="text" id="partnerName" title="Please enter developer name" name="partnerName" value="<%=partnerData.getPartnerName()%>" class="form-control">
                                <%} else {%>
                                <input type="text" id="partnerName" title="Please enter developer name" name="partnerName" class="form-control" required>
                                <%}%>
                            </div>
                            <label class="col-sm-2 control-label">Mobile No. <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (partnerData != null && partnerData.getPartnerPhone() != null && !partnerData.getPartnerPhone().isEmpty() && !partnerData.getPartnerPhone().equals("0")) {%>
                                <input type="text" value="<%=partnerData.getPartnerPhone()%>" title="Please enter mobile number" id="mobileNo" name="mobileNo" class="form-control">
                                <%} else {%>
                                <input type="text" id="mobileNo" name="mobileNo" class="form-control" required>
                                <%}%>
                            </div>                                                                        
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (partnerData != null && partnerData.getPartnerEmailid() != null && !partnerData.getPartnerEmailid().isEmpty()) {%>
                                <input type="text" value="<%=partnerData.getPartnerEmailid()%>" title="Please enter email id" id="emailId" name="emailId" class="form-control" required disabled>
                                <%} else {%>
                                <input type="text" id="emailId" name="emailId" class="form-control" required disabled>
                                <%}%>
                            </div>
                        </div>
                        <h6><i class="fa fa-home fa-2x text-info">  Address Information</i></h6><br/>    
                        <div class="form-group">                                    
                            <label class="col-sm-2 control-label" >Address Line 1 <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (address1 != null) {%>
                                <input type="text" id="addr1" name="addr1" title="Please enter address line 1" value="<%=address1%>" class="form-control">
                                <%} else {%>
                                <input type="text" id="addr1" name="addr1" title="Please enter address line 1" class="form-control">
                                <%}%>
                            </div>

                            <label class="col-sm-2 control-label" >Address Line 2 <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (address2 != null) {%>
                                <input type="text" id="addr2" name="addr2" title="Please enter address line 2" value="<%=address2%>" class="form-control">
                                <%} else {%>
                                <input type="text" id="addr2" name="addr2" title="Please enter address line 2" class="form-control">
                                <%}%>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Address Line 3</label>
                            <div class="col-sm-3">
                                <%if (address3 != null) {%>
                                <input type="text" id="addr3" name="addr3" value="<%=address3%>" class="form-control">
                                <%} else {%>
                                <input type="text" id="addr3" name="addr3" class="form-control">
                                <%}%>
                            </div>
                            <label class="col-sm-2 control-label" >Address Line 4</label>
                            <div class="col-sm-3">
                                <%if (address4 != null) {%>
                                <input type="text" id="addr4" name="addr4"  value="<%=address4%>" class="form-control">
                                <%} else {%>
                                <input type="text" id="addr4" name="addr4" class="form-control">
                                <%}%>
                            </div>
                        </div>

                        <div class="form-group">                                                                        
                            <label class="col-sm-2 control-label">City <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (city != null) {%>
                                <input type="text" id="city" name="city" title="Please enter city" value="<%=city%>" class="form-control" >
                                <%} else {%>
                                <input type="text" id="city" name="city" title="Please enter city" class="form-control">
                                <%}%>
                            </div>
                            <label class="col-sm-2 control-label" >State <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (state != null) {%>
                                <input type="text" id="state" title="Please enter state" name="state" value="<%=state%>" class="form-control">
                                <%} else {%>
                                <input type="text" id="state" title="Please enter state" name="state" class="form-control">
                                <%}%>
                            </div>                                    
                        </div>                                
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Country <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (country != null) {%>
                                <input type="text" id="country" name="country" title="Please enter country" value="<%=country%>" class="form-control" >
                                <%} else {%>
                                <input type="text" id="country" name="country" title="Please enter country" class="form-control">
                                <%}%>
                            </div>                                    
                            <label class="col-sm-2 control-label" >Pin Code <span style="color: red">*</span></label>
                            <div class="col-sm-3">
                                <%if (pinCode != null) {%>
                                <input type="text" id="pinCode" title="Please enter Pin Code" value="<%=pinCode%>" name="pinCode" class="form-control">
                                <%} else {%>
                                <input type="text" id="pinCode" title="Please enter Pin Code" name="pinCode" class="form-control">
                                <%}%>
                            </div>
                        </div>
                        <button class="btn btn-primary ladda-button validateProfileForm" data-style="zoom-in" onclick="validateProfileForm('<%=partnerData.getPartnerId()%>')" style="display: none">Save changes</button>    
                    </form>
                                                                        
                    <h6><i class="fa fa-image fa-2x text-info"> Profile Picture</h6></i> <br/>
                    <div style="background: #fff; height: 200px">
                        <div class="col-sm-8 col-lg-8 col-md-8" style="width: 68%;margin-left: 16%">
                            <form action="UploadEmailAttachme" method="post" class="dropzone" id="my-dropzone"></form>	
                            <small>Upload Profile pic. Support Image File size limit 1mb</small>
                        </div>
                    </div>
                    <h6><i class="fa fa-code fa-2x text-info"> Analytics script</h6></i> <br/>
                    <div style="background: #fff; height: 200px">
                        <form id="analyticScriptForm">
                            <div class="col-sm-8 col-lg-8 col-md-8" style="width: 68%;margin-left: 16%">
                                <%if(partnerData.getAnalyticScript() == null){%>
                                <textarea class="form-control" id="analyticScript" name="analyticScript" rows="8"></textarea>
                                <%}else{%>
                                <textarea class="form-control" id="analyticScript" name="analyticScript"><%=partnerData.getAnalyticScript()%></textarea>
                                <script>
                                    var doc = document.getElementById('analyticScript');
                                    var dataCode = CodeMirror.fromTextArea(doc, {
                                        lineNumbers: true,
                                        matchBrackets: true,
                                        styleActiveLine: true,
                                        tabMode: "indent",
                                    });
                                    dataCode.on('change', function ash() {
                                        $('#analyticScript').val(dataCode.getValue());
                                    });
                                    $("#analyticScript").focus();
                                    $("#analyticScript").trigger("click");
                                </script>
                                <%}%>
                                <small>You can add your analytic script here, If any</small>
                            </div>
                        </form>
                    </div>       
                    <br/><br/>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8" style="margin-left: 16%">
                            <a class="btn btn-default" href="./header.jsp">Cancel</a>                            
                            <a class="btn btn-info ladda-button" id="partUpdate" data-style="zoom-in"  onclick="callValidateProfileForm('<%=partnerData.getPartnerId()%>')">Save changes</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>                             
    </div>
</div>
<script>
    $(document).ready(function () {
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("#my-dropzone", {
            url: "UploadProfilePic",
            uploadMultiple: false,
            maxFilesize: 1,
            maxFiles: 1,
            acceptedFiles: "image/*",
            dictInvalidFileType: "You can't upload files of this type, only Image file",
            autoProcessQueue: true,
            parallelUploads: 1,
            addRemoveLinks: true,
            dictDefaultMessage: "Drop profile pic here to upload",
            removedfile: function(file) {
                removeImageFromSession(file.name);    
                var _ref;
                if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                        _ref.parentNode.removeChild(file.previewElement);
                    }
                }
                return this._updateMaxFilesReachedClass();
            }
        });
    });
</script>

