<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@include file="header.jsp"%> 
<%
 String packageName                = "NA";
 String paymentMode                = "NA";
 ArrayList<String> resourceDetails = new ArrayList<String>();
if(subscriObject1 != null){
    packageName               = subscriObject1.getBucketName();
    paymentMode               = subscriObject1.getPaymentMode();
    String accesspointDetails = subscriObject1.getApRateDetails();
    JSONArray jsOld = new JSONArray(accesspointDetails);
    String keyS = "";
    for (int j = 0; j < jsOld.length(); j++) {
        JSONObject jsonexists1 = jsOld.getJSONObject(j);
        Iterator<String> keys = jsonexists1.keys();
        while (keys.hasNext()) {
            String keyData = keys.next();
            String[] keyDetails = keyData.split(":");
            resourceDetails.add(keyDetails[1]);
        }
    }
}
%>
<!-- Main Wrapper -->
<div id="wrapper">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>My Subscription</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Subscription Package
                </h2>
                <small>Know your Subscription details from here</small>
            </div>
        </div>
    </div>
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center m-b-xl">
                    <h3>Existing Package</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hpanel hred">
                            <div class="panel-body">
                                <h3><%=packageName%></h3>
                                <small><strong>Package Model:</strong> <%=paymentMode%></small>
                                <p>The package come with following APIs:</p>
                                <div class="text-muted font-bold m-b-xs">
                                    <ul>
                                        <% 
                                            if(resourceDetails.size()!= 0){
                                                for(int i=0; i <resourceDetails.size();i++){                                                    
                                        %>
                                        <li><%=resourceDetails.get(i)%></li>
                                        <%
                                                }
                                            }
                                        %>
                                    </ul>
                                </div>
                                <a href="my_subscription_package_detail.jsp" class="btn btn-danger btn-sm">View Package</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="m-b-xl"/>
            <div class="col-lg-12">
                <div class="text-center m-b-xl">
                    <h3>Available Package</h3>
                </div>
                <div class="row">
                <%    
                    SgReqbucketdetails[] packageObj = null;
                    int count = 0;                
                    packageObj = new RequestPackageManagement().getPackageRequestsbyStatus(SessionId, GlobalStatus.APPROVED);
                    if (packageObj != null) {
                    for (int i = 0; i < packageObj.length; i++) {
                        if (("," + packageObj[i].getPartnerVisibility()).contains(",all,") || ("," + packageObj[i].getPartnerVisibility()).contains("," + parObj.getPartnerId() + ",")) {
                            count++;
                            ArrayList<String> avaResourceDetails = new ArrayList<String>();
                            String accesspointDetails = packageObj[i].getApRateDetails();
                            JSONArray jsOld = new JSONArray(accesspointDetails);
                            String keyS = "";
                            if(!avaResourceDetails.isEmpty()){
                                avaResourceDetails.clear();
                            }
                            for (int j = 0; j < jsOld.length(); j++) {
                                JSONObject jsonexists1 = jsOld.getJSONObject(j);
                                Iterator<String> keys = jsonexists1.keys();
                                while (keys.hasNext()) {
                                    String keyData = keys.next();
                                    String[] keyDetails = keyData.split(":");
                                    avaResourceDetails.add(keyDetails[1]);
                                }
                            }
                            if((count%4 != 0 && count < 4)||(count==11 || count%4 != 0)&& count < 15){
                        %>
                
                    <div class="col-lg-4">
                        <div class="hpanel hblue">
                            <div class="panel-body">
                                <h3><%=packageObj[i].getBucketName()%></h3>
                                <small><strong>Package Model:</strong> <%=packageObj[i].getPaymentMode()%></small>
                                <p>The package come with following APIs:</p>
                                <div class="text-muted font-bold m-b-xs">
                                    <ul>
                                        <% 
                                            if(avaResourceDetails.size()!= 0){
                                                for(int av=0; av <avaResourceDetails.size();av++){                                                    
                                        %>
                                        <li><%=avaResourceDetails.get(av)%></li>
                                        <%
                                                }
                                            }
                                        %>                                        
                                    </ul>
                                </div>
                                <a href="my_subscription_package_detail_live.jsp" class="btn btn-info btn-sm">View Package</a>
                                <a href="#" class="btn btn-primary btn-sm">Subscribe</a>
                            </div>
                        </div>
                    </div>
                    <%                                
                        }else if((count%4 != 0 || count == 4 )&& count < 7){
                    %>
                    <div class="col-lg-4">
                        <div class="hpanel hyellow">
                            <div class="panel-body">
                                <h3><%=packageObj[i].getBucketName()%></h3>
                                <small><strong>Package Model:</strong> <%=packageObj[i].getPaymentMode()%></small>
                                <p>The package come with following APIs:</p>
                                <div class="text-muted font-bold m-b-xs">
                                    <ul>
                                        <% 
                                            if(avaResourceDetails.size()!= 0){
                                                for(int av=0; av <avaResourceDetails.size();av++){                                                    
                                        %>
                                        <li><%=avaResourceDetails.get(av)%></li>
                                        <%
                                                }
                                            }
                                        %>   
                                    </ul>
                                </div>
                                <a href="my_subscription_package_detail_live.jsp" class="btn btn-warning btn-sm">View Package</a>
                                <a href="#" class="btn btn-primary btn-sm">Subscribe</a>
                            </div>
                        </div>
                    </div>
                    <%                                
                        }else if((count%4 != 0 || count == 8 )&& count < 11){
                    %>
                    <div class="col-lg-4">
                        <div class="hpanel hred">
                            <div class="panel-body">
                                <h3><%=packageObj[i].getBucketName()%></h3>
                                <small><strong>Package Model:</strong> <%=packageObj[i].getPaymentMode()%></small>
                                <p>The package come with following APIs:</p>
                                <div class="text-muted font-bold m-b-xs">
                                    <ul>
                                        <% 
                                            if(avaResourceDetails.size()!= 0){
                                                for(int av=0; av <avaResourceDetails.size();av++){                                                    
                                        %>
                                        <li><%=avaResourceDetails.get(av)%></li>
                                        <%
                                                }
                                            }
                                        %>   
                                    </ul>
                                </div>
                                <a href="my_subscription_package_detail_live.jsp" class="btn btn-danger btn-sm">View Package</a>
                                <a href="#" class="btn btn-primary btn-sm">Subscribe</a>
                            </div>
                        </div>
                    </div>                  
                    <%  
                                            }
                                        }
                                    }
                                }                    
                            
                    %>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Footer-->
    <%@include file="footer.jsp"%>