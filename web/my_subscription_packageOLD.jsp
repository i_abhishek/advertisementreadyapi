<%@include file="header.jsp"%> 

<!-- Main Wrapper -->
<div id="wrapper">
	<div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>My Subscription</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Subscription Package
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>

<div class="content animate-panel">

    <div class="row">
        <div class="col-lg-12">

            <div class="text-center m-b-xl">
                <h3>Existing Package</h3>
            </div>

            <div class="row">
				<div class="col-lg-12">
					<div class="hpanel hred">
						<div class="panel-body">
							<h3>Sandbox</h3>
							<small><strong>Package Model:</strong> Postpaid</small>
							<p>The package come with following APIs:</p>
							<div class="text-muted font-bold m-b-xs">
								<ul>
									<li>SMS</li>
									<li>Map</li>
									<li>MFA</li>
									<li>Digital Signing</li>
								</ul>
							</div>
							<a href="my_subscription_package_detail.jsp" class="btn btn-danger btn-sm">View Package</a>
						</div>

					</div>
				</div>
			</div>

        </div>
		
		<hr class="m-b-xl"/>
		
		<div class="col-lg-12">

            <div class="text-center m-b-xl">
                <h3>Available Package</h3>
            </div>

            <div class="row">
				<div class="col-lg-4">
					<div class="hpanel hblue">
						<div class="panel-body">
							<h3>Production</h3>
							<small><strong>Package Model:</strong> Postpaid</small>
							<p>The package come with following APIs:</p>
							<div class="text-muted font-bold m-b-xs">
								<ul>
									<li>SMS</li>
									<li>Map</li>
								</ul>
							</div>
							<a href="my_subscription_package_detail_live.jsp" class="btn btn-info btn-sm">View Package</a>
							<a href="#" class="btn btn-primary btn-sm">Subscribe</a>
						</div>

					</div>
				</div>
				
				<div class="col-lg-4">
					<div class="hpanel hyellow">
						<div class="panel-body">
							<h3>Customize Package</h3>
							<small><strong>Package Model:</strong> Prepaid</small>
							<p>The package come with following APIs:</p>
							<div class="text-muted font-bold m-b-xs">
								<ul>
									<li>MFA</li>
									<li>Digital Signing</li>
								</ul>
							</div>
							<a href="my_subscription_package_detail_live.jsp" class="btn btn-warning btn-sm">View Package</a>
							<a href="#" class="btn btn-primary btn-sm">Subscribe</a>
						</div>

					</div>
				</div>
			</div>

        </div>
		
		<hr class="m-b-xl"/>
		
		<div class="col-lg-12">

            <div class="text-center m-b-xl">
                <h3>Ala Carte</h3>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <div class="hpanel plan-box">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">SMS</h4>
                        </div>
                        <div class="panel-body text-center">
                            <i class="pe pe-7s-phone big-icon text-warning"></i>
                            <h4 class="font-bold">
                                RM 0.12/sms
                            </h4>

                            <p class="text-muted">
                                Send and receive SMS to mobile numbers with virtual TM number (xxxxxxx). Able to support two (2) way communication.
                            </p>
                            <a href="#" class="btn btn-warning btn-sm">Subscribe</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="hpanel plan-box">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Map</h4>
                        </div>
                        <div class="panel-body text-center">
                            <i class="pe pe-7s-map-marker big-icon text-success"></i>
                            <h4 class="font-bold">
                                RM 0.02/page/view
                            </h4>

                            <p class="text-muted">
                                A map-based API provided through Internet service that allow customer to embed TM map into their web based or client application.
                            </p>
                            <a href="#" class="btn btn-success btn-sm">Subscribe</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="hpanel plan-box">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">MFA</h4>
                        </div>
                        <div class="panel-body text-center">
                            <i class="pe pe-7s-key big-icon text-info"></i>
                            <h4 class="font-bold">
                                RM 150/mth/unlimited
                            </h4>

                            <p class="text-muted">
                                A method of computer access control which a user can pass by successfully presenting several separate authentication stages. Support for user ID, password, One Time Password (OTP), geographic location and others.
                            </p>
                            <a href="#" class="btn btn-info btn-sm">Subscribe</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="hpanel plan-box">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Digital Signing</h4>
                        </div>
                        <div class="panel-body text-center">
                            <i class="pe pe-7s-note big-icon text-danger"></i>
                            <h4 class="font-bold">
                                RM 150/mth/unlimited
                            </h4>

                            <p class="text-muted">
                                Increase trust by utilizing digital signing for communication need. Able to digitally sign documents, so recipients can confirm the validity of the received documents without any tampering.
                            </p>
                            <a href="#" class="btn btn-danger btn-sm">Subscribe</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
		
    </div>


</div>

    <!-- Footer-->
    <%@include file="footer.jsp"%>