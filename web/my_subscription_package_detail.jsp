<%@page import="java.math.BigDecimal"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="java.util.Iterator"%>
<%--<%@include file="header.jsp"%>--%>
<script src="scripts/packageDetails.js" type="text/javascript"></script>
<%    
    String packageName = "NA";
    String gstRate = "0";
    String vatRate = "0";
    String stRate = "0";
    PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
    SgSubscriptionDetails subscriObject1 = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(parObj.getPartnerId());
    if (subscriObject1 != null) {
        packageName = subscriObject1.getBucketName();
        String taxRate = subscriObject1.getTax();
        if (taxRate != null) {
            JSONObject taxJson = new JSONObject(taxRate);
            gstRate = taxJson.getString("gstTax");
            vatRate = taxJson.getString("vatTax");
            stRate = taxJson.getString("stTax");
        }
    }
%>
<!-- Main Wrapper -->
<!--<div id="wrapper">-->
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
<!--                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>My Subscription</span>
                        </li>
                    </ol>
                </div>-->
                <h4 class="font-light m-b-xs">
                    <b> My Subscription - <%=packageName%></b>
                </h4>
                <small>Your subscribed package details</small>
            </div>
        </div>
    </div>
    <div class="content animate-panel">
        <%if(subscriObject1 != null){%>
        <div class="row">
        <div class="col-lg-12">
            <div class="hpanel stats">
                <div class="panel-body">
                    <div>
                        <i class="pe-7s-cash fa-4x"></i>
                        <span class="m-xs text-success" style="font-size: 40px;">AUD <%=subscriObject1.getPlanAmount()%></span>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div>
            <div class="row">
                <div class="col-lg-12 form-horizontal">
                    <div class="hpanel">
                        <div class="panel-heading">
                            
                            Rate
                        </div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <%if (!subscriObject1.getPaymentMode().equalsIgnoreCase("postpaid")) {%>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Minimum Balance</label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<%=subscriObject1.getMinimumBalance()%>" disabled></div>
                                </div>
                                <%}%>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Credits</label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<%=new BigDecimal(subscriObject1.getMainCredits())%>" disabled></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Free Credits</label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<%=new BigDecimal(subscriObject1.getFreeCredits())%>" disabled></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Free Period</label>
                                    <div class="col-sm-8">
                                        <% if (subscriObject1.getDaysForFreeTrial() != null && !subscriObject1.getDaysForFreeTrial().equals("")) {
                                                if (subscriObject1.getDaysForFreeTrial().equals("7")) {%>
                                        <input type="text" class="form-control" value="7 Days" disabled/>
                                        <%} else if (subscriObject1.getDaysForFreeTrial().equals("14")) {%>
                                        <input type="text" class="form-control" value="14 Days" disabled/>
                                        <%} else if (subscriObject1.getDaysForFreeTrial().equals("1")) {%>
                                        <input type="text" class="form-control" value="1 Month" disabled/>
                                        <%} else {%>
                                        <input type="text" class="form-control" value="NA" disabled/>  
                                        <%}
                                        } else {%>                                                
                                        <input type="text" class="form-control" value="NA" disabled>
                                        <%}%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">GST Tax Rate (%)</label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value=<%=gstRate%> % disabled /></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">1st Setup Service Charge</label>
                                    <div class="col-sm-8">
                                        <%if (Float.parseFloat(subscriObject1.getServiceCharge()) > 0) {%>
                                        <input type="text" class="form-control" value="<%=subscriObject1.getServiceCharge()%>" disabled>
                                        <%} else {%>
                                        <input type="text" class="form-control" value="NA" disabled>
                                        <%}%>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Change Package Charge</label>
                                    <div class="col-sm-8">
                                        <%if (subscriObject1.getChangePackageRate() > 0) {%>
                                        <input type="text" class="form-control" value="<%=subscriObject1.getChangePackageRate()%>" disabled>
                                        <%} else {%>
                                        <input type="text" class="form-control" value="NA" disabled>
                                        <%}%>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Reactivation Charge</label>
                                    <div class="col-sm-8">
                                        <%if (subscriObject1.getReActivationCharge() > 0) {%>
                                        <input type="text" class="form-control" value="<%=subscriObject1.getReActivationCharge()%>" disabled>
                                        <%} else {%>
                                        <input type="text" class="form-control" value="NA" disabled>
                                        <%}%>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Cancellation Charge</label>
                                    <div class="col-sm-8">
                                        <%if (subscriObject1.getCancellationRate() > 0) {%>
                                        <input type="text" class="form-control" value="<%=subscriObject1.getCancellationRate()%>" disabled>
                                        <%} else {%>
                                        <input type="text" class="form-control" value="NA" disabled>
                                        <%}%>     
                                    </div>
                                </div>
                                <div class="form-group" style="display: none">
                                    <label class="col-sm-4 control-label">Late Penalty Charge</label>
                                    <div class="col-sm-8"><input type="text" class="form-control" disabled></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%if (subscriObject1.getLatePenaltyRate() != null && !subscriObject1.getLatePenaltyRate().equals("")) {
                        String latePenalites = subscriObject1.getLatePenaltyRate();
                        String startD1 = "0";
                        String endD1 = "0";
                        String penalities1 = "0";
                        String startD2 = "0";
                        String endD2 = "0";
                        String penalities2 = "0";
                        if (latePenalites != null) {
                            JSONObject latePenalitesJson = new JSONObject(latePenalites);
                            startD1 = latePenalitesJson.getString("latePenaltyChargeStartDay1");
                            endD1 = latePenalitesJson.getString("latePenaltyChargeEndDay1");
                            penalities1 = latePenalitesJson.getString("latePenaltyChargeRate1");
                            startD2 = latePenalitesJson.getString("latePenaltyChargeStartDay2");
                            endD2 = latePenalitesJson.getString("latePenaltyChargeEndDay2");
                            penalities2 = latePenalitesJson.getString("latePenaltyChargeRate2");
                        }
                        if (Float.parseFloat(startD1) > 0 && Float.parseFloat(endD1) > 0 && Float.parseFloat(penalities1) > 0
                                && Float.parseFloat(penalities2) > 0 && Float.parseFloat(endD2) > 0 && Float.parseFloat(startD2) > 0) {
                %>                    
                <div class="col-lg-12 form-horizontal">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            Late Penalty Charge
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">From day</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" value="<%=startD1%>" disabled></div>
                                    <label class="col-sm-2 control-label">To day</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" value="<%=endD1%>"  disabled></div>
                                    <label class="col-sm-2 control-label">Penalty (RM)</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" value="<%=penalities1%>" disabled></div>
                                </div>
                            </div>
                            <br><br><br>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">From day</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" value="<%=startD2%>" disabled></div>
                                    <label class="col-sm-2 control-label">To day</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" value="<%=endD2%>"  disabled></div>
                                    <label class="col-sm-2 control-label">Penalty (RM)</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" value="<%=penalities2%>" disabled></div>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
                <%}
                    }
                %> 
                <div class="col-md-12" style="margin-bottom: 7%"></div>
            </div>
        </div>
        <%}else{%>
        <div class="row">
            <div class="col-md-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <i class="pe-7s-wallet text-success big-icon"></i>
                        <h1></h1>
                        <strong>You have not subscribed yet</strong>
                        <p>
                            Sorry, please subscribe package first.
                        </p>
                        <a href="home.jsp" class="btn btn-xs btn-success">Go back to home page</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="margin-bottom: 10%"></div>
        </div>    
        <%}%>
    </div>
<!--</div>-->


