<%@page import="com.mollatech.serviceguard.nucleus.db.SgLoanDetails"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.LoanManagement"%>

<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPaymentdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%--<%@include file="header.jsp"%>--%>
<script src="scripts/packageDetails.js" type="text/javascript"></script>
<script src="scripts/packageOperation.js" type="text/javascript"></script>
<%    String packageName = request.getParameter("_name");
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
    String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
    SgReqbucketdetails packageObj = new RequestPackageManagement().getReqPackageByName(SessionId, ChannelId, packageName);
    SgSubscriptionDetails subscriObj = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(partnerObj.getPartnerId());
    //String packageName                = "NA";
    boolean prePaidLoanflag = false;
    boolean postpaidPaymentFlag = false;
    if (subscriObj != null) {
        if (subscriObj.getPaymentMode().equalsIgnoreCase("postpaid")) {
            SgPaymentdetails paymentObj = new PaymentManagement().getPaymentDetailsbyPartnerAndSubscriptionID(subscriObj.getBucketId(), partnerObj.getPartnerId());
            if (paymentObj == null && subscriObj.getStatus() != GlobalStatus.PAID) {
                postpaidPaymentFlag = true;
            }
        } else {
            SgLoanDetails loandetailObject = new LoanManagement().getLoanDetails(SessionId, ChannelId, partnerObj.getPartnerId(), subscriObj.getBucketId());
            if (loandetailObject != null) {
                if (loandetailObject.getStatus() == GlobalStatus.APPROVED) {
                    prePaidLoanflag = true;
                }
            }
        }
    }
    String gstRate = "0";
    String vatRate = "0";
    String stRate = "0";
    if (packageObj != null) {
        packageName = packageObj.getBucketName();
        String taxRate = packageObj.getTax();
        if (taxRate != null) {
            JSONObject taxJson = new JSONObject(taxRate);
            gstRate = taxJson.getString("gstTax");
            vatRate = taxJson.getString("vatTax");
            stRate = taxJson.getString("stTax");
        }
    }
%>
<!-- Main Wrapper -->


    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
<!--                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li><a href="ala-carte-package.jsp">Subscription Package</a></li>
                        <li class="active">
                            <span>My Subscription</span>
                        </li>
                    </ol>
                </div>-->
                <h2 class="font-light m-b-xs">
                    Package - <%=packageObj.getBucketName()%>
                </h2>
                <small>Details about other packages</small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">	
        <div class="col-lg-12">
            <div class="hpanel stats">
                <div class="panel-body">
                    <div class="col-md-10">
                        <i class="pe-7s-cash fa-4x"></i>
                        <span class="m-xs text-success" style="font-size: 40px;">$ <%=packageObj.getPlanAmount()%></span>
                    </div>
                    <div class="col-md-2 pull-right">
                        <%if (postpaidPaymentFlag) {%>
                        <a class="btn btn-primary btn-lg ladda-button"  onclick="showPackageChange('<%=packageObj.getBucketName()%>', '<%=subscriObj.getPaymentMode()%>')"><i class="fa fa-check-circle-o"></i> Subscribe</a>                        
                        <%} else if (packageObj.getPaymentMode().equalsIgnoreCase("postpaid")) {%>
                            <a  class="btn btn-primary btn-lg ladda-button" data-style="zoom-in" id="subscriptionButton0" onclick="subscribePostpaid('<%=packageName%>','0')"><i class="fa fa-check-circle"></i> Subscribe</a>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
<!--        <div>-->
            <div class="row">

                <div class="col-lg-12 form-horizontal">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            Rate
                        </div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <%if (!packageObj.getPaymentMode().equalsIgnoreCase("postpaid")) {%>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Minimum Balance</label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<%=packageObj.getMinimumBalance()%>" disabled></div>
                                </div>
                                <%}%>    
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Credits (RM)</label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<%=new BigDecimal(packageObj.getMainCredits())%>" disabled></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Free Credits (RM)</label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value="<%=new BigDecimal(packageObj.getFreeCredits())%>" disabled></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Free Period</label>
                                    <div class="col-sm-8">
                                        <% if (packageObj.getDaysForFreeTrial() != null && !packageObj.getDaysForFreeTrial().equals("")) {
                                                                                    if (packageObj.getDaysForFreeTrial().equals("7")) {%>
                                        <input type="text" class="form-control" value="7 Days" disabled/>
                                        <%} else if (packageObj.getDaysForFreeTrial().equals("14")) {%>
                                        <input type="text" class="form-control" value="14 Days" disabled/>
                                        <%} else if (packageObj.getDaysForFreeTrial().equals("1")) {%>
                                        <input type="text" class="form-control" value="1 Month" disabled/>
                                        <%} else {%>
                                        <input type="text" class="form-control" value="NA" disabled/>  
                                        <%}
                                                                            } else {%>                                                
                                        <input type="text" class="form-control" value="NA" disabled>
                                        <%}%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">GST Tax Rate (%)</label>
                                    <div class="col-sm-8"><input type="text" class="form-control" value=<%=gstRate%> % disabled></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">1st Setup Service Charge</label>
                                    <div class="col-sm-8">
                                        <%if (Float.parseFloat(packageObj.getServiceCharge()) > 0) {%>
                                        <input type="text" class="form-control" value="<%=packageObj.getServiceCharge()%>" disabled>
                                        <%} else {%>
                                        <input type="text" class="form-control" value="NA" disabled>
                                        <%}%>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Change Package Charge</label>
                                    <div class="col-sm-8">
                                        <%if (packageObj.getChangePackageRate() > 0) {%>
                                        <input type="text" class="form-control" value="<%=packageObj.getChangePackageRate()%>" disabled>
                                        <%} else {%>
                                        <input type="text" class="form-control" value="NA" disabled>
                                        <%}%>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Reactivation Charge</label>
                                    <div class="col-sm-8">
                                        <%if (packageObj.getReActivationCharge() > 0) {%>
                                        <input type="text" class="form-control" value="<%=packageObj.getReActivationCharge()%>" disabled>
                                        <%} else {%>
                                        <input type="text" class="form-control" value="NA" disabled>
                                        <%}%>    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Cancellation Charge</label>
                                    <div class="col-sm-8">
                                        <%if (packageObj.getCancellationRate() > 0) {%>
                                        <input type="text" class="form-control" value="<%=packageObj.getCancellationRate()%>" disabled>
                                        <%} else {%>
                                        <input type="text" class="form-control" value="NA" disabled>
                                        <%}%>   
                                    </div>
                                </div>
                                    <div class="form-group" style="display: none">
                                    <label class="col-sm-4 control-label">Late Penalty Charge</label>
                                    <div class="col-sm-8"><input type="text" class="form-control" disabled></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <%if (packageObj.getLatePenalitesRate()!= null && !packageObj.getLatePenalitesRate().equals("")) {
                    String latePenalites = packageObj.getLatePenalitesRate();
                    String startD1      = "0";
                    String endD1        = "0";
                    String penalities1  = "0";
                    String startD2      = "0";
                    String endD2        = "0";
                    String penalities2  = "0";
                    if (latePenalites  != null) {
                        JSONObject latePenalitesJson = new JSONObject(latePenalites);
                        startD1 = latePenalitesJson.getString("latePenaltyChargeStartDay1");
                        endD1 = latePenalitesJson.getString("latePenaltyChargeEndDay1");
                        penalities1 = latePenalitesJson.getString("latePenaltyChargeRate1");
                        startD2 = latePenalitesJson.getString("latePenaltyChargeStartDay2");
                        endD2 = latePenalitesJson.getString("latePenaltyChargeEndDay2");
                        penalities2 = latePenalitesJson.getString("latePenaltyChargeRate2");
                    }
                    if (Float.parseFloat(startD1) > 0 && Float.parseFloat(endD1) > 0 && Float.parseFloat(penalities1) > 0
                            && Float.parseFloat(penalities2) > 0 && Float.parseFloat(endD2) > 0 && Float.parseFloat(startD2) > 0) {
                %>   
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            Late Penalty Charge
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">From day</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" value="<%=startD1%>" disabled></div>
                                    <label class="col-sm-2 control-label">To day</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" value="<%=endD1%>"  disabled></div>
                                    <label class="col-sm-2 control-label">Penalty (RM)</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" value="<%=penalities1%>" disabled></div>
                                </div>
                            </div>
                                <br><br><br>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">From day</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" value="<%=startD2%>" disabled></div>
                                    <label class="col-sm-2 control-label">To day</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" value="<%=endD2%>"  disabled></div>
                                    <label class="col-sm-2 control-label">Penalty (RM)</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" value="<%=penalities2%>" disabled></div>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
                <%  
                        }
                    }
                %>                
                
<!--                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            Access Point Rate
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <select id="_Accesspoint12" name="_Accesspoint12"  class="form-control" onchange="acesspointResource(this.value, '<%=packageName%>')">
                                        <option value="select">Select Accesspoint</option>
                                        <%
                                            String apRate = packageObj.getApRateDetails();
                                            JSONArray jsOld = new JSONArray(apRate);
                                            //String keyS = "";
                                            for (int j = 0; j < jsOld.length(); j++) {
                                                JSONObject jsonexists1 = jsOld.getJSONObject(j);
                                                Iterator<String> keys = jsonexists1.keys();
                                                while (keys.hasNext()) {
                                                    String keyData = keys.next();
                                                    String[] keyDetails = keyData.split(":");                                                    
                                        %>
                                        <option value="<%=keyDetails[0]%>"><%=keyDetails[0]%></option>
                                        <%
                                                }
                                            }%>
                                    </select>
                                </div>
                                <div class="col-lg-3">                                    
                                    <select id="_ResourceForAPPricing2" name="_ResourceForAPPricing2"  class="form-control span2" onchange="showAPVersion(this.value, '<%=packageName%>')" style="width: 100%">
                                        <option value="-1">Select Resource</option>
                                    </select>                                     
                                </div> 
                                <div class="col-sm-2">
                                    <select id="_VersionForAPricing2" name="_VersionForAPricing2"  class="form-control span2" onchange="accesspointPrice('<%=packageName%>')" style="width: 100%">
                                        <option value="-1">Select Version</option>
                                    </select>
                                </div>
                                <br>
                                <div id="listOfAPI">                                            
                                </div>                           
                            </div>
                        </div>
                    </div>
                </div>-->
                
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            Flat Price
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group">                            
                                    <label class="col-sm-2 control-label">Flat price setting</label>                                  
                                    <div class=col-sm-2>                                   
                                        <%if (packageObj.getFlatPrice() != null) {%>                                
                                        <select id="flatPriceSetting" name="flatPriceSetting" class="form-control" disabled>                                    
                                            <option  value="Disable" >Disable</option>
                                            <option selected value="Enable" >Enable</option>
                                        </select>                                        
                                        <%} else {%>
                                        <select id="flatPriceSetting" name="flatPriceSetting" class="form-control" disabled> 
                                            <option selected value="Disable" >Disable</option>
                                            <option value="Enable" >Enable</option>
                                        </select>     
                                        <%}%>                                
                                    </div>                                    
                                </div>
                            </div>
                            <br><br><br>
                            <form class="form-horizontal" id="edit_flatPrice_form" name="edit_flatPrice_form" role="form">      
                            <%if(packageObj.getFlatPrice()!= null){%>                              
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Flat price details</label>                                   
                                <div class="col-lg-2" style="margin-left: 2%">
                                    <select id="_accesspointFlatPrice" name="_accesspointFlatPrice" class="form-control" onchange="flatPriceResource(this.value, '<%=packageName%>')">
                                        <option value="-1">Select Accesspoint</option>
                                        <%
                                            String flatPrice = packageObj.getFlatPrice();
                                            JSONArray jsapiThrottling= new JSONArray(flatPrice);
                                            String keyThrottling = "";
                                            for (int j = 0; j < jsapiThrottling.length(); j++) {
                                                JSONObject jsonexists1 = jsapiThrottling.getJSONObject(j);
                                                Iterator<String> keys = jsonexists1.keys();
                                                while (keys.hasNext()) {
                                                    String keyData = keys.next();
                                                    String[] keyDetails = keyData.split(":");
                                                    if (!keyThrottling.contains(keyDetails[0] + ",")) {
                                                        keyThrottling += keyDetails[0] + ",";

                                        %>
                                        <option value="<%=keyDetails[0]%>"><%=keyDetails[0]%></option>
                                        <%}
                                                }
                                            }%>
                                    </select>
                                </div>
                                <div class="col-lg-2">                                    
                                    <select id="_resourceFlatPrice" name="_resourceFlatPrice"  class="form-control span2" onchange="flatVersion(this.value, '<%=packageName%>')" style="width: 100%">
                                        <option value="-1">Select Resource</option>
                                    </select>                                    
                                </div>
                                <div class="col-lg-2">
                                    <select id="_versionFlatPrice" name="_versionFlatPrice"  class="form-control span2" onchange="flatPriceDetails('<%=packageName%>')" style="width: 100%">
                                        <option value="-1">Select Version</option>
                                    </select>
                                </div>
                                 </div> 
                                <%} %>                                 
                            
                            <div id="listflatPriceDetails">                                            
                                </div> 
                            </form>    
                        </div>
                    </div>
                </div>
                                                                
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            API Throttling
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group">                            
                                    <label class="col-sm-2 control-label">Throttling setting</label>                                  
                                    <div class=col-sm-2>                                   
                                        <%if (packageObj.getApiThrottling() != null) {%>                                
                                        <select id="flatPriceSetting" name="flatPriceSetting" class="form-control" disabled>                                    
                                            <option  value="Disable" >Disable</option>
                                            <option selected value="Enable" >Enable</option>
                                        </select>                                        
                                        <%} else {%>
                                        <select id="flatPriceSetting" name="flatPriceSetting" class="form-control" disabled> 
                                            <option selected value="Disable" >Disable</option>
                                            <option value="Enable" >Enable</option>
                                        </select>     
                                        <%}%>                                
                                    </div>                                    
                                </div>
                            </div>
                            <br><br><br>
                             <form class="form-horizontal" id="edit_ap_form" name="edit_ap_form" role="form">      
                            <%if(packageObj.getApiThrottling()!= null){%>
                            <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >API throttling</label>                                   
                                <div class="col-lg-2" >
                                    <select id="_throttlingAccesspoint" name="_throttlingAccesspoint" class="form-control" onchange="throttlingResource(this.value, '<%=packageName%>')">
                                        <option value="select">Select Accesspoint</option>
                                        <%
                                            String apithrottling = packageObj.getApiThrottling();
                                            JSONArray jsapiThrottling= new JSONArray(apithrottling);
                                            String keyThrottling = "";
                                            for (int j = 0; j < jsapiThrottling.length(); j++) {
                                                JSONObject jsonexists1 = jsapiThrottling.getJSONObject(j);
                                                Iterator<String> keys = jsonexists1.keys();
                                                while (keys.hasNext()) {
                                                    String keyData = keys.next();
                                                    String[] keyDetails = keyData.split(":");
                                                    if (!keyThrottling.contains(keyDetails[0] + ",")) {
                                                        keyThrottling += keyDetails[0] + ",";

                                        %>
                                        <option value="<%=keyDetails[0]%>"><%=keyDetails[0]%></option>
                                        <%}
                                                }
                                            }%>
                                    </select>
                                </div>
                                <div class="col-lg-3">                                    
                                    <select id="_resourceAPIThrottling" name="_resourceAPIThrottling"  class="form-control span2" onchange="throttlingVersion(this.value, '<%=packageName%>')" style="width: 100%">
                                        <option value="-1">Select Resource</option>
                                    </select>                                    
                                </div>
                                <div class="col-lg-3">
                                    <select id="_versionofAPIThrottling" name="_versionofAPIThrottling"  class="form-control span2" onchange="throttlingAPIDetails('<%=packageName%>')" style="width: 100%">
                                        <option value="-1">Select Version</option>
                                    </select>
                                </div>                                
                            </div> 
                        </div>
                        <div class="col-sm-12">                
                            <div id="listOfthrottlingAPI">                                            
                            </div>
                        </div>    
                        <%}%> 
                             </form>
                        </div>
                    </div>                    
                </div>

                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            Slab Pricing
                        </div>
                        <div class="panel-body">
                            <div class="col-lg-12">    
                                <% if (packageObj.getSlabApRateDetails() != null && !packageObj.getSlabApRateDetails().equals("")) {%>
                                <form class="form-horizontal" id="edit_sp_form" name="edit_sp_form" role="form">
                                <div class="form-group">                            
                                    <label class="col-lg-2 control-label">Slab Pricing</label>                                  
                                    <div class=col-sm-2>
                                        <select id="SlabPricing" disabled name="SlabPricing" class="form-control " onchange="slabPriceDiv(this.value)">
                                            <option  selected value="Enable">Enable</option>
                                        </select>                               
                                    </div>                                    
                                </div>
                                </form>
                            </div>
                                <br><br><br>
                                <form class="form-horizontal" id="edit_sp_form" name="edit_sp_form" role="form">
                                <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" style="">Access Point</label>                                   
                                    <div class="col-lg-2">
                                        <select id="_Accesspoint2" name="_Accesspoint2"  class="form-control span2" onchange="acesspointChangeResource(this.value, '<%=packageName%>')" style="width: 100%">
                                            <option value="-1" selected>Select AP</option>
                                            <%
                                                String apSlabRate = packageObj.getSlabApRateDetails();
                                                JSONArray jsSlabOld = new JSONArray();
                                                if (apSlabRate != null) {
                                                    jsSlabOld = new JSONArray(apSlabRate);
                                                }

                                                for (int k = 0; k < jsSlabOld.length(); k++) {
                                                    JSONObject jsonexistsObj = jsSlabOld.getJSONObject(k);
                                                    Iterator<String> keys = jsonexistsObj.keys();
                                                    while (keys.hasNext()) {
                                                        String keyData = keys.next();
                                                        String[] keyDetails = keyData.split(":");
                                            %>
                                            <option value="<%=keyDetails[0]%>"><%=keyDetails[0]%></option>
                                            <%}
                                                }%>
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_ResourceForSlabPricing2" name="_ResourceForSlabPricing2"  class="form-control span2" onchange="showSlabVersion(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select Resource</option>                                                                                  
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_VersionForSlabPricing2" name="_VersionForSlabPricing2"  class="form-control span2" onchange="showSlabAPI(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select Version</option>                                                                                  
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_APIForSlabPricing2" name="_APIForSlabPricing2"  class="form-control span2" onchange="showSlabPricing(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select API</option>                                                                                  
                                        </select>
                                    </div>                                        
                                </div>
                            </div>  
                            </form>            
                                <%} else {%>
                                <div class="form-group">                            
                                    <label class="col-lg-2 control-label"> Slab Pricing</label>                                  
                                    <div class=col-sm-2>                                  
                                        <select id="SlabPricing" disabled name="SlabPricing"  class="form-control" onchange="slabPriceDiv(this.value)">
                                            <option selected value="Disable">Disable</option>
                                        </select>
                                    </div>                                    
                                </div>
                                <%}%>                                                                                    
                                <div id="_slabPriceWindow">                                            
                                </div>                                                            
                        </div>
                    </div>
                </div>
                                
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            Tier Pricing
                        </div>
                        <div class="panel-body">
                                
                                <% if (packageObj.getTierRateDetails() != null && !packageObj.getTierRateDetails().equals("")) {%>
                                <form class="form-horizontal" id="edit_sp_form" name="edit_sp_form" role="form">
                                <div class="form-group">                            
                                    <label class="col-lg-2 control-label">Tier Pricing</label>                                  
                                    <div class=col-sm-2>
                                        <select id="SlabPricing" disabled name="SlabPricing" class="form-control " onchange="slabPriceDiv(this.value)">
                                            <option  selected value="Enable">Enable</option>
                                        </select>                               
                                    </div>                                    
                                </div>
                                </form>
                            </div>
                                <br><br><br>
                                <form class="form-horizontal" id="edit_sp_form" name="edit_sp_form" role="form">
<!--                                <div class="col-lg-12">-->
                                <div class="form-group">
                                    <input type="hidden" name="packageName" id="packageName" value="<%=packageName%>" />
                                    <label class="col-lg-2 control-label" style="">Access Point</label>                                   
                                    <div class="col-lg-2">
                                        <select id="_Accesspoint3" name="_Accesspoint3"  class="form-control span2" onchange="acessChangeResForTiering(this.value, '<%=packageName%>')" style="width: 100%">
                                            <option value="-1" selected>Select AP</option>
                                            <%
                                                String apTierRate = packageObj.getTierRateDetails();
                                                JSONArray jsSlabOld = new JSONArray();
                                                if (apTierRate != null) {
                                                    jsSlabOld = new JSONArray(apTierRate);
                                                }

                                                for (int k = 0; k < jsSlabOld.length(); k++) {
                                                    JSONObject jsonexistsObj = jsSlabOld.getJSONObject(k);
                                                    Iterator<String> keys = jsonexistsObj.keys();
                                                    while (keys.hasNext()) {
                                                        String keyData = keys.next();
                                                        String[] keyDetails = keyData.split(":");
                                            %>
                                            <option value="<%=keyDetails[0]%>"><%=keyDetails[0]%></option>
                                            <%}
                                                }%>
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_ResourceForTieringPricing2" name="_ResourceForTieringPricing2"  class="form-control span2" onchange="showTieringVersion(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select Resource</option>                                                                                  
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_VersionForTieringPricing2" name="_VersionForTieringPricing2"  class="form-control span2" onchange="showTieringAPI(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select Version</option>                                                                                  
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_APIForTieringPricing2" name="_APIForTieringPricing2"  class="form-control span2" onchange="showTieringPricing(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select API</option>                                                                                  
                                        </select>
                                    </div>                                        
                                </div>
<!--                            </div>  -->
                            </form>            
                                <%} else {%>
                                <div class="form-group">                            
                                    <label class="col-lg-2 control-label"> Tier Pricing</label>                                  
                                    <div class=col-sm-2>                                  
                                        <select id="SlabPricing" disabled name="SlabPricing"  class="form-control" onchange="slabPriceDiv(this.value)">
                                            <option selected value="Disable">Disable</option>
                                        </select>
                                    </div>                                    
                                </div>
                                <%}%>                                                                                    
                                <div id="_tieringPriceWindow">                                            
                                </div>   
                        </div>
                    </div>
                </div>
            </div>
<!--        </div>-->
<!--    </div>-->

    <!-- Footer-->
    <%@include file="footer.jsp"%>