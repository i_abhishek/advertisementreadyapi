<%@page import="java.text.DecimalFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<script src="scripts/packageDetails.js" type="text/javascript"></script>
<script src="scripts/packageOperation.js" type="text/javascript"></script>
<script>
    (function($) {
    $.fn.Sswitch = function(element, options ) {
        this.$element = $(this);
        
        this.options = $.extend({}, $.fn.Sswitch.defaults, {
            state: this.$element.is(":checked"),
            disabled: this.$element.is(":disabled"),
            readonly: this.$element.is("[readonly]"),
            parentClass: this.$element.data("parent"),
            onSwitchChange: element.onSwitchChange
        },options);

        this.$container = $("<div>", {
          "class": (function(_this){
                return function(){
                    var classes;
                    classes = [_this.options.parentClass];
                    classes.push(_this.options.state ? "" + _this.options.parentClass + "-on" : "" + _this.options.parentClass + "-off");
                    if (_this.options.disabled) {
                        classes.push("" + _this.options.parentClass + "-disabled");
                    }
                    if (_this.options.readonly) {
                        classes.push("" + _this.options.parentClass + "-readonly");
                    }
                    if (_this.$element.attr("id")) {
                        classes.push("" + _this.options.parentClass + "-id-" + (_this.$element.attr("id")));
                    }
                    return classes.join(" ");
                };
            })(this)()
        });
        this.$label = $("<span>", {
          html: this.options.labelText,
          "class": "" + this.options.parentClass + "-label"
        });
        this.$container = this.$element.wrap(this.$container).parent();
        this.$element.before(this.$label);
        
        return this.$container.on("click", (function(_this) {
            return function(event) {
                event.preventDefault();
                event.stopPropagation();
                if (_this.options.readonly || _this.options.disabled) {
                  return _this.target;
                }
                _this.options.state = !_this.options.state;
                _this.$element.prop("checked", _this.options.state);
                _this.$container.addClass(_this.options.state ? "" + _this.options.parentClass + "-on" : "" + _this.options.parentClass + "-off").removeClass(_this.options.state ? "" + _this.options.parentClass + "-off" : "" + _this.options.parentClass + "-on");
                _this.options.onSwitchChange(_this);
                return _this;
            };
        })(this));
        return this.$element;
    },
    $.fn.Sswitch.defaults = {
        text     : 'Default Title',
        fontsize : 10,
        state: true,
        disabled: false,
        readonly: false,
        parentClass: "s-switch",
        onSwitchChange: function() {}
    };
}(jQuery));
</script>
<link href="styles/switchUserDefined.css" rel="stylesheet" type="text/css"/>
<script src="scripts/switchJquery.js" type="text/javascript"></script>

<%
    SgReqbucketdetails[] packageObj = null;
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    packageObj = new RequestPackageManagement().getPackageRequestsbyStatus(SessionId, GlobalStatus.APPROVED);
    String basicMonth = null, basicYear = null, studentMonth = null, studentYear = null, standardMonth = null, standardYear = null, enterpriseMonth = null, enterpriseYear = null;
    float basicMAmount = 0;
    float basicYAmount = 0;
    float studentMAMount = 0;
    float studentYAmount = 0;
    float stdMAMount = 0;
    float stdYAmount = 0;
    float entMntAmount = 0;
    float entYAmount = 0;
    String strbasicMAmount = "0.00";
    String strbasicYAmount = "0.00";
    String strstudentMAMount = "0.00";
    String strstudentYAmount = "0.00";
    String strstdMAMount = "0.00";
    String strstdYAmount = "0.00";
    String strentMntAmount = "0.00";
    String strentYAmount = "0.00";
    DecimalFormat df = new DecimalFormat("#0.00");
    if (packageObj != null) {
        for (SgReqbucketdetails reqbucketdetails : packageObj) {
            String bucketName = reqbucketdetails.getBucketName().toLowerCase();
            if (bucketName.contains("basic") && bucketName.contains("month")) {
                basicMonth = reqbucketdetails.getBucketName();
                basicMAmount = reqbucketdetails.getPlanAmount();
                strbasicMAmount = df.format(basicMAmount);
            } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                basicYear = reqbucketdetails.getBucketName();
                basicYAmount = reqbucketdetails.getPlanAmount();
                strbasicYAmount = df.format(basicYAmount);
            } else if (bucketName.contains("student") && bucketName.contains("month")) {
                studentMonth = reqbucketdetails.getBucketName();
                studentMAMount = reqbucketdetails.getPlanAmount();
                strstudentMAMount = df.format(studentMAMount);
            } else if (bucketName.contains("student") && bucketName.contains("year")) {
                studentYear = reqbucketdetails.getBucketName();
                studentYAmount = reqbucketdetails.getPlanAmount();
                strstudentYAmount = df.format(studentYAmount);
            } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                standardMonth = reqbucketdetails.getBucketName();
                stdMAMount = reqbucketdetails.getPlanAmount();
                strstdMAMount = df.format(stdMAMount);
            } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                standardYear = reqbucketdetails.getBucketName();
                stdYAmount = reqbucketdetails.getPlanAmount();
                strstdYAmount = df.format(stdYAmount);
            } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                enterpriseMonth = reqbucketdetails.getBucketName();
                strentMntAmount = df.format(reqbucketdetails.getPlanAmount());
            } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                enterpriseYear = reqbucketdetails.getBucketName();
                strentYAmount = df.format(reqbucketdetails.getPlanAmount());
            }
        }
    }
%>
<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                Subscription Package
            </h2>
            <small>Subscribe package</small>
        </div>
    </div>
</div>
<div class="content animate-panel" data-effect="rotateInDownRight" data-child="element">
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="text-center m-b-xl">
                <h3>Choose a package</h3>
            </div>
            <div class="row">
<!--                <input id="TheCheckBox" type="checkbox" data-off-text="Yearly" data-on-text="Monthly" checked="false" class="BSswitch">
                <script>                                
                                $('.BSswitch').bootstrapSwitch('state', true);

                                //$('#CheckBoxValue').text($("#TheCheckBox").bootstrapSwitch('state'));
                                $('#TheCheckBox').on('switchChange.bootstrapSwitch', function () {
                                    //$("#CheckBoxValue").text($('#TheCheckBox').bootstrapSwitch('state'));

                                    if($('#TheCheckBox').bootstrapSwitch('state')){
                                        $("#monthly").show();
                                        $("#yearly").hide();
                                    }else{
                                        $("#yearly").show();
                                        $("#monthly").hide();
                                    }
                                });
                </script>-->
                
                <table> 
                    <tr>
                        <td style="padding: 10px!important; "><div id="monthlyLable"><h4>Monthly</h4></div> </td>
                        <td><input class="checkbox" name="checkboxName" type="checkbox"></td>
                        <td style="padding: 10px!important;"> <div id="yearlyLable" style="display: none"><h4>Yearly</h4></div></td>
                    </tr>                  
                </table>
                <br><br><br>
                <div id="monthly">
                <div class="col-sm-3 col-lg-3 col-md-3">
                    <div class="hpanel plan-box hyellow active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Basic</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 2x credit.
                            </p>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>
                                            Features
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (5 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (14 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   One on One API Discussion
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 95%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (3)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits (1000 Credits)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                            <h3 class="font-bold">
                                A$ <%=strbasicMAmount%>/Month
                            </h3>                           
                            <a href="#" onclick="subscribePrepaid('<%=basicMonth%>', 'prepaid')" class="btn btn-warning btn-sm m-t-xs btn-outline">Subscribe</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-lg-3 col-lg-3">
                    <div class="hpanel plan-box hblue active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Student</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 2x credit.
                            </p>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>
                                            Features
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (3 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (3 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email)
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   One on One API Discussion
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 98%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (5)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits  (1500)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                            <h3 class="font-bold">
                                A$ <%=strstudentMAMount%>/Month
                            </h3>  
                            <a href="#" class="btn btn-info btn-sm m-t-xs btn-outline disabled" onclick="subscribePrepaid('<%=studentMonth%>', 'prepaid')">Subscribe</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-lg-3 col-md-3">
                    <div class="hpanel plan-box hgreen active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Startup</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 2x credit.
                            </p>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>
                                            Features
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (1 day)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (1 day)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email & Call)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 2/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   One on One API Discussion (30mins/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 99%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (10)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits (5000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                            <h3 class="font-bold">
                                A$ <%=strstdMAMount%>/Month
                            </h3>                                                              
                            <a href="#" class="btn btn-success btn-sm m-t-xs btn-outline disabled" onclick="subscribePrepaid('<%=standardMonth%>', 'prepaid')">Subscribe</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-lg-3 col-md-3">
                    <div class="hpanel plan-box hred active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Enterprise</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 2x credit.
                            </p>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>
                                            Features
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (2 hours)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (2 hours)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email & Call) 
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                       
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 5/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   One on One API Discussion (90mins/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 99.90%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (30)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 24x7x365 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Monthly Credits (36000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                            <h3 class="font-bold">
                                A$ <%=strentMntAmount%>/Month
                            </h3>
                            <a href="#" class="btn btn-danger btn-sm m-t-xs btn-outline disabled" onclick="subscribePrepaid('<%=enterpriseMonth%>', 'prepaid')">Subscribe</a>
                        </div>
                    </div>
                </div>
                </div>
                        
                <div id="yearly" style="display: none">
                        <div class="col-sm-3">
                    <div class="hpanel plan-box hyellow active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Basic</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 3x credit.
                            </p>
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>
                                        Features
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (5 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (14 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   One on One API Discussion
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 95%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second  (3)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (50000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>                            
                            <h3 class="font-bold">
                                A$ <%=strbasicYAmount%>/Year
                            </h3>
                            <a onclick="subscribePrepaid('<%=basicYear%>', 'prepaid')" class="btn btn-warning btn-sm m-t-xs btn-outline disabled">Subscribe</a>
                        </div>
                    </div>
                </div>
                        <div class="col-sm-3">
                    <div class="hpanel plan-box hblue active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Student</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 3x credit.
                            </p>
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>
                                        Features
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (3 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (3 days)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-square-o"></i>   One on One API Discussion
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 98%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (5)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (150000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>                            
                            <h3 class="font-bold">
                                A$ <%=strstudentYAmount%>/Year
                            </h3>
                            <a onclick="subscribePrepaid('<%=studentYear%>', 'prepaid')" class="btn btn-info btn-sm m-t-xs btn-outline disabled">Subscribe</a>
                        </div>
                    </div>
                </div>
                         <div class="col-sm-3">
                    <div class="hpanel plan-box hgreen active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Startup</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 3x credit.
                            </p>
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>
                                        Features
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (1 day)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (1 day)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email & Call)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 2/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   One on One API Discussion (30mins/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 99%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (10)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (500000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>                            
                            <h3 class="font-bold">
                                A$ <%=strstdYAmount%>/Year
                            </h3>
                            <a onclick="subscribePrepaid('<%=standardYear%>', 'prepaid')" class="btn btn-success btn-sm m-t-xs btn-outline disabled">Sign Up</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="hpanel plan-box hred active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold">Enterprise</h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                For first time sign up you get 3x credit.
                            </p>
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>
                                        Features
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Response Time (2 hours)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Resolution Time (2 hours)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Support Available (Email & Call)
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i> Product Documentation
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>  Special designed API Console
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Reports
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   All In One Solution
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (Upto 5/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   One on One API Discussion (90mins/month)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Uptime 99.90%
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   API Calls per Second (30)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Support Hours 24x7x365
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-check-square-o"></i>   Non Expiry Yearly Credits (5400000)
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>                            
                            <h3 class="font-bold">
                                A$ <%=strentYAmount%>/Year
                            </h3>
                            <a onclick="subscribePrepaid('<%=enterpriseYear%>', 'prepaid')" class="btn btn-danger btn-sm m-t-xs btn-outline disabled">Subscribe</a>
                        </div>
                    </div>
                </div>
                    </div>        
            </div>
        </div>               
    </div>      
</div>
