<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title></title>

        <!--Including Bootstrap style files-->
        <link href="vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <%
            String paymentMode   = (String)session.getAttribute("payPalPackagePayment");
                  paymentMode   = new String(Base64.decode(paymentMode));
        %>
        <style>
            tr {
                line-height: 30px;
            }
            td {
                padding: 5px;
            }

            .divBorder {
                margin-top:10px;
                border: #eeeeee medium solid;
                border-radius: 10px;
                -moz-border-radius: 10px;
                -webkit-border-radius: 10px;
                padding: 15px;
            }

            .wSmall {
                width: 120px;
            }

            .wMedium {
                width: 200px;
            }

            .colCentered{
                float: none;
                margin: 0 auto;
            }

            #card-number, #cvv, #expiration-date {
                -webkit-transition: border-color 160ms;
                transition: border-color 160ms;
                height: 25px;
                width: 250px;
                -moz-appearance: none;
                border: 0 none;
                border-radius: 5px;
                box-shadow: 0 0 4px 1px #a5a5a5 inset;
                color: #DDDBD9;
                display: inline-block;
                float: left;
                font-size: 13px;
                height: 40px;
                margin-right: 2.12766%;
                padding-left: 10px;
            }
        </style>     	
    </head>  	
    <body>
        <div class="container-fluid">
            <div class="well">
                <h2 class="text-center">Payment Status</h2>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4 colCentered" >
                    <h3> <%=request.getSession().getAttribute("payPalFirstName")%>, thank you for your payment! </h3>
                    <br />
                    <h4> Address</h4>
                    <br />
                    <address>
                        <strong><%= request.getSession().getAttribute("payPalFirstName")%> </strong><br />
                        <%= request.getSession().getAttribute("payPalline1")%><br />
                        <%= (request.getSession().getAttribute("payPalline2") == null || request.getSession().getAttribute("payPalline2") == "") ? "" : request.getSession().getAttribute("payPalline2") + "<br />"%> 
                        <%= request.getSession().getAttribute("payPalcity")%>, <%= request.getSession().getAttribute("payPalstate")%>  <%= request.getSession().getAttribute("payPalpostal-code")%><br />
                        <%= request.getSession().getAttribute("payPalpostal-code")%>
                    </address>
                    <br />
                    <br />
                    <h4>Transaction Details</h4>
                    <br />
                    <table class="table">
                        <tr>
                            <th>Transaction ID:</th>
                            <td><%= request.getSession().getAttribute("payPaltransactionid")%></td>
                        </tr>
                        <tr>
                            <th>Status:</th>
                            <td><%= request.getSession().getAttribute("payPalstatus")%></td>
                        </tr>
                        <tr>
                            <th>Total Amount:</th>
                            <td>$ <%= request.getSession().getAttribute("_grossAmount")%></td>
                        </tr>
                    </table>
                    <br />
                    <h3>Return to <a href='home.jsp'>Home Page</a></h3>
                </div>
            </div>
        </div>		
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script> 		
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <br />
        <%if(request.getSession().getAttribute("payPalstatus")!= null){%>
        <script>
            function strCompare(a, b)
            {
                return (a < b ? -1 : (a > b ? 1 : 0));
            }
            var count=0;
            function subscribePackage() {
                if(count>=5){
                    return;
                }
                var paymentMode = '<%=paymentMode%>';
                var loanId      = '';
                var s = './SubscribePackage';
                if (paymentMode === 'postpaid') {
                    s = './PostpaidPayment';
                }
                if (loanId.trim() !== '' && loanId !== 'null') {
                    s = './LoanPayment';
                }        
                //pleasewait();
                $.ajax({
                    type: 'POST',
                    url: s,
                    dataType: 'json',
                    //data: $("#packagePaymentDetails").serialize(),
                    success: function (data) {
                        if (strCompare(data.result, "error") === 0) {
                            subscribePackage();
                        } else if (strCompare(data.result, "success") === 0) {
                            //addGST();
                            setTimeout(function () {
                                window.location = "./home.jsp";
                            }, 3000);
                        } else if (strCompare(data.result, "redirect") === 0) {
                            waiting.modal('hide');
                            showAlert(data.message, "danger", 6000);
                            var packageName = data.packageName;
                            setTimeout(function () {
                                window.location = "./invoiceDetail.jsp?package=" + packageName;
                            }, 4000);
                        }
                    }
                });
            }

            $(document).ready(function () {
                subscribePackage();
            });
</script>
<%}%>
    </body>
</html>
