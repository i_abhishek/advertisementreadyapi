<%--<%@page import="com.stripe.model.Plan"%>--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%--<%@page import="com.stripe.model.Subscription"%>--%>
<%@page import="java.nio.charset.StandardCharsets"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.nio.charset.Charset"%>
<%@page import="java.nio.file.Files"%>
<%@page import="java.nio.file.Paths"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>Ready APIs | Registration</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
        <link rel="stylesheet" href="vendor/ladda/dist/ladda-themeless.min.css" />
        <link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
        <script src="scripts/utilityFunction.js" type="text/javascript"></script>
        <script src="scripts/partnerRequest.js" type="text/javascript"></script>

        <!-- App styles -->
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="styles/style.css">

    </head>
    <body class="blank" style="background: rgba(43,48,131,1);
          background: -moz-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: -webkit-gradient(left top, right top, color-stop(0%, rgba(43,48,131,1)), color-stop(50%, rgba(35,129,196,1)), color-stop(100%, rgba(43,48,131,1)));
          background: -webkit-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: -o-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: -ms-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: linear-gradient(to right, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2b3083', endColorstr='#2b3083', GradientType=1 );">

        <!-- Simple splash screen-->
        <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <%
            String selectedpackageDetails = request.getParameter("pa");
            request.getSession().setAttribute("selectedpackageDetails", selectedpackageDetails);
            java.util.Date dFooter = new java.util.Date();
            SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
            String strYYYY = sdfFooter.format(dFooter);
        %>
        <div class="color-line"></div>
        <div class="back-link">
            <a href="login.jsp" class="btn btn-primary">Registered? Login here</a>
        </div>
        <div class="login-container" style="max-width: 800px !important">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center m-b-md">
                        <h3 style="color:#fff">Ready APIs Registration</h3>
                        <small></small>
                    </div>
                    <div class="hpanel">
                        <div class="panel-body col-centered" >
                            <input type="hidden" id="loginPage" name="loginPage" value="2">
                            <div style="text-align: center">
                                <a class="btn btn-info btn-xs" href="#step1" data-toggle="tab" id="step1">Step 1 - Sign Up</a>
                                <a class="btn btn-info btn-xs disabled" href="#step2" data-toggle="tab" id="step2">Step 2 - Subscribe a package</a>
                            </div>
                            <!--                            <a class="btn btn-info btn-xs disabled" href="#step3" data-toggle="tab" id="step3">Step 3 - Enjoy the Ready APIs</a>-->
                            <br/><br/>
                            <div id="signupWindow">                                
                                <label class="control-label" for="email">
                                    Sign Up With Social Networks </label><br/>  <br/>  
                                <div style="text-align: center">
                                    <a href="#" class="btn btn-danger2 btn-outline ladda-button" id="googleSignIn" data-style="zoom-in" type="button"><i class="fa fa-google"></i> <br/>Google</a>
                                    <a href="#" class="btn btn-info btn-outline ladda-button" onclick="fb_login();" data-style="zoom-in" type="button" id="facebookLadda"><i class="fa fa-facebook-official"></i> <br/>Facebook</a>
                                    <a href="#" class="btn btn-primary btn-outline ladda-button" type="button" data-style="zoom-in"   onclick="liAuth()" id="linkedInLadda"><i class="fa fa-linkedin"></i> <br/>LinkedIn</a>
                                </div>
                                <br/><br/>
                            </div>
                            <div id="paymentWindow"></div>                            
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center" style="color:#fff">
                    <strong>Powered by Service Guard And Axiom Protect 2.0<br>
                        Platform Owned By BlueBricks Technologies 2009-<%=strYYYY%> <br>(<a href="http://www.blue-bricks.com" target="_blank">www.blue-bricks.com</a>)</strong>
                </div>
            </div>
        </div>

        <!-- Vendor scripts -->
        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="vendor/sparkline/index.js"></script>
        <script src="vendor/ladda/dist/spin.min.js"></script>
        <script src="vendor/ladda/dist/ladda.min.js"></script>
        <script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
        <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
        <script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
        <script src="vendor/toastr/build/toastr.min.js"></script>
        <!-- App scripts -->
        <script src="scripts/homer.js"></script>
        <script src="https://apis.google.com/js/platform.js?onload=onLoadGmail" async defer></script>
        <script src="scripts/socialMedia.js"></script>
        <script type="text/javascript" src="//platform.linkedin.com/in.js">
                                        api_key:8137zz6ug56k1w
                                        onLoad:onLinkedInLoad
                                        authorize:true
        </script>
        <script >
            onLoadfacebook();
            initializeToastr();

        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107416064-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-107416064-2');
        </script>
    </body>
</html>