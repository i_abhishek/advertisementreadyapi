<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%--<%@include file="header.jsp"%>--%>
<!--<script src="scripts/homeV2.js" type="text/javascript"></script>-->
<script src="scripts/allReportsV2.js" type="text/javascript"></script>
<script src="scripts/billingReport.js" type="text/javascript"></script>
<script src="scripts/partnerRequest.js" type="text/javascript"></script>
<!--<script src="scripts/getReportsDiv.js" type="text/javascript"></script>-->
<!-- Main Wrapper -->
<!--<div id="wrapper">    -->
<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li style="font-size:18px !important;">Select Report <select id="apiConsoleSelectBox" name="apiConsoleSelectBox" onchange="getReportConsole(this.value)">
                            <optgroup label="Select Report">                                
                                <option value="0" selected>Credit Usage</option>
                                <option value="1">API Usage</option>
                                <option value="2">Transaction</option> 
                                <option value="3">Performance</option> 
                            </optgroup>
                        </select></li>

                </ol>
            </div>
            <h2 class="font-light m-b-xs">
                Reports
            </h2>

        </div>
    </div>
</div>

<div id="reportTypeDiv">
  
</div>  

<script>
    getReportConsole("0");
</script>

