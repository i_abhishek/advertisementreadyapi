<%@include file="header.jsp" %>
<script src="./js/partnerRequest.js"></script>
        <%        
        boolean txnFlag = false;
        try {
        %>
<div id="wrapper">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Payment Response</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Payment Response
                </h2>
                <small>Your online payment response</small>
            </div>
        </div>
    </div>
        <div class="content animate-panel">
        <div class="row">
            <div class="col-md-12">
                <div class="hpanel email-compose">
                    <div class="panel-heading hbuilt">
                        <div class="p-xs h4">
                            Transaction Status
                        </div>
                    </div>
                    <div class="panel-heading hbuilt">

        <%           
            String MERCHANTID = request.getParameter("MERCHANTID");
            String MERCHANT_TRANID = request.getParameter("MERCHANT_TRANID");
            String ERR_CODE = request.getParameter("ERR_CODE");
            String ERR_DESC = request.getParameter("ERR_DESC");
            String AMOUNT = request.getParameter("AMOUNT");
            String TXN_STATUS = request.getParameter("TXN_STATUS");
            String USR_MSG = request.getParameter("USR_MSG");
            String USR_CODE = request.getParameter("USR_CODE");
            
            if(TXN_STATUS != null && !TXN_STATUS.isEmpty()){
                if(TXN_STATUS.equals("S")){
                    txnFlag = true;
                }
            }
        %>
        <div class="p-xs">
                    <form class="form-horizontal" method="post">
                        <div class="form-group has-success">
                            <%if(txnFlag){%>
                            <div class="col-sm-4 text-success">
                                <span style="font-size: 15px;"><i class="glyphicon glyphicon-info-sign"></i> Transaction Status</span>
                            </div>
                            <%}else{%>
                            <div class="col-sm-4 text-danger">
                                <span style="font-size: 15px;"><i class="glyphicon glyphicon-info-sign"></i> Transaction Status</span>
                            </div>
                            <%}%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Merchant Id         : </label> <%=MERCHANTID%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Merchant_TRANID     : </label> <%=MERCHANT_TRANID%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Error code          : </label> <%=ERR_CODE%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Error Desc          : </label> <%=ERR_DESC%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">User_Msg            : </label> <%=USR_MSG%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">User_Code           : </label> <%=USR_CODE%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Transaction Status  : </label> <%=TXN_STATUS%>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3">Amount              : </label> <%=AMOUNT%>
                        </div> 
                        <div class="form-group">
                            <div class="col-sm-2"><a class="btn btn-sm btn-success" href="home.jsp">Go Back to Home</a></div>
                        </div>
                    </form>
                    <%
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    %>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>  