/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function apiCallReportAccesspoint(value) {    
    var s = './apiCallVersion.jsp?_accesspointId=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_VersionForAPICallReport').html(data); 
            var version = document.getElementById("_VersionForAPICallReport").value;
            apiCallReportAPI(version);
        }
    });
}

function apiCallReportAPI(versionData) {
    var ap = document.getElementById("_resource").value;    
    var s = './apiCallReportAPI.jsp?versionData=' + versionData + '&_apId=' + ap ;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForAPICallReport').html(data);                        
        }
    });
}

function generateAPICallreports() {
    var l = $('#generateButton').ladda();
    l.ladda('start');
    var month = document.getElementById('_apiCallMonth').value;
    var year = document.getElementById('_apiCallYear').value;
    var version = document.getElementById("_VersionForAPICallReport").value;
    var api = document.getElementById("_APIForAPICallReport").value;
    var accesspointPlusResource = document.getElementById("_resource").value;
    initializeToastr(); 
    if(accesspointPlusResource == "-1"){
        setTimeout(function () {
                    toastr.error('Error - ' + 'Please select resource first');
                    l.ladda('stop');   
                }, 3000);
                return;
    }
    var s = './apichartreport.jsp';
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#report_data').html(data);                        
                var linedata = UserAPIForMonthLineChart(accesspointPlusResource, version, api,  month, year);
                var arrYaxis1 = [];
                var arrXaxis1 = [];
                for (var key in linedata) {
                    var attrName1 = key;
                    var attrValue1 = linedata[key];
                    arrXaxis1.push(attrValue1.label);
                    arrYaxis1.push(attrValue1.value);
                }
                if (arrXaxis1.length === 1) {
                    var tempStore = arrXaxis1;
                    var date = new Date(arrXaxis1);
                    date.setDate(date.getDate() - 1);
                    arrXaxis1 = [];
                    arrXaxis1.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
                    arrXaxis1.push(tempStore);
                    arrYaxis1.push(0);
                }
                $("#linegraph").empty();
                var lineData = {
//            labels: ["11/02/2017", "12/02/2017", "13/02/2017", "14/02/2017", "15/02/2017", "16/02/2017", "17/02/2017", "18/02/2017", "19/02/2017", "20/02/2017", "21/02/2017", "12/02/2017"],
                    labels: arrXaxis1,
                    datasets: [
                        {
                            label: "Example dataset",
                            fillColor: "rgba(98,203,49,0.5)",
                            strokeColor: "rgba(98,203,49,0.7)",
                            pointColor: "rgba(98,203,49,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(26,179,148,1)",
//                    data: [33, 48, 40, 19, 54, 27, 54, 23, 43, 10, 22, 17]
                            data: arrYaxis1
                        }
                    ]
                };

                var lineOptions_status = {
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    bezierCurve: true,
                    bezierCurveTension: 0.4,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 1,
                    datasetFill: true,
                    responsive: true
                };


                var ctx = document.getElementById("linegraph").getContext("2d");
                var myNewChart = new Chart(ctx).Line(lineData, lineOptions_status);
                document.getElementById("report").style.display = "block";
                l.ladda('stop');          
        }
    });
}

function UserAPIForMonthLineChart(accesspointPlusResource, version, api, month, year) {
    var s = './APICallForMonth?_accesspointPlusResource=' + accesspointPlusResource + '&_version=' + version + '&_api=' + api + '&_month=' + month + '&_year=' + year;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}

function generateAPITranscationreports() {
    var l = $('#generateButton').ladda();
    l.ladda('start');
    var month = document.getElementById('_apiCallMonth').value;
    var year = document.getElementById('_apiCallYear').value;
    var version = document.getElementById("_VersionForAPICallReport").value;
    var api = document.getElementById("_APIForAPICallReport").value;
    var accesspointPlusResource = document.getElementById("_resource").value;
    initializeToastr();      
    if(accesspointPlusResource == "-1"){
        setTimeout(function () {
                    toastr.error('Error - ' + 'Please select resource first');
                    l.ladda('stop');   
                }, 3000);
                return;
    }
    var s = './apichartreport.jsp?type='+2;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#report_data').html(data);                        
                var linedata       = UserAPITranscationForMonthLineChart(accesspointPlusResource, version, api,  month, year);
                var notAllowedData = UserAPIFailedTranscationForMonthLineChart(accesspointPlusResource, version, api,  month, year);
                var arrYaxis1 = [];
                var arrXaxis1 = [];
                var arrYaxis = [];
                var arrXaxis = [];
                for (var key in linedata) {
                    var attrName1 = key;
                    var attrValue1 = linedata[key];
                    arrXaxis1.push(attrValue1.label);
                    arrYaxis1.push(attrValue1.value);
                }
                if (arrXaxis1.length === 1) {
                    var tempStore = arrXaxis1;
                    var date = new Date(arrXaxis1);
                    date.setDate(date.getDate() - 1);
                    arrXaxis1 = [];
                    arrXaxis1.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
                    arrXaxis1.push(tempStore);
                    arrYaxis1.push(0);
                }
                for (var key in notAllowedData) {
                    var attrName1 = key;
                    var attrValue1 = notAllowedData[key];
                    arrXaxis.push(attrValue1.label);
                    arrYaxis.push(attrValue1.value);
                }
                if (arrXaxis.length === 1) {
                    var tempStore = arrXaxis;
                    var date = new Date(arrXaxis);
                    date.setDate(date.getDate() - 1);
                    arrXaxis = [];
                    arrXaxis.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
                    arrXaxis.push(tempStore);
                    arrYaxis.push(0);
                }
                $("#linegraph").empty();
                var lineData = {
//            labels: ["11/02/2017", "12/02/2017", "13/02/2017", "14/02/2017", "15/02/2017", "16/02/2017", "17/02/2017", "18/02/2017", "19/02/2017", "20/02/2017", "21/02/2017", "12/02/2017"],
                    
                    labels: arrXaxis1,
                    datasets: [
                        {                            
                            label: "Transcation Allowed",
                            fillColor: "rgba(98,203,49,0.5)",
                            strokeColor: "rgba(98,203,49,0.7)",
                            pointColor: "rgba(98,203,49,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(26,179,148,1)",
                           // data: [22, 44, 67, 43, 76, 45, 12, 80, 50, 30, 45, 95]
                            data: arrYaxis1
                        },
                        {
                            labels: "Transcation Failed",
//                            fillColor: "rgba(220,220,220,0.5)",                          
//                            strokeColor: "rgba(220,220,220,1)",
//                            pointColor: "rgba(220,220,220,1)",
//                            pointStrokeColor: "#fff",
//                            pointHighlightFill: "#fff",
//                            pointHighlightStroke: "rgba(220,220,220,1)",
                            fillColor: "rgba(247, 70, 74, 0.2)",
                            strokeColor: "rgba(247, 70, 74, 1)",
                            pointColor: "rgba(247, 70, 74, 1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(247, 70, 74,0.8)",                            
                            //data: [33, 48, 40, 19, 54, 27, 54, 23, 43, 10, 22, 17]
                            //data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                            data: arrYaxis
                        }
            ]
                };

                var lineOptions_status = {
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    bezierCurve: true,
                    bezierCurveTension: 0.4,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 1,
                    datasetFill: true,
                    responsive: true 
                };


                var ctx = document.getElementById("linegraph").getContext("2d");
                var myNewChart = new Chart(ctx).Line(lineData, lineOptions_status);
                //new Chart(ctx, {type: 'line', data: lineData, options:lineOptions_status});
    
                document.getElementById("report").style.display = "block";
                l.ladda('stop');          
        }
    });
}
function UserAPITranscationForMonthLineChart(accesspointPlusResource, version, api, month, year) {
    var s = './APITranscationForMonth?_accesspointPlusResource=' + accesspointPlusResource + '&_version=' + version + '&_api=' + api + '&_month=' + month + '&_year=' + year;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}

function UserAPIFailedTranscationForMonthLineChart(accesspointPlusResource, version, api, month, year) {
    var s = './APIFailedTranscationForMonth?_accesspointPlusResource=' + accesspointPlusResource + '&_version=' + version + '&_api=' + api + '&_month=' + month + '&_year=' + year;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}