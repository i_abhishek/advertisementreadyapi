/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var splashData = '<div id="loading" ><div class="text-center"><h3>Loading...</3></div><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div>' +
        '';

function getAPIConsole(_apid) {
    var splashData = '<br><br><br><br><br><br><br><br><br><br><br><div id="loading" ><div class="text-center"><h3>Loading...</3></div><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div>' +
            '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
    var s = './apiConsoleAdvanced.jsp?_apid=' + _apid;
    var resourceName = _apid.split(":");
    $('#apiDetails').html(splashData);
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            document.getElementById("apiDetails").classList.remove("content");
            document.getElementById("apiDetails").classList.remove("animate-panel");
            $('#apiDetails').html(data);
            document.getElementById("resourceName").innerHTML = resourceName[2];
        }
    });
}

function assignAPIToken(apiName, idNo) {
    var l = $('#reAssignToken' + idNo).ladda();
    l.ladda('start');
    initializeToastr();
    var s = './AssignAPIToken?apiName=' + apiName;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#assign_token_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.error('Error - ' + data.message);
                }, 3000);
            } else if (strCompare(data.result, "success") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.success('Success - ' + data.message);
                }, 3000);
            }
        }
    });
}

function viewAPIToken(apiName, idNo) {
    var l = $('#viewToken' + idNo).ladda();
    l.ladda('start');
    initializeToastr();
    var s = './ViewAPIToken?apiName=' + apiName;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#assign_token_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.error('Error - ' + data.message);
                }, 3000);
            } else if (strCompare(data.result, "success") === 0) {
                var msgToken = "<p style='font-size:18px'>" + data.tokenDetails + "</p>";
                var type = "Token";
                var tokenType = "<h4>APITokenId: " + apiName + '<button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>';
                document.getElementById('tokenType').innerHTML = tokenType;
                document.getElementById('test1').innerHTML = msgToken;
                var inputTag = "<input type='text' id='copyTarget' style='display: none'>";
                //var copyTag = '<a href="#/" style="font-size:15px; margin-left:35%;text-decoration:none" onclick="copy('+ data.encodeTokenDetails +')"><b>Copy</b></a>';
                var copyTag = "<a href='#/' style='font-size:10px;text-decoration:none' class='btn btn-info btn-xs' onclick=copy('" + data.encodeTokenDetails + "','" + idNo + "')><b>Copy</b></a>";
                console.log("copyTag >>> " + copyTag);
                document.getElementById('copyDiv').innerHTML = copyTag;
                document.getElementById('inputTag').innerHTML = inputTag;
                $('#tmTermsAndCondition').modal();
                setTimeout(function () {
                    l.ladda('stop');
                }, 3000);
            }
        }
    });
}

function todayCredit(apiName, idNo) {
    $('#todayExpenditure').modal();
    $("#todayCreditForAPI").html(splashData);
    setTimeout(function () {
        loadCreditData(apiName, idNo);
    }, 1000);

}
function loadCreditData(apiName, idNo) {
    var l = $('#todayCredit' + idNo).ladda();
    l.ladda('start');
    //initializeToastr();

    var apiData = apiWiseTodayCreditGraph(apiName);
    var dateArr2 = [];
    var txAmount2 = [];
    var callCountArr2 = [];
    var temp;
    dateArr2.push("Data e Ora");
    callCountArr2.push('CallCount');
    txAmount2.push('TransactionAmount');
    var recordFound = "false";
    for (var key in apiData) {
        var attrName1 = key;
        var attrValue1 = apiData[key];
        temp = (attrValue1.label).split(" ");
//                                arrXaxis1.push(temp[1] + " call : " + attrValue1.value);
        dateArr2.push(temp[1]);
        txAmount2.push(attrValue1.value5);
        callCountArr2.push(attrValue1.value);
        var positiveValue = attrValue1.value5;
        if (positiveValue > 0) {
            recordFound = "true";
        }
    }
//    alert(JSON.stringify(arrXaxis1));
//    if (dateArr2.length <= 1) {
//        if (dateArr2.length === 1) {
//            var a = dateArr2[0];
//            var tim = new Date(moment(a, ['h:m a', 'H:m']));
//            dateArr2.push((tim.getHours() + 1) + ":59");
//            txAmount2.push(0);
//            callCountArr2.push(0);
//        } else {
//            dateArr2.push("00.59");
//            dateArr2.push("1:59");
//            txAmount2.push(0);
//            callCountArr2.push(0);
//        }
//    }
    console.log("arrXaxis >> " + dateArr2);
    console.log("callCountArr2 >> " + callCountArr2);
    console.log("txAmount2  >> " + txAmount2);
    if (recordFound === 'true') {
        document.getElementById("noRecordFoundData").style.display = "none";
        document.getElementById("todayCreditForAPI").style.display = "block";
        var chart = c3.generate({
            bindto: '#todayCreditForAPI',
            data: {
                x: 'Data e Ora',
                xFormat: '%H:%M',
                columns: [
                    dateArr2,
                    txAmount2,
                            //callCountArr2
                ],
                colors: {
//                TransactionAmount: '#62cb31',
//                callCountArr2: '#FF7F50'
                    dateArr2: '#62cb31',
                    //callCountArr2: '#FF7F50'
                },
                types: {
                    //CallCount: 'bar',
                    CallCount: 'bar'
                },
                groups: [
                    ['Transcation Credit']
                ]
            },
            subchart: {
                show: true
            },
            axis: {
                x: {
                    type: 'timeseries',
                    // if true, treat x value as localtime (Default)
                    // if false, convert to UTC internally                        
                    tick: {
                        format: '%H:%M'
                                //fomat:function (x) { return x.getFullYear(); }
                    }
                }
            }
        });
        chart.resize();
    } else {
        document.getElementById("noRecordFoundData").style.display = "block";
        document.getElementById("todayCreditForAPI").style.display = "none";
    }
    document.getElementById("todayCreditForAPILable").innerHTML = "Today Credit Used for " + apiName + "<button class='close btn btn-default' data-dismiss='modal' aria-hidden='true' style='margin-top: 0%; margin-right: 3%'>x</button>";
    setTimeout(function () {
        l.ladda('stop');
    }, 3000);
}

function todayPerformance(apiName, idNo) {
    $('#todayPerformance').modal();
    $("#todayPerformanceForAPI").html(splashData);
    setTimeout(function () {
        loadData(apiName, idNo);
    }, 1000);
}
function loadData(apiName, idNo) {

    var l = $('#todayPerformance' + idNo).ladda();
    l.ladda('start');
    //initializeToastr();

    var apiData = apiWiseTodayCreditGraph(apiName);
    var dateArr2 = [];
    var txAmount2 = [];
    var callCountArr2 = [];
    var temp;
    var recordFound = "false";
    dateArr2.push("Data e Ora");
    callCountArr2.push('CallCount');
    txAmount2.push('TransactionAmount');
    for (var key in apiData) {
        var attrName1 = key;
        var attrValue1 = apiData[key];
        temp = (attrValue1.label).split(" ");
//                                arrXaxis1.push(temp[1] + " call : " + attrValue1.value);
        dateArr2.push(temp[1]);
        txAmount2.push(attrValue1.value5);
        callCountArr2.push(attrValue1.value);
        var positiveValue = attrValue1.value;
        if (positiveValue > 0) {
            recordFound = "true";
        }

    }
//    alert(JSON.stringify(arrXaxis1));
//    if (dateArr2.length <= 1) {
//        if (dateArr2.length === 1) {
//            var a = dateArr2[0];
//            var tim = new Date(moment(a, ['h:m a', 'H:m']));
//            dateArr2.push((tim.getHours() + 1) + ":59");
//            txAmount2.push(0);
//            callCountArr2.push(0);
//        } else {
//            dateArr2.push("00.59");
//            dateArr2.push("1:59");
//            txAmount2.push(0);
//            callCountArr2.push(0);
//        }
//    }
    console.log("arrXaxis >> " + dateArr2);
    console.log("callCountArr2 >> " + callCountArr2);
    console.log("txAmount2  >> " + txAmount2);
    if (recordFound === 'true') {
        document.getElementById("noRecordFoundDataPerforamce").style.display = "none";
        document.getElementById("todayPerformanceForAPI").style.display = "block";
        var chart = c3.generate({
            bindto: '#todayPerformanceForAPI',
            data: {
                x: 'Data e Ora',
                xFormat: '%H:%M',
                columns: [
                    dateArr2,
                    // txAmount2,
                    callCountArr2
                ],
                colors: {
//                TransactionAmount: '#62cb31',
//                callCountArr2: '#FF7F50'
                    //dateArr2: '#62cb31',
                    callCountArr2: '#FF7F50'
                },
                types: {
                    //CallCount: 'bar',
                    CallCount: 'bar'
                },
                groups: [
                    ['Call Count']
                ]
            },
            subchart: {
                show: true
            },
            axis: {
                x: {
                    type: 'timeseries',
                    // if true, treat x value as localtime (Default)
                    // if false, convert to UTC internally                        
                    tick: {
                        format: '%H:%M'
                                //fomat:function (x) { return x.getFullYear(); }
                    }
                }
            }
        });
        chart.resize();
    } else {
        document.getElementById("noRecordFoundDataPerforamce").style.display = "block";
        document.getElementById("todayPerformanceForAPI").style.display = "none";
    }
    document.getElementById("todayPerformanceForAPILable").innerHTML = "Today Performance of " + apiName + "<button class='close btn btn-default' data-dismiss='modal' aria-hidden='true' style='margin-top: 0%; margin-right: 3%'>x</button>";
    setTimeout(function () {
        l.ladda('stop');
    }, 3000);
}

function apiWiseTodayCreditGraph(apiName) {
    var s = './APIWiseTodayCredit?apiName=' + apiName;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false,
        data: $("#assign_token_form").serialize()
    }).responseText;
    if (jsonData.length === 0) {
        jsonData = [{"label": "NA", "value": 0, "value5": 0}];
        return jsonData;
    } else {
        var myJsonObj = JSON.parse(jsonData);
        return myJsonObj;
    }
}

function monthlyCreditChart(apiName, idNo) {
    document.getElementById("apiForMothlyCredit").value = apiName;
    document.getElementById("monthlyCreditForAPILable").innerHTML = apiName;
    
    $('#monthlyCredit').modal();

    setTimeout(function () {
        generateCreditByMonth();
    }, 1000);

}

function monthlyPerformance(apiName, idNo) {
    $('#monthlyPerformance').modal();
    document.getElementById("apiForMothlyPerformance").value = apiName;
    document.getElementById("monthlyPerformanceForAPILable").innerHTML = apiName;
    setTimeout(function () {
        generatePerformanceByMonth();
    }, 1000);
}
function monthlyAPIWiseChartData(apiName) {
    var s = './APIWiseChartData?apiName=' + apiName;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false,
        data: $("#assign_token_form").serialize()
    }).responseText;
    if (jsonData.length === 0) {
        jsonData = [{"label": "NA", "value": 0, "value5": 0}];
        return jsonData;
    } else {
        var myJsonObj = JSON.parse(jsonData);
        return myJsonObj;
    }
}

function copy(text, id) {
    console.log('text >> ' + text);
    console.log('Decode ?? ' + Base64.decode(text));
    console.log("id >> " + id);
    document.getElementById("copyTarget").value = Base64.decode(text);
    document.getElementById("copyTarget").style.display = 'block';
    copyToClipboardMsg(document.getElementById("copyTarget"), "msg");
    document.getElementById("copyTarget").style.display = 'none';
    
}
function copyToClipboardMsg(elem, msgElem) {
    var succeed = copyToClipboard(elem);
    var msg;
    if (!succeed) {
        msg = "Copy not supported or blocked.  Press Ctrl+c to copy."
    } else {
        msg = "Text copied to the clipboard."
//        var copyTag = "<a href='#/' style='font-size:15px; margin-left:15%;text-decoration:none' onclick=copy('" + elem + "')><b>Copied</b></a>";
//        document.getElementById("copyDiv").innerHTML = copyTag;
        var t = document.getElementById("copyDiv");
        t.classList.add('copied');
        setTimeout(function () {
            t.classList.remove('copied');
        }, 1500);
    }
    console.log(msg);
//                if (typeof msgElem === "string") {
//                    msgElem = document.getElementById(msgElem);
//                }
//                msgElem.innerHTML = msg;
//                setTimeout(function () {
//                    msgElem.innerHTML = "";
//                }, 2000);
}

function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch (e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}

var Base64 = {_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
        var t = "";
        var n, r, i, s, o, u, a;
        var f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (isNaN(r)) {
                u = a = 64
            } else if (isNaN(i)) {
                a = 64
            }
            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        }
        return t
    }, decode: function (e) {
        var t = "";
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9+/=]/g, "");
        while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++));
            o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++));
            a = this._keyStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4;
            r = (o & 15) << 4 | u >> 2;
            i = (u & 3) << 6 | a;
            t = t + String.fromCharCode(n);
            if (u != 64) {
                t = t + String.fromCharCode(r)
            }
            if (a != 64) {
                t = t + String.fromCharCode(i)
            }
        }
        t = Base64._utf8_decode(t);
        return t
    }, _utf8_encode: function (e) {
        e = e.replace(/rn/g, "n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r)
            } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128)
            } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128)
            }
        }
        return t
    }, _utf8_decode: function (e) {
        var t = "";
        var n = 0;
        var r = c1 = c2 = 0;
        while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
                n++
            } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2
            } else {
                c2 = e.charCodeAt(n + 1);
                c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3
            }
        }
        return t
    }}


function generatePerformanceByMonth() {
    var inputData = document.getElementById("apiForMothlyPerformance").value;
    var month = document.getElementById("_apiCallMonth").value;
//    var l = $('#generatePerformanceByMonth').ladda();
    document.getElementById("noRecordFoundDataPerformance").style.display = "none";
    document.getElementById("monthlyPerformanceForAPI").style.display = "block";
    $('#monthlyPerformanceForAPI').html(splashData);

//    l.ladda('start');

//    $("#monthlyPerformanceForAPI").html("")
    var chartData = monthlyAPIWiseChartDataByMonth(month, inputData);
    var dateArr = [];
    var txAmountArr = [];
    var callCountArr = [];

    dateArr.push("Data e Ora");
    txAmountArr.push('TransactionAmount');
    callCountArr.push('Call Count');
    var recordFound = "false";
    for (var key in chartData) {
        var attrName = key;
        var attrValue = chartData[key];
        dateArr.push(attrValue.strDate);
        txAmountArr.push(attrValue.intTotal);
        callCountArr.push(attrValue.callCount);
        var positiveInt = attrValue.callCount;
        if (positiveInt > 0) {
            recordFound = "true";
        }
    }
//    if (dateArr.length === 1) {
//        var tempStore = dateArr;
//        var date = new Date(dateArr);
//        date.setDate(date.getDate() - 1);
//        dateArr = [];
//        dateArr.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
//        dateArr.push(tempStore);
//        txAmountArr.push(0);
//        callCountArr.push(0);
//    }
    console.log("");
    console.log("arrXaxis >> " + dateArr);
    console.log("callCountArr2 >> " + callCountArr);
    console.log("txAmount2  >> " + txAmountArr);

    if (recordFound === 'true') {
        document.getElementById("noRecordFoundDataPerformance").style.display = "none";
        document.getElementById("monthlyPerformanceForAPI").style.display = "block";
        $('#monthlyPerformanceForAPI').innerHTML = "";
        var chart = c3.generate({
            bindto: '#monthlyPerformanceForAPI',
            data: {
                x: 'Data e Ora',
                xFormat: '%Y-%m-%d',
                columns: [
                    dateArr,
                    //txAmountArr,
                    callCountArr
                ],
                colors: {
                    //txAmountArr: '#62cb31',
                    callCountArr: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
                },
                types: {
                    //CallCount: 'bar',
                    CallCount: 'bar'
                },
                groups: [
                    ['Call Count']
                ]
            },
            subchart: {
                show: true
            },
            axis: {
                x: {
                    type: 'timeseries',
                    // if true, treat x value as localtime (Default)
                    // if false, convert to UTC internally                        
                    tick: {
                        format: '%d-%m-%Y'
                                //fomat:function (x) { return x.getFullYear(); }
                    }
                }
            }
        });
        setTimeout(function () {
//            l.ladda('stop');
        }, 1000);
    } else {
        setTimeout(function () {
//            l.ladda('stop');
            document.getElementById("monthlyPerformanceForAPI").style.display = "none";
            document.getElementById("noRecordFoundDataPerformance").style.display = "block";
        }, 2000);
    }

//    document.getElementById("monthlyPerformanceForAPILable").innerHTML = "Monthly Performance of " + inputData + "<button class='close btn btn-default' data-dismiss='modal' aria-hidden='true' style='margin-top: 0%; margin-right: 3%'>x</button>";
    //$('#monthlyPerformance').modal();
    var objSelect = document.getElementById("_apiCallMonth");
    //Set selected
    //setSelectedValue(objSelect,dateCallAPI.getMonth());

}

function generateCreditByMonth() {
    document.getElementById("noRecordFoundDataMonthlyCredit").style.display = "NONE";
    document.getElementById("monthlyCreditForAPI").style.display = "block";
    var inputData = document.getElementById("apiForMothlyCredit").value;
    var month = document.getElementById("_apiCallMonthMonthlyCredit").value;
//    var l = $('#generateCreditByMonth').ladda();
    $("#monthlyCreditForAPI").html(splashData);
//    l.ladda('start');

    var chartData = monthlyAPIWiseChartDataByMonth(month, inputData);
    var dateArr = [];
    var txAmountArr = [];
    var callCountArr = [];

    dateArr.push("Data e Ora");
    txAmountArr.push('Transaction Credit');
    callCountArr.push('Call Count');
    var recordFound = "false";
    for (var key in chartData) {
        var attrName = key;
        var attrValue = chartData[key];
        dateArr.push(attrValue.strDate);
        txAmountArr.push(attrValue.intTotal);
        callCountArr.push(attrValue.callCount);
        var positiveInt = attrValue.intTotal;
        if (positiveInt > 0) {
            recordFound = "true";
        }
    }
    console.log("");
    console.log("arrXaxis >> " + dateArr);
    console.log("callCountArr2 >> " + callCountArr);
    console.log("txAmount2  >> " + txAmountArr);
    if (recordFound === 'true') {
        document.getElementById("noRecordFoundDataMonthlyCredit").style.display = "none";
        document.getElementById("monthlyCreditForAPI").style.display = "block";

        var chart = c3.generate({
            bindto: '#monthlyCreditForAPI',
            data: {
                x: 'Data e Ora',
                xFormat: '%Y-%m-%d',
                columns: [
                    dateArr,
                    txAmountArr,
                            //callCountArr
                ],
                colors: {
                    txAmountArr: '#62cb31',
                    //callCountArr: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
                },
                types: {
                    //CallCount: 'bar',
                    CallCount: 'bar'
                },
                groups: [
                    ['Call Count']
                ]
            },
            subchart: {
                show: true
            },
            axis: {
                x: {
                    type: 'timeseries',
                    // if true, treat x value as localtime (Default)
                    // if false, convert to UTC internally                        
                    tick: {
                        format: '%d-%m-%Y'
                                //fomat:function (x) { return x.getFullYear(); }
                    }
                }
            }
        });

    } else {
        setTimeout(function () {
//            l.ladda('stop');
            document.getElementById("monthlyCreditForAPI").style.display = "none";
            document.getElementById("noRecordFoundDataMonthlyCredit").style.display = "block";
        }, 2000);
    }

//    document.getElementById("monthlyCreditForAPILable").innerHTML = "Monthly Credit Used For " + inputData + "<button class='close btn btn-default' data-dismiss='modal' aria-hidden='true' style='margin-top: 0%; margin-right: 3%'>x</button>";


}

function monthlyAPIWiseChartDataByMonth(month, apiname) {

    var s = './ChartDataMonthAPIWise?month=' + month + '&apiName=' + apiname;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false,
        data: $("#assign_token_form").serialize()
    }).responseText;
    if (jsonData.length === 0) {
        jsonData = [{"label": "NA", "value": 0, "value5": 0}];
        return jsonData;
    } else {
        var myJsonObj = JSON.parse(jsonData);
        return myJsonObj;
    }
}

function viewAPIDes(apiName, apiDes, idNo) {
    var l = $('#viewApiDes' + idNo).ladda();
    l.ladda('start');
    initializeToastr();
    var msgToken = "<p style='font-size:18px'>Hello</p>";
    var type = "Token";
    var tokenType = "<h4>API Description: " + apiName + '<button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4>';
    document.getElementById('apiDescriptionFor').innerHTML = tokenType;
    document.getElementById('apiDescription').innerHTML = apiDes;
    var inputTag = "<input type='text' id='copyTarget' style='display: none'>";
    //var copyTag = '<a href="#/" style="font-size:15px; margin-left:35%;text-decoration:none" onclick="copy('+ data.encodeTokenDetails +')"><b>Copy</b></a>';
    var copyTag = "<a href='#/' style='font-size:15px; margin-left:15%;text-decoration:none' onclick=copy('" + apiDes + "','" + idNo + "')><b>Copy</b></a>";
    console.log("copyTag >>> " + copyTag);
    document.getElementById('copyDiv').style.display = 'none';
    document.getElementById('inputTag').innerHTML = inputTag;
    $('#apiDescriptionModal').modal();

    setTimeout(function () {
        l.ladda('stop');
    }, 3000);

}