/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function bootboxmodelshow(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            content +
            '<a class="btn btn-info btn-xs" data-dismiss="modal" class="close"> OK</a></div></div></div></div>');
    popup.modal();
}
function Alert4Users(msg) {
    bootboxmodelshow("<h5>" + msg + "</h5>");
}
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waiting.modal();
}
function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        $("body")
                .append($('<div id="container" style="' +
                        'width: %; margin-left: 65%; margin-top: 10%;">'));
    }
    type = type || "info";
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    $("#alerts-container").prepend(alert);
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}
function generatereAPIreport() {
    var sdate = document.getElementById('_startdate').value;
    var edate = document.getElementById('_enddate').value;
    var l = $('#generateButton').ladda();

    l.ladda('start');
    var apiUsageEnvironment = document.getElementById('apiUsageEnvironment').value;
    var apiUsageAccesspoint = $("#apiUsageAccesspoint").val();
    var s = './billingUsageDetails.jsp?_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + '00:00 AM' + '&_etime=' + '23:59 PM' + '&_apiUsageEnvironment=' + apiUsageEnvironment + '&_apiUsageAccesspoint=' + apiUsageAccesspoint;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            setTimeout(function () {
                l.ladda('stop');
            }, 3000);
            setTimeout(function () {
                document.getElementById("fillBlankSpace").style.display = 'none';
                $('#report_data').html(data);
            }, 4000);
        }
    });
}


function downloadBillingPDF(sdate, edate, stime, etime, apiUsageEnvironment, apiUsageAccesspoint) {

    var s = './generateBillingPDF?_sdate=' + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=0" + "&_apiUsageEnvironment=" + apiUsageEnvironment + "&_apiUsageAccesspoint=" + apiUsageAccesspoint;
    window.location.href = s;
    return false;
}
