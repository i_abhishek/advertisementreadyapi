/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    $('input[type="checkbox"]').click(function(){
        var inputValue = $(this).attr("value");
        $("." + inputValue).toggle();
    });
});

function addDeveloperProductionDetail() {
    var s = './RegisterDeveloperProductionDetail';
    var l = $('#registerDeveloperDetailButton').ladda();    
    initializeToastr();
//    $("#register_partnership").validate({
//        rules: {
//            partnerCompanyName: {
//                required: true
//            },
//            partnerCompanyRegistrationNo: {
//                required: true,
//                number: true
//            },
//            partnerFixedLineNumber: {
//                required: true
//            }
//        },
 //       submitHandler: function (form) {
            var terms = document.getElementById('tnc').checked;
            //var selectedAPI = document.getElementById('selectedAPI').checked;
            var partnerCompanyName = document.getElementById("register_partnership").elements["partnerCompanyName"].value.trim().length;
            var partnerCompanyRegistrationNo = document.getElementById("register_partnership").elements["partnerCompanyRegistrationNo"].value.trim().length;
            var partnerFixedLineNumber = document.getElementById("register_partnership").elements["partnerFixedLineNumber"].value.trim().length;
            var partnerAddress = document.getElementById("register_partnership").elements["partnerAddress"].value.trim().length;
            var partnerState = document.getElementById("register_partnership").elements["partnerState"].value.trim().length;
            var partnerPostcode = document.getElementById("register_partnership").elements["partnerPostcode"].value.trim().length;
            var partnerCountry = document.getElementById("register_partnership").elements["partnerCountry"].value.trim().length;
            var selectedAPI = document.getElementById("register_partnership").elements["selectedAPI"];
            if (!terms) {
                setTimeout(function () {
                    toastr.error('Error - ' + 'Please accept terms and condition');
                }, 3000);
                return;
            }
            if(partnerCompanyName == 0){
                toastr.error('Error - ' + 'Please enter company name');
                return;
            }
            if(partnerCompanyRegistrationNo == 0){
                toastr.error('Error - ' + 'Please enter company registration no.');
                return;
            }
            if(partnerFixedLineNumber == 0){
                toastr.error('Error - ' + 'Please enter fixed line number ');
                return;
            }
            if(partnerAddress == 0){
                toastr.error('Error - ' + 'Please enter address ');
                return;
            }
            if(partnerState == 0){
                toastr.error('Error - ' + 'Please enter state');
                return;
            }
            if(partnerPostcode == 0){
                toastr.error('Error - ' + 'Please enter postcode ');
                return;
            }
            if(partnerCountry == 0){
                toastr.error('Error - ' + 'Please enter country ');
                return;
            }
//            var isChecked = true;
//            for (var i = 0; i < selectedAPI.length; i++) {
//                if ( selectedAPI[i].checked ) {
//                    isChecked = false;
//                    break;
//                };
//            };
//            if (isChecked) {                
//                toastr.error('Error - ' + 'Please choose atleast one API');                
//                return;
//            }
            
            l.ladda('start');
            $.ajax({
                type: 'POST',
                //fileElementId: 'fileImageToUpload',
                url: s,
                dataType: 'json',
                data: $("#register_partnership").serialize(),
                success: function (data) {
                    if (strCompare(data._result, "error") === 0) {
                        setTimeout(function () {
                            toastr.error('Error - ' + data._message);
                            l.ladda('stop');
                        }, 3000);
                        return;
                    } else if (strCompare(data._result, "success") === 0) {
                        l.ladda('stop');
                        setTimeout(function () {
                            toastr.success('Success - ' + data._message);
                            l.ladda('stop');                           
                        }, 3000);
                    }
                }
            });
        //}
    //}); 
}

function addDeveloperEnterpriceProductionDetail() {
    var s = './RegisterDeveloperProductionDetail';
    var l = $('#registerDeveloperEnterpriceDetailButton').ladda();    
    initializeToastr();
//    $("#register_partnership").validate({
//        rules: {
//            partnerCompanyName: {
//                required: true
//            },
//            partnerCompanyRegistrationNo: {
//                required: true,
//                number: true
//            },
//            partnerFixedLineNumber: {
//                required: true
//            }
//        },
 //       submitHandler: function (form) {
            var terms = document.getElementById('tncEnterprise').checked;            
            var partnerCompanyName = document.getElementById("register_partnership_enterprise").elements["partnerCompanyName"].value.trim().length;
            var partnerCompanyRegistrationNo = document.getElementById("register_partnership_enterprise").elements["partnerCompanyRegistrationNo"].value.trim().length;
            var partnerFixedLineNumber = document.getElementById("register_partnership_enterprise").elements["partnerFixedLineNumber"].value.trim().length;
            var partnerAddress = document.getElementById("register_partnership_enterprise").elements["partnerAddress"].value.trim().length;
            var partnerState = document.getElementById("register_partnership_enterprise").elements["partnerState"].value.trim().length;
            var partnerPostcode = document.getElementById("register_partnership_enterprise").elements["partnerPostcode"].value.trim().length;
            var partnerCountry = document.getElementById("register_partnership_enterprise").elements["partnerCountry"].value.trim().length;
            var selectedAPI = document.getElementById("register_partnership_enterprise").elements["selectedAPI"];
            if (!terms) {
                setTimeout(function () {
                    toastr.error('Error - ' + 'Please accept terms and condition');
                }, 3000);
                return;
            }
            if(partnerCompanyName == 0){
                toastr.error('Error - ' + 'Please enter company name');
                return;
            }
            if(partnerCompanyRegistrationNo == 0){
                toastr.error('Error - ' + 'Please enter company registration no.');
                return;
            }
            if(partnerFixedLineNumber == 0){
                toastr.error('Error - ' + 'Please enter fixed line number ');
                return;
            }
            if(partnerAddress == 0){
                toastr.error('Error - ' + 'Please enter address ');
                return;
            }
            if(partnerState == 0){
                toastr.error('Error - ' + 'Please enter state');
                return;
            }
            if(partnerPostcode == 0){
                toastr.error('Error - ' + 'Please enter postcode ');
                return;
            }
            if(partnerCountry == 0){
                toastr.error('Error - ' + 'Please enter country ');
                return;
            }
//            var isChecked = true;
//            for (var i = 0; i < selectedAPI.length; i++) {
//                if ( selectedAPI[i].checked ) {
//                    isChecked = false;
//                    break;
//                };
//            };
//            if (isChecked) {                
//                toastr.error('Error - ' + 'Please choose atleast one API');                
//                return;
//            }   
            l.ladda('start');
            $.ajax({
                type: 'POST',
                //fileElementId: 'fileImageToUpload',
                url: s,
                dataType: 'json',
                data: $("#register_partnership_enterprise").serialize(),
                success: function (data) {
                    if (strCompare(data._result, "error") === 0) {
                        setTimeout(function () {
                            toastr.error('Error - ' + data._message);
                            l.ladda('stop');
                        }, 3000);
                        return;
                    } else if (strCompare(data._result, "success") === 0) {
                        l.ladda('stop');
                        setTimeout(function () {
                            toastr.success('Success - ' + data._message);
                            l.ladda('stop');                           
                        }, 3000);
                    }
                }
            });
        //}
    //}); 
}

