/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function downloadMobileMonitoring() {
    var s = './AppServerMonitoring';
    window.location.href = s;
    return false;
}

function downloadWebAppPush() {
    var s = './WebAppPushDownload';
    window.location.href = s;
    return false;
}

function downloadiOSAppPush() {
    var s = './iOSAppPushDownload';
    window.location.href = s;
    return false;
}

function downloadAndroidAppPush() {
    var s = './AndroidAppPushDownload';
    window.location.href = s;
    return false;
}

function confirmSafariPuahPackage() {
    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    updateSafariDetails();
                } else {
                    swal("Cancelled", "Your request has been cancelled)", "error");
                }
            });
}

function updateSafariDetails() {
    var l = $('#safariUpdate').ladda();
    //var parId = document.getElementById("partnerId").value;
    l.ladda('start');
    initializeToastr();
    var s = './UpdateSafariDetails';
    $.ajax({
        type: 'POST',
        url: s,
        data: $("#safariInfo").serialize(),
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
                $("#safariJSDownload").removeClass('disabled');
                downloadWebAppPush();
            }
        }
    });
}

function validateSafariForm() {
    $("#safariInfo").validate({
        rules: {
            domainName: {
                required: true,
                url: true
            },
            siteName: {
                required: true
            }, 
            websitePushID: {
                required: true
            },
            p12Password: {
                required: true
            }
        },
        submitHandler: function (form) {
            confirmSafariPuahPackage();
        }
    });
}
function callValidateSafariForm() {
    document.querySelector('.validateSafariForm').click();
}

function removeIconFileFromSession(file) {
    var s = './RemoveIconSetFileFromSession?file=' + file;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {

            } else if (strCompare(data._result, "success") === 0) {

            }
        }
    });
}