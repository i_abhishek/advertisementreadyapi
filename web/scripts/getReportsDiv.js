/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function monthlyapiCharData() {
    var s = './MonthlyApiTxChart';
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    if (jsonData.length === 0) {
        jsonData = [{"label": "NA", "value": 0, "value5": 0}];
        return jsonData;
    } else {
        var myJsonObj = JSON.parse(jsonData);
        return myJsonObj;
    }
}

function monthlyTxReportByButton() {
//    var apiData = monthlyapiCharData();
    var chartData = monthlyapiCharData();
    var dateArr = [];
    var txAmountArr = [];
    var callCountArr = [];

    dateArr.push("Data e Ora");
    txAmountArr.push('CreditUsed');
    callCountArr.push('CallCount');
    var recordFound = "false";
    for (var key in chartData) {
        var attrName = key;
        var attrValue = chartData[key];
        dateArr.push(attrValue.strDate);
        txAmountArr.push(attrValue.intTotal);
        callCountArr.push(attrValue.callCount);
        var positiveValue = attrValue.callCount;
        if (positiveValue > 0) {
            recordFound = "true";
            console.log("positiveValue monthlyTxReport>> " + positiveValue);
        }
    }
//    if (dateArr.length === 1) {
//        var tempStore = dateArr;
//        var date = new Date(dateArr);
//        date.setDate(date.getDate() - 1);
//        dateArr = [];
//        dateArr.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
//        dateArr.push(tempStore);
//        txAmountArr.push(0);
//        callCountArr.push(0);
//    }
    console.log("arrXaxis monthlyTxReport>> " + dateArr);
    console.log("arrYaxis >> " + callCountArr);
    console.log("arrYaxis1  >> " + txAmountArr);
    if (recordFound === 'true') {
        document.getElementById("noRecordFoundData").style.display = "none";
        document.getElementById("expenditureReport").style.display = "block";
        var chart = c3.generate({
            bindto: '#expenditureReport',
            data: {
                x: 'Data e Ora',
                xFormat: '%Y-%m-%d',
                columns: [
                    dateArr,
                    txAmountArr,
                    callCountArr
                ],
                colors: {
                    txAmountArr: '#62cb31',
                    CreditUsed: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
                },
                types: {
                    //CallCount: 'bar',
                    CreditUsed: 'bar'
                },
                groups: [
                    ['Transcation Amount', 'Call Count']
                ]
            },
            subchart: {
                show: true
            },
            axis: {
                x: {
                    type: 'timeseries',
                    // if true, treat x value as localtime (Default)
                    // if false, convert to UTC internally                        
                    tick: {
                        format: '%d-%m-%Y'
                                //fomat:function (x) { return x.getFullYear(); }
                    }
                }
            }
        });

    } else {
        document.getElementById("noRecordFoundData").style.display = "block";
        document.getElementById("expenditureReport").style.display = "none";
    }
    document.getElementById("homeReportLable").innerHTML = " Last 30 day's expenditure";
}

$(function () {
// Data used for weekly chart
    var apiData = apiCharData();
    var dateArr2 = [];
    var txAmount2 = [];
    var callCountArr2 = [];
    var temp;
    dateArr2.push("Data e Ora");
    callCountArr2.push('Call Count');
    txAmount2.push('CreditUsed');
    var recordFound = "false";
    for (var key in apiData) {
        var attrName1 = key;
        var attrValue1 = apiData[key];
        temp = (attrValue1.label).split(" ");
//                                arrXaxis1.push(temp[1] + " call : " + attrValue1.value);
        dateArr2.push(temp[1]);
        txAmount2.push(attrValue1.value5);
        callCountArr2.push(attrValue1.value);
        var positiveValue = attrValue1.value;
        if (positiveValue > 0) {
            recordFound = "true";
        }
    }
//    alert(JSON.stringify(arrXaxis1));
//    if (dateArr2.length <= 1) {
//        if (dateArr2.length === 1) {
//            var a = dateArr2[0];
//            var tim = new Date(moment(a, ['h:m a', 'H:m']));
//            dateArr2.push((tim.getHours() + 1) + ":59");
//            txAmount2.push(0);
//            callCountArr2.push(0);
//        } else {
//            dateArr2.push("00.59");
//            dateArr2.push("1:59");
//            txAmount2.push(0);
//            callCountArr2.push(0);
//        }
//    }

    console.log("arrXaxis >> " + dateArr2);
    console.log("callCountArr2 >> " + callCountArr2);
    console.log("txAmount2  >> " + txAmount2);
    if (recordFound === 'true') {
        document.getElementById("noRecordFoundData").style.display = "none";
        document.getElementById("expenditureReport").style.display = "block";
        var chart = c3.generate({
            bindto: '#expenditureReport',
            data: {
                x: 'Data e Ora',
                xFormat: '%H:%M',
                columns: [
                    dateArr2,
                    txAmount2,
                    callCountArr2
                ],
                colors: {
//                TransactionAmount: '#62cb31',
//                callCountArr2: '#FF7F50'
                    dateArr2: '#62cb31',
                    CreditUsed: '#FF7F50'
                },
                types: {
                    //CallCount: 'bar',
                    CreditUsed: 'bar'
                },
                groups: [
                    ['Transcation Amount', 'Call Count']
                ]
            },
            subchart: {
                show: true
            },
            axis: {
                x: {
                    type: 'timeseries',
                    // if true, treat x value as localtime (Default)
                    // if false, convert to UTC internally                        
                    tick: {
                        format: '%H:%M '
                                //fomat:function (x) { return x.getFullYear(); }
                    }
                }
            }
        });
    } else {
        document.getElementById("noRecordFoundData").style.display = "block";
        document.getElementById("expenditureReport").style.display = "none";
    }
    document.getElementById("homeReportLable").innerHTML = " Daily expenditure";
    document.getElementById("report_data_button").style.display = "block";
    setTimeout(function () {
        lastSevenDayExV2();
        document.getElementById("homeReportLable").innerHTML = " Last 7 day's expenditure";
    }, 4000);
});

function dailyTxReport() {
    var apiData = apiCharData();
    var dateArr2 = [];
    var txAmount2 = [];
    var callCountArr2 = [];
    var temp;
    dateArr2.push("Data e Ora");
    callCountArr2.push('Call Count');
    txAmount2.push('CreditUsed');
    var recordFound = "false";
    for (var key in apiData) {
        var attrName1 = key;
        var attrValue1 = apiData[key];
        temp = (attrValue1.label).split(" ");
//                                arrXaxis1.push(temp[1] + " call : " + attrValue1.value);
        dateArr2.push(temp[1]);
        txAmount2.push(attrValue1.value5);
        callCountArr2.push(attrValue1.value);
        var positiveValue = attrValue1.value;
        if (positiveValue > 0) {
            recordFound = "true";
        }
    }
    console.log("arrXaxis >> " + dateArr2);
    console.log("callCountArr2 >> " + callCountArr2);
    console.log("txAmount2  >> " + txAmount2);
    if (recordFound === 'true') {
        document.getElementById("noRecordFoundData").style.display = "none";
        document.getElementById("expenditureReport").style.display = "block";
        var chart = c3.generate({
            bindto: '#expenditureReport',
            data: {
                x: 'Data e Ora',
                xFormat: '%H:%M',
                columns: [
                    dateArr2,
                    txAmount2,
                    callCountArr2
                ],
                colors: {
//                CreditUsed: '#62cb31',
//                callCountArr2: '#FF7F50'
                    dateArr2: '#62cb31',
                    CreditUsed: '#FF7F50'
                },
                types: {
                    //CallCount: 'bar',
                    CreditUsed: 'bar'
                },
                groups: [
                    ['Transcation Amount', 'Call Count']
                ]
            },
            subchart: {
                show: true
            },
            axis: {
                x: {
                    type: 'timeseries',
                    // if true, treat x value as localtime (Default)
                    // if false, convert to UTC internally                        
                    tick: {
                        format: '%H:%M '
                                //fomat:function (x) { return x.getFullYear(); }
                    }
                }
            }
        });
        chart.resize(); 
    } else {
        document.getElementById("noRecordFoundData").style.display = "block";
        document.getElementById("expenditureReport").style.display = "none";
    }
    document.getElementById("homeReportLable").innerHTML = " Daily User Search";
    document.getElementById("report_data_button").style.display = "block";

}

function weeklyAreaChart() {
    var s = './AreaChart';
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    if (jsonData.length == 0) {
        var date = new Date();
        date.setDate(date.getDate() - 1);
        var yesterDay = (date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear());
        jsonData = [{"strDate": yesterDay, "intTotal": 0}];
        return jsonData;
    } else {
        var myJsonObj = JSON.parse(jsonData);
        return myJsonObj;
    }
}

function apiCharData() {
    var s = './APILineChartreport';
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    if (jsonData.length === 0) {
        jsonData = [{"label": "NA", "value": 0, "value5": 0}];
        return jsonData;
    } else {
        var myJsonObj = JSON.parse(jsonData);
        return myJsonObj;
    }
}
function lastSevenDayExV2() {
    var chartData = weeklyAreaChart();
    var dateArr = [];
    var txAmountArr = [];
    var callCountArr = [];

    dateArr.push("Data e Ora");
    txAmountArr.push('CreditUsed');
    callCountArr.push('CallCount');
    var recordFound = "false";
    for (var key in chartData) {
        var attrName = key;
        var attrValue = chartData[key];
        dateArr.push(attrValue.strDate);
        txAmountArr.push(attrValue.intTotal);
        callCountArr.push(attrValue.callCount);
        var positiveValue = attrValue.callCount;
        if (positiveValue > 0) {
            recordFound = "true";
        }
    }
//    if (dateArr.length === 1) {
//        var tempStore = dateArr;
//        var date = new Date(dateArr);
//        date.setDate(date.getDate() - 1);
//        dateArr = [];
//        dateArr.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
//        dateArr.push(tempStore);
//        txAmountArr.push(0);
//        callCountArr.push(0);
//    }
    console.log("arrXaxis >> " + dateArr);
    console.log("arrYaxis >> " + callCountArr);
    console.log("arrYaxis1  >> " + txAmountArr);
    if (recordFound === 'true') {
        document.getElementById("noRecordFoundData").style.display = "none";
        document.getElementById("expenditureReport").style.display = "block";
        var chart = c3.generate({
            bindto: '#expenditureReport',
            data: {
                x: 'Data e Ora',
                xFormat: '%Y-%m-%d',
                columns: [
                    dateArr,
                    txAmountArr,
                    callCountArr
                ],
                colors: {
                    txAmountArr: '#62cb31',
                    CreditUsed: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
                },
                types: {
                    //CallCount: 'bar',
                    CreditUsed: 'bar'
                },
                groups: [
                    ['Transcation Amount', 'Call Count']
                ]
            },
            subchart: {
                show: true
            },
            axis: {
                x: {
                    type: 'timeseries',
                    // if true, treat x value as localtime (Default)
                    // if false, convert to UTC internally                        
                    tick: {
                        format: '%d-%m-%Y'
                                //fomat:function (x) { return x.getFullYear(); }
                    }
                }
            }
        });
    } else {
        document.getElementById("noRecordFoundData").style.display = "block";
        document.getElementById("expenditureReport").style.display = "none";
    }
//    document.getElementById("homeReportLable").innerHTML = " Last 7 day expenditure";
    setTimeout(function () {
        monthlyTxReport();
        document.getElementById("homeReportLable").innerHTML = " Last 30 day's expenditure";
    }, 4000);
}

function lastSevenDayEx() {
    var chartData = weeklyAreaChart();
    var dateArr = [];
    var txAmountArr = [];
    var callCountArr = [];

    dateArr.push("Data e Ora");
    txAmountArr.push('CreditUsed');
    callCountArr.push('CallCount');
    var recordFound = "false";
    for (var key in chartData) {
        var attrName = key;
        var attrValue = chartData[key];
        dateArr.push(attrValue.strDate);
        txAmountArr.push(attrValue.intTotal);
        callCountArr.push(attrValue.callCount);
        var positiveValue = attrValue.callCount;
        if (positiveValue > 0) {
            recordFound = "true";
        }
    }
//    if (dateArr.length === 1) {
//        var tempStore = dateArr;
//        var date = new Date(dateArr);
//        date.setDate(date.getDate() - 1);
//        dateArr = [];
//        dateArr.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
//        dateArr.push(tempStore);
//        txAmountArr.push(0);
//        callCountArr.push(0);
//        
//    }
    console.log("arrXaxis >> " + dateArr);
    console.log("arrYaxis >> " + callCountArr);
    console.log("arrYaxis1  >> " + txAmountArr);
    if (recordFound === 'true') {
        document.getElementById("expenditureReport").style.display = "block";
        document.getElementById("noRecordFoundData").style.display = "none";
        var chart = c3.generate({
            bindto: '#expenditureReport',
            data: {
                x: 'Data e Ora',
                xFormat: '%Y-%m-%d',
                columns: [
                    dateArr,
                    txAmountArr,
                    callCountArr
                ],
                colors: {
                    txAmountArr: '#62cb31',
                    CreditUsed: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
                },
                types: {
                    //CallCount: 'bar',
                    CreditUsed: 'bar'
                },
                groups: [
                    ['Transcation Amount', 'Call Count']
                ]
            },
            subchart: {
                show: true
            },
            axis: {
                x: {
                    type: 'timeseries',
                    // if true, treat x value as localtime (Default)
                    // if false, convert to UTC internally                        
                    tick: {
                        format: '%d-%m-%Y'
                                //fomat:function (x) { return x.getFullYear(); }
                    }
                }
            }
        });

    } else {
        document.getElementById("expenditureReport").style.display = "none";
        document.getElementById("noRecordFoundData").style.display = "block";
    }
    document.getElementById("homeReportLable").innerHTML = " Last 7 day expenditure";
}
function monthlyTxReport() {
//    var apiData = monthlyapiCharData();
    var chartData = monthlyapiCharData();
    var dateArr = [];
    var txAmountArr = [];
    var callCountArr = [];

    dateArr.push("Data e Ora");
    txAmountArr.push('CreditUsed');
    callCountArr.push('CallCount');
    var recordFound = "false";
    for (var key in chartData) {
        var attrName = key;
        var attrValue = chartData[key];
        dateArr.push(attrValue.strDate);
        txAmountArr.push(attrValue.intTotal);
        callCountArr.push(attrValue.callCount);
        var positiveValue = attrValue.callCount;
        if (positiveValue > 0) {
            recordFound = "true";
        }
    }
//    if (dateArr.length === 1) {
//        var tempStore = dateArr;
//        var date = new Date(dateArr);
//        date.setDate(date.getDate() - 1);
//        dateArr = [];
//        dateArr.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
//        dateArr.push(tempStore);
//        txAmountArr.push(0);
//        callCountArr.push(0);
//    }
    console.log("arrXaxis >> " + dateArr);
    console.log("arrYaxis >> " + callCountArr);
    console.log("arrYaxis1  >> " + txAmountArr);
    if (recordFound === 'true') {
        document.getElementById("expenditureReport").style.display = "block";
        document.getElementById("noRecordFoundData").style.display = "none";
        var chart = c3.generate({
            bindto: '#expenditureReport',
            data: {
                x: 'Data e Ora',
                xFormat: '%Y-%m-%d',
                columns: [
                    dateArr,
                    txAmountArr,
                    callCountArr
                ],
                colors: {
                    txAmountArr: '#62cb31',
                    CreditUsed: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
                },
                types: {
                    //CallCount: 'bar',
                    CreditUsed: 'bar'
                },
                groups: [
                    ['Transcation Amount', 'Call Count']
                ]
            },
            subchart: {
                show: true
            },
            axis: {
                x: {
                    type: 'timeseries',
                    // if true, treat x value as localtime (Default)
                    // if false, convert to UTC internally                        
                    tick: {
                        format: '%d-%m-%Y'
                                //fomat:function (x) { return x.getFullYear(); }
                    }
                }
            }
        });
    } else {
        document.getElementById("expenditureReport").style.display = "none";
        document.getElementById("noRecordFoundData").style.display = "block";
    }

    document.getElementById("homeReportLable").innerHTML = " Last 30 day's expenditure";
}
