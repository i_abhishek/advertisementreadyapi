/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function viewUrl(urlData, numberId) {
    var message = "MO URL";
    var type = "MO URL";
    var tokenType = "<h4>" + "MO URL" + "<h4>";
    document.getElementById('urlType').innerHTML = tokenType;
    document.getElementById('url').value = urlData;
    document.getElementById('numberID').value = numberId;
    $('#moUrl').modal();
}
function editUrl() {
    var l = $('#editMoUrl').ladda();
    l.ladda('start');
    initializeToastr();
    var moUrl = document.getElementById('url').value;
    var numberId = document.getElementById('numberID').value;
    
    var s = './EditMoUrl?moUrl=' + encodeURIComponent(moUrl) +'&numberID='+numberId;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                l.ladda('stop');
                toastr.error('Error - ' + data._message);
            } else {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.success('success - ' + data._message);
                }, 3000);
                setTimeout(function () {
                    window.location.href = "./my_service.jsp";
                }, 5000);
            }
        }
    });
}
