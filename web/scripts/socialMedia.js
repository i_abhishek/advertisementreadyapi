//https://services.blue-bricks.com/MPServicesPortal facebookappId
var facebookappId = '229709337440067';
//local facebookappId
//var facebookappId = '1358517987531810';
//https://services.blue-bricks.com/MPServicesPortal googleappId
var googleppId = '742684492272-rapafotak9fd3h6l16sbgllgo6buk9te.apps.googleusercontent.com';
//local googleappId
//var googleppId = '597994829224-7u4mlrti7716p1sr955m3qd797v28m0c.apps.googleusercontent.com';
//server linkedinId
//var linkedInappId = '8137zz6ug56k1w';

// localhost linkedIn key
//var linkedInappId = '81qhnzg756okk4';
var linkedInappId = '81akug1v7h1k9r';
function onLoadGmail() {
    gapi.load('auth2', function () {
        auth2 = gapi.auth2.init({
            client_id: googleppId,
            cookiepolicy: 'single_host_origin',
            scope: 'profile'
        });
        auth2.attachClickHandler(element, {},
                onSuccess, function (error) {
                    console.log('Sign-in error', error);
                }
        );
    });
    element = document.getElementById('googleSignIn');
}

function onSuccess(googleUser) {
    var profile = googleUser.getBasicProfile();
    console.log("prNa >> " + profile.getName() + ", prEm >> " + profile.getEmail() + ", prImageU >> " + profile.getImageUrl());
    RegisterWithSocailMedia(profile.getName(), profile.getEmail(), profile.getImageUrl(), "Google");
}

function logoutGmail() {
    try {
        $.ajax({
            type: "POST",
            url: "https://accounts.google.com/Logout",
            dataType: 'jsonp',
            crossDomain: true
        })
    } catch (err) {

    }
}

function onLoadfacebook() {
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    window.fbAsyncInit = function () {
        FB.init({
            appId: facebookappId,
            oauth: true,
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true // parse XFBML
        });
    };
}

function fb_login() {
    FB.login(function (response) {

        if (response.authResponse) {
            console.log('Welcome!  Fetching your information.... ');
            console.log(response); // dump complete info
            access_token = response.authResponse.accessToken; //get access token
            user_id = response.authResponse.userID; //get FB UID

            FB.api('/me', {fields: 'first_name,last_name,name,id,email'}, function (response) {
                console.log(response);
                RegisterWithSocailMedia(response.name, response.email, response.id, "FB");
            });
        } else {
//user hit cancel button
            console.log('User cancelled login or did not fully authorize.');
        }
    }, {
        // scope: 'publish_stream,email,public_profile'
        scope: 'email,public_profile'
    });
}


function fblogout() {
//                FB.Auth.setAuthResponse(null, 'unknown');
    FB.logout(function (response) {

    });
}

function liAuth1() {

}

function liAuth() {
    console.log("Called liAuth");

    IN.User.authorize(function () {
        // callback();
    });
}
function onLinkedInLoad(a) {
//    try {
//        IN.User.logout();
//    } catch (err) {
//    }
    IN.Event.on(IN, "auth", onLinkedInAuth);
}


// 2. Runs when the viewer has authenticated
function onLinkedInAuth() {

    IN.API.Profile("me").fields("id", "first-name", "last-name", "email-address", "picture-url").result(displayProfiles);
}

// 2. Runs when the Profile() API call returns successfully
function displayProfiles(profiles) {
    member = profiles.values[0];
    RegisterWithSocailMedia(member.firstName + " " + member.lastName, member.emailAddress, member.pictureUrl, "LinkedIn");
}

function closeSession() {
    IN.User.logout();
    return true;
}

//function RegisterWithSocailMedia(name, email, profileImageUrl, loggedUsing) {
//    var s = './RegisterWithSocailMedia';
//    initializeToastr();
//    var loginPage = document.getElementById("loginPage").value;
//    var dataSend = {"name": name, "email": email, "loggedUsing": loggedUsing, "profileImageUrl": profileImageUrl};
//    if (loggedUsing === 'LinkedIn') {
//        closeSession();
//        var l = $('#linkedInLadda').ladda();
//        l.ladda('start');
//    } else if (loggedUsing === 'FB') {
//        fblogout();
//        var l = $('#facebookLadda').ladda();
//        l.ladda('start');
//    } else if (loggedUsing === 'Google') {
//        logoutGmail();
//        var l = $('#googleSignIn').ladda();
//        l.ladda('start');
//    }
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: dataSend,
//        success: function (data) {
//            if (strCompare(data._result, "error") === 0) {
//                setTimeout(function () {
//                    l.ladda('stop');
//                    toastr.error('Error - ' + data._message);
//                }, 1000);
//                setTimeout(function () {
//                    l.ladda('stop');
//                    window.location.href = "./login.jsp";
//                }, 3000);
//            } else if (strCompare(data._result, "success") === 0) {
//                setTimeout(function () {
//                    l.ladda('stop');
//                    toastr.success('Success - ' + data._message);
//                }, 1000);
//                setTimeout(function () {
//                    window.location.href = "./header.jsp";
//                }, 3000);
//            } else if (strCompare(data._result, "successWithRedirect") === 0) {
//                setTimeout(function () {
//                    l.ladda('stop');
//                    toastr.success('Success - ' + data._message);
//                }, 1000);
//                setTimeout(function () {
//                    if (loginPage == 1) {
//                        window.location.href = "./packages.jsp";
//                    } else {
//                        getPackageInvoice();
//                    }
//                    $("#step1").addClass("disabled");
//                    $("#step2").removeClass("disabled");
//                }, 3000);
//            }
//        }, error: function (xhr, errorType, exception) {
//            var responseText;
//            try {
//                responseText = jQuery.parseJSON(xhr.responseText);
//                console.log("<div><b>" + errorType + " " + exception + "</b></div>");
//                console.log("<div><u>Exception</u>:<br /><br />" + responseText.ExceptionType + "</div>");
//                console.log("<div><u>StackTrace</u>:<br /><br />" + responseText.StackTrace + "</div>");
//                console.log("<div><u>Message</u>:<br /><br />" + responseText.Message + "</div>");
//            } catch (e) {
//                responseText = xhr.responseText;
//                console.log(responseText);
//            }
//
//        }
//    });
//}


function RegisterWithSocailMedia(name, email, profileImageUrl, loggedUsing) {
    var s = './RegisterWithSocailMedia';
    initializeToastr();
    var loginPage = document.getElementById("loginPage").value;
    var buttonId;
    var preventClick = false;
    var dataSend = {"name": name, "email": email, "loggedUsing": loggedUsing, "profileImageUrl": profileImageUrl};
    if (loggedUsing === 'LinkedIn') {
        closeSession();
        var l = $('#linkedInLadda').ladda();
        l.ladda('start');
        buttonId = document.getElementById("linkedInLadda");
    } else if (loggedUsing === 'FB') {
        fblogout();
        var l = $('#facebookLadda').ladda();
        l.ladda('start');
        buttonId = document.getElementById("facebookLadda");
    } else if (loggedUsing === 'Google') {
        logoutGmail();
        var l = $('#googleSignIn').ladda();
        l.ladda('start');
        buttonId = document.getElementById("googleSignIn");
    }
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: dataSend,
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.error('Error - ' + data._message);
                }, 1000);
                setTimeout(function () {
                    l.ladda('stop');
                    window.location.href = "./login.jsp";
                }, 3000);
            } else if (strCompare(data._result, "success") === 0) {
                setTimeout(function () {
                    l.ladda('stop');
                    toastr.success('Success - ' + data._message);
                }, 1000);
                setTimeout(function () {
                    window.location.href = "./header.jsp";
                }, 3000);
            } else if (strCompare(data._result, "successWithRedirect") === 0) {
                
                setTimeout(function () {
                    l.ladda('stop');
                    //toastr.success('Success - ' + 'You have been successfully sign in and password shared on your mail');
//                    var btn = document.getElementById("myBtn");
//                    btn.click();
                    buttonId.style.pointerEvents = "none";
                    $(buttonId).click(function(e) {
                        $(this)
                           .css('cursor', 'default')
                           .css('text-decoration', 'none')

                        if (!preventClick) {
                            $(this).html($(this).html() + ' lalala');
                        }

                        preventClick = true;

                        return false;
                    });
                    var modal = document.getElementById('myModal');
                    modal.style.display = "block";
                }, 2000);
                setTimeout(function () {
                    if (loginPage == 1) {
                        //window.location.href = "./packages.jsp";
                        subscribeAPackgageAutomatically();
                        
                    } else {
                        subscribeAPackgageAutomatically();
                    }
//                    $("#step1").addClass("disabled");
//                    $("#step2").removeClass("disabled");
                    
                }, 3000);
            }
        }, error: function (xhr, errorType, exception) {
            var responseText;
            try {
                responseText = jQuery.parseJSON(xhr.responseText);
                console.log("<div><b>" + errorType + " " + exception + "</b></div>");
                console.log("<div><u>Exception</u>:<br /><br />" + responseText.ExceptionType + "</div>");
                console.log("<div><u>StackTrace</u>:<br /><br />" + responseText.StackTrace + "</div>");
                console.log("<div><u>Message</u>:<br /><br />" + responseText.Message + "</div>");
            } catch (e) {
                responseText = xhr.responseText;
                console.log(responseText);
            }

        }
    });
}

function getPackageInvoice() {
    $('#signupWindow').hide();
    var s = "getInvoiceFirstSignUp.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#paymentWindow').html(data);
            $('#paymentWindow').show();
        },
        error: function (data) {
        }
    });
}

function subscribeAPackgageAutomatically(){        
    var s = './SubscribeAPackageAutomatically?packageName=BasicKitMonthly&grossPaymentAmount=1.10';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',        
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                setTimeout(function () {                    
                    console.log("Package subscription failed "+JSON.stringify(data));
                }, 3000);
            } else if (strCompare(data.result, "success") === 0) {                
                setTimeout(function () {                                       
                    //window.location.href = "./header.jsp";                
                    console.log("Package subscription successed "+JSON.stringify(data));
                }, 10000);
            }
        }
    });
}