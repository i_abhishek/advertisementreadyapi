var stripe = Stripe('pk_test_PeQG0xzLGX0rfkPKvRs7odeM');
var elements = stripe.elements();

var card = elements.create('card', {
  iconStyle: 'solid',
  style: {
    base: {
      iconColor: '#8898AA',
      color: 'white',
      lineHeight: '36px',
      fontWeight: 300,
      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
      fontSize: '19px',

      '::placeholder': {
        color: '#8898AA',
      },
    },
    invalid: {
      iconColor: '#e85746',
      color: '#e85746',
    }
  },
  classes: {
    focus: 'is-focused',
    empty: 'is-empty',
  },
});
card.mount('#card-element');

var inputs = document.querySelectorAll('input.field');
Array.prototype.forEach.call(inputs, function(input) {
  input.addEventListener('focus', function() {
    input.classList.add('is-focused');
  });
  input.addEventListener('blur', function() {
    input.classList.remove('is-focused');
  });
  input.addEventListener('keyup', function() {
    if (input.value.length === 0) {
      input.classList.add('is-empty');
    } else {
      input.classList.remove('is-empty');
    }
  });
});

function setOutcome(result) {
  var successElement = document.querySelector('.success');
  var errorElement = document.querySelector('.error');
  successElement.classList.remove('visible');
  errorElement.classList.remove('visible');

  if (result.token) {
    // Use the token to create a charge or a customer
    // https://stripe.com/docs/charges
    successElement.querySelector('.token').textContent = result.token.id;
    createCharge(result.token.id);
    //successElement.classList.add('visible');
  } else if (result.error) {
    errorElement.textContent = result.error.message;
    errorElement.classList.add('visible');
  }
}

card.on('change', function(event) {
  setOutcome(event);
});

document.querySelector('form').addEventListener('submit', function(e) {
  e.preventDefault();
  var form = document.querySelector('form');
  var extraDetails = {
    name: form.querySelector('input[name=cardholder-name]').value,
  };
  stripe.createToken(card, extraDetails).then(setOutcome);
});
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function createCharge(tokenid){
    var s = './StripeCreateCharge';
    var dataSend = {"stripeToken": tokenid};
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: dataSend,
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {                
                setTimeout(function () {
                    window.location.href = "./stripeResponse.jsp";
                }, 3000);
            } else if (strCompare(data._result, "success") === 0) {               
                setTimeout(function () {
                    window.location.href = "./stripeResponse.jsp";
                    //subscriptionResponse();
                }, 3000);
            }
        }
    });
}

function subscriptionResponse(){
     var s="stripeResponse.jsp";
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#wrapper1').hide();
            $('#wrapper2').hide();
            $('#wrapper3').hide();
            $('#wrapper4').hide();
            $('#wrapper5').hide();
            $('#subscribePackage').html(data);
            $('#wrapper6').show();
            $('#wrapper7').hide();
            $('#wrapper8').hide();
            
        },
    });
}