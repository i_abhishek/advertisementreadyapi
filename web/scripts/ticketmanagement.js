/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//function bootboxmodel(content) {
//    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
//            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
//            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button><font style="font-weight: 900;color: blue">' +
//            content +
//            '</font><br><a class="btn btn-primary" data-dismiss="modal" class="close" style="align:right"> &nbsp; OK </a></div></div></div></div>');
//    popup.modal();
//}
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Users(msg) {
    bootboxmodel("<h2>" + msg + "</h2>");
}
function ticketback() {
    window.location.href = './ticket.jsp';
}
function addticketing() {
    window.location.href = './ticketadd.jsp';
}
function ViewonClick(URL) {
    bootboxmodel("<h5>" + URL + "</h5>");
}
var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waiting.modal();
}
function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
                .append($('<div id="alerts-container" style="position: fixed;' +
                        'width: 50%; left: 25%; top: 10%;">'));
    }
    // default to alert-info; other options include success, warning, danger
    type = type || "info";
    // create the alert div
    message = '<i class="fa fa-info-circle"></i> ' + message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);
    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}

function addticket() {
    var subject = document.getElementById("subject").value;
    var Message = document.getElementById("message").value;
    var image = document.getElementById("fileImageToUpload").value;
    if (subject === "" || Message === "") {
//        bootboxmodel("<h2><font style='color:red'>" + "Please enter all fields!!" + "</font></h2>");
        showAlert("Please enter all field !!!", "danger", 3000);
        return;
    } else if (image === "") {
        //bootboxmodel("<h2><font style='color:red'>" + "Please attach screenshot first!!" + "</font></h2>");
        showAlert("Please attach file !!!", "danger", 3000);
        return;
    } else {
        var s = './UploadImage';
        pleasewait();
        $.ajaxFileUpload({
            type: 'POST',
            fileElementId: 'fileImageToUpload',
            url: s,
            dataType: 'json',
            success: function (data) {

                if (strCompare(data.result, "error") === 0) {
                    //Alert4Users("<span><font color=red>" + data.message + "</font></span>");
                    waiting.modal('hide');
                    showAlert(data.message, "danger", 3000);
                } else if (strCompare(data.result, "success") === 0) {
                    $('#_filename').val(data.filename);
                    TicketAddition();
                }
            },
            error: function (data, status, e)
            {
                waiting.modal('hide');
                alert(e);
            }
        });
    }
}

function TicketAddition() {

    var s = './AddTicket';
    $.ajax({
        type: 'POST',
        fileElementId: 'fileImageToUpload',
        url: s,
        dataType: 'json',
        data: $("#add_ticket").serialize(),
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                //Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                waiting.modal('hide');
                showAlert(data._message, "danger", 3000);
            } else if (strCompare(data._result, "success") === 0) {
                //Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                waiting.modal('hide');
                showAlert(data._message, "success", 3000);
                setTimeout(function () {
                    window.location.href = "./ticket.jsp";
                }, 3000);
            }
        }
    });
}

function ImageUpload() {

    var s = './UploadImage';
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'fileImageToUpload',
        url: s,
        dataType: 'json',
        success: function (data) {

            if (strcmpPDFSign(data.result, "error") === 0) {
                Alert4PDFSign("<span><font color=blue>" + data.message + "</font></span>");
            } else if (strcmpPDFSign(data.result, "success") === 0) {
                $('#_filename').val(data.filename);
                Alert4PDFSign("<span><font color=blue>" + data.message + "</font></span>");
                document.getElementById("signeraddition").style.display = 'block';
            }
        },
        error: function (data, status, e)
        {
            alert(e);
        }
    });
}

function emailSend() {
    var subject = document.getElementById("subject").value;
    var msgBody = document.getElementById("msgBody").value;
    var category = document.getElementById("_setCategory").value;
    var message = null;
    if (subject === null || subject === "" || subject === "Select") {
        message = "Please enter subject !";
        showAlert(message, "danger", 3000);
        return;
    }
    if (category === null || category === "" || category === "Select") {
        message = "Please select category !";
        showAlert(message, "danger", 3000);
        return;
    }
    if (msgBody === null || msgBody === "") {
        message = "Please enter message body !";
        showAlert(message, "danger", 3000);
        return;
    }
    pleaseWaitProcess();
    var s = './TicketEmailSending';
    $.ajax({
        type: 'POST',
        fileElementId: 'ticketCompose',
        url: s,
        dataType: 'json',
        data: $("#ticketCompose").serialize(),
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                //Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                waiting.modal('hide');
                showAlert(data._message, "danger", 3000);
            } else if (strCompare(data._result, "success") === 0) {
                //Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                waiting.modal('hide');
                showAlert(data._message, "success", 3000);
                setTimeout(function () {
                    window.location.href = "./ticketComposeMail.jsp";
                }, 3000);
            }

        },
        error: function (data, status, e)
        {
            waiting.modal('hide');
            alert(e);
        }
    });
}

function changeEmailstatus(EmailNo) {
    var s = './ChangeEmailStatus?_EmailNo=' + EmailNo;
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                waiting.modal('hide');
                //Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                showAlert(data._message, "danger", 3000);
                setTimeout(function () {
                    window.location = "./partners.jsp";
                }, 3000);

            } else if (strCompare(data._result, "success") === 0) {
                waiting.modal('hide');
                //Alert4Users("<h2><font color=blue>" + data._message + "</font></h2>");
                showAlert(data._message, "danger", 3000);
                setTimeout(function () {
                    window.location = "./partners.jsp";
                }, 3000);
            }
        }
    });
}


function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
                .append($('<div id="alerts-container" style="position: fixed;' +
                        'width: 50%; left: 25%; top: 10%;">'));
    }
    // default to alert-info; other options include success, warning, danger
    type = type || "info";
    // create the alert div
    message = '<i class="fa fa-info-circle"></i> ' + message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);
    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}

function deleteEmailById(id) {
    bootbox.confirm({
        message: ("<h4><font color=red>" + "Are you sure ?" + "</font><h4>"),
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-xs'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-xs'
            }
        },
        callback: function (result) {
            // console.log('This was logged in the callback: ' + result);
            if (result === true) {
                pleaseWaitProcess();
                var s = './deleteEmail?id=' + id;
                $.ajax({
                    type: 'POST',
                    url: s,
                    dataType: 'json',
                    success: function (data) {
                        if (strCompare(data._result, "error") === 0) {
                            waiting.modal('hide');
                            showAlert(data._message, "danger", 3000);
                        } else if (strCompare(data._result, "success") === 0) {
                            waiting.modal('hide');
                            showAlert(data._message, "success", 3000);
                            setTimeout(function () {
                                window.location.reload(true);
                            }, 3000);
                        }
                    },
                    error: function (data, status, e)
                    {
                        waiting.modal('hide');
                        alert(e);
                    }
                });
            }
        }
    });
}

function pleaseWaitProcess() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waiting.modal();
}

function sendEmailToRT() {
    var l = $('#submitMail').ladda();
    l.ladda('start');
    initializeToastr();
    var contact = document.getElementById("contact_no").value;
    var subject = document.getElementById("subjectBody").value;
    var msg = $('#msgBody').code();
    msg = msg + "\n" + "Developer's contact no : " + contact;
    if (subject === null || subject === '' || msg === null || msg === '' || contact === '' || contact === null) {
        toastr.error('Error - ' + 'Enter all details');
        l.ladda('stop');
        return;
    }
    if (contact.length > 6 && contact.length < 14) {

    } else {
        toastr.error('Error - ' + 'Enter Proper Contact Number.');
        l.ladda('stop');
        return;
    }
    var emailData = {"subjectBody": subject, "msgBody": msg};
    var s = './SendEmailToRT';
    $.ajax({
        type: 'POST',
        url: s,
        data: emailData,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                toastr.error('Error - ' + data._message);
                l.ladda('stop');
            } else if (strCompare(data._result, "success") === 0) {
                toastr.success('Success - ' + data._message);
                l.ladda('stop');
                setTimeout(function () {
                    window.location = "./header.jsp";
                }, 3000);
            }
        }
    });
}


function viewDateails(emailNo, sessionId, getpartnerEmailid) {
    var s = './ViewEmail?emailNo=' + emailNo + '&sessionId=' + sessionId + '&emailId=' + getpartnerEmailid;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "success") === 0) {
                window.location.href = './ticketViewEmail.jsp?_emailNo=' + emailNo + '&sessionId=' + sessionId + '&emailFrom=' + getpartnerEmailid;
            }
        }
    });
}

function removeFromSession(file){
    var s = './RemoveUploadFileFromSession?file='+file;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                
            } else if (strCompare(data._result, "success") === 0) {
              
            }
        }
    });
}
