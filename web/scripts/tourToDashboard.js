/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tour;
function startTour() {
    // Instance the tour

    tour = new Tour({
        backdrop: true,
        reflex: false,
        orphan: true,
        backdropContainer: 'body',
        onShown: function (tour) {

            // ISSUE    - https://github.com/sorich87/bootstrap-tour/issues/189
            // FIX      - https://github.com/sorich87/bootstrap-tour/issues/189#issuecomment-49007822

            // You have to write your used animated effect class
            // Standard animated class
            $('.animated').removeClass('fadeIn');
            // Animate class from animate-panel plugin
            $('.animated-panel').removeClass('zoomIn');

        },
        onEnd: function (tour) {
            clearPanels();
            document.getElementById("tourDiv").style.display = 'none';
            if (typeof (test) != "undefined") {
                if (enable === true) {
                    test.classList.add("show-sidebar");
                    test.classList.remove("hide-sidebar");
                } else {
                    test.classList.remove("show-sidebar");
                    test.classList.add("hide-sidebar");
                }
            }
        },
        steps: [
            {
                //element: ".tourWelcome",
                title: "Welcome to Ready APIs",
                content: "We're thankful you've joined with us. Please take a brief tour to dashboard to introduce each menu and how it works.",
                placement: "right",
                orphan: true,
                onPrev: function (tour) {
                    clearPanels();
                    document.getElementById("footerPage").style.display = 'block';
                    document.getElementById("tourDiv").style.display = 'none';
                }

            }, {
                element: ".tour-1",
                title: "15 days credit used (1/12)",
                content: "From here you get information of your credit usage from last 15 days.",
                placement: "right",
            },
            {
                element: ".tour-3",
                title: "Services (2/12)",
                content: "You can get service details with your APIToken key, API description, API today and monthly credit and performance reports also, Test the API with our API console in SOAP/REST environment.",
                placement: "top",
                onNext: function (tour) {
                    if ((test.className).indexOf("show-sidebar") > -1) {
                        test.classList.remove("show-sidebar");
                        test.classList.add("hide-sidebar");
                    }
//                    test.className.remove("show-sidebar");
//                    test.classList.add("hide-sidebar");
                }, onPrev: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-20",
                title: "Here is list of services(2.a/12)",
                content: "Here you get the API details briefly and you can choose a serivce that you want to use.",
                placement: "top",
                onPrev: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }
            },
            {
                element: ".tour-13",
                title: "Know more about API (2.b/12)",
                content: "Here you get the API details as per the services you selected.",
                placement: "top",
//                onPrev: function (tour) {
//                    if ((test.className).indexOf("hide-sidebar") > -1) {
//                        test.classList.remove("hide-sidebar");
//                        test.classList.add("show-sidebar");
//                    }
//                }
            },
            {
                element: ".tour-iconAPIConsole",
                title: "Test the  API (2.c/12)",
                content: "You can open the API Console from here and test the API in both SOAP/REST environment.",
                placement: "left"
            },
            {
                element: ".tour-21",
                title: "Your request window (2.d/12)",
                content: "From here you can set your required headers and pass the parameter in body.",
                placement: "top"
            },
            {
                element: ".tour-23",
                title: "Headers (2.e/12)",
                content: "Here you need to set APITokenId which is unique to API, PartnerId which is your unique id, PartnerTokenId which is unique to your services.",
                placement: "top"
            },
            {
                element: ".tour-24",
                title: "Body (2.f/12)",
                content: "Here you can pass the required parameter for the API. ",
                placement: "top"
            },
            {
                element: ".tour-22",
                title: "Response Window (2.g/12)",
                content: "From here you can get the response from the server",
                placement: "top"
            },
            {
                element: "#questionAndAnswer",
                title: "Need a help (2.h/12)",
                content: "Incase after calling API you get error from server, here you get the reference for why that error may occur.",
                placement: "top",
                onNext: function (tour) {
                    //alert("show-sidebar >> "+(test.className).indexOf("show-sidebar"))
                    if ((test.className).indexOf("show-sidebar") > -1) {
                        test.classList.add("hide-sidebar");
                    } else {
                        test.classList.add("show-sidebar");
                    }
//                    
                }
            },
            {
                element: ".tour-4",
                title: "Reports (3/12)",
                content: "Reports provide visibility into APIs and services from different perspectives with API performance, API credit used, with dynamic time frame ",
                placement: "top",
                onPrev: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                },
                onNext: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-reportWindow",
                title: "Reports (3.a/12)",
                content: "Reports provide visibility into APIs and services from different perspectives with API performance, API credit used, with dynamic time frame ",
                placement: "top",
                onPrev: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }
            },
            {
                element: ".tour-apiUsageTour",
                title: "API Usage Report (3.b/12)",
                content: "You can view your API usage details from here ",
                placement: "top"
            },
            {
                element: ".tour-transactionReportWindow",
                title: "API Transaction Report (3.c/12)",
                content: "You can view your API transaction details like below",
                placement: "top"
            },
            {
                element: ".tour-performanceReportWindow",
                title: "API Transaction Report (3.d/12)",
                content: "You can view your API performance details like below",
                placement: "top",
                onNext: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }
            },
            {
                element: ".tour-5",
                title: "Invoice (4/12)",
                content: "You can download your payment invoice also get overview of your payment history.",
                placement: "right",
                onPrev: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }, onNext: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-invoiceTour",
                title: "Your Invoice Details (4.a/12)",
                content: "You can download your payment invoice also get overview of your payment history.",
                placement: "top",
                onPrev: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }, onNext: function (tour) {
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }
            },
            {
                element: ".tour-6",
                title: "Subscribe Package (5/12)",
                content: "You can subscribe/unsubscribe to your package from here. To subscribe to new package firstly you need to unsubscribe from your currently subscribed package.",
                placement: "right",
                onPrev: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }, onNext: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-subscriptionTour",
                title: "Subscribe Package (5.a/12)",
                content: "You can subscribe/unsubscribe to your package from here. To subscribe to new package firstly you need to unsubscribe from your currently subscribed package.",
                placement: "top",
                onPrev: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }, onNext: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-7",
                title: "API Docs (6/12)",
                content: "You can get reference for all our Services and their APIs in terms of how they work.",
                placement: "right", onPrev: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }, onNext: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-HelpDesk",
                title: "Helpdesk (7/12)",
                content: "You can raise a ticket or ask a question to support team with filling email template and upload a attachment any.  ",
                placement: "top", onPrev: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }, onNext: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }

            },
            {
                element: ".tourVideo",
                title: "Videos (8/12)",
                content: "You can view the videos about the dashboard and services.",
                placement: "right", onPrev: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }, onNext: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-8",
                title: "Today Used Credit (9/12)",
                content: "You can analyse your todays credit used in percentage of your total remaining credit",
                placement: "top", onPrev: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    } else {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }, onNext: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    } else {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-9",
                title: "Last 7 days (10/12)",
                content: "You can get total credit used in last seven days.",
                placement: "top", onPrev: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    } else {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }, onNext: function (tour) {
                    //alert((test.className).indexOf("hide-sidebar"));
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    } else {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                }

            },
            {
                element: ".tour-10",
                title: "Last month (11/12)",
                content: "You can get total credit used in last month.",
                placement: "top"

            },
            {
                element: ".tour-12",
                title: "Mostly Used Services (12/12)",
                content: "Top view from the basis of services.",
                placement: "top"

            },
            {
                element: ".tour-11",
                title: "Service Analysis (13/13)",
                content: "Top view from the basis of services.",
                placement: "top",
                onNext: function (tour) {                   
                    clearPanels();
                    if ((test.className).indexOf("hide-sidebar") > -1) {
                        test.classList.add("hide-sidebar");
                        test.classList.remove("show-sidebar");
                    } else {
                        test.classList.remove("hide-sidebar");
                        test.classList.add("show-sidebar");
                    }
                    tour.stop();
                },
            }


        ]});
//    tour.init();

}
var test;
var enable;
function firstTimeTour() {
    $('.mobile-menu-toggle').click();
    test = document.getElementById("Ashish");
    if ((test.className).indexOf("page-small") > -1) {
        enable = false;
    } else {
        enable = true;
    }
    if ((test.className).indexOf("hide-sidebar") > -1) {
        test.classList.add("show-sidebar");
    } else {
        test.classList.add("show-sidebar");
    }

    setTimeout(function () {
        tour.restart();
        dashboardTour();
    }, 1000);

    document.getElementById("tourDiv").style.display = 'block';
}

function generateTopServiceTour() {
    var apiname = ['api name', 'DocumentUtility', 'ValueAddedServices', 'InstantWebChat', 'GoogleAuthToken', 'ValueAddedServices'];
    var partnerCount = ['Your Count', 130, 190, 250, 90, 160];
    var otherUserCount = ['Others Count', 90, 230, 170, 70, 140];
    console.log("otherUserCount :: " + JSON.stringify(otherUserCount));
    console.log("partnerCount :: " + partnerCount);
    console.log("apiname :: " + apiname);
    var chart = c3.generate({
        bindto: '#topFiveSerTour',
        size: {
            height: 240,
        },
        data: {
            x: 'api name',
            columns: [
                apiname,
                otherUserCount,
                partnerCount
            ],
            colors: {
                otherUserCount: '#62cb31',
                partnerCount: '#FF7F50'
            },
            types: {
                CallCount: 'bar'
            },
            groups: [
                ['Your Count', 'Others Count']
            ]
        },
        subchart: {
            show: false
        },
        axis: {
            x: {
                type: 'category',
                categories: apiname
            },
            y: {
                padding: {top: 200, bottom: 0}
            },
        }
    });
    document.getElementById("homeReportLable").innerHTML = "Todays top five services count";
    setTimeout(function () {
        generateTopServiceTourV2();
    }, 1000);
}


function generateTopServiceTourV2() {
    var apiname = ['api name', 'DocumentUtility', 'ValueAddedServices', 'InstantWebChat', 'GoogleAuthToken', 'ValueAddedServices'];
    var partnerCount = ['Your Count', 290, 230, 170, 270, 140];
    var otherUserCount = ['Others Count', 130, 190, 250, 290, 160];

    console.log("otherUserCount :: " + JSON.stringify(otherUserCount));
    console.log("partnerCount :: " + partnerCount);
    console.log("apiname :: " + apiname);

    var chart = c3.generate({
        bindto: '#topFiveSerTour',
        size: {
            height: 240,
        },
        data: {
            x: 'api name',
            columns: [
                apiname,
                otherUserCount,
                partnerCount
            ],
            colors: {
                otherUserCount: '#62cb31',
                partnerCount: '#FF7F50'
//                  callCountArr: '#62cb31',
//                  TransactionAmount: '#FF7F50'
            },
            types: {
                //CallCount: 'bar',
                CallCount: 'bar'
            },
            groups: [
                ['Your Count', 'Others Count']
            ]
        },
        subchart: {
            show: false
        },
        axis: {
            x: {
                type: 'category',
                categories: apiname
            },
            y: {
                padding: {top: 200, bottom: 0}
            },
        }
    });
    document.getElementById("homeReportLable").innerHTML = "Todays top five services";
}

function yourMostlyServiceTour() {
    //var apiname = [];

    document.getElementById("mostlyFiveTour").innerHTML = "Your Mostly used 5 services";

    var chart1 = c3.generate({
        bindto: '#flot-pie-chartTour',
        data: {
            // iris data from R
            columns: [
                ["Image Utility", 70],
                ["Tokenization", 50],
                ["InAppPush", 30],
                ["ReverseProxy", 20],
                ["IP2GIO", 40]
            ],
            type: 'pie'
        },
        colors: {
            data1: '#F39C12',
            data2: '#F8C471',
            data3: '#F5B041',
            data4: '#F39C12',
            data5: '#D68910'
        },
        legend: {
            show: false
        },
        size: {
            width: 150,
            height: 175
        },
        label: {
            format: function (value, ratio, id) {
                return d3.format('$')(value);
            }
        }
    });
}