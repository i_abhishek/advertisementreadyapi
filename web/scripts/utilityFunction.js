/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//loading button 
var l;

function initializeLaddaVariable(id) {
    l = $('#' + id).ladda();
}
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function initializeToastr() {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "toastClass": "animated fadeInDown",
    }
}

function ChangePassword() {
    var s = './ChangePassword';
    var l = $('#changePasswordButton').ladda();
    $("#change_password").validate({
        rules: {
            current_password: {
                required: true
            },
            new_password: {
                required: true,
                minlength: 8
            },
            confirm_password: {
                required: true,
                minlength: 8
            }
        },
        submitHandler: function (form) {
            l.ladda('start');
            initializeToastr();
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                data: $("#change_password").serialize(),
                success: function (data) {
                    if (strCompare(data._result, "error") === 0) {
                        setTimeout(function () {
                            toastr.error('Error - ' + data._message);
                            l.ladda('stop');
                        }, 3000);
                    } else if (strCompare(data._result, "success") === 0) {
                        setTimeout(function () {
                            toastr.success('Success - ' + data._message);
                            l.ladda('stop');
                        }, 3000);
                        setTimeout(function () {
                            window.location.href = "./header.jsp";
                        }, 5000);
                    }
                }
            });
        }
    });
}

function forgetPassword(userId) {
    var s = './TMForgetPassword?userId=' + encodeURIComponent(userId);
    var l = $('#forgetPasswordButton').ladda();
    $("#forgetPasswordForm").validate({
        rules: {
            fpEmailID: {
                required: true
            },
            newPassword: {
                required: true,
                minlength: 8
            },
            confirmPassword: {
                required: true,
                minlength: 8
            }
        },
        submitHandler: function (form) {
            l.ladda('start');
            initializeToastr();
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                data: $("#forgetPasswordForm").serialize(),
                success: function (data) {
                    if (strCompare(data._result, "error") === 0) {
                        setTimeout(function () {
                            toastr.error('Error - ' + data._message);
                            l.ladda('stop');
                        }, 3000);
                    } else if (strCompare(data._result, "success") === 0) {
                        setTimeout(function () {
                            toastr.success('Success - ' + data._message);
                            l.ladda('stop');
                        }, 3000);
                        setTimeout(function () {
                            window.location.href = "./login.jsp";
                        }, 5000);
                    }
                }
            });
        }
    });
}
function goBack() {
    window.history.back();
}
function cancelChanges() {
    window.location.href = "/home.jsp";
}
function RefreshLogout() {
    window.location.href = "./logout.jsp";
}
function checkValidSession() {
    var s = './validateSession';
    initializeToastr();
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (data._result === "error") {
                toastr.error('Error - ' + data._message);
                RefreshLogout();
            }
        }
    });
}
