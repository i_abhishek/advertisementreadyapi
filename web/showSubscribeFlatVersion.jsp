<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <select id="_versionofFlatAPI" name="_versionofFlatAPI"  class="form-control span2" onchange="" style="width: 100%">
                <%
                    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                    String channelId = (String) request.getSession().getAttribute("_ChannelId");
                    String apName = request.getParameter("_apname");
                    String packageName = request.getParameter("_packageName");
                    String resourceName = request.getParameter("_resourceName");
                    PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
                    SgSubscriptionDetails reqObj = new PackageSubscriptionManagement().getSubscriptionbyPartnerId(SessionId, channelId, partnerObj.getPartnerId());

                    String flatPriceDetails = reqObj.getFlatPrice();
                    JSONArray jsOld = new JSONArray(flatPriceDetails);
                    String ver = "";
                    for (int j = 0; j < jsOld.length(); j++) {
                        JSONObject jsonexists1 = jsOld.getJSONObject(j);
                        Iterator<String> keys = jsonexists1.keys();
                        while (keys.hasNext()) {
                            String keyData = keys.next();
                            String[] keyDetails = keyData.split(":");
                            if (keyDetails[0].equals(apName)) {
                                if (keyDetails[1].equals(resourceName)) {
                                    ver += keyDetails[2] + ",";
                                }
                            }
                        }
                    }
                    if (!ver.equals("")) {
                        String[] resourcesDetails = ver.split(",");
                        if (resourcesDetails != null) {
                            for (int i = 0; i < resourcesDetails.length; i++) {
                %>
                <option value="<%=resourcesDetails[i]%>"><%=resourcesDetails[i]%></option>               
                <%}
                    }
                } else {%>                 
                <option value="-1">Select Version</option>
                <%}%>
            </select>
        </div>
    </div>
