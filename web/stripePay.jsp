<script src="https://js.stripe.com/v3/"></script>
<script src="vendor/jquery/dist/jquery.min.js"></script>
<link href="styles/stripePage.css" rel="stylesheet" type="text/css"/>
<%
    String totalPaymentAmountWithTax = (String) session.getAttribute("_grossAmount");
%>
<form>    
  <label>
    <input name="cardholder-name" class="field is-empty" placeholder="Jane Doe" />
    <span><span>Name</span></span>
  </label>
  <label>
    <input class="field is-empty" type="tel" placeholder="(123) 456-7890" />
    <span><span>Phone number</span></span>
  </label>
  <label>
    <div id="card-element" class="field is-empty"></div>
    <span><span>Credit or debit card</span></span>
  </label>
    <button type="submit">Pay $<%=totalPaymentAmountWithTax%></button>
  <div class="outcome">
    <div class="error" role="alert"></div>
    <div class="success">
      Success! Your Stripe token is <span class="token"></span>
    </div>
  </div>
</form>
<script src="scripts/stripe.js" type="text/javascript"></script>