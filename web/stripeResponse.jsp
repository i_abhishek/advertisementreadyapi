<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.stripe.model.Plan"%>
<%@page import="com.stripe.model.Subscription"%>
<%@page import="com.stripe.model.ChargeOutcome"%>
<%@page import="com.stripe.model.BalanceTransaction"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="com.stripe.model.Charge"%>
<%--<%@include file="header.jsp" %>--%>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Page title -->
        <title>Ready APIs</title>
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->
        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
        <!-- App styles -->
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="styles/style.css">
        <script src="scripts/packageDetails.js" type="text/javascript"></script>
        <script src="scripts/packageOperation.js" type="text/javascript"></script>
    </head>
    <body class="blank">
        <style>
            .col-centered{
                float: none;
                margin: 0 auto;
            }
        </style>
        <!-- Simple splash screen-->
        <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="color-line"></div>
        <div id="wrapper" style="margin: 0 0 0 0px!important">
            <div class="content animate-panel">
                <div class="row  animate-panel" data-effect="rotateInDownRight" data-child="element">
                    <h3 style="text-align: center; margin: 3%">Payment Transaction Status</h3>
                    <div class="col-lg-5  col-centered">

                        <%
                            Subscription charge = (Subscription) session.getAttribute("chargeObj");
                            String packageName = (String) request.getSession().getAttribute("_originalPackageName");
                            String invoiceId = (String) request.getSession().getAttribute("_invoiceId");
                            String paidAmount = (String) request.getSession().getAttribute("_grossAmount");
                            PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
                            SgSubscriptionDetails subscriObject1 = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(parObj.getPartnerId());
                            boolean txnFlag = false;
                            String txId = "NA";
                            String TXN_STATUS = "Failed";
                            String AMOUNT = "NA";
                            String ERR_DESC = "NA";
                            String subpackageName = "NA";
                            if (charge != null) {
                                Plan planObj = charge.getPlan();
                                if (charge.getStatus() != null && charge.getStatus().equalsIgnoreCase("active")) {
                                    txnFlag = true;
                                }
                                if (charge.getStatus() != null) {
                                    TXN_STATUS = charge.getStatus();
                                }
                                if (charge != null) {
                                    txId = charge.getId();
                                }

                                if (planObj != null) {
                                    ERR_DESC = charge.getStatus();
                                }
                                if (packageName != null) {
                                    subpackageName = packageName;
                                }
                                if (paidAmount != null) {
                                    AMOUNT = paidAmount;
                                }
                            }
                        %>    
                        <!--<div id="wrapper">-->
                        <!--    <div class="small-header transition animated fadeIn">
                                <div class="hpanel">
                                    <div class="panel-body">
                                        <div id="hbreadcrumb" class="pull-right">
                                            <ol class="hbreadcrumb breadcrumb">
                                                <li><a href="home.jsp">Dashboard</a></li>
                                                <li class="active">
                                                    <span>Payment Response</span>
                                                </li>
                                            </ol>
                                        </div>
                                        <h2 class="font-light m-b-xs">
                                            Payment Response
                                        </h2>
                                        <small>Your online payment response</small>
                                    </div>
                                </div>
                            </div>-->
                        <!--        <div class="content animate-panel">
                                <div class="row">-->
                        <!--            <div class="col-md-12">-->
                        <div class="hpanel">
                            <div class="panel-heading hbuilt">
                                <div class="p-xs h4">
                                    Payment Status
                                </div>
                            </div>
                            <div class="panel-heading hbuilt">

                                <form class="form-horizontal" id="packagePaymentDetails"> 

                                </form>

                                <div class="p-xs">
                                    <form class="form-horizontal" method="post">
                                        <div class="form-group has-success">
                                            <%if (txnFlag) {%>
                                            <div class="col-sm-10 text-success">
                                                <span style="font-size: 15px;"><i class="glyphicon glyphicon-info-sign"></i> Payment has been made successfully.</span>
                                            </div>
                                            <%} else {%>
                                            <div class="col-sm-8 text-danger">
                                                <span style="font-size: 15px;"><i class="glyphicon glyphicon-info-sign"></i> Your payment request failed.</span>
                                            </div>
                                            <%}%>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4">Subscription Id      : </label> <%=txId%>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4">Invoice Id       : </label> <%=invoiceId%>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4">Package             : </label> <%=subpackageName%>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4">Amount              : </label>AUD <%=AMOUNT%>
                                        </div>
                                        <!--                        <div class="form-group">
                                                                    <label class="col-sm-3">Response Desc       : </label> <%=ERR_DESC%>
                                                                </div>                                                -->
                                        <div class="form-group">
                                            <label class="col-sm-4">Status  : </label> <%=TXN_STATUS%>
                                        </div>
                                        <%if (subscriObject1 != null) {%> 
                                        <div class="form-group">
                                            <div class="col-sm-2"><a class="btn btn-sm btn-success" href="header.jsp">Go Back to Home</a></div>
                                        </div>                        
                                        <%} else if (!txnFlag) {%>
                                        <div class="form-group">
                                            <div class="col-sm-2"><a class="btn btn-sm btn-success" href="packages.jsp">Go Back</a></div>
                                        </div>
                                        <%} else {%>
                                        <div class="form-group">
                                            <div class="col-sm-2"><a class="btn btn-sm btn-success" href="header.jsp">Go Back to Home</a></div>
                                        </div>  
                                        <%}%>
                                    </form>                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <strong>Powered by Service Guard And Axiom Protect 2.0</strong>
                    </div>
                    <%
                        java.util.Date dFooter = new java.util.Date();
                        SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
                        String strYYYY = sdfFooter.format(dFooter);
                    %>
                    <div align="center"  class="col-md-12 text-center">
                        <p>&copy; BlueBricks Technologies 2009-<%=strYYYY%> (<a href="http://www.blue-bricks.com" target="_blank">www.blue-bricks.com</a>)</p>
                    </div>
                </div>
            </div>
        </div>
        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="vendor/sparkline/index.js"></script>
        <script src="vendor/ladda/dist/spin.min.js"></script>
        <script src="vendor/ladda/dist/ladda.min.js"></script>
        <script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
        <script src="vendor/toastr/build/toastr.min.js"></script>
        <script src="scripts/utilityFunction.js" type="text/javascript"></script>
        <!-- App scripts -->
        <script src="scripts/homer.js"></script>
        <%if (txnFlag) {%>                
        <script>
            function strCompare(a, b)
            {
                return (a < b ? -1 : (a > b ? 1 : 0));
            }
            function subscribePackage() {
                var paymentMode = '';

                var s = './SubscribePackage';
                if (paymentMode === 'postpaid') {
                    s = './PostpaidPayment';
                }
                //pleasewait();
                $.ajax({
                    type: 'POST',
                    url: s,
                    dataType: 'json',
                    //data: $("#packagePaymentDetails").serialize(),
                    success: function (data) {
                        if (strCompare(data.result, "error") === 0) {

                        } else if (strCompare(data.result, "success") === 0) {
                            setTimeout(function () {
                                window.location = "./header.jsp";
                            }, 3000);
                        } else if (strCompare(data.result, "redirect") === 0) {
                            waiting.modal('hide');
                            showAlert(data.message, "danger", 6000);
                            var packageName = data.packageName;
                            setTimeout(function () {
                                window.location = "./invoiceDetail.jsp?package=" + packageName;
                            }, 4000);
                        }
                    }
                });
            }

            $(document).ready(function () {
                subscribePackage();
            });

        </script>
        <%}%>

    </body>
</html> 
