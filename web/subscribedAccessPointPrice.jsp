<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%

        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_ChannelId");
        String apName = request.getParameter("_apname");
        String packageName = request.getParameter("_packageName");
        String resourceName = request.getParameter("_resourceName");
        String version = request.getParameter("versionData");

        PartnerDetails partnerObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        SgSubscriptionDetails reqObj = new PackageSubscriptionManagement().getSubscriptionbyPartnerId(SessionId, channelId, partnerObj.getPartnerId());
        String apDetails = reqObj.getApRateDetails();
        JSONArray jsOld = new JSONArray(apDetails);
        String ver = "";
        String key = apName + ":" + resourceName + ":" + version;
        JSONObject reqJSONObj = null;
        for (int j = 0; j < jsOld.length(); j++) {
            JSONObject jsonexists1 = jsOld.getJSONObject(j);
            if (jsonexists1.has(key)) {
                reqJSONObj = jsonexists1.getJSONObject(key);
                if (reqJSONObj != null) {
                    break;
                }
            }
        }
        if (reqJSONObj != null) {
            Iterator<String> keys = reqJSONObj.keys();
            String mainCredit = reqJSONObj.getString("mainCredit");
            String freeCredit = reqJSONObj.getString("freeCredit");
            while (keys.hasNext()) {
                String keyData = keys.next();
                String valueData = reqJSONObj.getString(keyData);

                if (keyData.equals("freeCredit") || keyData.equals("mainCredit")) {
                    continue;
                }

    %>
    <br><br>
    <div class="form-group">
        <label class=" control-label col-lg-3"><%=keyData%></label>
        <div class="col-lg-3" >
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" class="form-control" placeholder="apiPrice" id="apiPrice" name="apiPrice" disabled value="<%=valueData%>">                                                                  
            </div>
        </div>
    </div>

    <%
            }
        }

    %>


