<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
      	<meta name="viewport" content="width=device-width, initial-scale=1">
      	
      	<title></title>
      
      	<!--Including Bootstrap style files-->
      	 <link href="vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      	
		<style>
            tr {
                line-height: 30px;
            }
            td {
                padding: 5px;
            }

            .divBorder {
	            margin-top:10px;
	            border: #eeeeee medium solid;
	            border-radius: 10px;
	            -moz-border-radius: 10px;
	            -webkit-border-radius: 10px;
                padding: 15px;
            }

            .wSmall {
                width: 120px;
            }
            
            .wMedium {
                width: 200px;
            }
            
            .colCentered{
			    float: none;
			    margin: 0 auto;
			}
            
            #card-number, #cvv, #expiration-date {
				-webkit-transition: border-color 160ms;
				transition: border-color 160ms;
				height: 25px;
				width: 250px;
				-moz-appearance: none;
				border: 0 none;
				border-radius: 5px;
				box-shadow: 0 0 4px 1px #a5a5a5 inset;
				color: #DDDBD9;
				display: inline-block;
				float: left;
				font-size: 13px;
				height: 40px;
				margin-right: 2.12766%;
				padding-left: 10px;
			}
        </style>     	
  	</head>  	
	<body>
            
		<div class="container-fluid">

            <div class="well">
                <h2 class="text-center">Payment Status</h2>
            </div>
            
        </div>

		<div class="container-fluid">
            <div class="row">

                <div class="col-sm-4 colCentered" >

    				<h3> <%=request.getAttribute("first-name") %>, thank you for your payment! </h3>
    				<br />
    				
    				<h4> Address</h4>
    				<br />
    				 
    				<address>
						<strong><%= request.getAttribute("first-name") %> <%= request.getAttribute("last-name") %></strong><br />
						<%= request.getAttribute("line1") %><br />
						<%= (request.getAttribute("line2") == null || request.getAttribute("line2") == "") ? "" : request.getAttribute("line2") + "<br />" %> 
						<%= request.getAttribute("city") %>, <%= request.getAttribute("state") %>  <%= request.getAttribute("postal-code") %><br />
 						<%= request.getAttribute("country-code") %>
					</address>
    						
    				<br />
    				<br />
    				
    				<h4>Transaction Details</h4>
    				<br />
    				
    				<table class="table">
                        <tr>
                        	<th>Transaction ID:</th>
                        	<td><%= request.getAttribute("transactionid") %></td>
                        </tr>
                        <tr>
                        	<th>State:</th>
                        	<td><%= request.getAttribute("status") %></td>
                        </tr>
                        <tr>
                        	<th>Total Amount:</th>
                        	<td>$ <%= request.getAttribute("total-amount") %></td>
                        </tr>
                    </table>
                    
    				<br />
	
					<h3>Return to <a href='home.jsp'>Home Page</a></h3>
					
  				</div>
  			</div>
		</div>
		
      	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script> 
		
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="vendor/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>


		<br />
	</body>
</html>
