/* eslint-env browser, serviceworker, es6 */
'use strict';


const applicationServerPublicKey = 'BHJbjjDO-6F-eXTqtVVu_aHZ_yssYATwe-3bU8pqXwe8Xik0lkq6LI9rOojUbVqh46GQrxoObj4A4zv_T9hBpZ0';  //DO NOT CHANGE THESE KEY

/* eslint-enable max-len */

function urlB64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
            .replace(/\-/g, '+')
            .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}



var siteURL = "";
self.addEventListener('push', function (event) {
    console.log('Received a push message', event);
    var json = event.data.json().message[0];
    var title;
    var body;
    var icon;
    var tag;
    var payload = event.data ? JSON.parse(json) : {
        title: 'ReadyAPI Push Notification',
        body: 'We have received a push message.',
        icon: 'http://website.mollatech.com/wp-content/uploads/2017/05/favicon-1.jpg',
        url: 'https://www.mollatech.com'
    };
    siteURL = payload.url;

    return self.registration.showNotification(payload.title, {
        body: payload.body,
        icon: payload.icon,
        tag: payload.url + payload.body + payload.icon + payload.title,

        actions: [
            {action: 'Yes', title: 'Yes', icon: 'https://cdn2.iconfinder.com/data/icons/oxygen/48x48/actions/ok.png'},
            {action: 'No', title: 'No', icon: 'https://cdn3.iconfinder.com/data/icons/freeapplication/png/24x24/Erase.png'}]
    });

    event.waitUntil(
            self.registration.showNotification(title, {

                body: body,
                icon: icon,
                tag: tag,
                actions: [
                    {action: 'Yes', title: 'Yes', icon: 'https://cdn2.iconfinder.com/data/icons/oxygen/48x48/actions/ok.png'},
                    {action: 'No', title: 'No', icon: 'https://cdn3.iconfinder.com/data/icons/freeapplication/png/24x24/Erase.png'}]
            })
            );
});


self.addEventListener('notificationclick', function (event) {
    console.log('[Service Worker] Notification click Received.');

    event.notification.close();

    event.waitUntil(
            clients.openWindow(siteURL)
            );
});

self.addEventListener('pushsubscriptionchange', function (event) {
    console.log('[Service Worker]: \'pushsubscriptionchange\' event fired.');
    const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
    event.waitUntil(
            self.registration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: applicationServerKey
            }).then(function (newSubscription) {
        /* TODO: Send the subscription object to application server.
         *       notifications are sent from the server using this object.
         */
        console.log('[Service Worker] New subscription: ', newSubscription);
        console.log(JSON.stringify(newSubscription));
    })
            );
});
