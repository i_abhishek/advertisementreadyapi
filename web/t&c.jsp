<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>Ready APIs </title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="styles/style.css">

</head>
<body class="blank" style="background: rgba(43,48,131,1);
background: -moz-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
background: -webkit-gradient(left top, right top, color-stop(0%, rgba(43,48,131,1)), color-stop(50%, rgba(35,129,196,1)), color-stop(100%, rgba(43,48,131,1)));
background: -webkit-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
background: -o-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
background: -ms-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
background: linear-gradient(to right, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2b3083', endColorstr='#2b3083', GradientType=1 );">

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="color-line"></div>
<div class="legal-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3 style="color:#fff">Terms and conditions</h3>
                <small></small>
            </div>
            <div class="hpanel">
                <div class="panel-body">
					<h3>1.0 ACCEPTANCE OF TERMS - GENERAL</h3>
					<p>1.1 The use of Blue Bricks Website is subject to the terms and conditions ("T&C") provided herein. By accessing and using Blue Bricks Website you agree to be bound by this T&C and any modifications and amendments, which may be posted by Blue Bricks on Blue Bricks Website from time to time.</p>
					<p>1.2 You agree that Blue Bricks may modify and amend this T&C at any time without notice. Unless explicitly stated otherwise, any new service introduced by Blue Bricks, shall be subject to this T&C. All such notices are hereby incorporated by reference into the T&C. You are responsible for regularly reviewing Blue Bricks’s T&C posted on Blue Bricks Website. By continuing to use Blue Bricks Website, you are deemed to have agreed to be bound by any modifications and amendments to this T&C.</p>
					<p>1.3  You can access Blue Bricks Website home page and browse the Website without disclosing your personal data. The service and links of the Blue Bricks Website may include links to third party websites and are subject to Section 7 herein.</p>
					<p>1.4 Owing to the global nature of the Internet infrastructure, the information you provide may be transferred in transit to countries that do not have similar protection regarding your data and it’s use as set out in this T&C. By submitting your information you consent to these transfers.</p>
					<p>1.5 "You" shall mean any user of Blue Bricks Website.</p>
					<p>1.6 "Blue Bricks" shall mean Blue Bricks Pty Ltd and "Blue Bricks Group" shall mean any subsidiary and affiliate companies of Blue Bricks Pty Ltd.</p>
					<p>1.7 "Blue Bricks Service" shall mean the telecommunications and multimedia services and products provided and made available to you by Blue Bricks Group.</p>
					
					<h3>2.0 USE AND SECURITY</h3>
					<p>2.1 Information on Blue Bricks Website may contain technical inaccuracies or typographical errors. Information may be changed or updated without notice. Blue Bricks may also make improvements and/or changes to Blue Bricks Website and to the fees and rates for Blue Bricks Services, at any time without notice.</p>
					<p>2.2 Blue Bricks Website may not be available to all devices which connect to the Internet. You assume all responsibility regarding the end-user equipment and software necessary to access Blue Bricks Website and assume all risk associated therewith.</p>
					<p>2.3 Blue Bricks Website may not be available to be accessed from all regions and countries of the world. Blue Bricks reserves the right, in its sole discretion, to limit or terminate your ability to access Blue Bricks Website from certain regions or countries, and you assume all risk associated with the accessibility of Blue Bricks Service on Blue Bricks Website from any given region or country of the world.</p>
					<p>2.4 Blue Bricks reserves the right to limit or prohibit your entry into its website in its sole discretion and shall not be liable for such limitation or prohibition.</p>
					
					<h3>3.0 YOUR CONDUCT</h3>
					<p>3.1 	You agree to use Blue Bricks Website only for lawful purposes and in accordance with this T&C.</p>
					<p>3.2 You agree to notify Blue Bricks immediately of any unauthorised use or any other breach of security at the end of each session. Blue Bricks cannot and will not be liable for any loss or damage arising from your failure to comply with this provision.</p>
					<p>3.3 You agree to abide by all applicable local, state, national and international laws and regulations and are solely responsible for all acts or omissions that occur under your usage, including the content of your communication through Blue Bricks Service. Recognising the global nature of the Internet, you agree to comply with all local rules regarding online conduct. Specifically, you agree to comply with all applicable laws regarding online communication in the country in which you reside.</p>
					<p>3.4 You may not use Blue Bricks Website in any manner that could damage, disable, overburden, or impair the Blue Bricks Website (or the network(s) connected to Blue Bricks Service) or interfere with any other party's use and enjoyment of Blue Bricks Service. You may not attempt to gain unauthorised access to the Blue Bricks Website, computer systems or networks connected to Blue Bricks Website, through hacking, password mining or any other means. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available through Blue Bricks Website.</p>
					
					<h3>4.0 TERMINATION/ACCESS RESTRICTION</h3>
					<p>4.1 Without limiting anything herein contained, Blue Bricks reserves the right to terminate Blue Bricks Website at any time without notice and for any reason.</p>
					<p>4.2 In addition to any termination rights Blue Bricks may have under this T&C, Blue Bricks may terminate your access to Blue Bricks Website without notice and in its own discretion in the occurrence of the following:</p>
					<ul>
						<li>You materially violate this T&C or any notice posted from time to time on the Blue Bricks Website, any applicable law, rule or regulation relating to the use of the Blue Bricks Service; or</li>
						<li>Any law, regulation, or governmental action renders all or any portion of the Blue Bricks Website (or your use of the Blue Bricks Service) unlawful or impracticable; or</li>
						<li>Your use of Blue Bricks Website impairs or threatens to impair the integrity or functionality of Blue Bricks network in any manner; or</li>
						<li>If Blue Bricks believes that you have violated or acted inconsistently with the intention or spirit of this T&C.</li>
					</ul>
					<p>4.3 You agree that Blue Bricks shall not be liable to you or any third-party for any termination of your access to the Blue Bricks Website or your Blue Bricks Account.</p>

					<h3>5.0 DISCLAIMER & LIMITATION OF LIABILITY</h3>
					<p>5.1 Blue Bricks makes no warranty that Blue Bricks Website will meet your requirement or that Blue Bricks Website will be secure, timely, uninterrupted or error free, or that any data, content, information, software or other material accessible on or through Blue Bricks Website are true, accurate, or are free of viruses or other harmful components.</p>
					<p>5.2 You are responsible for implementing sufficient procedures and checkpoints to satisfy your particular requirements for accuracy of data input and output, and for maintaining a means external to Blue Bricks Website for the reconstruction of any lost data. All use of Blue Bricks Website is at your own risk and you are solely responsible for all damages resulting therefrom.</p>
					<p>5.3 Blue Bricks Website is not intended for "mission critical" circumstances and is provided on an "as is," where is" and "as available" basis, without warranties of any kind, express or implied, including, but not limited to warranties of title, quiet enjoyment, non-infringement or implied warranties of merchantability or fitness for a particular purpose.</p>
					<p>5.4 To the maximum extent permitted by applicable law, Blue Bricks disclaims all warranties, express or implied, including but not limited to implied warranties of merchantability, fitness for a particular purpose, title and non-infringement with respect to Blue Bricks Website and transactions performed through the service or on the Internet generally. Blue Bricks makes no representations or warranties as to the quality of the call or any connection to or any transmission over the Internet.</p>
					<p>5.5 To the fullest extent permitted by applicable law, under no circumstances, including, but not limited to negligence, shall Blue Bricks be liable for any direct, indirect, incidental, special or consequential damages, damages for loss of profits (even if Blue Bricks has been advised of the possibility of such damages or even if such damage is foreseeable), that result from the use of or the inability to use Blue Bricks Website, any changes to Blue Bricks Website and unauthorized access to or alteration of your transmissions or data, any communication, transmission, material or data sent or received or not sent or received, or any transactions entered into through Blue Bricks Website or arising in connection with the fraudulent or unlawful acts of any third party.</p>
					<p>5.6 You specifically agree that Blue Bricks is not responsible or liable for any threatening, defamatory, obscene, offensive or illegal content or conduct of any other party or any infringement of another's rights, including intellectual property rights.</p>
					<p>5.7 If you are dissatisfied with Blue Bricks Website, the materials available on or through the Blue Bricks Service, Website or with the provisions of this T&C, you agree that your sole and exclusive remedy is to discontinue using Blue Bricks Website or any of Blue Bricks’s other products and services.</p>
					<p>5.8 Blue Bricks will take reasonable measures to maintain the privacy and security of all private information provided by you to Blue Bricks Website, but third parties (such as hackers) may breach or attempt to breach Blue Bricks's security measures or may gain unauthorized access to Blue Bricks database or other equipment containing your information. You agree that Blue Bricks shall not be liable for damages of any sort, whether arising under contract, tort, or otherwise, with respect to any breach of security of Blue Bricks Website or any other company equipment or user information.</p>
					
					<h3>6.0 INDEMNITY</h3>
					<p>6.1 You agree to indemnify, defend and hold Blue Bricks, its officers, directors, employees, agents, shareholders, licensors, suppliers and any third party information providers to Blue Bricks Service and Website harmless from and against all losses, expenses, damages and costs, including attorneys’ fees, resulting from any violation by you of this T&C or asserted by any third party due to or arising out of your use of or conduct on Blue Bricks Website.</p>
					
					<h3>7.0	LINKS TO THIRD PARTY SITES</h3>
					<p>7.1 Blue Bricks Website may contain links to third party websites ("Linked Websites") and include advertisements. You agree that Linked Websites and advertisements are necessary for Blue Bricks to provide Blue Bricks Service. Such links and advertisements are provided to you only as a convenience. Blue Bricks shall not be responsible if the Linked Website is not working appropriately. Blue Bricks has no control over such websites and resources and shall not be responsible for the contents of any Linked Website or advertisement, including without limitation any link contained in a Linked Website and does not endorse any content, advertising, products, or other materials on or available from such websites or resources. You are responsible for viewing and abiding by the privacy statements and terms of use posted at the Linked Websites, and for taking precaution to ensure that whatever you select for your use is free of viruses, worms, Trojan horses and other items of a destructive nature.</p>
					<p>7.2 Any dealings with third parties (including advertisers) over the Linked Websites or participation in promotions, including the delivery of and the payment for goods and services, and any other terms, conditions, warranties or representations associated with such dealings or promotions, are solely between you and the advertiser, merchant or other third party. You agree that Blue Bricks shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such website or resource. The purchase of any product or service from a merchant from a Linked Website is a transaction solely between you and that merchant, and any question or dispute you may have regarding any such product or service should be addressed directly to the responsible merchant. Blue Bricks has no responsibility or liability for any merchant or your relationship with such merchant.</p>
					
					<h3>8.0	DISCLOSURE AND CONFIDENTIALITY</h3>
					<p>8.1 Blue Bricks reserves the right to disclose any personal information about you or your usage of the Blue Bricks Service without your prior permission if Blue Bricks in good faith, believes that such action is necessary to: (1) conform to legal requirements or comply with legal process; (2) protect and defend the rights or property of Blue Bricks or its holding or affiliated company;(3) enforce this T&C; or (4) act to protect the interests of its members or others.</p>
					<p>8.2 Any non-personal information or material sent to Blue Bricks by you will generally NOT be treated as confidential.</p>
					
					<h3>9.0 PROPRIETORY RIGHTS COPYRIGHT</h3>
					<p>9.1 Blue Bricks Website, all images, text, graphics, logos, sound, programs and any other material ("Content") found on Blue Bricks Website are protected by trademark, copyright, or other intellectual property laws and international treaties. Any commercial use of Blue Bricks Website and Content found on Blue Bricks Website is strictly prohibited, without Blue Bricks’s express and prior written consent. Any reproduction or redistribution of Blue Bricks Service, and Content found on Blue Bricks Website, not in accordance with the T&C herein is expressly prohibited by law, and may result in severe civil and criminal penalties.
					<p>9.2 Elements of Blue Bricks Website, its design and layout are protected by trade dress and other laws and may not be copied or imitated in whole or in part.
					<p>9.3 No logo, graphic, sound or image from this Website may be copied or retransmitted unless expressly permitted by Blue Bricks.
					<p>9.4 Blue Bricks respects the trademark, copyrighted materials or any intellectual property (hereby collectively referred as "Intellectual Property") of others. Should you have a reason to believe that your Intellectual Property has been used by Blue Bricks in a way that constitutes an infringement, and/or that your Intellectual Property rights have been otherwise violated (hereby referred as "Infringing Materials), please notify Blue Bricks via an electronic mail at tmwebmaster@tm.com.my (hereby referred as "Notice") and upon obtainment of such, Blue Bricks shall remove and/or disable  the access to the Infringing Materials within forty-eight</p>
					(48) hours from the time of receipt of the following information:- <br/>
					(a)	an electronic and/or physical signature of the proprietor, owner, authorized agent and/or evidence indicating the proprietorship or ownership of the Intellectual Property; <br/>
					(b)	a clear and brief description of the Infringing Materials and the Uniform Resource Locator ("URL"), with adequate details that make it able for Blue Bricks to locate the whereabouts of the Infringing Materials;<br/>
					(c)	a written undertaking that you have a bona fide (good faith) belief that the use by Blue Bricks is with no authorization under the law, by the proprietor, owner or authorized agent of the Intellectual Property and the information provided herein is accurate and that you are the proprietor, owner or authorized agent of the Intellectual Property.
					
					<h4>GRANT OF LICENSE AND RESTRICTIONS </h4>
					<p>9.5 You acknowledge and agree that Blue Bricks Website and any necessary software used in connection with Blue Bricks Website ("the Software") contain proprietary and confidential information that is protected by applicable intellectual property and other laws. Blue Bricks reserves all rights in the not expressly granted herein, including without limitation ownership and proprietary rights.</p>
					<p>9.6 The Software is made available for downloading solely for use by you for your individual, non transferable, non-exclusive and non-commercial use, use in connection with Blue Bricks Service. The Software is made available solely for use by you in accordance with the terms of this T&C and the terms of any end user license agreement ("License Agreement") which accompanies or is included with the Software. You may not install or use any part of the Software unless you first agree to the terms of this T&C and any License Agreement.</p>
					<p>9.7 You may not copy the Software to any other server or location for further reproduction or redistribution. You may not reverse engineer, decompile, disassemble or modify the Software, except insofar as such restriction is prohibited by law.</p>
					<p>9.8 Any reproduction or redistribution of the Software not in accordance with this T&C and any License Agreement is expressly prohibited by law, and may result in severe civil and criminal penalties. Violators will be prosecuted to the maximum extent possible.</p>
					<p>9.9 Without limiting the provisions of Section 5 above, the Software is provided "as is," and Blue Bricks hereby disclaims all warranties and conditions with regard to the Software, including all implied warranties and conditions of merchantability, fitness for a particular purpose, title and non-infringement.</p>
					
					<h4>TRADEMARKS AND THIRD PARTY MATERIALS </h4>
					<p>9.10 Blue Bricks reserves all rights in its corporate names, service marks, logos, trade names, trademarks, websites and domain names (collectively "Marks") and nothing in this T&C grants you the license to use such Marks. Additionally, other marks which appear on this site may be marks of third parties that are not affiliated with Blue Bricks. Blue Bricks and Blue Bricks Group do not control or endorse the content of third party websites.</p>  

					<h3>10.0 GENERAL</h3>
					<p>10.1 This T&C are governed by the laws of Malaysia, without reference to conflict of laws principles. Any dispute between you and Blue Bricks regarding this T&C will be subject to the exclusive jurisdiction of the courts located in Malaysia and you expressly waive all defenses to jurisdiction. You agree that Blue Bricks Website shall, for the purpose of determining jurisdiction and applicable law, be deemed to have originated and been provided by Blue Bricks in and from Malaysia.</p>
					<p>10.2 This T&C contain the entire understanding between the parties hereto with respect to Blue Bricks Website, and supersedes all previous oral or written agreements or understandings between you and Blue Bricks, and no advice or information, whether oral or written, obtained by you from this Website (whether before or after the date you accept this T&C) shall create any obligation or warranty on the part of Blue Bricks not expressly stated herein. You may also be subject to additional terms and conditions that may apply when you use any services offered by Blue Bricks Group, third party content or third-party software. Neither the course of conduct between the parties nor trade practice shall act to modify any provision of this T&C.</p>
					<p>10.3 Blue Bricks's performance of this T&C is subject to existing laws and legal process, and nothing contained in this T&C is in derogation of Blue Bricks's right and obligation to comply with governmental, court and law enforcement requests or requirements relating to your use of Blue Bricks Website or information provided to or gathered by Blue Bricks with respect to such use.</p>
					<p>10.4 If any provision of this T&C is held invalid, the remainder of this T&C will continue in full force and effect, and if any provision(s) of the T&C is held by a court of competent jurisdiction to be contrary to law, then such provision(s) shall be construed, as nearly as possible, to reflect the intentions of the parties with the other provisions remaining in full force and effect. This T&C shall not be construed against any party as the principal draftsperson hereof. A printed version of this T&C and of any notice given in electronic form shall be admissible in judicial or administrative proceedings.</p>
					<p>10.5 Blue Bricks's failure to insist upon or enforce strict performance of any provision of this T&C shall not be construed as a waiver of any provision or right unless acknowledged and agreed to by Blue Bricks in writing.</p>
					<p>10.6 Notices to you shall not be modified, except by an express modification by Blue Bricks as described herein and may be made via email or regular mail or by displaying notices or links to you on Blue Bricks Website.</p>
					<p>10.7 Blue Bricks reserves the right to amend any of the provisions of this T&C from time to time. Amendments will be effective immediately upon notification on Blue Bricks website. Your continued use of Blue Bricks Website and subscription to Blue Bricks Service will represent an agreement by you to be bound by this T&C as amended.</p>
					<p>10.8 You may not assert any claim against Blue Bricks in connection with Blue Bricks Website unless you have given Blue Bricks written notice of the claim within fourteen (14) days after you knew or should have known of the facts giving rise to such claim. You agree that any cause of action arising out of or related to Blue Bricks Website must be initiated within one (1) year after the cause of action arose; otherwise, such cause of action is permanently barred.</p>
					<p>10.9 Blue Bricks may assign its rights and duties under this T&C to any party at any time without notice to you and this T&C shall be binding upon and inure to the benefit of each party's respective permitted successors and assignees.</p>
					
					<p>Should you have any questions concerning this T&C, or if you desire to contact Blue Bricks for further inquiries, please visit our Customer Support section of this Website. </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center" style="color:#fff">
            <strong>Ready API</strong>
        </div>
    </div>
</div>

<!-- Vendor scripts -->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/sparkline/index.js"></script>

<!-- App scripts -->
<script src="scripts/homer.js"></script>

</body>
</html>
