<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MSConfig"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.AccessPolicy"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%--<%@include file="header.jsp"%>--%>
<script src="scripts/apiTest.js" type="text/javascript"></script>
<script src="scripts/tokenManagement.js" type="text/javascript"></script>
<%    String _resId = request.getParameter("_resId");
    int resId = Integer.parseInt(_resId);
    //String value = request.getParameter("value");
    String _apid = request.getParameter("_apid");
    String envmt = request.getParameter("envt");
    int apId = Integer.parseInt(_apid);
    String mName = request.getParameter("methodName");

    String requestType = request.getParameter("type");
    String restType = request.getParameter("restType");
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
    Accesspoint ap = new AccessPointManagement().getAccessPointById(SessionId, ChannelId, apId);
    String apName = ap.getName();
    TransformDetails tf = new TransformManagement().getTransformDetails(SessionId, ChannelId, apId, resId);
    Warfiles warFiles = new WarFileManagement().getWarFile(SessionId, ChannelId, ap.getApId());
    Map map = (Map) Serializer.deserialize(warFiles.getWfile());
    ResourceDetails details = new ResourceManagement().getResourceById(Integer.parseInt(_resId));
    String ver = "1";

    for (Object key : map.keySet()) {
        if (key.toString().split(":")[1].equalsIgnoreCase("Running")) {
            ver = key.toString().split(":")[0].replace("SB", "");
        }
    }
    Map methodMap = (Map) request.getSession().getAttribute("apiConsolemethods");
    Methods methods = (Methods) methodMap.get(apName);
    int version = Integer.parseInt(ver);
%>
<!-- Main Wrapper -->
<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li style="font-size:18px !important;">Resource 
                        <select style="width: 200px" id="apiConsoleSelectService" name="apiConsoleSelectService" onchange="getAPIList(this.value)">
                            <optgroup label="Select Service">
                                <%
                                    Accesspoint[] accessObj = new AccessPointManagement().getAceesspoints();
                                    Warfiles warfiles = null;
                                    if (accessObj != null) {
                                        for (int acci = 0; acci < accessObj.length; acci++) {
                                            if (accessObj[acci].getStatus() != GlobalStatus.DELETED && accessObj[acci].getStatus() != GlobalStatus.SUSPEND) {
                                                Accesspoint apdetails = accessObj[acci];
                                                TransformDetails transformDetails = new TransformManagement().getTransformDetails(apdetails.getChannelid(), apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                                                if (transformDetails != null) {
                                                    warfiles = new WarFileManagement().getWarFile(apdetails.getChannelid(), apdetails.getApId());
                                                    if (warfiles != null) {
                                                        String[] resources = apdetails.getResources().split(",");
                                                        ResourceDetails resObj = null;
                                                        if (resources != null) {
                                                            for (int j = 0; j < resources.length; j++) {
                                                                resObj = new ResourceManagement().getResourceById(Integer.parseInt(resources[j]));
                                                                int passId = ap.getApId();
                                                                int passresId = resObj.getResourceId();
                                                                String apNameS = apdetails.getName();
                                                                if (apdetails.getDisplayName() != null) {
                                                                    apNameS = apdetails.getDisplayName();
                                                                }
                                                                if (passId == apdetails.getApId() && passresId == resObj.getResourceId()) {
                                %>
                                <option value="<%=apdetails.getApId()%>:<%=resObj.getResourceId()%>:<%=apNameS%>" selected><%=apNameS%></option>
                                <%                      } else {
                                %>
                                <option value="<%=apdetails.getApId()%>:<%=resObj.getResourceId()%>:<%=apNameS%>"><%=apNameS%></option>
                                <%
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                %>                                                    
                                <option value="0:0">NA</option>                            
                                <%}%>  
                            </optgroup>
                        </select>
                        <font id="apiListAsPerService" style="width: 20%"></font>
                        <font id="apiRequestAsPerService" style="width: 20%"></font>
                        <a href="#APIDescOnPage" class="btn btn-info btn-xs" style="font-size: 14px!important;margin-bottom: 4px" data-toggle="tooltip" data-placement="right" title="API Description"><i class="fa fa-info-circle"></i></a>
                    </li>

                </ol>
            </div>
            <h2 class="font-light m-b-xs" style="font-size: 22px!important">
                <font style="font-weight: 80%">Test Console</font>
            </h2>

        </div>
    </div>
</div>
<!--<div class="small-header transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    <li style="font-size:18px !important;">Resource
                        <select class="js-source-states" style="width: 20%"  >
                            <optgroup label="Select Service">
                                  
</optgroup>
</select>
<font id="apiListAsPerService" style="width: 20%"></font>
<font id="apiRequestAsPerService" style="width: 20%"></font>
</li>

</ol>
</div>
<h2 class="font-light m-b-xs">
API Console
</h2>

</div>
</div>
</div>-->



<div class="transition animated fadeIn">	        
    <div id="apiConsoleWindow" class="content animate-panel">
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br>
    </div>                
    <form class="form-horizontal" id="assign_token_form" name="assign_token_form" role="form">
        <input type="hidden" name="apName" id="apName" value="<%=apName%>">
        <input type="hidden" name="resourceName" id="resourceName" value="<%=details.getName()%>">
        <input type="hidden" name="version" id="version" value="<%=version%>">
    </form>
    <div>           
        <form class="form-horizontal" id="apiConsoleData" name="apiConsoleData" role="form">
            <input type="hidden" name="_resId" id="_resId" value="<%=_resId%>">
            <input type="hidden" name="_apid" id="_apid" value="<%=_apid%>">
            <input type="hidden" name="envt" id="envt" value="<%=envmt%>">
            <input type="hidden" name="type" id="type" value="<%=requestType%>">
            <input type="hidden" name="restType" id="restType" value="<%=restType%>">                
        </form>
    </div>
    <script>

        getAPIList('<%=ap.getApId()%>:<%=details.getResourceId()%>', '<%=mName%>');


    </script>
</div>


<!-- Footer-->
<%--<%@include file="footer.jsp"%>--%>
