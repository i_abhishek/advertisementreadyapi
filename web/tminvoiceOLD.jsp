<%@page import="java.util.Calendar"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PgDailyTxManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDailyPgtx"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PgHourlyTXManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgHourlyPgtx"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TXManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgTranscationdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CAASCDRCalculationManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CDRTxManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCaastransactiondetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SlabPriceCalculationManagement"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDailytransactiondetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.DailyTxManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TaxCalculationManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.LatePenalitiesCalculationManagement"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPaymentdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.GenerateInvoiceId"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartnerrequest"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Developer Portal</title>
        <!-- Bootstrap Core CSS -->
<!--        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>-->
        <link href="vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!--    <link href="invoice_css/invoiceDetail.css" rel="stylesheet" type="text/css"/>
            <link href="invoice_css/style.css" rel="stylesheet" type="text/css"/>-->
        <!-- Custom CSS -->        
        <link href="styles/shop-homepage.css" rel="stylesheet" type="text/css"/>
        <script src="jshash/sha1-min.js" type="text/javascript"></script>
        <script src="jshash/sha1.js" type="text/javascript"></script>        
        <script src="scripts/paymentOperation.js" type="text/javascript"></script>
<!--        <script src="js/promocode.js" type="text/javascript"></script>-->
        <!-- jQuery -->
<!--        <script src="js/jquery.js"></script>-->
        <script src="vendor/jquery/dist/jquery.js" type="text/javascript"></script>        
<!--        <script src="js/jquery-ui-1.10.4.min.js"></script>-->        
        <script src="vendor/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<!--        <script src="js/jquery-1.8.3.min.js"></script>-->
        <script src="vendor/jquery/dist/jquery.min.js" type="text/javascript"></script>
<!--        <script src="js/bootstrap.min.js"></script>-->
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<!--        <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>-->
<!--        <script src="./js/modal.js"></script>-->
    </head>
    <%
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String ChannelId = (String) request.getSession().getAttribute("_ChannelId");
        if (SessionId == null) {
            response.sendRedirect("logout.jsp");
            return;
        }
        String packageName = request.getParameter("_packageName");
        String paymentMode = request.getParameter("_paymentMode");       
        String caasAccessPointName  = "caas";
        Integer caasAccessPointId = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        SimpleDateFormat dailyTxFormat = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat apiCallDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        NumberFormat form = new DecimalFormat("#0.00");
        String invoiceid = GenerateInvoiceId.getDate() + parObj.getPartnerId() + GenerateInvoiceId.getRandom();

        SgReqbucketdetails packageObject = new RequestPackageManagement().getReqPackageByName(SessionId, ChannelId, packageName);
        SgSubscriptionDetails subscriObj = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(parObj.getPartnerId());
        Accesspoint accesspoint = new AccessPointManagement().getAccessPointByName(SessionId, ChannelId, caasAccessPointName);
        if(accesspoint != null){
            caasAccessPointId = accesspoint.getApId();
        }
        Double changeOnPackageCharge = null;
        Double reActivationCharge = null;
        Double cancellationCharge = null;
        Double latePenaltyCharge = null;

        String outPackageName = subscriObj.getBucketName();
        Map<String, Double> packageamount = new LinkedHashMap<String, Double>();
        Map<String, String> taxMap = new LinkedHashMap<String, String>();
       JSONObject caasCDRPrice = new JSONObject();
        Double totalAmountWithoutTax = 0.00;
        Double gstTax = 0.00;
        Double vatTax = 0.00;
        Double serviceTax = 0.00;
        String taxRate = null;
        if (subscriObj != null) {
            double planAmount = subscriObj.getPlanAmount();
            packageamount.put("Package Amount", planAmount);
            packageName = subscriObj.getBucketName();
            paymentMode = subscriObj.getPaymentMode();
            double serviceCharge = Double.parseDouble(subscriObj.getServiceCharge());
            packageamount.put("Service Charge", serviceCharge);
            if (packageObject != null) {
                if (!packageObject.getBucketName().equals(subscriObj.getBucketName())) {
                    double changePackageCharge = subscriObj.getChangePackageRate();
                    packageamount.put("Change Package Charge", changePackageCharge);
                    changeOnPackageCharge = changePackageCharge;
                }
                if (packageObject.getBucketName().equals(subscriObj.getBucketName())) {
                    double reactivationCharge = Float.parseFloat(String.valueOf(subscriObj.getReActivationCharge()));
                    packageamount.put("Reactivation Charge", reactivationCharge);
                    reActivationCharge = reactivationCharge;
                }
            }
            String expiryDate = dateFormat.format(subscriObj.getExpiryDateNTime());
            String strDate    = dateFormat.format(new Date());
            Date curDate      = dateFormat.parse(strDate);
            Date exDate       = dateFormat.parse(expiryDate);
            if (curDate.before(exDate)) {
                double packageCancellationCharge = subscriObj.getCancellationRate();
                packageamount.put("Cancellation Charge", packageCancellationCharge);
                cancellationCharge = packageCancellationCharge;
            }
            if (subscriObj.getPaymentMode().equalsIgnoreCase("postpaid")) {
                if (curDate.after(exDate)) {
                    String latePenalites = subscriObj.getLatePenaltyRate();
                    if (latePenalites != null) {
                        double latePenalty = LatePenalitiesCalculationManagement.calculateLatePenalty(subscriObj);
                        packageamount.put("Late Penalty", latePenalty);
                        latePenaltyCharge = latePenalty;
                    }
                }
            }
            taxRate = subscriObj.getTax();
        }
        String tmMerchantID = LoadSettings.g_sSettings.getProperty("tm.pg.merchantId");
        String tmMerchantPWD = LoadSettings.g_sSettings.getProperty("tm.pg.merchantPWD");
        long tmMerchant_TranID = (long) (Math.random() * 100000000000000L);
        tmMerchant_TranID += 500000000000000L;

        // get daily transcation api usage details of a developer
        JSONObject partnerObj = new JSONObject();
        JSONObject pgTxObj    = new JSONObject();
        SgDailytransactiondetails[] dailyTransObj = new DailyTxManagement().getTxDetails(ChannelId, parObj.getPartnerId(), dailyTxFormat.format(subscriObj.getCreationDate()), dailyTxFormat.format(new Date()), "00:00 AM", "11:59 PM");
        
        if (dailyTransObj != null) {
            for (int i = 0; i < dailyTransObj.length; i++) {
                JSONObject data = new JSONObject(dailyTransObj[i].getRawData());
                partnerObj.put("" + dailyTransObj[i].getExecuutionDate(), data.toString());
            }
        }
//        if (subscriObj.getFlatPrice() == null) {
            if (subscriObj.getSlabApRateDetails() != null) {
                partnerObj = new SlabPriceCalculationManagement().slabPriceCalculation(partnerObj, subscriObj.getSlabApRateDetails(), "Live", "slab");
            } else if (subscriObj.getTierRateDetails() != null) {
                partnerObj = new SlabPriceCalculationManagement().slabPriceCalculation(partnerObj, subscriObj.getTierRateDetails(), "Live", "tier");
            }            
//        }
        session.setAttribute("tierOrSlabUsageDetails", partnerObj);
        SimpleDateFormat invoiceDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Double flatPrice = 0.00;
        // check promocode facility enable for subscribe package
        String feature = subscriObj.getFeatureList();
        JSONObject featJSONObj = null;
        boolean promocodeFacility = false;
        if (feature != null) {
            JSONArray alertJson = new JSONArray(feature);
            for (int i = 0; i < alertJson.length(); i++) {
                JSONObject jsonexists = alertJson.getJSONObject(i);
                if (jsonexists.has(packageName)) {
                    featJSONObj = jsonexists.getJSONObject(packageName);
                    if (featJSONObj != null) {
                        if (featJSONObj.getString("promoCodeFlag").equals("Enable")) {
                            promocodeFacility = true;
                        }
                    }
                }
            }
        }
        SgDeveloperProductionEnvt productionEnvt = new ProductionAccessEnvtManagement().getProudDetailsByPartnerId(parObj.getPartnerId());
        String billingAddress       = "NA";
        String billingState         = "NA";
        String billingPostCode      = "NA";
        String billingCountry       = "NA";
        
        if (productionEnvt != null) {
            billingAddress  = productionEnvt.getBillingAddress();
            billingState    = productionEnvt.getBillingState();
            billingPostCode = productionEnvt.getBillingPostCode();
            billingCountry  = productionEnvt.getBillingCountry();
        }
        //String jsonFlatPriceDetails = null;
        SgCaastransactiondetails[] caasCDRDetail = new CDRTxManagement().getTxDetails(parObj.getPartnerId(), subscriObj.getCreationDate(), subscriObj.getExpiryDateNTime(),"00:00 AM", "11:59 PM");
        if(caasCDRDetail != null){            
           caasCDRPrice = new CAASCDRCalculationManagement().calculateCDR(caasCDRDetail,accesspoint);
        }
        session.setAttribute("caasServiceUsageDetails", caasCDRPrice);

        // tm payment calculation 
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SgHourlyPgtx[] hourlytransactiondetails = null;
        SgDailyPgtx[] pgDailyTXObj = null;
        if(sdf.format(subscriObj.getCreationDate()).equals(sdf.format(new Date()))){
            hourlytransactiondetails = new PgHourlyTXManagement().getTxDetails(ChannelId, parObj.getPartnerId(), dailyTxFormat.format(subscriObj.getCreationDate()), dailyTxFormat.format(new Date()), "00:00 AM", "11:59 PM");            
        }else{
            Calendar pgCal = Calendar.getInstance();
            pgCal.setTime(new Date());
            pgCal.add(Calendar.DATE, -1);
            Date previousOneDate = pgCal.getTime();
            pgDailyTXObj = new PgDailyTxManagement().getTxDetails(ChannelId, parObj.getPartnerId(), dailyTxFormat.format(subscriObj.getCreationDate()), dailyTxFormat.format(previousOneDate), "00:00 AM", "11:59 PM");
            hourlytransactiondetails = new PgHourlyTXManagement().getTxDetails(ChannelId, parObj.getPartnerId(), dailyTxFormat.format(new Date()), dailyTxFormat.format(new Date()), "00:00 AM", "11:59 PM");
        }
        if (pgDailyTXObj != null) {
            for (int i = 0; i < pgDailyTXObj.length; i++) {
                JSONObject data = new JSONObject(pgDailyTXObj[i].getRawData());
                pgTxObj.put("" + pgDailyTXObj[i].getExecuutionDate()+";"+i, data.toString());
            }
        }
        if (hourlytransactiondetails != null) {
            for (int i = 0; i < hourlytransactiondetails.length; i++) {
                JSONObject data = new JSONObject(hourlytransactiondetails[i].getRawData());
                pgTxObj.put("" + hourlytransactiondetails[i].getExecuutionDate()+";"+i, data.toString());
            }
        }
        session.setAttribute("pgTransactionDetails", pgTxObj);
%>
    <script>
        function print_specific_div_content() {
            var win = window.open('', '', 'left=0,top=0,width=700,height=800,toolbar=0,scrollbars=0,status =0');
            var content = "<html>";
            content += "<link href='css/shop-homepage.css' rel='stylesheet' type='text/css'/>";
            content += "<body onload=\"window.print(); window.close();\">";
            content += document.getElementById("invoice-container1").innerHTML;
            content += "</body>";
            content += "</html>";
            win.document.write(content);
            win.document.close();
        }
    </script>
    <div id="havePromocode" class="modal fade" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 id="validatesPackageModal">Have a promocode ?</h4>
                </div>          
                <div class="modal-body" style="height: 70px">
                    <form  id="validatePromoForm">                        
                        <label class="control-label col-lg-3"  for="partnername">Promo code</label>
                        <div class="controls col-lg-9">
                            <input type="hidden"  id="_promoId" name="_promoId" >
                            <input type="hidden"  id="_promoDiscount" name="_promoDiscount" value=0 >
                            <input type="hidden"  id="_promostatus" name="_promostatus" >
                            <input type="hidden"  class="form-group-sm" id="_promoPackage" name="_promoPackage" value="<%=packageName%>">
                            <input type="text"  id="_promoCode" name="_promoCode" autofocus>                                    
                        </div>
                    </form>
                </div>                                  
                <div class="modal-footer">
                    <div id="validate-promo-result"></div>
                    <button class="btn btn-info btn-xs" data-dismiss="modal" >Close</button>
                    <button class="btn btn-success btn-xs" onclick="validatePromoCode('<%=paymentMode%>', '<%=packageName%>')" id="addPartnerButtonE">Validate</button>
                </div>
                <script>
                    var input = document.getElementById('_promoCode');
                    input.onkeyup = function () {
                        this.value = this.value.toUpperCase();
                    }
                </script>                                    
            </div>
        </div>
    </div>
    <%if (promocodeFacility) {%>            
    <body onload="havePromoCode()">
        <%} else {%>
    <body>
        <%}%>        
        <div style="padding-left: 110px;padding-top: 10px">            
            <!--        <a href="#" class="btn btn-primary" onclick="print_specific_div_content()"><i class="fa fa-print"></i> Print</a>
                    <a href="#" class="btn btn-success" onclick="payWithXPAY()"> XPAY</a> -->
            <a href="#" class="btn btn-info" onclick="payWithTM()"> Pay</a>
<!--            <a href="#" class="btn btn-info" onclick="downloadInvoice('<%=invoiceid%>')"> Download Invoice</a>-->
            <%if (promocodeFacility) {%>
            <a onclick="havePromoCode()" style="margin-left: 2%"> Have a Promo code ?</a>
            <%}%>
        </div>
        <div id="invoice-container1">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#"><img src="images/logo_2.png" width="150" height="106" alt=""/></a>
                    </div>
                </div>
                <!-- /.container -->
            </nav>
            <!-- Page Content -->
            <div class="container invoice">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <h3>Tax Invoice</h3>
                                </div>
                                <div class="col-sm-6 col-lg-6 col-md-6 company-name">
                                    <p>TELEKOM MALAYSIA BERHAD( 128740-P )</p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <table>
                                        <tr>
                                            <td class="table-header">Bill To</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p><%=parObj.getPartnerName()%><br/>
                                                    <%=billingAddress%><br/>
                                                    <%=billingState%><br/>
                                                    <%=billingPostCode%><br/>
                                                    <%=billingCountry%><br/>
                                            </td>
                                        </tr>
                                    </table>	
                                    <table>
                                        <tr>
                                            <td class="table-header">Remit To</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p><strong>Please issue cheque payable to :</strong><br/>
                                                    TELEKOM MALAYSIA BERHAD<br/>
                                                    And send to :<br/>
                                                    Wilayah Persekutuan Malaysia</p>
                                            </td>
                                        </tr>
                                    </table>								
                                </div>
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <table>
                                        <tr>
                                            <td class="table-header">Information</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr class="information">
                                                        <td><strong>Invoice No</strong></td>
                                                        <td>:</td>
                                                        <td><%=invoiceid%> </td>
                                                    </tr>
                                                    <tr class="information">
                                                        <td><strong>Package Name</strong></td>
                                                        <td>:</td>
                                                        <td><%=subscriObj.getBucketName()%></td>
                                                    </tr>
                                                    <tr class="information">
                                                        <td><strong>Invoice Date</strong></td>
                                                        <td>:</td>
                                                        <td><%=invoiceDateFormat.format(new Date())%></td>
                                                    </tr>
                                                    <tr class="information">
                                                        <td><strong>Term of payment</strong></td>
                                                        <td>:</td>
                                                        <td><%=subscriObj.getBucketDuration()%></td>
                                                    </tr>
                                                    <tr class="information">
                                                        <td><strong>Due Date</strong></td>
                                                        <td>:</td>
                                                        <td><%=invoiceDateFormat.format(subscriObj.getExpiryDateNTime())%></td>
                                                    </tr>
                                                    <tr class="information">
                                                        <td><strong>Currency</strong></td>
                                                        <td>:</td>
                                                        <td>MYR</td>
                                                    </tr>
                                                    <!--											<tr class="information">
                                                                                                                                                    <td><strong>Revenue Code</strong></td>
                                                                                                                                                    <td>:</td>
                                                                                                                                                    <td>951</td>                                                                                                                                            </tr>-->
                                                </table>
                                            </td>
                                        </tr>
                                    </table>	
                                </div>
                            </div>					
                            <div class="col-sm-12 col-lg-12 col-md-12" style="padding: 30px;">						
                                <table id="invoiceDetailsTable">
                                    <thead>
                                        <tr>								
                                            <td class="table-header">Access point</td>
                                            <td class="table-header">Duration</td>
                                            <td class="table-header">API</td>
                                            <td class="table-header">Call Count</td>
                                            <td class="table-header">Unit Price</td>
                                            <td class="table-header">Total</td>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tr>
                                        <%
                                            int count = 1;
                                            Double totalAmount = 0.0;
                                            DecimalFormat df = new DecimalFormat();
                                            df.setMaximumFractionDigits(2);
                                            String apName = "NA";
                                            String resName = "NA";
                                            String envt = "NA";
                                            String apiName = "NA";
                                            int version = 0;
                                            int callCount = 0;
                                            float amount = 0;
                                            if (partnerObj.length() != 0) {
                                                Iterator itrS = partnerObj.keys();
                                                while (itrS.hasNext()) {
                                                    Object keyS = itrS.next();
                                                    Date apiCallDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(keyS.toString());
                                                    JSONObject data = new JSONObject(partnerObj.getString((String) keyS));
                                                    Iterator itr = data.keys();
                                                    while (itr.hasNext()) {
                                                        float totalCharge = 0.0f;
                                                        Object key = itr.next();
                                                        String keyData = (String) key;
                                                        String value = data.getString((String) key);
                                                        int acc = Integer.parseInt(keyData.split(":")[0]);
                                                        int res = Integer.parseInt(keyData.split(":")[1]);
                                                        version = Integer.parseInt(keyData.split(":")[2]);
                                                        envt = (keyData.split(":")[3]);
                                                        apiName = (keyData.split(":")[4]);
                                                        Accesspoint ap = new AccessPointManagement().getAccessPointById(ChannelId, acc);
                                                        ResourceDetails rs = new ResourceManagement().getResourceById(res);
                                                        if (ap != null) {
                                                            apName = ap.getName();
                                                        }
                                                        if (rs != null) {
                                                            resName = rs.getName();
                                                        }
                                                        if (!envt.equalsIgnoreCase("Live")) {
                                                            continue;
                                                        }
                                                        callCount = Integer.parseInt(value.split(":")[0]);
                                                        amount = Float.parseFloat(value.split(":")[1]);
                                                        if (amount != -99 && ap.getApId() != caasAccessPointId && !apiName.equalsIgnoreCase("DoTransaction")) {
                                                            totalCharge = callCount * amount;
                                        %> 
                                        <td><%=apName%></td>
                                        <td><%=apiCallDateFormat.format(apiCallDate)%></td>
                                        <td><%=apiName%></td>
                                        <td><%=callCount%></td>
                                        <td>RM <%=df.format(amount)%></td>
                                        <td>RM <%=df.format(totalCharge)%></td>
                                    </tr>
                                    <%
                                                        count++;
                                                        totalAmount = totalAmount + totalCharge;
                                                    }
                                                }
                                            }
                                        }
                                        if (subscriObj != null && subscriObj.getFlatPrice() != null) {
                                            String flatPricede = subscriObj.getFlatPrice();
                                            JSONArray jsOld = new JSONArray(flatPricede);
                                            for (int j = 0; j < jsOld.length(); j++) {
                                                JSONObject jsonexists1 = jsOld.getJSONObject(j);
                                                Iterator<String> keys = jsonexists1.keys();
                                                if (keys.hasNext()) {
                                                    String key = (String) keys.next();
                                                    String[] keyArr = key.split(":");
                                                    String value = jsonexists1.getString(key);
                                                    String[] valueArr = value.split(":");
                                    %>
                                    <tr>
                                        <td><%=keyArr[0]%></td>
                                        <td></td>
                                        <td>Flat Price</td>
                                        <td></td>
                                        <td>RM <%=df.format(Double.parseDouble(valueArr[1]))%></td>
                                        <td>RM <%=df.format(Double.parseDouble(valueArr[1]))%></td>
                                    </tr>        
                                    <%
                                                    totalAmount = totalAmount + Double.parseDouble(valueArr[1]);

                                                }
                                            }
                                        }
                                        if(caasCDRPrice.length() != 0){
                                            Iterator itrS = caasCDRPrice.keys();
                                                while (itrS.hasNext()) {
                                                Object keyS = itrS.next();                                                
                                                String caaskey = (String) keyS;
                                                String value = caasCDRPrice.getString((String) caaskey);
                                                String[] keyData = caaskey.split(";");
                                                Date caasAPIDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(keyData[2]);
                                    %>
                                    <tr>
                                        <td></td>
                                        <td><%=apiCallDateFormat.format(caasAPIDate)%></td>
                                        <td>PG</td>
                                        <td><%=keyData[1]%> Min</td>
                                        <td></td>
                                        <td>RM <%=df.format(Float.parseFloat(value))%></td>
                                    </tr>  
                                    
                                    <%
                                            totalAmount = totalAmount + Float.parseFloat(value);
                                            }
                                        }if(pgTxObj.length() != 0){
                                            Iterator itrS = pgTxObj.keys();
                                            while (itrS.hasNext()) {
                                                Object keyS = itrS.next();
                                                Date apiCallDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(keyS.toString().split(";")[0]);
                                                JSONObject data = new JSONObject(pgTxObj.getString((String) keyS));
                                                Iterator itr = data.keys();
                                                while (itr.hasNext()) {
                                                    
                                                    Object key = itr.next();
                                                    String keyData = (String) key;
                                                    String value = data.getString((String) key);
                                                    String[] valueDetails = value.split(":");
                                                     envt    = (keyData.split(":")[3]);
                                                    if (!envt.equalsIgnoreCase("Live")) {
                                                        continue;
                                                    }
                                    %> 
                                    <tr>
                                        <td></td>
                                        <td><%=apiCallDateFormat.format(apiCallDate)%></td>
                                        <td>TM PG</td>
                                        <td><%=valueDetails[0]%> transaction</td>
                                        <td>RM <%=valueDetails[1]%></td>
                                        <td>RM <%=df.format(Float.parseFloat(valueDetails[1]))%></td>
                                    </tr>                                                                          
                                    <%
                                            totalAmount = totalAmount + Double.parseDouble(valueDetails[1]);
                                            }
                                        }
                                    }
                                    for (Map.Entry<String, Double> entry : packageamount.entrySet()) {
                                    %>                                                                  
                                    <tr>
                                        <td colspan="2"></td>
                                        <td><%=entry.getKey()%></td>
                                        <td></td>
                                        <td>RM <%=df.format(entry.getValue())%></td>
                                        <td>RM <%=df.format(entry.getValue())%></td>
                                    </tr>
                                    <%      count++;
                                            totalAmount = totalAmount + entry.getValue();
                                        }
                                        totalAmountWithoutTax = totalAmount;
                                        session.setAttribute("totalPaymentAmountWithoutTax", totalAmountWithoutTax);
                                        taxMap = TaxCalculationManagement.calculateTax(taxRate, totalAmountWithoutTax);
                                        if (!taxMap.isEmpty()) {
                                            for (Map.Entry<String, String> entry : taxMap.entrySet()) {
                                                if (entry.getKey().equalsIgnoreCase("GST Tax")) {
                                                    String[] taxArr = entry.getValue().split(":");

                                    %>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td><%=entry.getKey()%></td>
                                        <td><%=taxArr[0]%> %</td>
                                        <td>RM <%=df.format(Double.parseDouble(taxArr[1]))%></td>
                                        <td>RM <%=df.format(Double.parseDouble(taxArr[1]))%></td>
                                    </tr>
                                    <%
                                                    count++;
                                                    totalAmount = totalAmount + Float.parseFloat(taxArr[1]);
                                                }
                                            }
                                        }
                                    %>
                                    <tr>
                                        <td colspan="4"></td>
                                        <td style="text-align: right"><strong>TOTAL</strong></td>
                                        <td id="totalAmount">RM <%=df.format(totalAmount)%></td>
                                    </tr>
                                    <%
                                        count++;
                                        String tmWellFormAmount = form.format(totalAmount);
                                        String wellFormTotalBeforeTax = form.format(totalAmountWithoutTax);
                                    %>
                                    <tr>
                                        <td colspan="6">
                                            <h4>PRIVACY STATEMENT</h4>
                                            <p>PENYATAAN PRIVASI TM Dalam usaha memastikan pematuhan kepada Akta Perlindungan Data Peribadi 2010 (APDP), TM telah mewujudkan satu dasar perlindungan data peribadi yang akan mengawal penggunaan dan perlindungan data peribadi anda sebagai pelanggan TM. Untuk mengetahui dasar tersebut secara terperinci, sila rujuk Penyataan Privasi TM di http:///www.tm.com.my, yang mana tertakluk kepada perubahan dari masa ke semasa oleh TM. 
                                                <br><br>
                                                TM'S PRIVACY STATEMENT In its effort to ensure compliance to the Personal Data Protection Act 2010 (PDPA), TM has put in place a personal data protection policy which shall govern the use and protection of your personal data as TM's customer. For details of the policy, please refer to TM's Privacy Statement at http:///www.tm.com.my, which may be reviewed by TM from time to time.
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" id="countDetails" name="countDetails" value="<%=count - 1%>">
                                <input type="hidden" id="appliedPromocode" name="appliedPromocode" value="no">                        
                                <input type="hidden" id="finalPaymentAmount" name="finalPaymentAmount" value="<%=tmWellFormAmount%>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form class="form-horizontal" id="paymentDetailsForm"> 
                <input type="hidden" id="totalBenefitAmount" name="totalBenefitAmount" value="<%=totalAmount%>">
                <input type="hidden" id="_promocodeId" name="_promocodeId">
                <input type="hidden" id="_packageName" name="_packageName" value="<%=outPackageName%>">
                <input type="hidden" id="originalPackageN" name="originalPackageN" value="<%=packageName%>">
                <input type="hidden" id="_invoiceId" name="_invoiceId" value="<%=invoiceid%>">
                <input type="hidden" id="_paymentMode" name="_paymentMode" value="<%=paymentMode%>">
                <input type="hidden" id="_totalAmountWithoutTax" name="_totalAmountWithoutTax" value="<%=wellFormTotalBeforeTax%>">
                <input type="hidden" id="_gstTax" name="_gstTax" value="<%=gstTax%>">
                <input type="hidden" id="_vatTax" name="_vatTax" value="<%=vatTax%>">
                <input type="hidden" id="_serviceTax" name="_serviceTax" value="<%=serviceTax%>">            
                <input type="hidden" id="MERCHANTID" name="MERCHANTID" value="<%=tmMerchantID%>">
                <input type="hidden" id="PSWD" name="PSWD" value="<%=tmMerchantPWD%>">
                <input type="hidden" id="MERCHANT_TRANID" name="MERCHANT_TRANID" value="<%=tmMerchant_TranID%>">
                <input type="hidden" id="AMOUNT" name="AMOUNT" value="<%=tmWellFormAmount%>">
                <input type="hidden" id="SIGNATURE" name="SIGNATURE" >

                <%if (changeOnPackageCharge != null) {%>
                <input type="hidden" id="_changeOnPackageCharge" name="_changeOnPackageCharge" value="<%=changeOnPackageCharge%>">
                <%}
                    if (reActivationCharge != null) {%>       
                <input type="hidden" id="_reactivationCharge" name="_reactivationCharge" value="<%=reActivationCharge%>">
                <%}
                    if (cancellationCharge != null) {%>        
                <input type="hidden" id="_cancellationCharge" name="_cancellationCharge" value="<%=cancellationCharge%>">
                <%}
                    if (latePenaltyCharge != null) {%>
                <input type="hidden" id="_latePenaltyCharge" name="_latePenaltyCharge" value="<%=latePenaltyCharge%>">
                <%}%>
            </form>
            <script language="JavaScript">
                function generateHash(paymentDetailsForm) {
                    var str = "##" + paymentDetailsForm.MERCHANTID.value.toUpperCase() + "##" + paymentDetailsForm.PSWD.value.toUpperCase() + "##" +
                            paymentDetailsForm.MERCHANT_TRANID.value.toUpperCase() + "##" + paymentDetailsForm.AMOUNT.value.toUpperCase() + "##0##";
                    console.log(str);
                    paymentDetailsForm.SIGNATURE.value = hex_sha1(str).toUpperCase();
                }
                generateHash(paymentDetailsForm);
            </script>                     
            <!-- /.container -->
            <div class="container">
                <hr>
                <!-- Footer -->
                <footer>
                    <div class="row">
                        <div class="col-lg-12">
                            <p><strong>TELEKOM MALAYSIA BERHAD( 128740-P )</strong><br/>
                                Level 51 North Wing, Menara TM Jalan Pantai Baharu 50672 Kuala Lumpur Wilayah Persekutuan Malaysia<br/>
                                Tel: 03-2240 9494 Fax: 03-2283 2415 WWW.TM.COM.MY</p>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <!-- /.container -->    
        <!-- Bootstrap Core JavaScript -->            
    </body>
</html>
