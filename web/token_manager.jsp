<%@page import="com.sun.org.apache.xerces.internal.impl.dv.util.Base64"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartnerrequest"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@include file="header.jsp"%>
<!--<link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />-->
<script src="scripts/profile.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.4/TweenMax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.5/TimelineLite.min.js"></script>
<link href="styles/copyTextEff.css" rel="stylesheet" type="text/css"/>
<script src="scripts/copyTextJS.js" type="text/javascript"></script>
<script src="vendor/toastr/build/toastr.min.js"></script>
<script src="vendor/ladda/dist/spin.min.js"></script>
<script src="vendor/ladda/dist/ladda.min.js"></script>
<script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
<script src="vendor/jquery-validation/jquery.validate.min.js"></script>
<%    if (PartnerType.equalsIgnoreCase("admin")) {
        PartnerDetails partnerd = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        SgPartnerrequest sg = new PartnerRequestManagement().getPartnerRequestsPartnerbyId(SessionId, partnerd.getPartnerId());
        PartnerDetails pd = new PartnerManagement().getPartnerDetails(SessionId, ChannelId, partnerd.getPartnerId());
%>
<!-- Main Wrapper -->
<div id="wrapper">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Token</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Token Management
                </h2>
                <small>Get your API level token from here</small>
            </div>
        </div>
    </div>

    <div class="normalheader transition animated fadeIn">
        <div class="hpanel">	
            <ul class="nav nav-tabs">
                <!--<li class="active"><a data-toggle="tab" href="#test"> Test Environment</a></li>-->
                <li class="active"><a data-toggle="tab" href="#live"> Live Environment</a></li>
            </ul>

            <div class="tab-content">
                <div id="test" class="tab-pane">
                    <% if (pd != null) {%>
                    <div class="panel-body">
                        <div class="form-group">
                            <% if (pd.getCertData() != null) {%>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-primary ladda-button btn-block"  data-style="zoom-in" id="viewTestcert" onclick="viewCrtData(<%=pd.getPartnerId()%>, 'test')" type="button"><i class="fa fa-eye"></i> <span class="bold">View Certificate</span></button>
                            </div>
                            <%} else {%>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-primary disabled" type="button"><i class="fa fa-eye"></i> <span class="bold">View Certificate</span></button>
                            </div>
                            <%}%>
                            <% if (pd.getCertData() != null) {%>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-primary2 ladda-button btn-block" data-style="zoom-in" id="downTcert" onclick="downloadCert('<%=pd.getPartnerId()%>', 'test')"  type="button"><i class="fa fa-cloud-download"></i> <span class="bold">Download Certificate</span></button>
                            </div>
                            <%} else {%>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-primary2 disabled"  type="button"><i class="fa fa-cloud-download"></i> <span class="bold">Download Certificate</span></button>
                            </div>
                            <%}%>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-info " type="button" onclick="showTokenForPartner('<%=pd.getToken()%>', '<%=pd.getTokenForLive()%>')"><i class="fa fa-eye"></i> <span class="bold">View PartnerTokenId Token</span></button>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-warning ladda-button btn-block" data-style="zoom-in" id="regTestToken" type="button" onclick="reGenerateAPToken('<%=pd.getPartnerId()%>', '<%=pd.getPartnerName()%>', '<%=pd.getPartnerEmailid()%>', '<%=pd.getPartnerPhone()%>', 'Test')"><i class="fa fa-refresh"></i> <span class="bold">Regenerate Token</span></button>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-success ladda-button btn-block" data-style="zoom-in" id="sendTestToken" type="button" onclick = "emailAPToken('<%=pd.getPartnerName()%>', '<%=pd.getPartnerEmailid()%>', '<%=pd.getToken()%>', '<%=pd.getTokenForLive()%>', 'Test')"><i class="fa fa-envelope"></i> <span class="bold">Send Token (Email)</span></button>
                            </div>
                            <div class="col-sm-2">
                            </div>
                        </div>
                    </div>
                    <%}%>
                </div>
                <div id="live" class="tab-pane active">
                    <%if (pd != null) {%>
                    <div class="panel-body">
                        <div class="form-group">
                            <% if (pd.getCertDataForLive() != null) {%>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-primary ladda-button btn-block"  data-style="zoom-in" id="viewProdcert" onclick="viewCrtData('<%=pd.getPartnerId()%>', 'Production')" type="button"><i class="fa fa-eye"></i> <span class="bold">View Certificate</span></button>
                            </div>
                            <%} else {%>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-primary disabled" type="button"><i class="fa fa-eye"></i> <span class="bold">View Certificate</span></button>
                            </div>
                            <%}%>
                            <% if (pd.getCertDataForLive() != null) {%>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-primary2 ladda-button btn-block" data-style="zoom-in" id="downPcert" onclick="downloadCert('<%=pd.getPartnerId()%>', 'Production')" type="button"><i class="fa fa-cloud-download"></i> <span class="bold">Download Certificate</span></button>
                            </div>
                            <%} else {%>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-primary2 disabled" type="button"><i class="fa fa-cloud-download"></i> <span class="bold">Download Certificate</span></button>
                            </div>
                            <%}%>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-info " type="button" onclick="showLiveTokenForPartner('<%=pd.getToken()%>', '<%=pd.getTokenForLive()%>')"><i class="fa fa-eye"></i> <span class="bold"> View PartnerTokenId</span></button>
                            </div>                            
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-warning ladda-button btn-block" data-style="zoom-in" id="regProdToken" type="button" onclick="reGenerateAPToken('<%=pd.getPartnerId()%>', '<%=pd.getPartnerName()%>', '<%=pd.getPartnerEmailid()%>', '<%=pd.getPartnerPhone()%>', 'Production')"><i class="fa fa-refresh"></i> <span class="bold">Generate PartnerTokenId</span></button>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-success ladda-button btn-block" data-style="zoom-in" id="sendProdToken" type="button" onclick = "emailAPToken('<%=pd.getPartnerName()%>', '<%=pd.getPartnerEmailid()%>', '<%=pd.getToken()%>', '<%=pd.getTokenForLive()%>', 'Production')"><i class="fa fa-envelope"></i> <span class="bold">Email PartnerTokenId</span></button>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-xs btn-danger " type="button" onclick="showPartnerId('<%=pd.getPartnerUniqueId()%>')"><i class="fa fa-eye"></i> <span class="bold"> View PartnerId</span></button>
                            </div>
                        </div>
                    </div>
                    <%}%>
                </div>
            </div>
        </div>
    </div>
    <!--certificate Design start here-->
    <div class="modal fade" id="live_cert" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header" style="padding: 10px 0px !important">
                    <h3 style="margin-left: 2%">Your Certificate Details <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 1%; margin-right: 3%">x</button></h3>                                        
                    <small id="userDetails" class="font-bold" style="display: none"></small>                    
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <img src="images/CertificateLogo.png" alt="" style="margin-left: 38%" height="130" width="130"/>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Version</label>
                            <div class="col-sm-8">
                                <input type="text" id="_version" class="form-control" readonly>
                                <!--                                <lable class="font-bold" id="_version" style="height: 5%"></label>-->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Serial No: Algorithm</label>
                            <div class="col-sm-4"><input type="text" id="_srNo" class="form-control" readonly></div> <div class="col-sm-4"><input type="text" id="_algo" class="form-control" readonly></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Issuer Details</label>
                            <div class="col-sm-8"><textarea id="_issuerdn" class="form-control"  style="height: 80px" readonly></textarea></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Issuer To</label>
                            <div class="col-sm-8"><textarea id="_subjectdn" class="form-control" style="height: 80px" readonly></textarea></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Validity </label>
                            <div class="col-sm-4">From <input type="text" id="_notbefore" class="form-control" readonly></div> <div class="col-sm-4">To <input type="text" id="_notafter" class="form-control" readonly></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>               
            </div>
        </div>
    </div>
    <!--certificate Design end here-->

    <div class="modal fade hmodal-success" id="tmTermsAndCondition" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header" style="padding: 10px 0px !important;padding-bottom: 3px">
                    <h4 id="tokenType" style="margin-left: 2%">Your PartnerTokenId For Live enviroment is                                                             
                        <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4> 
                </div>
                <div class="modal-body" style="padding-bottom: 50px !important">
                    <img src="images/TokenLogo.png" alt="" style="margin-left: 35%; margin-bottom: 7%"/>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <p id="test1" style="margin-left: 35%"></p>
                        </div>
                        <input type="text" id="copyTarget1" style="display: none">
                        <div class="col-lg-6">
                            <div class="sentance">
                                <div class="sentance__half left">
                                  <span class="inner">
                                    It all starts 
                                  </span>
                                </div>
                                <div class="sentance__half right">
                                  <span class="inner">
                                    with an idea
                                  </span>
                                  <span class="dot"></span>
                                </div>
                              </div>
                            <a href="#/" style="font-size: 15px; margin-left: 25%;text-decoration: none" onclick="copy('<%= new String(Base64.encode(pd.getTokenForLive().getBytes()))%>',1)"><b>Copy</b></a>                   
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div> 
                        
    <div class="modal fade hmodal-success" id="showPartnerIdModal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header" style="padding: 10px 0px !important;padding-bottom: 3px">
                    <h4 id="tokenType" style="margin-left: 2%">Your PartnerId is                                                             
                        <button class="close btn btn-default" data-dismiss="modal" aria-hidden="true" style="margin-top: 0%; margin-right: 3%">x</button></h4> 
                </div>
                <div class="modal-body" style="padding-bottom: 50px !important">
                    <img src="images/TokenLogo.png" alt="" style="margin-left: 35%; margin-bottom: 7%"/>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <p id="test12" style="margin-left: 35%"></p>
                        </div>
                        <input type="text" id="copyTarget2" style="display: none">
                        <div class="col-lg-6">
                            <a href="#/" style="font-size: 15px; margin-left: 25%;text-decoration: none" onclick="copy('<%= new String(Base64.encode(pd.getPartnerUniqueId().getBytes()))%>',2)"><b>Copy</b></a>                   
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div> 

    <div class="content animate-panel" data-effect="rotateInDownRight" data-child="element">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center m-b-xl">
                    <h3>APIs Token Management</h3>
                </div>
                <hr class="m-b-xl"/>
                <%
                    Accesspoint[] accesspoints2 = null;
                    PartnerDetails partnerObj = (PartnerDetails) session.getAttribute("_partnerDetails");
                    int pGroupId = 0;
                    if (partnerObj.getPartnerGroupId() != null && !partnerObj.getPartnerGroupId().equals("")) {
                        pGroupId = Integer.parseInt(partnerObj.getPartnerGroupId());
                    }
                    partnerObj.getPartnerGroupId();
                    accesspoints2 = new AccessPointManagement().getAccessPointByGroupId(SessionId, ChannelId, pGroupId);
                    if (accesspoints2 != null) {
                        int boxcount = 0;
                        for (int i = 0; i < accesspoints2.length; i++) {
                            Warfiles warfiles = new WarFileManagement().getWarFile(SessionId, ChannelId, accesspoints2[i].getApId());
                            if (warfiles != null && accesspoints2[i].getStatus() != GlobalStatus.DELETED) {
                                if (boxcount == 4) {
                                    boxcount = 1;
                                } else {
                                    boxcount++;
                                }
                %>
                <!--                <div class="col-sm-4">
                                    <div class="hpanel plan-box">
                                        <div class="panel-heading hbuilt text-center">
                                            <h4 class="font-bold"><%=accesspoints2[i].getName()%></h4>
                                        </div>
                                        <div class="panel-body text-center">
                                            <i class="pe pe-7s-phone big-icon text-warning"></i>
                
                                            <h4 class="font-bold">
                                            </h4>
                                            <p class="text-muted">
                                            </p>
                                            <a href='token_manager_api.jsp?&accessPointName=<%=accesspoints2[i].getName()%>' class="btn btn-warning btn-sm">View Token</a>                                
                                        </div>
                                    </div>
                                </div>-->
                <%if (boxcount == 1) {%>
                <div class="col-md-3 element">
                    <div class="hpanel plan-box">
                        <div class="panel-body h-200">
                            <div class="text-center">
                                <h6 class="m-b-xs element font-bold"><%=accesspoints2[i].getName()%></h6>
                                <p class=" element">Service</p>
                                <div class="m element">
                                    <i class="pe-7s-science fa-5x text-success"></i>
                                </div>
                                <p class="small element">
                                    <%if (accesspoints2[i].getDescription() != null) {%>
                                    <%=accesspoints2[i].getDescription()%>
                                    <%} else {%>
                                    Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    <%}%>
                                </p>
                                <a href='token_manager_api.jsp?&accessPointName=<%=accesspoints2[i].getName()%>' class="btn btn-success btn-sm element">View APITokenId</a>
                            </div>
                        </div>
                    </div>
                </div>
                <%} else if (boxcount == 2) {%>
                <div class="col-md-3 element">
                    <div class="hpanel plan-box">
                        <div class="panel-body h-200">
                            <div class="text-center">
                                <h6 class="m-b-xs font-bold element"><%=accesspoints2[i].getName()%></h6>
                                <p class=" element">Service</p>
                                <div class="m element">
                                    <i class="pe-7s-global fa-5x text-info"></i>
                                </div>
                                <p class="small element">
                                    <%if (accesspoints2[i].getDescription() != null) {%>
                                    <%=accesspoints2[i].getDescription()%>
                                    <%} else {%>
                                    Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    <%}%>
                                </p>
                                <a href='token_manager_api.jsp?&accessPointName=<%=accesspoints2[i].getName()%>' class="btn btn-info btn-sm element">View APITokenId</a>
                            </div>
                        </div>
                    </div>
                </div>
                <%} else if (boxcount % 3 == 0) {%>
                <div class="col-md-3 element">
                    <div class="hpanel plan-box">
                        <div class="panel-body  h-200">
                            <div class="text-center">
                                <h6 class="m-b-xs font-bold  element"><%=accesspoints2[i].getName()%></h6>
                                <p class="  element">Service</p>
                                <div class="m element">
                                    <i class="pe-7s-display1 fa-5x text-warning"></i>
                                </div>
                                <p class="small element">
                                    <%if (accesspoints2[i].getDescription() != null) {%>
                                    <%=accesspoints2[i].getDescription()%>
                                    <%} else {%>
                                    Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    <%}%>
                                </p>
                                <a href='token_manager_api.jsp?&accessPointName=<%=accesspoints2[i].getName()%>' class="btn btn-warning btn-sm element">View APITokenId</a>
                            </div>
                        </div>
                    </div>
                </div>
                <%} else if (boxcount % 4 == 0) {%>
                <div class="col-md-3 element">
                    <div class="hpanel plan-box">
                        <div class="panel-body  h-200">
                            <div class="text-center">
                                <h6 class="m-b-xs font-bold  element"><%=accesspoints2[i].getName()%></h6>
                                <p class=" element">Service</p>
                                <div class="m element">
                                    <i class="pe-7s-airplay fa-5x text-danger"></i>
                                </div>
                                <p class="small element">
                                    <%if (accesspoints2[i].getDescription() != null) {%>
                                    <%=accesspoints2[i].getDescription()%>
                                    <%} else {%>
                                    Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                    <%}%>
                                </p>
                                <a href='token_manager_api.jsp?&accessPointName=<%=accesspoints2[i].getName()%>' class="btn btn-danger btn-sm element">View APITokenId</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">

                </div>               
                <%}
                    }
                        }
                            } else {%>
                <div class="col-md-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <i class="pe-7s-way text-success big-icon"></i>
                            <h1>Not found</h1>
                            <strong>No accesspoint found</strong>
                            <p>
                                Sorry, but the accesspoint list not been found.
                            </p>
                            <a href="#" class="btn btn-xs btn-success ladda-button" data-style="zoom-in" id="previousButton" onclick="goBack()">Go back to previous page</a>
                        </div>
                    </div>
                </div>
                <%}%>            
            </div>
        </div>
    </div>    
    <%}%>
    <!-- Footer-->
    <%@include file="footer.jsp"%>
    <script>
        function copy(text,id) {
            console.log('text >> ' + text);
            console.log('Decode ?? ' + Base64.decode(text));
            document.getElementById("copyTarget"+id).value = Base64.decode(text);
            document.getElementById("copyTarget"+id).style.display = 'block';
            copyToClipboardMsg(document.getElementById("copyTarget"+id), "msg");
            document.getElementById("copyTarget"+id).style.display = 'none';
        }
        function copyToClipboardMsg(elem, msgElem) {
            var succeed = copyToClipboard(elem);
            var msg;
            if (!succeed) {
                msg = "Copy not supported or blocked.  Press Ctrl+c to copy."
            } else {
                msg = "Text copied to the clipboard."
            }
            console.log(msg);
//                if (typeof msgElem === "string") {
//                    msgElem = document.getElementById(msgElem);
//                }
//                msgElem.innerHTML = msg;
//                setTimeout(function () {
//                    msgElem.innerHTML = "";
//                }, 2000);
        }

        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch (e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }

        var Base64 = {_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
                var t = "";
                var n, r, i, s, o, u, a;
                var f = 0;
                e = Base64._utf8_encode(e);
                while (f < e.length) {
                    n = e.charCodeAt(f++);
                    r = e.charCodeAt(f++);
                    i = e.charCodeAt(f++);
                    s = n >> 2;
                    o = (n & 3) << 4 | r >> 4;
                    u = (r & 15) << 2 | i >> 6;
                    a = i & 63;
                    if (isNaN(r)) {
                        u = a = 64
                    } else if (isNaN(i)) {
                        a = 64
                    }
                    t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
                }
                return t
            }, decode: function (e) {
                var t = "";
                var n, r, i;
                var s, o, u, a;
                var f = 0;
                e = e.replace(/[^A-Za-z0-9+/=]/g, "");
                while (f < e.length) {
                    s = this._keyStr.indexOf(e.charAt(f++));
                    o = this._keyStr.indexOf(e.charAt(f++));
                    u = this._keyStr.indexOf(e.charAt(f++));
                    a = this._keyStr.indexOf(e.charAt(f++));
                    n = s << 2 | o >> 4;
                    r = (o & 15) << 4 | u >> 2;
                    i = (u & 3) << 6 | a;
                    t = t + String.fromCharCode(n);
                    if (u != 64) {
                        t = t + String.fromCharCode(r)
                    }
                    if (a != 64) {
                        t = t + String.fromCharCode(i)
                    }
                }
                t = Base64._utf8_decode(t);
                return t
            }, _utf8_encode: function (e) {
                e = e.replace(/rn/g, "n");
                var t = "";
                for (var n = 0; n < e.length; n++) {
                    var r = e.charCodeAt(n);
                    if (r < 128) {
                        t += String.fromCharCode(r)
                    } else if (r > 127 && r < 2048) {
                        t += String.fromCharCode(r >> 6 | 192);
                        t += String.fromCharCode(r & 63 | 128)
                    } else {
                        t += String.fromCharCode(r >> 12 | 224);
                        t += String.fromCharCode(r >> 6 & 63 | 128);
                        t += String.fromCharCode(r & 63 | 128)
                    }
                }
                return t
            }, _utf8_decode: function (e) {
                var t = "";
                var n = 0;
                var r = c1 = c2 = 0;
                while (n < e.length) {
                    r = e.charCodeAt(n);
                    if (r < 128) {
                        t += String.fromCharCode(r);
                        n++
                    } else if (r > 191 && r < 224) {
                        c2 = e.charCodeAt(n + 1);
                        t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                        n += 2
                    } else {
                        c2 = e.charCodeAt(n + 1);
                        c3 = e.charCodeAt(n + 2);
                        t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                        n += 3
                    }
                }
                return t
            }}
    </script>

</div>
