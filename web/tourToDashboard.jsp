
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<!DOCTYPE html>

<html>
    <%response.addHeader("X-FRAME-OPTIONS", "DENY");%>    
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!--                 Page title -->
        <title>Ready APIs</title>        
    </head>

    <body class="fixed-navbar">
        <%
            String mailId = LoadSettings.g_sSettings.getProperty("support.email.id");
        %>
        <style>
            span.capitalize {
                text-transform: capitalize;
            }
        </style>        
        <!-- Simple splash screen-->

        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <%
            Accesspoint[] accessObj = new AccessPointManagement().getAceesspoints();
            Warfiles warfiles = null;
            int dbaccesspointID = 0;
            int dbResId = 0;
            String dbSerDesc = null;
            String dbdisplayName = null;
            int documentUtilityaccesspointID = 0;
            int documentUtilityResId = 0;
            String docUtilityDesc = null;
            String docUtilitylabel = null;
            int gogAuthaccesspointID = 0;
            int gogAuthResId = 0;
            String gogAuthDesc = null;
            String gogAuthlabel = null;
            int imgUtilityAcc = 0;
            int imgUtilityRes = 0;
            String imgUtilityDesc = null;
            String imgUtilitylabel = null;
            int tokanisationAcc = 0;
            int tokanisationRes = 0;
            String tokanisationDesc = null;
            String tokanisationlabel = null;
            int valueAddedAccId = 0;
            int valueAddedResId = 0;
            String valueAddedAccDesc = null;
            String valueAddedReslabel = null;
            int ip2geoAccId = 0;
            int ip2geoResId = 0;
            String ip2geoAccDesc = null;
            String ip2geoAcclabel = null;
            int instantWebChatAccId = 0;
            int instantWebChatResId = 0;
            String instantWebChatDesc = null;
            String instantWebChatlabel = null;
            int reverseProxyAccId = 0;
            int reverseProxytResId = 0;
            String reverseProxyDesc = null;
            String reverseProxylabel = null;
            int inappPushAccId = 0;
            int inappPushResId = 0;
            String inappPushDesc = null;
            String inappPushlabel = null;
            int serMoniAccId = 0;
            int serMoniResId = 0;
            String serMoniDesc = null;
            String serMonilabel = null;
            int digiCertAccId = 0;
            int digiCertResId = 0;
            String digiCertDesc = null;
            String digiCertlabel = null;
            int SSLCertDiscoverytAccId = 0;
            int SSLCertDiscoveryResId = 0;
            String SSLCertDiscoveryDesc = null;
            String SSLCertDiscoverylabel = null;
            int dynamicImgAuthAccId = 0;
            int dynamicImgAuthResId = 0;
            String dynamicImgAuthDesc = null;
            String dynamicImgAuthlabel = null;
            if (accessObj != null) {
                for (int acci = 0; acci < accessObj.length; acci++) {
                    if (accessObj[acci].getStatus() != GlobalStatus.DELETED && accessObj[acci].getStatus() != GlobalStatus.SUSPEND) {
                        Accesspoint apdetails = accessObj[acci];
                        TransformDetails transformDetails = new TransformManagement().getTransformDetails(apdetails.getChannelid(), apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                        if (transformDetails != null) {
                            warfiles = new WarFileManagement().getWarFile(apdetails.getChannelid(), apdetails.getApId());
                            if (warfiles != null) {
                                String[] resources = apdetails.getResources().split(",");
                                ResourceDetails resObj = null;
                                if (resources != null) {
                                    for (int j = 0; j < resources.length; j++) {
                                        resObj = new ResourceManagement().getResourceById(Integer.parseInt(resources[j]));
                                        if (apdetails.getName().contains("DBAsService")) {
                                            dbaccesspointID = apdetails.getApId();
                                            dbResId = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                dbSerDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                dbdisplayName = apdetails.getDisplayName();
                                            }
                                        } else if (apdetails.getName().contains("DocumentUtility")) {
                                            documentUtilityaccesspointID = apdetails.getApId();
                                            documentUtilityResId = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                docUtilityDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                docUtilitylabel = apdetails.getDisplayName();
                                            }
                                        } else if (apdetails.getName().contains("GoogleTokenAuth")) {
                                            gogAuthaccesspointID = apdetails.getApId();
                                            gogAuthResId = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                gogAuthDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                gogAuthlabel = apdetails.getDisplayName();
                                            }
                                        } else if (apdetails.getName().contains("ImageUtility")) {
                                            imgUtilityAcc = apdetails.getApId();
                                            imgUtilityRes = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                imgUtilityDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                imgUtilitylabel = apdetails.getDisplayName();
                                            }
                                        } else if (apdetails.getName().contains("Tokenization")) {
                                            tokanisationAcc = apdetails.getApId();
                                            tokanisationRes = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                tokanisationDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                tokanisationlabel = apdetails.getDisplayName();
                                            }
                                        } else if (apdetails.getName().contains("ValueAddedServices")) {
                                            valueAddedAccId = apdetails.getApId();
                                            valueAddedResId = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                valueAddedAccDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                valueAddedReslabel = apdetails.getDisplayName();
                                            }
                                        } else if (apdetails.getName().contains("IpToGeo")) {
                                            ip2geoAccId = apdetails.getApId();
                                            ip2geoResId = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                ip2geoAccDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                ip2geoAcclabel = apdetails.getDisplayName();
                                            }
                                        } else if (apdetails.getName().contains("InstantWebChat")) {
                                            instantWebChatAccId = apdetails.getApId();
                                            instantWebChatResId = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                instantWebChatDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                instantWebChatlabel = apdetails.getDisplayName();
                                            }
                                        } else if (apdetails.getName().contains("ReverseProxy")) {
                                            reverseProxyAccId = apdetails.getApId();
                                            reverseProxytResId = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                reverseProxyDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                reverseProxylabel = apdetails.getDisplayName();
                                            }
                                        } else if (apdetails.getName().contains("InAppPush")) {
                                            inappPushAccId = apdetails.getApId();
                                            inappPushResId = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                inappPushDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                inappPushlabel = apdetails.getDisplayName();
                                            }
                                        } else if (apdetails.getName().contains("ServerMonitoring")) {
                                            serMoniAccId = apdetails.getApId();
                                            serMoniResId = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                serMoniDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                serMonilabel = apdetails.getDisplayName();
                                            }
                                        } else if (apdetails.getName().contains("DigitalCertificateIssuer")) {
                                            digiCertAccId = apdetails.getApId();
                                            digiCertResId = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                digiCertDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                digiCertlabel = apdetails.getDisplayName();
                                            }
                                        } else if (apdetails.getName().contains("SSLCertDiscovery")) {
                                            SSLCertDiscoverytAccId = apdetails.getApId();
                                            SSLCertDiscoveryResId = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                SSLCertDiscoveryDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                SSLCertDiscoverylabel = apdetails.getDisplayName();
                                            }
                                        } else if (apdetails.getName().contains("DynamicImageAuth")) {
                                            dynamicImgAuthAccId = apdetails.getApId();
                                            dynamicImgAuthResId = resObj.getResourceId();
                                            if (resObj.getDescription() != null) {
                                                dynamicImgAuthDesc = resObj.getDescription();
                                            }
                                            if (apdetails.getDisplayName() != null) {
                                                dynamicImgAuthlabel = apdetails.getDisplayName();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        %>
        <div id="wrapper1">
            <div id="wrapper" style="min-height:0%!important">
                <div id="homePage">
                    <div class="content animate-panel">
                        <div class="row">
                            <div class="col-lg-3 tour-8">
                                <div class="hpanel stats">
                                    <div class="panel-heading">

                                        Today's total credit used
                                    </div>
                                    <div class="panel-body h-200">
                                        <div class="stats-icon text-center ">
                                            <i class="pe-7s-share fa-4x"></i>
                                        </div>
                                        <div class="m-t-xl">
                                            <div class="progress m-t-xs full progress-small">
                                                <div id="percentage" aria-valuemax="100" style="width: 10%" aria-valuemin="0" aria-valuenow="0" role="progressbar" class=" progress-bar progress-bar-info">
                                                    <span class="sr-only">100%</span>
                                                </div>
                                            </div> 
                                            <div class="row" style="height: 60px">
                                                <div class="col-xs-6">
                                                    <small class="stats-label">Credit used</small>
                                                    <h4>50</h4>
                                                </div>
                                                <div class="col-xs-6">
                                                    <small class="stats-label">% Credit used</small>
                                                    <h4>10%</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 tour-9">
                                <div class="hpanel">
                                    <div class="panel-heading">

                                        Last 7 days total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-graph1 fa-4x"></i>
                                        <h1 class="m-b-xs text-info" id="lastWeekAmount">300</h1>
                                        <small id="weekMsg">You have used total 300 credits in last week for all services</small>
                                        <h3 class="font-bold no-margins lead text-info row">
                                            Weekly expense
                                        </h3> <!--<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit</small>-->
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 tour-10">
                                <div class="hpanel">
                                    <div class="panel-heading">

                                        Last month total credit used
                                    </div>
                                    <div class="panel-body text-center h-200">
                                        <i class="pe-7s-global fa-4x"></i>
                                        <h1 class="m-b-xs text-info" id="monthCredit">1000</h1>

                                        <small id="monthMsg">You have used total 1000 credits in last week for all services</small>
                                        <h3 class="font-bold no-margins lead text-info row">
                                            Monthly usage
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 tour-12" id="topmostServices" data-child="hpanel" data-effect="fadeInLeft">
                                <div class="hpanel">
                                    <div class="panel-heading">

                                        <span id="mostlyFiveTour"></span>
                                    </div>
                                    <div class="panel-body" style="height: 213px">
                                        <div class="flot-chart text-center" style="width: 200;height:250">
                                            <div class="flot-chart-content"  id="flot-pie-chartTour"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3" id = "mostlyservices"  style="display: none">
                                <div class="hpanel">
                                    <div class="panel-heading">

                                        Your mostly used services
                                    </div>
                                    <div class="panel-body">                                        
                                        <img src="images/no_record_found.png" alt="No record found" width="200" height="170"/>
                                        <br>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row tour-11">
                            <div class="col-lg-12 ">
                                <div class="hpanel ">
                                    <div class="panel-heading">

                                        Services Information And Statistics
                                    </div>
                                    <div class="panel-body" >
                                        <div class="text-left">
                                            <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Last 7 day expenditure</span>
                                        </div>
                                        <div class="btn-group" style="padding-left: 60%">
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in">Top five service</button>
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" >Today total expenditure</button>
                                            <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in">Todays trending services</button>
                                        </div>
                                        <div  id="topFiveSerTour"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>

        <div id="wrapper" style="min-height: 0%!important">
            <div class="content animate-panel">
                <div class="small-header transition animated fadeIn">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12 tour-20">
                            <div class="hpanel">
                                <div class="panel-body">
                                    <div class="row text-center">
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="pe-7s-server text-info icon-big fa-3x"></i></h4>
                                                    <%if (dbdisplayName == null) {%>
                                                <strong>DB As Service</strong>
                                                <%} else {%>
                                                <strong><%=dbdisplayName%></strong>
                                                <%}
                                                    if (dbSerDesc == null) {%>                        
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(dbSerDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="pe-7s-copy-file text-info icon-big fa-3x"></i></h4>
                                                    <%if (dbdisplayName == null) {%>
                                                <strong>Document Utility</strong>
                                                <%} else {%>
                                                <strong><%=docUtilitylabel%></strong>
                                                <%}
                                                    if (docUtilityDesc == null) {%>         
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(docUtilityDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="fa fa-google text-info icon-big fa-3x"></i></h4>
                                                    <%if (gogAuthlabel == null) {%>
                                                <strong>Google Auth Token</strong>
                                                <%} else {%>
                                                <strong><%=gogAuthlabel%></strong>
                                                <%}
                                                    if (gogAuthDesc == null) {%>       
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(gogAuthDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="fa fa-photo text-info icon-bi fa-3x"></i></h4>
                                                    <%if (imgUtilitylabel == null) {%>
                                                <strong>Image Utility</strong>
                                                <%} else {%>
                                                <strong><%=imgUtilitylabel%></strong>
                                                <%}
                                                    if (imgUtilityDesc == null) {%>    
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(imgUtilityDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row text-center">
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="fa fa-try text-info icon-big fa-3x"></i></h4>
                                                    <%if (tokanisationlabel == null) {%>
                                                <strong>Tokenization</strong>
                                                <%} else {%>
                                                <strong><%=tokanisationlabel%></strong>
                                                <%}
                                                    if (tokanisationDesc == null) {%>        
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(tokanisationDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="pe-7s-science text-info icon-big fa-3x"></i></h4>
                                                    <%if (valueAddedReslabel == null) {%>
                                                <strong>Value Added Service</strong>
                                                <%} else {%>
                                                <strong><%=valueAddedReslabel%></strong>
                                                <%}
                                                    if (valueAddedAccDesc == null) {%>       
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(valueAddedAccDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="pe-7s-map-marker text-info icon-big fa-3x"></i></h4>
                                                    <%if (ip2geoAcclabel == null) {%>
                                                <strong>IP2GEO</strong>
                                                <%} else {%>
                                                <strong><%=ip2geoAcclabel%></strong>
                                                <%}
                                                    if (ip2geoAccDesc == null) {%>         
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(ip2geoAccDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>                    
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="fa fa-users text-info icon-big fa-3x"></i></h4>
                                                    <%if (instantWebChatlabel == null) {%>
                                                <strong>Instant WebChat</strong>
                                                <%} else {%>
                                                <strong><%=instantWebChatlabel%></strong>
                                                <%}
                                                    if (instantWebChatDesc == null) {%>         
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(instantWebChatDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row text-center">                    
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="pe-7s-loop text-info icon-big fa-3x"></i></h4>
                                                    <%if (reverseProxylabel == null) {%>
                                                <strong>Reverse Proxy</strong>
                                                <%} else {%>
                                                <strong><%=reverseProxylabel%></strong>
                                                <%}
                                                    if (reverseProxyDesc == null) {%>             
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(reverseProxyDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="pe-7s-comment text-info icon-big fa-3x"></i></h4>
                                                    <%if (inappPushlabel == null) {%>
                                                <strong>In App Push</strong>
                                                <%} else {%>
                                                <strong><%=inappPushlabel%></strong>
                                                <%}
                                                    if (inappPushDesc == null) {%>         
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(inappPushDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="pe-7s-network text-info icon-big fa-3x"></i></h4>
                                                    <%if (serMonilabel == null) {%>
                                                <strong>Application Server Monitoring</strong>
                                                <%} else {%>
                                                <strong><%=serMonilabel%></strong>
                                                <%}
                                                    if (serMoniDesc == null) {%>  
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(serMoniDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="fa fa-file-code-o text-info icon-big fa-3x"></i></h4>
                                                    <%if (digiCertlabel == null) {%>
                                                <strong>Digital Cerificate Issuer</strong>
                                                <%} else {%>
                                                <strong><%=digiCertlabel%></strong>
                                                <%}
                                                    if (digiCertDesc == null) {%>  
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(digiCertDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>                                        
                                    </div>
                                    <div class="row text-center">
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="fa  fa-gears text-info icon-big fa-3x"></i></h4>
                                                    <%if (SSLCertDiscoverylabel == null) {%>
                                                <strong>SSL Cert Discovery</strong>
                                                <%} else {%>
                                                <strong><%=SSLCertDiscoverylabel%></strong>
                                                <%}
                                                    if (SSLCertDiscoveryDesc == null) {%>  
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(SSLCertDiscoveryDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>                    
                                        <div class="col-md-3">
                                            <a><h4 class="m-t-lg"><i class="fa fa-file-picture-o text-info icon-big fa-3x"></i></h4>
                                                    <%if (dynamicImgAuthlabel == null) {%>
                                                <strong>Dynamic Image Authentication</strong>
                                                <%} else {%>
                                                <strong><%=dynamicImgAuthlabel%></strong>
                                                <%}
                                                    if (dynamicImgAuthDesc == null) {%>  
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                                <%} else {%>
                                                <p><%=new String(Base64.decode(dynamicImgAuthDesc))%></p>
                                                <%}%>
                                            </a>
                                        </div>                    
                                    </div>                
                                </div>
                            </div>
                            <br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="wrapper" style="min-height:0%!important">
            <div class="hpanel" style="margin-bottom: 0px!important">
                <div class="panel-body">                                                      
                    <div>
                        <table>
                            <tr>
                                <td><h5><a>https://www.readyapis.com:8443/InstantWebChatV1/Impl_InstantWebChat?wsdl</a>&nbsp;&nbsp;&nbsp; <a class="btn btn-info btn-xs">Copy</a></h5></td>
                                <td>
                                    <font class="col-lg-2" id="SOAPURLCopyDiv"></font>
                                </td>
                            </tr>
                        </table>                        
                    </div>    
                </div>
            </div>
            <div class="content animate-panel">
                <div class="row">
                    <div class="col-lg-6 tour-21">				
                        <div class="hpanel">		
                            <div class="panel-heading">
                                <h3>Your request</h3>
                            </div>				
                            <div class="panel-body">					                                        
                                <form  role="form" class="form-horizontal" id="getBody" name="getBody">
                                    <div class="tour-23">
                                        <div class="form-group">
                                            <div class="col-sm-2"><h3>Header</h3></div>
                                            <div class="col-sm-2" style="margin-top: 3%">
                                                <a class="btn btn-info btn-xs">Copy</a>
                                            </div>
                                        </div>

                                        <table id="_headerTable" CELLSPACING="15" >                                                
                                            <tr>                                                    
                                                <td width="35%">
                                                    <input type="text" class="form-control" id="_myNameRM" name="_myNameRM" value="APITokenId">                                                        
                                                </td>
                                                <td width="90%">
                                                    <div style='padding: 1%'>
                                                        <input type="text" class="form-control" id="_myResType" name="_myResType" value="vEBDceyF4TRRkZq1qBo+NaYRgV4=">                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="35%">
                                                    <input type="text" class="form-control" id="_myNameRM" name="_myNameRM" value="PartnerId">                                                        
                                                </td>
                                                <td width="90%">
                                                    <div style='padding: 1%'>
                                                        <input type="text" class="form-control" id="_myResType" name="_myResType" value="XteIg8dYAYwRiHGz0A300Lwog+w=">                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="35%">
                                                    <input type="text" class="form-control" id="_myNameRM" name="_myNameRM" value="PartnerTokenId">                                                        
                                                </td>
                                                <td width="90%">
                                                    <div style='padding: 1%'>
                                                        <input type="text" class="form-control" id="_myResType" name="_myResType" value="wkV52UiG64dweuOFX+5UoJXNfG4=">                                                        
                                                    </div>
                                                </td>
                                                <td >                                                        
                                                    <a class='btn btn-info btn-sm'   id="addUserButton"><span class='fa fa-plus-circle'></span></a>
                                                </td>  
                                                <td style="padding: 1%">
                                                    <a class='btn btn-warning btn-sm' id="minusUserButton"><span class='fa fa-minus-circle'></span></a>
                                                </td>                                              
                                            </tr>                                                   
                                        </table>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div style="display: none">
                                        <h3>Basic Authentication</h3>
                                        <div class="form-group">
                                            <div class="col-sm-6"><input type="text" id="_userNameAuth" name="_userNameAuth"  class="form-control" placeholder="Username"></div>
                                            <div class="col-sm-6"><input type="text" id="_passwordAuth" name="_passwordAuth" class="form-control" placeholder="Password"></div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                    </div>
                                    <div class="tour-24">
                                        <div class="form-group">
                                            <div class="col-sm-2"><h3>Body</h3></div>
                                            <div class="col-sm-2" style="margin-top: 3%">
                                                <a class="btn btn-info btn-xs">Copy</a>
                                            </div>
                                        </div>
                                        <!--                                        <h3>Body</h3> &nbsp;&nbsp;&nbsp; <a class="btn btn-info btn-xs">Copy</a>-->
                                        <textarea class="form-control" id="_APIbody" name="_APIbody" style="min-height: 500px;">
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.wsdl.wadl.sg.mollatech.com/">
        <soapenv:Header/>
        <soapenv:Body>
            <web:createChatRoom>
                <!--Optional:-->
                <developerEmail>abhishek@mollatech.com</developerEmail>
                <!--Optional:-->
                <numberOfUsers>2</numberOfUsers>
                <!--Optional:-->
                <groupName>Buzz</groupName>
                <!--Zero or more repetitions:-->
                <userEmailIds>jerry@noemail.com</userEmailIds>
                <userEmailIds>tom@noemail.com</userEmailIds>
                <!--Optional:-->
                <urlexpiryTimeInMinutes>15</urlexpiryTimeInMinutes>
            </web:createChatRoom>
        </soapenv:Body>
    </soapenv:Envelope>
                                        </textarea>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <button class="btn btn-primary ladda-button" data-style="zoom-in" id="apiConsoleRequest" type="button"><i class="fa fa-terminal"></i> Submit</button>                                    
                                </form>								
                            </div>					
                        </div>				
                    </div>
                    <div class="col-lg-6 ">
                        <div class="hpanel tour-22">
                            <div class="panel-heading">
                                <h3>Your response</h3>
                            </div>
                            <div class="panel-body">
                                <form>
                                    <textarea style="min-height: 220px;" class="form-control" id="responseID" name="responseID">
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:createChatRoomResponse xmlns:ns2="http://webservice.wsdl.wadl.sg.mollatech.com/" xmlns:ns3="http://com.mollatech.sg.sgholder">
            <return>
                <chatURL>https://www.readyapis.com:9443/WebChat/index.jsp?3a4e8f81-f33d-4c81-8b04-54a162de020c</chatURL>
                <resultCode>0</resultCode>
                <result>3a4e8f81-f33d-4c81-8b04-54a162de020c Chat Room Is Created Successfully.</result>
                <password>55f0a7</password>
            </return>
        </ns2:createChatRoomResponse>
    </S:Body>
</S:Envelope>
                                    </textarea>
                                </form>
                                <br>
                                <div id="startValuesAndTargetExample">
                                    <p class="text-info"><i class="fa fa-check-square"></i> Congratulations, Your API takes  00:06 sec to get response. </p>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div id="questionAndAnswer" class="tour-25 col-lg-6 ">
                        <div class="hpanel panel-group " id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel-body">
                                <h4 class="m-t-none m-b-none">Need Help </h4>
                                <small class="text-muted">All general questions about our API.</small>&nbsp;&nbsp;&nbsp;
                                <a  class="btn btn-primary btn-xs ladda-button" data-style="zoom-in" type="button"  style="margin-left: 1px"><i class="fa fa-check"></i> Raise Ticket</a> <br>
                            </div>
                            <div class="panel-body" id="_apiLevelToken" >
                                <a data-toggle="collapse" data-parent="#accordion" href="#q1" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Where can I get APITokenId ?
                                </a>
                                <div id="q1" class="panel-collapse collapse">
                                    <hr>APITokenId is unique for each API, you can get it from when you click on Services tab you get the service icon list after click on specific icon you get the list of API of that service. Form that list under API Brief column you get your APITokenId.
                                </div>
                            </div>                                                               <!--
                            -->                                <div class="panel-body" id="_headerError">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q2" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Why need Headers ?
                                </a>
                                <div id="q2" class="panel-collapse collapse">
                                    <hr>Headers are required for security constraint. To validate your request.
                                </div>
                            </div><!--
                            -->                             <div class="panel-body" id="connectionrefuse" style="display: none">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q3" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    Connection refused (Connection refused)?
                                </a>
                                <div id="q3" class="panel-collapse collapse">
                                    <hr>When your calling service is not found or not available that time you get this error
                                </div>
                            </div>                                               
                            <div class="panel-body">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q5" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    No Such Developer is Available.
                                </a>
                                <div id="q5" class="panel-collapse collapse">
                                    <hr>You get this error when your account is removed by the administrator.
                                </div>
                            </div>                                                                                                                         
                            <div class="panel-body">
                                <a data-toggle="collapse" data-parent="#accordion" href="#q12" aria-expanded="true">
                                    <i class="fa fa-chevron-down pull-right text-muted"></i>
                                    No Such Web service is Available.
                                </a>
                                <div id="q12" class="panel-collapse collapse">
                                    <hr>This web service is removed by the administrator thats why you get error.
                                </div>                                
                            </div>                     
                        </div>
                    </div>
                </div>             
            </div>
        </div>


        <div id="wrapper" style="min-height:0%!important">
            <div class="content">
                <div class="row">
                    <div class="col-md-12 tour-13">
                        <div class="hpanel">
                            <div class="panel-body">
                                <input type="text" class="form-control input-sm m-b-md" id="filter" placeholder="Search in table">                    
                                <table id="api" class="footable table table-stripped table-responsive " data-page-size="25" data-filter=#filter>    
                                    <thead>
                                        <tr>                                    
                                            <th style="text-align: center">API Name</th>
                                            <th style="text-align: center" class="tour-14">API Brief</th>                                    
                                            <th style="text-align: center" class="tour-15">Credits</th>
                                            <th style="text-align: center" class="tour-16">Performace</th>
                                            <th data-hide="phone,tablet" style="text-align: center">Credit Used As</th>                                    
                                            <th style="text-align: center" class="tour-17">Test</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                                   
                                            <td  style="font-size: 15px">testConnection</td>                               
                                            <td style="text-align: center">                                    
                                                <a class="text-warning ladda-button" data-style="zoom-in" id="reAssignToken"  type="button" data-toggle="tooltip" data-placement="right" title="Regenerate API Token"><i class="pe pe-7s-refresh-cloud pe-3x"></i></a>                                                    
                                                <a class="text-info ladda-button" data-style="zoom-in" id="viewToken" type="button" data-toggle="tooltip" data-placement="right" title="View API Token" style="margin-left: 5%"><i class="pe pe-7s-display2 pe-3x"></i></a>
                                                <a class="text-warning ladda-button" data-style="zoom-in" data-toggle="tooltip" id="viewApiDes" data-placement="right" title=""  style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>
                                                <a class="text-danger" type="button" data-toggle="tooltip" data-placement="right" title="API Documentation" target="_blank" style="margin-left: 5%"><i class="pe pe-7s-note2 pe-3x"></i></a> 
                                            </td>                                              

                                            <td style="text-align: center">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit"  type="button"  data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit" type="button" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>

                                            <td style="text-align: center">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance" type="button" data-toggle="tooltip" data-placement="right" title="Tody Performance "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance" type="button" data-toggle="tooltip" data-placement="right" title="Monthly Performace" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>
                                            <td style="text-align: center">
                                                <p style="font-size: 15px">1 Credit Per API</p>
                                            </td>
                                            <td style="text-align: center" class="tour-iconAPIConsole">
                                                <a class="text-success " data-toggle="tooltip" data-placement="right" title="Test" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                            </td>                                                               
                                        </tr>  

                                        <tr>                                   
                                            <td  style="font-size: 15px">saveConnection</td>                               
                                            <td style="text-align: center">                                    
                                                <a class="text-warning ladda-button" data-style="zoom-in" id="reAssignToken"  type="button"  data-toggle="tooltip" data-placement="right" title="Regenerate API Token"><i class="pe pe-7s-refresh-cloud pe-3x"></i></a>                                                    
                                                <a class="text-info ladda-button" data-style="zoom-in" id="viewToken" type="button"  data-toggle="tooltip" data-placement="right" title="View API Token" style="margin-left: 5%"><i class="pe pe-7s-display2 pe-3x"></i></a>
                                                <a  class="text-warning ladda-button" data-style="zoom-in" data-toggle="tooltip" id="viewApiDes" data-placement="right" title=""  style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>
                                                <a  class="text-danger" type="button" data-toggle="tooltip" data-placement="right" title="API Documentation" target="_blank" style="margin-left: 5%"><i class="pe pe-7s-note2 pe-3x"></i></a> 
                                            </td>                                              

                                            <td style="text-align: center">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit"  type="button"  data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit" type="button"  data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>

                                            <td style="text-align: center">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance" type="button"  data-toggle="tooltip" data-placement="right" title="Tody Performance "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance" type="button" data-toggle="tooltip" data-placement="right" title="Monthly Performace" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>
                                            <td style="text-align: center">
                                                <p style="font-size: 15px">1 Credit Per API</p>
                                            </td>
                                            <td style="text-align: center">
                                                <a class="text-success" data-toggle="tooltip" data-placement="right" title="Test" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                            </td>                                                               
                                        </tr>        

                                        <tr>                                   
                                            <td  style="font-size: 15px">getConnectionId</td>                               
                                            <td style="text-align: center">                                    
                                                <a class="text-warning ladda-button" data-style="zoom-in" id="reAssignToken"  type="button"  data-toggle="tooltip" data-placement="right" title="Regenerate API Token"><i class="pe pe-7s-refresh-cloud pe-3x"></i></a>                                                    
                                                <a class="text-info ladda-button" data-style="zoom-in" id="viewToken" type="button" data-toggle="tooltip" data-placement="right" title="View API Token" style="margin-left: 5%"><i class="pe pe-7s-display2 pe-3x"></i></a>
                                                <a  class="text-warning ladda-button" data-style="zoom-in" data-toggle="tooltip" id="viewApiDes" data-placement="right" title=""  style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>
                                                <a class="text-danger" type="button" data-toggle="tooltip" data-placement="right" title="API Documentation" target="_blank" style="margin-left: 5%"><i class="pe pe-7s-note2 pe-3x"></i></a> 
                                            </td>                                              

                                            <td style="text-align: center">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit"  type="button" data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit" type="button"  data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>

                                            <td style="text-align: center">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance" type="button"  data-toggle="tooltip" data-placement="right" title="Tody Performance "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance" type="button"  data-toggle="tooltip" data-placement="right" title="Monthly Performace" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>
                                            <td style="text-align: center">
                                                <p style="font-size: 15px">1 Credit Per API</p>
                                            </td>
                                            <td style="text-align: center">
                                                <a class="text-success" data-toggle="tooltip" data-placement="right" title="Test" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                            </td>                                                               
                                        </tr>        

                                        <tr>                                   
                                            <td  style="font-size: 15px">getSQLId</td>                               
                                            <td style="text-align: center">                                    
                                                <a class="text-warning ladda-button" data-style="zoom-in" id="reAssignToken"  type="button" data-toggle="tooltip" data-placement="right" title="Regenerate API Token"><i class="pe pe-7s-refresh-cloud pe-3x"></i></a>                                                    
                                                <a class="text-info ladda-button" data-style="zoom-in" id="viewToken" type="button" data-toggle="tooltip" data-placement="right" title="View API Token" style="margin-left: 5%"><i class="pe pe-7s-display2 pe-3x"></i></a>
                                                <a  class="text-warning ladda-button" data-style="zoom-in" data-toggle="tooltip" id="viewApiDes" data-placement="right" title=""  style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>
                                                <a  class="text-danger" type="button" data-toggle="tooltip" data-placement="right" title="API Documentation" target="_blank" style="margin-left: 5%"><i class="pe pe-7s-note2 pe-3x"></i></a> 
                                            </td>                                              

                                            <td style="text-align: center">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit"  type="button" data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit" type="button" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>

                                            <td style="text-align: center">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance" type="button" data-toggle="tooltip" data-placement="right" title="Tody Performance "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance" type="button" data-toggle="tooltip" data-placement="right" title="Monthly Performace" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>
                                            <td style="text-align: center">
                                                <p style="font-size: 15px">1 Credit Per API</p>
                                            </td>
                                            <td style="text-align: center">
                                                <a class="text-success" data-toggle="tooltip" data-placement="right" title="Test" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                            </td>                                                               
                                        </tr>        

                                        <tr>                                   
                                            <td  style="font-size: 15px">getSQL</td>                               
                                            <td style="text-align: center">                                    
                                                <a class="text-warning ladda-button" data-style="zoom-in" id="reAssignToken"  type="button" data-toggle="tooltip" data-placement="right" title="Regenerate API Token"><i class="pe pe-7s-refresh-cloud pe-3x"></i></a>                                                    
                                                <a class="text-info ladda-button" data-style="zoom-in" id="viewToken" type="button" data-toggle="tooltip" data-placement="right" title="View API Token" style="margin-left: 5%"><i class="pe pe-7s-display2 pe-3x"></i></a>
                                                <a  class="text-warning ladda-button" data-style="zoom-in" data-toggle="tooltip" id="viewApiDes" data-placement="right" title=""  style="margin-left: 5%"><i class="pe pe-7s-news-paper pe-3x"></i></a>
                                                <a class="text-danger" type="button" data-toggle="tooltip" data-placement="right" title="API Documentation" target="_blank" style="margin-left: 5%"><i class="pe pe-7s-note2 pe-3x"></i></a> 
                                            </td>                                              

                                            <td style="text-align: center">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayCredit"  type="button" data-toggle="tooltip" data-placement="right" title="Today Credit Used for"><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyCredit" type="button" data-toggle="tooltip" data-placement="right" title="Monthly Credit Used for" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>

                                            <td style="text-align: center">
                                                <a class="text-error ladda-button" data-style="zoom-in" id="todayPerformance" type="button" data-toggle="tooltip" data-placement="right" title="Tody Performance "><i class="pe pe-7s-graph2 pe-3x"></i></a>                                                    
                                                <a class="text-primary-2 ladda-button" data-style="zoom-in" id="monthlyPerformance" type="button" data-toggle="tooltip" data-placement="right" title="Monthly Performace" style="margin-left: 5%"><i class="pe pe-7s-display1 pe-3x"></i></a>
                                            </td>
                                            <td style="text-align: center">
                                                <p style="font-size: 15px">1 Credit Per API</p>
                                            </td>
                                            <td style="text-align: center">
                                                <a class="text-success" data-toggle="tooltip" data-placement="right" title="Test" id="testFirstAPI"><i class="pe pe-7s-science pe-3x"></i></a>
                                            </td>                                                               
                                        </tr>        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="6">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>                   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  

        <div id="wrapper" style="min-height:0%!important">
            <div class="content animate-panel">        
                <div class="row ">
                    <div class="col-lg-12 tour-reportWindow">
                        <div class="hpanel">
                            <div class="panel-heading">
                                <div class="panel-tools">
                                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                    <a class="closebox"><i class="fa fa-times"></i></a>
                                </div>
                                <h4>Credit Information And Statistics</h4>
                            </div>
                            <div class="panel-body">
                                <div class="text-left">
                                    <i class="fa fa-bar-chart-o fa-fw"></i><span id="homeReportLable">  Last 7 days expenditure</span>
                                </div>
                                <div id="report_data_button">
                                    <div class="btn-group" style="padding-left: 70%">
                                        <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" > Daily</button>
                                        <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" > Last 7 days</button>
                                        <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" > Last 30 days</button>
                                    </div>
                                </div>
                                <hr class="m-b-xl"/>                         
                                <div id="expenditureReportTour">
                                </div>
                                <div id="noRecordFoundData" style="display: none" style="margin-bottom: 30%">
                                    <img src="images/no_record_found.jpg" alt="No record found" width="300px" height="300px" style="margin-left: 35%"/>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>       
            </div>   
        </div>                
        <script>
            $(function () {

                var dateArr2 = ["Data e Ora", '2017-07-30', '2017-07-31', '2017-08-01', '2017-08-02', '2017-08-03', '2017-08-04', '2017-08-05'];
                var txAmount2 = ['CreditUsed', 10, 50, 60, 90, 40, 100, 40];
                var callCountArr2 = ['Call Count', 2, 15, 20, 30, 15, 40, 20];
                console.log("arrXaxis >> " + dateArr2);
                console.log("callCountArr2 >> " + callCountArr2);
                console.log("txAmount2  >> " + txAmount2);

                var chart = c3.generate({
                    bindto: '#expenditureReportTour',
                    data: {
                        x: 'Data e Ora',
                        xFormat: '%Y-%m-%d',
                        columns: [
                            dateArr2,
                            txAmount2,
                            callCountArr2
                        ],
                        colors: {
                            //                TransactionAmount: '#62cb31',
                            //                callCountArr2: '#FF7F50'
                            dateArr2: '#62cb31',
                            CreditUsed: '#FF7F50'
                        },
                        types: {
                            //CallCount: 'bar',
                            CreditUsed: 'bar'
                        },
                        groups: [
                            ['Transcation Amount', 'Call Count']
                        ]
                    },
                    subchart: {
                        show: true
                    },
                    axis: {
                        x: {
                            type: 'timeseries',
                            // if true, treat x value as localtime (Default)
                            // if false, convert to UTC internally                        
                            tick: {
                                format: '%d-%m-%Y'
                                        //fomat:function (x) { return x.getFullYear(); }
                            }
                        }
                    }
                });
            });
        </script>  

        <div id="wrapper" style="min-height:0%!important">
            <div class="content animate-panel">
                <div class="row tour-apiUsageTour">
                    <div class="hpanel">
                        <div class="panel-body">                    
                            <div class="col-md-12">
                                <div class="col-sm-3">
                                    <select class="js-source-states-33" id="apiUsageAccesspoint" name="apiUsageAccesspoint" multiple="multiple" style="width: 100%">
                                        <option value="DBAsService" selected>DB As Service</option>
                                    </select>
                                </div>
                                <div class="col-sm-3"><input  value="08/26/2017" type="text" class="form-control" placeholder="from"></div>
                                <div class="col-sm-3"><input value="09/10/2017" type="text" class="form-control" placeholder="to"></div>
                                <div class="col-sm-3">
                                    <button class="btn btn-success ladda-button btn-sm btn-block" data-style="zoom-in" id="generateButton" type="submit"><i class="fa fa-bar-chart"></i> Generate</button> 
                                </div>
                            </div>

                            <br/><br/><br/>
                            <script>
                                $(function () {
                                    $(".js-source-states-33").select2();

                                });
                            </script>

                            <div class="col-lg-12">
                                <form class="form-horizontal " method="get">       
                                    <div class="dataTable_wrapper table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead id="apiUsageHeader">
                                                <tr>
                                                    <th>Sr.No.</th>
                                                    <th>Date</th>
                                                    <th>API</th>
                                                    <th>Service</th>                                        
                                                    <th>Version</th>                                        
                                                    <th>Usage</th>                                        
                                                    <th>Credit Used</th>                                        
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>                                    
                                                    <td>01/09/2017</td>         
                                                    <td>getColsFromTables</td>
                                                    <td>DB As Service</td>                                    
                                                    <td>1</td>
                                                    <td>1797</td>
                                                    <td>1,797</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>                                    
                                                    <td>01/09/2017</td>         
                                                    <td>testConnection</td>
                                                    <td>DB As Service</td>                                    
                                                    <td>1</td>
                                                    <td>3054</td>
                                                    <td>3,054</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6"><strong>TOTAL</strong></td>                                    
                                                    <td>4,851</td>                                                 
                                                </tr>   
                                            </tbody>
                                        </table>
                                    </div>
                                </form>
                                <a  class="btn btn-success btn-xs">Download PDF <i class="fa fa-file-pdf-o"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="wrapper" style="min-height:0%!important">
            <div class="content animate-panel">        
                <div class="row ">
                    <div class="col-lg-12 tour-transactionReportWindow">
                        <div class="hpanel">
                            <div class="panel-heading">                            
                                <h4>Transaction Report</h4>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="col-sm-3">
                                        <select class="form-control m-b">
                                            <option value="In App Push" selected>Document Utility</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3"><input  value="08/26/2017" type="text" class="form-control" placeholder="from"></div>
                                    <div class="col-sm-3"><input value="09/10/2017" type="text" class="form-control" placeholder="to"></div>
                                    <div class="col-sm-3">
                                        <button class="btn btn-success ladda-button btn-sm btn-block" data-style="zoom-in" id="generateButton" type="submit"><i class="fa fa-bar-chart"></i> Generate</button> 
                                    </div>
                                </div>

                                <div id="report_data_button">
                                    <div class="btn-group" style="padding-left: 730px">
                                        <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" > Transaction Status</button>
                                        <button  class="btn btn-default btn-xs ladda-button" data-style="zoom-in" > Transaction Per Day</button>                                    
                                    </div>
                                </div>                                                    
                                <div id="transactionReport">
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>       
            </div>   
        </div>
        <script>
            $(function () {
                var txarrXaxis;
                var txarrYaxis;
                //        txarrXaxis.push();
                //        txarrYaxis.push();
                txarrXaxis = ["Call Count", 'ALLOWED', 'TIME', 'DAY', 'IP', 'TPS', 'TPD', 'SSL', 'TOKEN', 'BLOCK_IP', 'SUBSCRIPTION', 'APITOKEN', 'BILLING', 'SERVER_ERROR'];
                txarrYaxis = ["Transactions Status", 230, 120, 60, 40, 20, 160, 120, 270, 300, 120, 70, 190, 10];
                c3.generate({
                    bindto: '#transactionReport',
                    data: {
                        x: 'Call Count',
                        //xFormat: '%Y-%m-%d %H:%M:%S',
                        columns: [
                            txarrXaxis,
                            txarrYaxis
                        ],
                        colors: {
                            //data1: '#62cb31',
                            arrYaxis: '#FF7F50'
                        },
                        type: 'bar',
                        groups: [
                            ['Transactions Status']
                        ]
                    },
                    subchart: {
                        show: true
                    },
                    axis: {
                        x: {
                            type: 'category',
                            // if true, treat x value as localtime (Default)
                            // if false, convert to UTC internally                        
                            categories: txarrXaxis
                        }
                    }
                });
            });
        </script>

        <div id="wrapper" style="min-height:0%!important">
            <div class="content animate-panel">        
                <div class="row ">
                    <div class="col-lg-12 tour-performanceReportWindow">
                        <div class="hpanel">
                            <div class="panel-heading">                            
                                <h4>Performance Report</h4>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="col-sm-3">
                                        <select class="form-control m-b">
                                            <option value="In App Push" selected>Document Utility</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select class="form-control m-b">
                                            <option value="10" selected>10</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select class="form-control m-b">
                                            <option value="10" selected>Sep</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select class="form-control m-b">
                                            <option value="10" selected>2017</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn btn-success ladda-button btn-sm btn-block" data-style="zoom-in" id="generateButton" type="submit"><i class="fa fa-bar-chart"></i> Generate</button> 
                                    </div>
                                </div>
                                <span id="report"><b>Performace Report</b></span>                                                
                                <div id="performanceReport">
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>       
            </div>   
        </div>
        <script>
            $(function () {
                var txarrXaxis;
                var txarrYaxis;
                txarrXaxis = ["Data e Ora", '2017-09-01 11:05:55', '2017-09-01 11:06:55', '2017-09-01 11:07:55', '2017-09-01 11:08:55', '2017-09-01 11:09:55', '2017-09-01 11:10:55', '2017-09-01 11:11:55', '2017-09-01 11:12:55', '2017-09-01 11:13:55', '2017-09-01 11:14:55', '2017-09-01 11:15:55', '2017-09-01 11:16:55', '2017-09-01 11:17:55'];
                txarrYaxis = ["Response Time", 5230, 5000, 4320, 3210, 2000, 6010, 5130, 4320, 3210, 6520, 5470, 1290, 3201];
                c3.generate({
                    bindto: '#performanceReport',
                    data: {
                        x: 'Data e Ora',
                        xFormat: '%Y-%m-%d %H:%M:%S',
                        columns: [
                            txarrXaxis,
                            txarrYaxis
                                    //data1
                        ],
                        colors: {
                            //arrYaxis1: '#62cb31',
                            arrYaxis1: '#FF7F50'
                        },
                        types: {
                            arrXaxis1: 'bar'
                        },
                        groups: [
                            ['Response Time']
                        ]
                    },
                    subchart: {
                        show: true
                    },
                    axis: {
                        x: {
                            type: 'timeseries',
                            // if true, treat x value as localtime (Default)
                            // if false, convert to UTC internally                        
                            tick: {
                                format: '%H:%M'
                                        //fomat:function (x) { return x.getFullYear(); }
                            }
                        }
                    }
                });
            });
        </script>

        <div id="wrapper" style="min-height:0%!important">
            <div class="content animate-panel">
                <div class="row tour-invoiceTour">
                    <div class="col-lg-12">
                        <div class="hpanel">
                            <div class="panel-heading">
                                <div class="panel-tools">
                                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                </div>
                                Invoices
                            </div>
                            <div class="panel-body table-responsive">
                                <table id="apiInvoiceTable" class="table table-striped table-bordered table-hover">
                                    <thead id="invoiceHeader">
                                        <tr>
                                            <th>Sr.No.</th>
                                            <th>Package Name</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Invoice Number</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            <th>Download</th>   
                                        </tr>
                                    </thead>
                                    <tbody>                                
                                        <tr>
                                            <td>1</td>                                    
                                            <td>Basic</td> 
                                            <td>12/09/2017</td>
                                            <td>05:02 PM</td>                                            
                                            <td>17220893124</td>
                                            <td>AUD 60.00</td>                                    
                                            <td>Paid</td>  
                                            <td><a class=""><i class="fa fa-file-pdf-o fa-2x"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>                                    
                                            <td>Basic</td> 
                                            <td>12/10/2017</td>
                                            <td>05:02 PM</td>                                            
                                            <td>1722089675</td>
                                            <td>AUD 60.00</td>                                    
                                            <td>Paid</td>  
                                            <td><a class=""><i class="fa fa-file-pdf-o fa-2x"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>                                    
                                            <td>Basic</td> 
                                            <td>12/11/2017</td>
                                            <td>05:02 PM</td>                                            
                                            <td>1722087652</td>
                                            <td>AUD 60.00</td>                                    
                                            <td>Paid</td>  
                                            <td><a class=""><i class="fa fa-file-pdf-o fa-2x"></i></a></td>
                                        </tr>

                                    </tbody>
                                </table>
                                <script>
                                    $(function () {
                                        $('#apiInvoiceTable').dataTable();
                                    });
                                </script> 
                            </div>
                        </div>
                    </div>                           
                </div>        
            </div>
        </div>        
        <div id="wrapper" style="min-height:0%!important">
            <div class="content animate-panel" data-effect="rotateInDownRight" data-child="element">
                <div class="row tour-subscriptionTour">
                    <br><br>
                    <div class="col-lg-12 ">
                        <input id="TheCheckBox" type="checkbox" data-off-text="Yearly" data-on-text="Monthly" checked="false" class="BSswitch">
                    </div>
                    <br><br><br><br>
                    <script>
                        $('.BSswitch').bootstrapSwitch('state', true);

                        //$('#CheckBoxValue').text($("#TheCheckBox").bootstrapSwitch('state'));
                        $('#TheCheckBox').on('switchChange.bootstrapSwitch', function () {
                            //$("#CheckBoxValue").text($('#TheCheckBox').bootstrapSwitch('state'));

                            if ($('#TheCheckBox').bootstrapSwitch('state')) {
                                $("#monthly").show();
                                $("#yearly").hide();
                            } else {
                                $("#yearly").show();
                                $("#monthly").hide();
                            }
                        });
                    </script>
                    <div class="col-lg-12 ">
                        <div id="monthly">
                            <div class="col-sm-3">
                                <div class="hpanel plan-box hyellow active">
                                    <div class="panel-heading hbuilt text-center">
                                        <h4 class="font-bold">Basic</h4>
                                        <!--                                <h4 class="font-bold">$5/Month</h4>
                                                                        <h4 class="font-bold">$60/Year&nbsp;</h4>-->
                                    </div>
                                    <div class="panel-body">
                                        <p class="text-muted">
                                            Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.
                                        </p>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        Features
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Response Time 5 days
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Ticket Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-square-o"></i> Call Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Desk (Shared)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Product Documentation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  SOAP and REST Implementation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Reports
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   All In One Solution
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-square-o"></i>   One on One Discussion
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Uptime 95%
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Transactions per Second (3)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>                                                          
                                        <h3 class="font-bold">
                                            A$ $5/Month
                                        </h3>
                                        <a class="btn btn-warning btn-sm m-t-xs btn-outline">Subscribe</a>        

                                        <!--                                <a href="./registration.jsp?pa=baMo" style="float: left" class="btn btn-warning btn-sm m-t-xs">Monthly</a>
                                                                        <a href="./registration.jsp?pa=baYr" style="float: right"  class="btn btn-warning btn-sm m-t-xs">Yearly</a>                                                                                   -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="hpanel plan-box hblue active">
                                    <div class="panel-heading hbuilt text-center">
                                        <h4 class="font-bold">Student</h4>
                                        <!--                                <h4 class="font-bold">$15/Month</h4>
                                                                        <h4 class="font-bold">$180/Year&nbsp;</h4>-->
                                    </div>
                                    <div class="panel-body">
                                        <p class="text-muted">
                                            Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.
                                        </p>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        Features
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Response Time 3 days
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Ticket Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-square-o"></i> Call Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Desk (Shared)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Product Documentation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  SOAP and REST Implementation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Reports
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   All In One Solution
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-square-o"></i>   One on One Discussion
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Uptime 98%
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Transactions per Second (5)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <h3 class="font-bold">
                                            A$ $15/Month
                                        </h3>
                                        <a class="btn btn-info btn-sm m-t-xs btn-outline">Subscribe</a>                                                               

                                        <!--                                <a href="./registration.jsp?pa=stMo" style="float: left"  class="btn btn-info btn-sm m-t-xs">Monthly</a>
                                                                        <a href="./registration.jsp?pa=stYr" style="float: right"  class="btn btn-info btn-sm m-t-xs">Yearly</a>-->

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="hpanel plan-box hgreen active">
                                    <div class="panel-heading hbuilt text-center">
                                        <h4 class="font-bold">Standard</h4>
                                        <!--                                <h4 class="font-bold">$50/Month</h4>
                                                                        <h4 class="font-bold">$575/Year</h4>-->
                                    </div>
                                    <div class="panel-body">
                                        <p class="text-muted">
                                            Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.
                                        </p>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        Features
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Response Time 1 days
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Ticket Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Call Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Desk (Shared)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Product Documentation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  SOAP and REST Implementation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Reports
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   All In One Solution
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (1/month)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   One on One Discussion (30 mins/month)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Uptime 99%
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Transactions per Second (10)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <h3 class="font-bold">
                                            A$ 300/Month
                                        </h3>
                                        <a class="btn btn-success btn-sm m-t-xs btn-outline">Subscribe</a>                                                               
                                        <!--                                <a href="./registration.jsp?pa=stdMo" style="float: left"  class="btn btn-success btn-sm m-t-xs">Monthly</a>
                                                                        <a href="./registration.jsp?pa=stdYr" style="float: right"  class="btn btn-success btn-sm m-t-xs">Yearly</a>-->
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="hpanel plan-box hred active">
                                    <div class="panel-heading hbuilt text-center">
                                        <h4 class="font-bold">Enterprise</h4>
                                        <!--                                <h4 class="font-bold">$300/Month</h4>
                                                                        <h4 class="font-bold">$3300/Year</h4>-->
                                    </div>
                                    <div class="panel-body">
                                        <p class="text-muted">
                                            Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.
                                        </p>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        Features
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Response Time 2 hours
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Ticket Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Call Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Desk (Dedicated)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Product Documentation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  SOAP and REST Implementation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Reports
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   All In One Solution
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (3/month)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   One on One Discussion (120 mins/month)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Uptime 99.90%
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Transactions per Second (30)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Support Hours 24x7x365 Weekdays
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <h3 class="font-bold">
                                            A$ 300/Month
                                        </h3>
                                        <a class="btn btn-danger btn-sm m-t-xs btn-outline">Subscribe</a>
                                        <!--                                <a href="./registration.jsp?pa=entMo" style="float: left"  class="btn btn-danger btn-sm m-t-xs">Monthly</a>
                                                                        <a href="./registration.jsp?pa=entYr" style="float: right"  class="btn btn-danger btn-sm m-t-xs">Yearly</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="yearly" style="display: none">
                            <div class="col-sm-3">
                                <div class="hpanel plan-box hyellow active">
                                    <div class="panel-heading hbuilt text-center">
                                        <h4 class="font-bold">Basic</h4>
                                    </div>
                                    <div class="panel-body">
                                        <p class="text-muted">
                                            Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.
                                        </p>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        Features
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Response Time 5 days
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Ticket Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-square-o"></i> Call Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Desk (Shared)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Product Documentation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  SOAP and REST Implementation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Reports
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   All In One Solution
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-square-o"></i>   One on One Discussion
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Uptime 95%
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Transactions per Second (3)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>                            
                                        <h3 class="font-bold">
                                            A$ 60/Year
                                        </h3>
                                        <a class="btn btn-warning btn-sm m-t-xs btn-outline">Subscribe</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="hpanel plan-box hblue active">
                                    <div class="panel-heading hbuilt text-center">
                                        <h4 class="font-bold">Student</h4>
                                    </div>
                                    <div class="panel-body">
                                        <p class="text-muted">
                                            Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.
                                        </p>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        Features
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Response Time 3 days
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Ticket Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-square-o"></i> Call Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Desk (Shared)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Product Documentation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  SOAP and REST Implementation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Reports
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   All In One Solution
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-square-o"></i>   Webex Remote Resolution
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-square-o"></i>   One on One Discussion
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Uptime 98%
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Transactions per Second (5)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>                            
                                        <h3 class="font-bold">
                                            A$ 180/Year
                                        </h3>
                                        <a class="btn btn-info btn-sm m-t-xs btn-outline">Subscribe</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="hpanel plan-box hgreen active">
                                    <div class="panel-heading hbuilt text-center">
                                        <h4 class="font-bold">Standard</h4>
                                    </div>
                                    <div class="panel-body">
                                        <p class="text-muted">
                                            Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.
                                        </p>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        Features
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Response Time 1 days
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Ticket Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Call Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Desk (Shared)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Product Documentation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  SOAP and REST Implementation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Reports
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   All In One Solution
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (1/month)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   One on One Discussion (30mins/month)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Uptime 99%
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Transactions per Second (10)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>                            
                                        <h3 class="font-bold">
                                            A$ 575/Year
                                        </h3>
                                        <a class="btn btn-success btn-sm m-t-xs btn-outline">Subscribe</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="hpanel plan-box hred active">
                                    <div class="panel-heading hbuilt text-center">
                                        <h4 class="font-bold">Enterprise</h4>
                                    </div>
                                    <div class="panel-body">
                                        <p class="text-muted">
                                            Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.
                                        </p>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        Features
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Response Time 1 days
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Ticket Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Call Support
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Service Desk (Shared)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i> Product Documentation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  SOAP and REST Implementation
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>  Special designed API Console
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Reports
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   All In One Solution
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Webex Remote Resolution (1/month)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   One on One Discussion (30mins/month)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Uptime 99%
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Transactions per Second (10)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-check-square-o"></i>   Support Hours 9x5 Weekdays
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>                            
                                        <h3 class="font-bold">
                                            A$ 3300/Year
                                        </h3>
                                        <a class="btn btn-danger btn-sm m-t-xs btn-outline">Subscribe</a>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>               
                </div>               
            </div>
        </div> 
        <div id="wrapper" style="min-height:0%!important">
            <div class="content animate-panel">
                <div class="row tour-HelpDesk">
                    <div class="col-md-12">
                        <div class="hpanel email-compose">
                            <div class="panel-heading hbuilt">
                                <div class="p-xs h4">
                                    New message
                                </div>
                            </div>
                            <div class="panel-heading hbuilt">
                                <div class="p-xs">
                                    <form  class="form-horizontal" method="POST" id="ticketComposeRT">
                                        <div class="form-group"><label class="col-sm-2 control-label text-left">To:</label>
                                            <div class="col-sm-10"><input type="text" class="form-control input-sm" value="<%=mailId%>" disabled></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label text-left">Contact:</label>
                                            <div class="col-sm-10"><input type="text" id="contact_no" onkeypress="if (isNaN(String.fromCharCode(event.keyCode)))
                                                        return false;" class="form-control input-sm" placeholder="Enter your contact number"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label text-left">Subject:</label>
                                            <div class="col-sm-10">
                                                <select id="subjectBody" class="js-source-states" style="width: 100%">
                                                    <optgroup label="General">
                                                        <option value="General Questions">General Questions</option>
                                                    </optgroup>
                                                    <optgroup label="API">
                                                        <%
                                                            Accesspoint[] as = new AccessPointManagement().getAceesspoints();
                                                            if (as != null) {
                                                                for (Accesspoint accesspoint : as) {%>
                                                        <option value="<%=accesspoint.getName()%>"><%=accesspoint.getName()%></option>
                                                        <%}
                                                            }
                                                        %>
                                                    </optgroup>
                                                    <optgroup label="Fault">
                                                        <option value="Access_Token_Issue">Access Token Issue</option>
                                                        <option value="password">Can't Change Password</option>
                                                        <option value="others">Others</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="panel-body">
                                        <div><textarea class="summernote" id="msgBody"></textarea></div>                                    
                                    </div>
                                    <br><br>
                                    <div style="background: #fff; height: 200px">
                                        <div class="col-sm-12">
                                            <form action="UploadEmailAttachment" method="post" class="dropzone" id="my-dropzone"></form>	
                                            <small>Upload attachment. Support Image File size limit 5mb</small>
                                        </div>
                                    </div>

                                    <div class="panel-footer">

                                        <a id="submitMail" class="btn btn-primary ladda-button" data-style="zoom-in">Send email</a>
                                        <a class="btn btn-danger" tabindex="2" ><i class="fa fa-close"></i> Clear</a>
                                    </div>                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(function () {
                var elem = $(".summernote").summernote({
                    callbacks: {
                        onInit: function () {
                            var editor = elem.next(),
                                    placeholder = editor.find(".note-placeholder");

                            function isEditorEmpty() {
                                var code = elem.summernote("code");
                                return code === "<p><br></p>" || code === "";
                            }

                            editor.on("focusin focusout", ".note-editable", function (e) {
                                if (isEditorEmpty()) {
                                    placeholder[e.type === "focusout" ? "show" : "hide"]();
                                }
                            });
                        }
                    }
                });
            });


            $(document).ready(function () {
                Dropzone.autoDiscover = false;
                var myDropzone = new Dropzone("#my-dropzone", {
                    url: "UploadZip",
                    uploadMultiple: true,
                    maxFilesize: 5,
                    maxFiles: 1,
                    acceptedFiles: "image/*",
                    dictInvalidFileType: "You can't upload files of this type, only Image file",
                    autoProcessQueue: true,
                    parallelUploads: 1,
                    addRemoveLinks: true,
                });
            });


        </script>

        <!--    <style>
                .tour-step-background{background: none!important;}
            </style>     -->

        <script>

            $(document).ready(function () {
                //var creditUsedData = fifteenDaysCreditExpenditure();


                var doc = document.getElementById('_APIbody');
                var dataCode = CodeMirror.fromTextArea(doc, {
                    lineNumbers: true,
                    matchBrackets: true,
                    styleActiveLine: true,
                    tabMode: "indent",
                });
                dataCode.on('change', function ash() {
                    $('#_APIbody').val(dataCode.getValue());
                });

                var doc = document.getElementById('responseID');
                var dataCode = CodeMirror.fromTextArea(doc, {
                    lineNumbers: true,
                    matchBrackets: true,
                    styleActiveLine: true,
                    tabMode: "indent",
                });
                dataCode.on('change', function ash() {
                    $('#responseID').val(dataCode.getValue());
                });
            });
            setTimeout(function () {
                generateTopServiceTour();
            }, 1000);

            setTimeout(function () {
                yourMostlyServiceTour();
            }, 1000);

            //    
        </script>

    </body>
</html>