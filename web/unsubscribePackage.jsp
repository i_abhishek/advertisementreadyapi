<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<script src="scripts/packageDetails.js" type="text/javascript"></script>
<script src="scripts/packageOperation.js" type="text/javascript"></script>
<div class="small-header transition animated fadeIn" id="subscriptionPackageHeader">
    <div class="hpanel">
        <div class="panel-body">
            <h2 class="font-light m-b-xs">
                Subscription Package
            </h2>
            <small>You can Un-subscribe recurring and subscribe with other package from here.</small>
        </div>
    </div>
</div>
<div class="content animate-panel" data-effect="rotateInDownRight" data-child="element">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <!--                <div class="text-center m-b-xl">
                                <h3>Best pricing for your app</h3>
                            </div>-->
            <div class="row">
                <%
                    PartnerDetails parObj = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
                    if (parObj.getStripeSubscriptionId() == null) {%>
                <script>
                    function subscribe() {
                        $('#otherPage').empty();
                        $('#wrapper1').hide();
                        document.getElementById("wrapper3").style.display = "block";
                        var s = "packageListToSubscribe.jsp";
                        $.ajax({
                            type: 'GET',
                            url: s,
                            success: function (data) {
                                $('#otherPage').html(data);
                                document.getElementById("wrapper3").style.display = "none";
                                $('#wrapper2').show();

                            },
                            error: function (data) {
                            }
                        });
                    }
                    subscribe();
                </script>
                <%}
                    SgSubscriptionDetails subscriObject1 = new PackageSubscriptionManagement().getPartnerSubscriptionbyPId(parObj.getPartnerId());
                    if (subscriObject1 != null) {

                        String duration = subscriObject1.getBucketDuration();
                        String bucketName = subscriObject1.getBucketName().toLowerCase();
                        String packageName = subscriObject1.getBucketName();
                        if (duration.equalsIgnoreCase("monthly")) {
                            duration = "Month";
                        } else if (duration.equalsIgnoreCase("yearly")) {
                            duration = "Year";
                        }
                        if (bucketName.contains("basic") && bucketName.contains("month")) {
                            packageName = "Basic";
                        } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                            packageName = "Basic";
                        } else if (bucketName.contains("student") && bucketName.contains("month")) {
                            packageName = "Student";
                        } else if (bucketName.contains("student") && bucketName.contains("year")) {
                            packageName = "Student";
                        } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                            packageName = "Standard";
                        } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                            packageName = "Standard";
                        } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                            packageName = "Enterprise";
                        } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                            packageName = "Enterprise";
                        }

                %>                                           
                <div class="col-sm-5 col-lg-5 col-md-5">
                    <div class="hpanel plan-box hblue active">
                        <div class="panel-heading hbuilt text-center">
                            <h4 class="font-bold"><%=packageName%></h4>

                        </div>
                        <div class="panel-body">
                            <p class="text-muted">
                                <%if (subscriObject1.getPackageDescription() == null) {%>
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.
                                <%} else {%>
                                <%=subscriObject1.getPackageDescription()%>
                                <%}%>
                            </p>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>
                                            Features
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Service Support
                                        </td>
                                    </tr>
                                    <tr>
                                        <%if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Basic")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Response Time 5 days
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Student")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Response Time 3 days
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Standard")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Response Time 1 days
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Enterprise")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Response Time 2 hours
                                        </td>
                                        <%} else {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Response Time
                                        </td>
                                        <%}%>
                                    </tr>
                                    <tr>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Ticket Support
                                        </td>
                                    </tr>
                                    <tr>
                                        <%if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Standard")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Call Support
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Enterprise")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Call Support
                                        </td>
                                        <%} else {%>
                                        <td>
                                            <i class="fa fa-square-o"></i> Call Support
                                        </td>
                                        <%}%>
                                    </tr>
                                    <tr>
                                        <%if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Basic")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Service Desk (Shared)
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Student")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Service Desk (Shared)
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Standard")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Service Desk (Shared)
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Enterprise")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Service Desk (Dedicated)
                                        </td>
                                        <%} else {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Service Desk
                                        </td>
                                        <%}%>                                        
                                    </tr>
                                    <tr>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Product Documentation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <i class="fa fa-check-square-o"></i>  SOAP and REST Implementation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <i class="fa fa-check-square-o"></i>  Special designed API Console
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <i class="fa fa-check-square-o"></i>  Reports
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <i class="fa fa-check-square-o"></i>  All In One Solution
                                        </td>
                                    </tr>
                                    <tr>
                                        <%if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Standard")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Webex Remote Resolution (1 per month)
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Enterprise")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Webex Remote Resolution (3 per month)
                                        </td>
                                        <%} else {%>
                                        <td>
                                            <i class="fa fa-square-o"></i> Webex Remote Resolution
                                        </td>
                                        <%}%>
                                    </tr>
                                    <tr>
                                        <%if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Standard")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> One on One Discussion (30 mins  per month)
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Enterprise")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> One on One Discussion (120 mins  per month)
                                        </td>
                                        <%} else {%>
                                        <td>
                                            <i class="fa fa-square-o"></i> One on One Discussion
                                        </td>
                                        <%}%>
                                    </tr>
                                    <tr>
                                        <%if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Basic")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Uptime 95%
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Student")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Uptime 98%
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Standard")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Uptime 99%
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Enterprise")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Uptime 99.99%
                                        </td>
                                        <%} else {%>
                                        <td>
                                            <i class="fa fa-square-o"></i> Uptime
                                        </td>
                                        <%}%>             
                                    </tr>
                                    <tr>
                                        <%if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Basic")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Transactions per Second (3)
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Student")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Transactions per Second (5)
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Standard")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Transactions per Second (10)
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Enterprise")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Transactions per Second (30)
                                        </td>
                                        <%} else {%>
                                        <td>
                                            <i class="fa fa-square-o"></i> Transactions per Second
                                        </td>
                                        <%}%>             
                                    </tr>
                                    <tr>
                                        <%if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Basic")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Support Hours 9x5 Weekdays
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Student")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Support Hours 9x5 Weekdays
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Standard")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Support Hours 9x5 Weekdays
                                        </td>
                                        <%} else if (subscriObject1.getBucketName() != null && subscriObject1.getBucketName().contains("Enterprise")) {%>
                                        <td>
                                            <i class="fa fa-check-square-o"></i> Support Hours 24x7x365
                                        </td>
                                        <%} else {%>
                                        <td>
                                            <i class="fa fa-square-o"></i> Support Hours
                                        </td>
                                        <%}%>             
                                    </tr>
                                </tbody>
                            </table>
                            <h3 class="font-bold">
                                A$  <%=subscriObject1.getPlanAmount()%>/<%=duration%>
                            </h3>

                            <a class="btn btn-info btn-sm ladda-button element" data-style="zoom-in" id="unSubscripbePackage" onclick="confirmUnSubscribePackage('<%=subscriObject1.getBucketId()%>')"> Un-Subscribe Recurring</a>                                                                                                                                                                      
                        </div>
                    </div>
                </div>
                <%
                } else {
                %>
                <div class="row">
                    <div class="col-md-12">
                        <div class="hpanel">
                            <div class="panel-body">
                                <i class="pe-7s-way text-success big-icon"></i>
                                <h1>Not found</h1>
                                <strong>No package found</strong>
                                <p>
                                    Sorry, but the package list as per your production request has not been found.

                                </p>
                                <a href="#" class="btn btn-xs btn-success ladda-button" data-style="zoom-in" id="previousButton" onclick="goBack()">Go back to previous page</a>
                            </div>
                        </div>
                    </div>
                </div> 
                <%}%>
            </div>
        </div>
        <!--        <div class="col-md-12" style="margin-bottom: 40%" id="marginFooter"></div>-->
    </div>
    <div id="subscriptionFooter">       
        <!-- Footer-->

    </div>
</div>    

<script>
    jQuery(function ($) {
        $('.stripe-button').on('token', function (e, token) {
            var id = $('<span/>');
            id.text(token.id);
            $('.stripe-button-container').replaceWith('<p>Your Stripe token is: <strong>' + id.html() + '</strong>.</p>');
        });
    });
</script>