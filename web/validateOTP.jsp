<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgUsers"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Page title -->
        <title>MP Services API | Login</title>
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->
        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="vendor/sweetalert/lib/sweet-alert.css" />
        <link rel="stylesheet" href="vendor/ladda/dist/ladda-themeless.min.css" />
        <link rel="stylesheet" href="vendor/toastr/build/toastr.min.css" />
        <script src="scripts/utilityFunction.js" type="text/javascript"></script>
        <script src="scripts/partnerRequest.js" type="text/javascript"></script>
        <!-- App styles -->
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="styles/style.css">
    </head>
    <body class="blank" style="background: rgba(43,48,131,1);
          background: -moz-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: -webkit-gradient(left top, right top, color-stop(0%, rgba(43,48,131,1)), color-stop(50%, rgba(35,129,196,1)), color-stop(100%, rgba(43,48,131,1)));
          background: -webkit-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: -o-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: -ms-linear-gradient(left, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          background: linear-gradient(to right, rgba(43,48,131,1) 0%, rgba(35,129,196,1) 50%, rgba(43,48,131,1) 100%);
          filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2b3083', endColorstr='#2b3083', GradientType=1 );">

        <!-- Simple splash screen-->
        <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Loading...</h1><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <%
            String ref = request.getParameter("ref");
            String userEmail = request.getParameter("_to");
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");
            SgUsers developerObj = new UsersManagement().getSgUsersByEmail(userEmail);
            Date otpExpiryTime = null;
            String strExpiryTime = "NA";
            if (developerObj != null) {
                otpExpiryTime = developerObj.getOtpExpiry();
                strExpiryTime = UtilityFunctions.getTMReqDate(otpExpiryTime);
            }

            Date dFooter = new Date();
            SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
            String strYYYY = sdfFooter.format(dFooter);

            long LongTime = dFooter.getTime() / 1000;
            SimpleDateFormat tz = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
            String completeTimewithLocalTZ = tz.format(dFooter);

        %>
        <div class="color-line"></div>

        <div class="login-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center m-b-md">
                        <h3 style="color:#fff">Identity Verification</h3>
                        <small></small>
                    </div>
                    <div class="hpanel">
                        <div class="panel-body">
                            <form id="login_form" action="#">                            
                                <div class="form-group">
                                    <label class="control-label" for="otp">One Time Password</label>
                                    <input type="text" title="Please enter your OTP" placeholder="******" onkeypress="loginFunction(event)" required="" value="" name="_otp" id="_otp" class="form-control">
                                    <%if (ref != null && ref.equalsIgnoreCase("s")) {%>
                                    <span class="help-block small">We have sent OTP via EMAIL/SMS on your mobile. Expire at <%=strExpiryTime%>.</span>
                                    <%} else {%>
                                    <span class="help-block small"  style="color: red">Failed to send OTP. Please try after some time</span>
                                    <%}%>
                                    <input id="_name" name="_name" type="hidden" value="<%=userEmail%>"/> 
                                </div>
                                <a href="#" class="btn btn-success btn-block ladda-button" data-style="zoom-in" id="loginButton" onclick="userLogin('html')">Login</a>
                                <a  class="btn btn-warning btn-block ladda-button" data-style="zoom-in" id="resendOTPButton" onclick="resendOTP('<%=userEmail%>')">Resend OTP</a>
                            </form>
                        </div>
                    </div>
                    <div class="modal fade" id="sendotp" tabindex="-1" role="dialog"  aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="color-line"></div>
                                <div class="modal-header">
                                    <h4 class="modal-title">Success!</h4>
                                    <small class="font-bold"></small>
                                </div>
                                <div class="modal-body">
                                    <p>OTP resend to your registered mobile phone number.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="modal fade" id="resendotperror" tabindex="-1" role="dialog"  aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="color-line"></div>
                                <div class="modal-header">
                                    <h4 class="modal-title">Error!</h4>
                                    <small class="font-bold"></small>
                                </div>
                                <div class="modal-body">
                                    <p id="resendOTPErrorMsg">Error - .</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>                                
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center" style="color:#fff">
                    <strong>Powered by Service Guard And Axiom Protect 2.0<br>
                        � BlueBricks Technologies 2009-2017 (www.blue-bricks.com)</p></strong> 
                </div>
            </div>
        </div>
        <!-- Vendor scripts -->
        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="vendor/iCheck/icheck.min.js"></script>
        <script src="vendor/sparkline/index.js"></script>
        <script src="vendor/ladda/dist/spin.min.js"></script>
        <script src="vendor/ladda/dist/ladda.min.js"></script>
        <script src="vendor/ladda/dist/ladda.jquery.min.js"></script>
        <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
        <script src="vendor/sweetalert/lib/sweet-alert.min.js"></script>
        <script src="vendor/toastr/build/toastr.min.js"></script>
        <!-- App scripts -->
        <script src="scripts/homer.js"></script>
        <script>

        </script>
    </body>
</html>
