<%@include file="header.jsp"%> 

<!-- Main Wrapper -->
<div id="wrapper">

	<div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="home.jsp">Dashboard</a></li>
                        <li class="active">
                            <span>Virtual Number</span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    My Virtual Number
                </h2>
                <small>Short description to developer what is this page about</small>
            </div>
        </div>
    </div>


<div class="content animate-panel">

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    </div>
					Assigned virtual number
                </div>
                <div class="panel-body">
                <table id="api" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
					<th>Services</th>
                    <th>Virtual Number</th>
                </tr>
                </thead>
                <tbody>
                <tr>
					<td>SMS</td>
                    <td>0154</td>
                </tr>
                </tbody>
                </table>

                </div>
            </div>
        </div>
		
    </div>

    <%@include file="footer.jsp"%> 


<script>

    $(function () {

        // Initialize Example 2
        $('#api').dataTable();

    });
	
	$(function () {

        // Initialize Example 2
        $('#data_type').dataTable();

    });

</script>